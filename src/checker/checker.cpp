/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "utils/compatibility.hpp"

#include "fsm/Fsm.hpp"
#include "testsuite/TestSuite.hpp"
#include "testsuite/Trace.hpp"
#include "creators/input/FsmFromFileCreator.hpp"
#include "creators/transformers/ToAutoCompletedTransformer.hpp"
#include "version.hpp"
#include <optional>
#include <tuple>
#include <memory>
#include <iostream>
#include <algorithm>
#include <functional>
#include "creators/transformers/AdaptToPresentationLayer.hpp"

using namespace std;
using namespace libfsmtest;

static ostream &operator<<(ostream &out, Strings const &vec) {
    auto iter = vec.cbegin();
    if(not vec.empty()) {
        out << *iter;
        while(++iter != vec.cend()) {
            out << ',' << *iter;
        }
    }
    return out;
}

static void printUsage(char **argv) {
    cerr << "libfsmtest " << version.major << "." << version.minor << " checker" << endl;
    cerr << "Usage: " << argv[0] << " [options] <Path to test suite> <Path to reference model> <Path to implementation>" << endl;
    cerr << "Options:" << endl;
    cerr << "         -cself : This selects application of the auto-completion transformation with self loops" << endl;
    cerr << "         -cerror : This selects application of the auto-completion transformation with error state" << endl;
}

static
optional<
    tuple<
        unique_ptr<Fsm>,
        TestSuite
    >
>
parseArguments(int argc, char **argv) {
    if(argc < 4) {
        cerr << "Missing arguments!" << endl;
        printUsage(argv);
        return {};
    }

    int argIdx = 1;

    bool selfAutoCompletion = false;
    bool errorAutoCompletion = false;
    
    string nullString("_null");
    string errorOutString("_error");
    string errorStateString("ERROR");
    
    while ( argIdx < argc - 3) {
        if ( string(argv[argIdx]) == "-cself" ) {
            selfAutoCompletion = true;
            errorAutoCompletion = false;
        }
        else if ( string(argv[argIdx]) == "-cerror" ) {
            selfAutoCompletion = false;
            errorAutoCompletion = true;
        }
        else if ( string(argv[argIdx]) == "-null" ) {
            if ( argIdx > argc-2 ) {
                printUsage(argv);
                return {};
            }
            nullString = string(argv[++argIdx]);
        }
        else if ( string(argv[argIdx]) == "-errorout" ) {
            if ( argIdx > argc-2 ) {
                printUsage(argv);
                return {};
            }
            errorOutString = string(argv[++argIdx]);
        }
        else if ( string(argv[argIdx]) == "-errorstate" ) {
            if ( argIdx > argc-2 ) {
                printUsage(argv);
                return {};
            }
            errorStateString = string(argv[++argIdx]);
        }
        else if ( string(argv[argIdx]).at(0) == '-' ) {
            cerr << "Illegal option " << string(argv[argIdx]) << endl;
            printUsage(argv);
            return {};
        }
        else {
            printUsage(argv);
            return {};
        }
        argIdx++;
    }
    // Create reference FSM from file
    
    // If selected, create an auto completion transformer
    unique_ptr<ToAutoCompleteTransformer> transformer = nullptr;

    // Copy the presentation layer by default.
    std::function<PresentationLayer(PresentationLayer const&)> presentationLayerTransformer = [](PresentationLayer const& pl) -> PresentationLayer {
        return { pl };
    };

    auto addToStringVectorIfNotPresent = [](Strings &vec, const string& entry) {
        if(find(vec.cbegin(), vec.cend(), entry) == vec.cend()) {
            vec.push_back(entry);
        }
    };
    if ( selfAutoCompletion ) {
        transformer = make_unique<ToAutoCompleteWithSelfLoopTransformer>(nullptr, nullString);
        presentationLayerTransformer = [nullString,addToStringVectorIfNotPresent](PresentationLayer const& pl) -> PresentationLayer {
            PresentationLayer result(pl);
            addToStringVectorIfNotPresent(result.outputAlphabet, nullString);
            return result;
        };
    }
    else if ( errorAutoCompletion ) {
        transformer = make_unique<ToAutoCompleteWithErrorStateTransformer>(nullptr,
                                                                           nullString,
                                                                           errorOutString,
                                                                           errorStateString);
        presentationLayerTransformer = [nullString,errorOutString,errorStateString,addToStringVectorIfNotPresent](PresentationLayer const& pl) -> PresentationLayer {
            PresentationLayer result(pl);
            addToStringVectorIfNotPresent(result.outputAlphabet, nullString);
            addToStringVectorIfNotPresent(result.outputAlphabet, errorOutString);
            addToStringVectorIfNotPresent(result.stateNames, errorStateString);
            return result;
        };
    }
    
    // argv[argc-2] contains path/file basename to the reference FSM
    // argv[argc-2] contains path/file basename to the SUT FSM
    // For fsm name, we only want the basename, without path prefix of file extension suffix.
    auto getBasename = [](string fsmName) {
        size_t pos = fsmName.rfind('/');
        if ( pos != string::npos ) {
            fsmName.erase(0,pos+1);
        }
        pos = fsmName.rfind('.');
        if ( pos != string::npos) {
            fsmName.erase(pos);
        }
        return fsmName;
    };
    string fsmNameRef(getBasename(argv[argc-2]));
    string fsmNameSUT(getBasename(argv[argc-1]));
    
    auto ref = createFsm<FsmFromFileCreator>(argv[argc-2],fsmNameRef,nullptr);
    auto fsmSUT = createFsm<FsmFromFileCreator>(argv[argc-1],fsmNameSUT,move(transformer));

    auto testsuite = TestSuite(argv[argc-3], argv[argc-2]);
    auto modifiedReference = testsuite.getFsm().modify(nullopt, nullopt, presentationLayerTransformer(ref->getPresentationLayer()), nullopt);
    modifiedReference = createFsm<AdaptToPresentationLayer>(modifiedReference.get(), fsmSUT->getPresentationLayer(), "");

    auto testsuiteWithModifiedReference = TestSuite(argv[argc-3], move(modifiedReference));
    testsuiteWithModifiedReference.setInputTracesRaw(testsuite.getInputTracesRaw());

    return make_tuple(move(fsmSUT), move(testsuiteWithModifiedReference));
}

static bool runTestSuite(Fsm const *fsm, TestSuite const &testsuite) {
    bool result = true;
    for(auto const &inputTrace : testsuite.getInputTracesRaw()) {
        auto referenceTraces = testsuite.getFsm().getIOTraces(inputTrace, true);
        auto sutTraces = fsm->getIOTraces(inputTrace, true);

        typedef vector<Trace> ReferenceResponseType;
        typedef vector<Trace> SUTResponseType;
        ReferenceResponseType referenceResponse;
        SUTResponseType sutResponse;
        transform(referenceTraces.cbegin(), referenceTraces.cend(), back_inserter(referenceResponse), [](auto const &elem) { return elem.output; });
        transform(sutTraces.cbegin(), sutTraces.cend(), back_inserter(sutResponse), [](auto const &elem) { return elem.output; });
        sort(referenceResponse.begin(), referenceResponse.end());
        sort(sutResponse.begin(), sutResponse.end());

        if(referenceResponse != sutResponse) {
            result = false;
            ReferenceResponseType responsesOnlyInReference;
            SUTResponseType responsesOnlyInImplementation;
            set_difference(referenceResponse.cbegin(),
                                referenceResponse.cend(),
                                sutResponse.cbegin(),
                                sutResponse.cend(),
                                back_inserter(responsesOnlyInReference));
            set_difference(sutResponse.cbegin(),
                                sutResponse.cend(),
                                referenceResponse.cbegin(),
                                referenceResponse.cend(),
                                back_inserter(responsesOnlyInImplementation));
            for(auto const &refResponse : responsesOnlyInReference) {
                auto bestMatch = max_element(sutResponse.cbegin(),
                                                  sutResponse.cend(),
                                                  [&refResponse](auto const &response1, auto const &response2) {
                    auto const miss1 = mismatch(response1.cbegin(), response1.cend(), refResponse.cbegin(), refResponse.cend());
                    auto const miss2 = mismatch(response2.cbegin(), response2.cend(), refResponse.cbegin(), refResponse.cend());
                    return distance(refResponse.cbegin(), miss1.second) < distance(refResponse.cbegin(), miss2.second);
                });
                auto mismatchPositionOfBestMatch = distance(refResponse.cbegin(), mismatch(refResponse.cbegin(), refResponse.cend(), bestMatch->cbegin(), bestMatch->cend()).first);
                auto mismatchIterRef = refResponse.cbegin();
                auto mismatchIterSUT = bestMatch->cbegin();
                advance(mismatchIterRef, mismatchPositionOfBestMatch);
                advance(mismatchIterSUT, mismatchPositionOfBestMatch);
                auto refRespTrans = refResponse.translate(fsm->getPresentationLayer().outputAlphabet);
                cout << "FAILURE: SUT does not implement output trace \"" << refRespTrans;
                cout << "\" for input trace \"" << inputTrace.translate(fsm->getPresentationLayer().inputAlphabet) << "\"" << endl;
                if(mismatchPositionOfBestMatch > 0) {
                    cout << "         Closest match diverges at step " << mismatchPositionOfBestMatch + 1 << ": \"" << Trace(mismatchIterSUT, bestMatch->cend()).translate(fsm->getPresentationLayer().outputAlphabet) << "\"" << endl;
                    cout << "         (Expected: \"" << Trace(mismatchIterRef, refResponse.cend()).translate(fsm->getPresentationLayer().outputAlphabet) << "\")" << endl;
                }
            }
            for(auto const &sutResponseInImplementation : responsesOnlyInImplementation) {
                auto bestMatch = max_element(referenceResponse.cbegin(),
                                                  referenceResponse.cend(),
                                                  [&sutResponseInImplementation](auto const &response1, auto const &response2) {
                    auto const miss1 = mismatch(response1.cbegin(), response1.cend(), sutResponseInImplementation.cbegin(), sutResponseInImplementation.cend());
                    auto const miss2 = mismatch(response2.cbegin(), response2.cend(), sutResponseInImplementation.cbegin(), sutResponseInImplementation.cend());
                    return distance(sutResponseInImplementation.cbegin(), miss1.second) < distance(sutResponseInImplementation.cbegin(), miss2.second);
                });
                auto mismatchPositionOfBestMatch = distance(sutResponseInImplementation.cbegin(), mismatch(sutResponseInImplementation.cbegin(), sutResponseInImplementation.cend(), bestMatch->cbegin(), bestMatch->cend()).first);
                auto mismatchIterSUT = sutResponseInImplementation.cbegin();
                auto mismatchIterRef = bestMatch->cbegin();
                advance(mismatchIterSUT, mismatchPositionOfBestMatch);
                advance(mismatchIterRef, mismatchPositionOfBestMatch);
                cout << "FAILURE: SUT implements unspecified output trace \"" << sutResponseInImplementation.translate(fsm->getPresentationLayer().outputAlphabet) << "\" for input trace \"" << inputTrace.translate(fsm->getPresentationLayer().inputAlphabet) << "\"" << endl;
                if(mismatchPositionOfBestMatch > 0) {
                    cout << "         Closest match diverges at step " << mismatchPositionOfBestMatch + 1 << ": \"" << Trace(mismatchIterRef, bestMatch->cend()).translate(fsm->getPresentationLayer().outputAlphabet) << "\"" << endl;
                }
            }
        }
    }
    if(result) {
        cout << "PASS" << endl;
    }
    return result;
}

int main(int argc, char **argv) {
    if(auto const args = parseArguments(argc, argv); args.has_value()) {
        auto const &[fsm, testsuite] = args.value();
        if(fsm->getPresentationLayer().inputAlphabet != testsuite.getFsm().getPresentationLayer().inputAlphabet ||
           fsm->getPresentationLayer().outputAlphabet != testsuite.getFsm().getPresentationLayer().outputAlphabet) {
            cerr << "ERROR: Reference model and implementation need to have the same input and output alphabets" << endl;
            return -3;
        }
        if(runTestSuite(fsm.get(), testsuite)) {
            return 0;
        } else {
            return -2;
        }
    } else {
        return -1;
    }
}
