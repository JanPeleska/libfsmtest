/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <map>
#include <set>
#include <vector>
#include <memory>
#include <fstream>
#include <functional>
#include <optional>

#include "testsuite/Trace.hpp"
#include "testsuite/definitions.hpp"

namespace libfsmtest {

class TreeNode {
private:
    
    static size_t nextNodeId;
    
    /** Unique node id */
    size_t id;
    
    /** The node that is the parent of this node and therefore owns this. */
    TreeNode* parent;

    /**
     * The child nodes of this node, owned by this node.
     * Ordered by input- or output indices.
     */
    std::map<size_t, std::unique_ptr<TreeNode>> children;

    /** The value of this node. Not necessary. */
    std::optional<size_t> value;

public:
    /**
     * Constructor.
     * @param parentPtr The parent of this node.
     * @param valueParam The value assigned to this node.
     */
    TreeNode(TreeNode* parentPtr, std::optional<size_t> valueParam = std::nullopt) : parent(parentPtr), value(valueParam) { id = nextNodeId++; }

    /**
     * Copy constructor.
     * Will create a deep copy the node and all children.
     * @param other The node to copy.
     */
    TreeNode(const TreeNode& other);

    ~TreeNode() = default;
    
    size_t getNodeId() const { return id; }

    /**
     * Check whether this node is a leaf.
     * @return true iff this node does not have child nodes, otherwise false
     */
    bool isLeaf() const;

    /**
     * Add a child to this node.
     * Creates a new child and sets its value.
     * @param ioIndex The index associated with the edge.
     * @param value The value to be assigned to the node
     * @returns The pointer to the newly created child.
     */
    TreeNode* add(size_t ioIndex, std::optional<size_t> value = std::nullopt);

    /**
     * Calculates all leaves following this node and adds them to the vector.
     * @param leaves The vector to add leaves to.
     */
    void calculateLeaves(std::vector<TreeNode*>& leaves);
    
    /** Return number of Leaves below this node */
    size_t getNumberOfLeaves() const;
    
    /** Return number of nodes below and including  this node */
    size_t size() const;

    /**
     * Sets the parent.
     * @param parent The parent to use.
     */
    void setParent(TreeNode *parent);

    /**
     * Gets the parent.
     * @return The parent of this node or
     *         nullptr, when this is the root.
     */
    TreeNode *getParent() const;

    
    /** Get the node value */
    std::optional<size_t> getValue() const { return value; }
    
    /**
     * Sets the value.
     * @param value The value to use.
     */
    void setValue(std::optional<size_t> value);

    /**
     * Adds a full trace to the node, if it doesn't already exist.
     * @see #add(Trace::const_iterator, Trace::const_iterator)
     * @param t The trace to add.
     */
    void add(const Trace& t);

    /**
     * Adds the section of a trace defined by two iterator positions to the node.
     * @param start An iterator to define first index
     * @param end An iterator after the last index
     */
    void add(Trace::const_iterator start, Trace::const_iterator end);

    void getExistingNodes(std::set<TreeNode*>& existingNodesSet);
    
    /**
     * Adds a trace to this and each child node.
     * @param t The trace to add
     */
    void addToAllNodes(const Trace& t, const std::set<TreeNode*>& existingNodes);

    /**
     * Return the TreeNode after an input- or output index.
     * @param index The index after which to get the node.
     * @return nullptr, when there is no node for that index, or
     *         TreeNode for that index.
     */
    TreeNode* after(size_t index) const;

    /**
     * Return the TreeNode after following a trace.
     * @see after(Trace::const_iterator, Trace::const_iterator)
     * @param t The trace to follow.
     * @return nullptr, when the trace isn't a path of this tree
     *         TreeNode after following the trace.
     */
    TreeNode* after(const Trace& t);

    /**
     * Return the TreeNode after following a trace defined by iterator positions.
     * @param start The iterator position to start from.
     * @param end The iterator position after the end.
     * @return nullptr, when the trace isn't a path of this tree
     *         TreeNode after following the trace.
     */
    TreeNode* after(Trace::const_iterator start, Trace::const_iterator end);

    /**
     * Return the path of indices from the root node to this.
     * @return The path
     */
    Trace getPath() const;
    Trace getPath(TreeNode const *subtreeRoot) const;

    /**
     * Traverses the subtree from this node and adds all traces
     * to the collection.
     * @param traces The collection to add traces to.
     * @param path The path to this node (prefix of all traces to add).
     */
    void traverseForTraces(Traces& traces, Trace const &path) const;

    /**
     * Return the index a specific child node is associated with.
     * @param node The node to get the index for
     * @return The index for that node or
     *         std::nullopt, when the node does not exist.
     */
    std::optional<size_t> getIndex(const TreeNode* node) const;
    
    /** Find node by value, checking this and its children */
    TreeNode* findByValue(std::optional<size_t> value);
        
    bool checkNodes();
    
    void toDot(std::ofstream& outStream);

    /**
     * Apply a function recursively to this node and its children.
     * @param f Function to apply.
     * @param existingNodes Nodes in the tree that f can be applied to.
     *                      All nodes not in this set are skipped and not
     *                      recursed on.
     */
    void applyRecursively(const std::function<void (libfsmtest::TreeNode *)>& f, const std::set<TreeNode*>& existingNodes);
    void applyRecursively(const std::function<void (libfsmtest::TreeNode const*)>& f, const std::set<TreeNode const*>& existingNodes) const;

    /**
     * Collect the labels of all outgoing edges of this node.
     */
    std::vector<size_t> getLabelsOfOutgoingEdges() const;
};

} // namespace libfsmtest


