/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "utils/compatibility.hpp"

#include "creators/input/FsmFromFileCreator.hpp"
#include "fsm/Fsm.hpp"
#include "version.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <memory>

using namespace libfsmtest;

static void printUsage(char const *name) {
    std::cout << name << " <Model file> [Output file]" << std::endl;
}

static std::optional<
            std::tuple<
                std::string,
                std::optional<std::string>>>
parseArguments(int argc, char *argv[]) {
    if(argc < 2 or argc > 3) {
        return std::nullopt;
    }

    return std::make_tuple(argv[1], argc == 3 ? std::make_optional<std::string>(argv[2]) : std::nullopt);
}

static void createOutput(libfsmtest::Fsm &fsm, std::ostream &outStream) {
    ToDotFileVisitor outputGenerator(outStream);
    fsm.accept(outputGenerator);
    outputGenerator.writeToFile();
}

static std::string getFsmName(std::string const &modelPath) {
    std::string fsmName(modelPath);

    size_t pos = fsmName.rfind('/');
    if ( pos != std::string::npos ) {
        fsmName.erase(0,pos+1);
    }
    pos = fsmName.rfind('.');
    if ( pos != std::string::npos) {
        fsmName.erase(pos);
    }
    return fsmName;
}

int main(int argc, char *argv[]) {
    if(auto arguments = parseArguments(argc, argv);
        arguments.has_value()) {
        auto const [modelFile, outputFile] = arguments.value();
        std::unique_ptr<Fsm> fsm = createFsm<FsmFromFileCreator>(modelFile, getFsmName(modelFile), nullptr);

        if(outputFile.has_value()) {
            std::ofstream outputFileStream(outputFile.value());
            if(not outputFileStream.is_open()) {
                throw std::runtime_error("Could not open dot output file '" + outputFile.value() + "'");
            }
            createOutput(*fsm, outputFileStream);
        } else {
            createOutput(*fsm, std::cout);
        }
    } else {
        printUsage(argv[0]);
        return -1;
    }
}

