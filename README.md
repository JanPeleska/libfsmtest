# README #

This library has been programmed in C++. Its classes contain algorithms for
instantiating, evaluating, transforming, and creating test suites from 
finite state machines (FSM) representing Mealy Automata. The underlying
theory has been summarised in the lecture
notes [Test Automation - Foundations and Applications of Model-based Testing](http://www.informatik.uni-bremen.de/agbs/jp/papers/test-automation-huang-peleska.pdf). 

## Documentation
See file  `doc/libfsmtest.pdf`


## Building

To build the library, a compiler supporting the C++17 standard is required.
For building under Linux, MacOS or Windows, we offer CMake build files that require at
least CMake 3.20.

### Building with CMake and make

To build the library using the CMake files, create a build directory and invoke
CMake, pointing it to the `src` directory:

```
mkdir build;
cd build;
cmake ../src
```

Alternatively, you can use the `build.sh` script on Linux, which will generate the
directories `build.Debug` and `build.Release`, for the debug and the release
build respectively.  On Mac OSX platforms, the script `buildOSX.sh` should be 
used for this purpose.

Then, regardless of which option you chose, navigate to the build directory of
your choice, i.e. either `build`, `build.Debug` or `build.Release`, and simply
invoke `make`. This will then build the library and all executables shipped
with it. The resulting binaries will be located in the build directory as well.

### Building with CMake and Xcode

To build the library using the CMake files you can use the `build-for-xcode.sh`
script, which will generate an Xcode project file in a build directory `xcodebuild.Debug`
Then, use the project file to build the library and all binaries.

### Building with CMake and Visual Studio

We assume that the cmake binary is in your `PATH` environment variable. Then,
you can execute the `build-for-vs.bat` script, which will generate a Visual
Studio Solution in a build directory `visualstudio.Debug`
Afterwards, use the solution file to build the library and all binaries.

## Build artifacts

The build scripts are configured to generate a set of files. The core piece is
a library file, `libfsmtest.a` (or `fsmtest.lib` on Windows systems), that is
used as the basis for all other executables and that can be included in other
projects as well (see the LICENSE file for details).
Based on this library we provide the executables `generator`, `checker` and `usage-demo`.

The `generator` executable can be used to read an FSM from a set of files,
apply a test suite generation method and write the resulting test suite to
another file.
Using the `checker` executable, an FSM and a test suite can be read from the
file system. The test suite is then applied to the FSM and errors are printed
to the standard output.

For general usage of the library, most use cases are presented at least once in
the source code of `usage-demo`. It can be executed as a test of the library.
