/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "Trace.hpp"
#include <iostream>
#include <algorithm>

namespace libfsmtest {

/**
 * Check whether the first n elements of two containers match.
 * @tparam Container A container type providing size and cbegin
 * @param a One container with elements
 * @param b Another container with elements
 * @param n The number of elements to compare from the start of the two containers
 * @return true, if the first n elements from both containers match
 *         false, otherwise
 */
template<typename Container>
static bool firstNElementsMatch(Container&& a, Container&& b, size_t n) {
    // Both containers have to contain at least n elements
    if (a.size() < n || b.size() < n) return false;

    auto aIt = a.cbegin();
    auto bIt = b.cbegin();
    size_t i = 0;

    while(i < n) {
        if (*aIt != *bIt) return false;
        aIt++;
        bIt++;
        i++;
    }
    return true;
}

Trace::Trace(Trace::iterator const &begin, Trace::iterator const &end)
: trace(begin, end) { }
Trace::Trace(Trace::const_iterator const &begin, Trace::const_iterator const &end)
: trace(begin, end) { }

void Trace::append(const Index e) {
    trace.push_back(e);
}

void Trace::append(const std::list<Index> &other) {
    trace.insert(trace.end(), other.begin(), other.end());
}

void Trace::append(std::initializer_list<Index> l) {
    trace.insert(trace.end(), l.begin(), l.end());
}

void Trace::append(const Trace &other) {
    append(other.trace);
}

void Trace::prepend(const std::list<Index> &other) {
    trace.insert(trace.begin(), other.begin(), other.end());
}

void Trace::prepend(std::initializer_list<Index> l) {
    trace.insert(trace.begin(), l.begin(), l.end());
}

void Trace::prepend(const Trace &other) {
    prepend(other.trace);
}

void Trace::prepend(const Index e) {
    trace.push_front(e);
}

bool Trace::hasPrefix(const Trace &other) const {
    // Check whether the elements contained in the other are the first elements in this.
    return firstNElementsMatch(*this, other, other.size());
}

bool Trace::isPrefixOf(const Trace &other) const {
    // Check whether the elements contained in this are the first elements in the other.
    return firstNElementsMatch(other, *this, size());
}

std::vector<Trace> Trace::getNonemptyPrefixes(bool proper) const
{
    std::vector<Trace> result;
    size_t prefixIndex = proper ? 1 : 0;
    if (trace.size() > prefixIndex)
    {
        for (size_t i = prefixIndex; i < trace.size(); ++i)
        {
            auto end = trace.begin();
            std::advance(end,trace.size() - i);
            std::list<Index> l(trace.begin(), end);
            Trace prefix;
            prefix.append(l);

            result.push_back(prefix);
        }
    }
    return result;
}

Strings Trace::translate(Strings const &alphabet) const {
    Strings result;
    result.reserve(this->size());
    std::transform(this->cbegin(), this->cend(), std::back_inserter(result), [&alphabet](auto idx) {
        return alphabet.at(idx);
    });
    return result;
}

bool operator==(Trace const & trace1, Trace const & trace2)
{
	return trace1.trace == trace2.trace;
}

bool operator<(Trace const & trace1, Trace const & trace2)
{
	return trace1.trace < trace2.trace;
}

} // namespace libfsmtest

