/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <map>
#include <deque>
#include <iostream>
#include <memory>

#include "Dfsm.hpp"

using namespace std;

namespace libfsmtest {


void Dfsm::resetDfsmTable() {
    for ( Index p = 0; p < this->states.size(); p++ ) {
        for ( Index q = 0; q < this->presentationLayer.inputAlphabet.size(); q++ ) {
           delete[] this->dfsmTable[p][q];
        }
        delete[] this->dfsmTable[p];
    }
    delete[] this->dfsmTable;
    this->dfsmTable = nullptr;
}

void Dfsm::createDfsmTable() {
    
    if(dfsmTable == nullptr) {
        dfsmTable = new optional<Index>**[states.size()];
        
        for ( Index p = 0; p < states.size(); p++ ) {
            dfsmTable[p] = new optional<Index>*[presentationLayer.inputAlphabet.size()];
            for ( Index q = 0; q < presentationLayer.inputAlphabet.size(); q++ ) {
                dfsmTable[p][q] = new optional<Index>[2];
            }
        }
    }
    
    // Overwrite nullopt by defined post states and outputs for all transitions
    for ( Index s = 0; s < states.size(); s++ ) {
        State& theState = states.at(s);
        for ( const auto& tr : theState.getTransitions() ) {
            if(states.size() <= tr.getTargetId()) {
                throw runtime_error("ERROR: Invalid transition: Target state ID too large.");
            }
            if(presentationLayer.inputAlphabet.size() <= tr.getInput()) {
                throw runtime_error("ERROR: Invalid input ID.");
            }
            if(presentationLayer.outputAlphabet.size() <= tr.getOutput()) {
                throw runtime_error("ERROR: Invalid output ID.");
            }
            dfsmTable[s][tr.getInput()][0] = tr.getTargetId();
            dfsmTable[s][tr.getInput()][1] = tr.getOutput();
        }
    }
}


Dfsm::Dfsm(const string &fsmName,
           States stateVec,
           PresentationLayer pl,
           Index initialState) :
Ofsm(fsmName,std::move(stateVec),move(pl),initialState),
dfsmTable(nullptr)
{
    createDfsmTable();
}

Dfsm::Dfsm(Dfsm const &dfsm) : Ofsm(dfsm), dfsmTable(nullptr) {
    createDfsmTable();
}

Dfsm::~Dfsm() {
    resetDfsmTable();
}

void Dfsm::calcOFSMTables(map< size_t, set<Index> >& classMap,
                          vector< map<Index,size_t> >& state2classMaps) const {
    
    // Initially, we only have one class, containing all states
    classMap[0] = set<Index>();
    for ( size_t s = 0; s < states.size(); s++ ) {
        classMap[0].insert(s);
    }
    
    // This map maps states to their current class
    map<Index,size_t> state2class;
    // Initially, all states are in class 0
    for ( Index s = 0; s < states.size(); s++ ) {
        state2class[s] = 0;
    }
    state2classMaps.push_back(state2class);
    
    
    // Calculate the equivalence classes
    bool haveNewClass;
    do {
        
        haveNewClass = false;
        size_t numClasses = classMap.size();
        size_t nextClass = numClasses;
        for ( size_t c = 0; c < numClasses; c++ ) {
            size_t representant = *classMap[c].cbegin();
            // Check if class number c should be split
            auto sIte = classMap[c].begin();
            for ( ++sIte;
                 sIte != classMap[c].end();
                 sIte++ ) { // loop_c
                Index otherStateId = *sIte;
                for ( Index x = 0; x < presentationLayer.inputAlphabet.size(); x++ ) {
                    optional<Index> representantPostStateId = dfsmTable[representant][x][0];
                    optional<Index> otherPostStateid = dfsmTable[otherStateId][x][0];
                    optional<Index> representantOutput = dfsmTable[representant][x][1];
                    optional<Index> otherOutput = dfsmTable[otherStateId][x][1];
                    
                    // representant and otherStateId are NOT equivalent if
                    // (1) either one of the post states is undefined (-1), or
                    // (2) they have different values output values under x, or
                    // (3) both post states are defined, outputs are the same,
                    //     but the target states are located in different classes
                    if ( (representantPostStateId.has_value() ) != (otherPostStateid.has_value()) ||
                        ((representantPostStateId.has_value() && otherPostStateid.has_value()) &&
                         (representantOutput.value() != otherOutput.value() ||
                          state2class[representantPostStateId.value()] != state2class[otherPostStateid.value()])) ) {
                        haveNewClass = true;
                        // The new class has number nextClass and representative otherStateId
                        classMap[nextClass] = set<Index> { otherStateId };
                        // Erase otherStateId from its old class
                        sIte = classMap[c].erase(sIte);
                        // Check whether other elements of classMap[c] should also be moved into the new class
                        for ( auto sIte2 = sIte; sIte2 != classMap[c].end();
                             /* sIte2 updated at end of loop */ ) {
                            for ( Index a = 0; a < presentationLayer.inputAlphabet.size(); a++ ) {
                                otherPostStateid = dfsmTable[otherStateId][a][0];
                                optional<Index> sItPostStateid = dfsmTable[*sIte2][a][0];
                                optional<Index> otherOutputOfa = dfsmTable[otherStateId][a][1];
                                optional<Index> sItOutputOfa = dfsmTable[*sIte2][a][1];
                                
                                // Jump if not equivalent
                                if ( (otherPostStateid.has_value() != sItPostStateid.has_value() ) ||
                                    ((otherPostStateid.has_value() && sItPostStateid.has_value()) &&
                                     (otherOutputOfa.value() != sItOutputOfa.value() ||
                                      state2class[otherPostStateid.value()] != state2class[sItPostStateid.value()])) ) {
                                    goto notEquivalent;
                                }
                            }
                            // *sIte2 and otherStateId are equivalent, put *sIte2 into
                            // otherStateId's class and remove it from classMap[c]
                            classMap[nextClass].insert(*sIte2);
                            sIte2 = classMap[c].erase(sIte2);
                            continue;
                            
                            // We jump here if *sIte and otherStateId are NOT equivalent
                        notEquivalent:
                            sIte2++;
                            ;
                        }
                        
                        // We are done with the new class, increment nextClass
                        nextClass++;
                        
                        // Set the iterator of loop c to the first element (representative)
                        // Then loop c will increment sIte to point to the new element
                        // behind the representative
                        sIte = classMap[c].begin();
                        
                        // start next do-while cycle
                        goto loop_c_end;
                    }
                }
            loop_c_end:
                ;
            }
        }
        
        // update the state2class map
        for ( size_t d = 0; d < classMap.size(); d++ ) {
            for ( auto& id : classMap[d] ) {
                state2class[id] = d;
            }
        }
        state2classMaps.push_back(state2class);
        
#if 0
        cout << "state2class" << endl;
        for ( int i = 0; i < state2class.size(); i++ ) {
            cout << i << " --> " << state2class.at(i) << endl;
        }
        
        cout << "classMap" << endl;
        for ( size_t c = 0; c < classMap.size(); c++ ) {
            cout << c << " --> {";
            for ( auto& id : classMap[c] ) {
                cout << id << ",";
            }
            cout << "}" << endl;
        }
#endif
        
    } while (haveNewClass);
    
    
}


bool Dfsm::isInitiallyConnected() const {
    
    set<Index> initiallyConnectedStates;
    
    // Initial state is always reachable
    initiallyConnectedStates.insert( initialStateId );
    
    // Breadth-first-search queue
    deque<Index> bfsQueue;
    bfsQueue.push_back(initialStateId);

    while ( ! bfsQueue.empty() ) {
        
        Index currentStateId = bfsQueue.front();
        bfsQueue.pop_front();
        
        for ( Index x = 0; x < presentationLayer.inputAlphabet.size(); x++ ) {
            optional<Index> targetStateId = dfsmTable[currentStateId][x][0];
            if (targetStateId.has_value() &&
                initiallyConnectedStates.find(targetStateId.value()) == initiallyConnectedStates.end() ) {
                initiallyConnectedStates.insert(targetStateId.value());
                bfsQueue.push_back(targetStateId.value());
            }
        }
    }
    
    // Initially connected iff cardinality of initiallyConnectedNodes
    // equals number of states
    return ( initiallyConnectedStates.size() == size() );
}

unique_ptr<Fsm> Dfsm::clone() const {
    return make_unique<Dfsm>(*this);
}

std::unique_ptr<Fsm> Dfsm::modify(std::optional<std::string> fsmName,
                                  std::optional<States> newStates,
                                  std::optional<PresentationLayer> pl,
                                  std::optional<Index> newInitialStateId) const {
    if(not fsmName.has_value()) {
        fsmName = this->name;
    }
    if(not newStates.has_value()) {
        newStates = this->states;
    }
    if(not pl.has_value()) {
        pl = presentationLayer;
    }
    if(not newInitialStateId.has_value()) {
        newInitialStateId = this->initialStateId;
    }
    return make_unique<Dfsm>(fsmName.value(), newStates.value(), move(pl.value()), newInitialStateId.value());
}

optional<Index> Dfsm::getTarget(Index sourceId, const Trace& trace) const {
    if(states.size() <= sourceId) {
        throw runtime_error("ERROR: Invalid starting state for trace.");
    }
    optional<Index> targetId = sourceId;

    for (auto& x : trace) {
        targetId = dfsmTable[targetId.value()][x][0];
        if (not targetId.has_value()) return nullopt;
    }
    return targetId;
}

string Dfsm::getTraceAndStates(const Trace& tr) const {
    
    string traceAndStates;
    Index thisState = initialStateId;
    
    for ( const auto& x : tr ) {
        auto postState = dfsmTable[thisState][x][0];
        if ( not postState.has_value() ) break; // for partial DFSMs
        auto y = dfsmTable[thisState][x][1];
        if ( ! traceAndStates.empty() ) {
            traceAndStates += ",";
        }
        traceAndStates += presentationLayer.inputAlphabet[x] + "/" +
                          presentationLayer.outputAlphabet[y.value()] + " [" +
                          presentationLayer.stateNames[postState.value()] + "]";
        
        
    }
    return traceAndStates;
}

}
