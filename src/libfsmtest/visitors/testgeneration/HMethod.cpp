/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstdlib>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <deque>
#include <map>
#include <optional>
#include <cassert>

#include "HMethod.hpp"
#include "fsm/Dfsm.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "testsuite/cutTreeWithFSMLanguage.hpp"

using namespace std;

namespace libfsmtest {

bool nodeIntersectionNotEmptyAndTraceFulfillsPredicate(TreeNode *node1, TreeNode *node2, size_t inputAlphabetSize, std::function<bool(Trace const&)> const &diff) {
    std::deque<std::pair<TreeNode*,TreeNode*>> worklist;
    worklist.emplace_back(node1, node2);
    while(not worklist.empty()) {
        auto [left, right] = worklist.front();
        if(not diff(left->getPath(node1))) {
            return false;
        }

        for(size_t i = 0; i < inputAlphabetSize; ++i) {
            if(left->after(i) != nullptr && right->after(i) != nullptr) {
                worklist.emplace_back(left->after(i), right->after(i));
            }
        }

        worklist.pop_front();
    }
    return true;
}

bool alreadyDistinguished(Fsm &fsm, Tree *tree, Trace const &trace1, Trace const &trace2, size_t inputAlphabetSize) {
    // intersect subtrees after both traces
    auto treeAfter1 = tree->after(trace1);
    auto treeAfter2 = tree->after(trace2);

    // If any of the state pairs after the traces is not distinguished, this function shall return false
    auto states1 = fsm.getTargets(trace1);
    auto states2 = fsm.getTargets(trace2);

    auto distinguishes = [&states1, &states2, &fsm](Trace const &trace) {
        for(auto const &reachedState1 : states1) {
            for(auto const &reachedState2 : states2) {
                auto traces1 = fsm.getIOTracesFromState(trace, reachedState1, false);
                auto traces2 = fsm.getIOTracesFromState(trace, reachedState2, false);
                if(traces1 == traces2) {
                    return false;
                }
            }
        }
        return true;
    };
    // Check whether empty
    return nodeIntersectionNotEmptyAndTraceFulfillsPredicate(treeAfter1, treeAfter2, inputAlphabetSize, distinguishes);
}

bool HMethod::haveNewIndexVec(vector<size_t>& indexVec,
                              const vector< tuple<Trace, Trace, vector< pair<Trace,Trace> > > >& oneOfContainer ) {
    
    optional<size_t> idx;
    for ( size_t i = 0; i < oneOfContainer.size(); i++ ) {
        if ( indexVec.at(i) + 1 < std::get<2>(oneOfContainer.at(i)).size() ) {
            idx = i;
            break;
        }
    }
    if ( not idx.has_value() ) {
        return false;
    }
    
    for ( size_t i = 0; i < idx.value(); i++ ) {
        indexVec.at(i) = 0;
    }
    indexVec.at(idx.value()) += 1;
    
    return true;
}



void HMethod::addBestCandidateTraces(Fsm &fsm, size_t inputAlphabetSize) {
    
    if ( oneOfContainer.empty() )  return;
    
#if 0
    size_t idx = 0;
    for ( const auto& cand : oneOfContainer ) {
        cout << "oneOfContainer at " << idx++ << endl;
        for ( const auto& tr : cand ) {
            cout << testsuite->trace2string(tr.first) << endl;
            cout << testsuite->trace2string(tr.second) << endl;
        }
    }
#endif
    
    
    vector<size_t> indexVec;
    for ( size_t i = 0; i < oneOfContainer.size(); i++ ) {
        indexVec.push_back(0);
    }
    
    unique_ptr<Tree> bestTestCaseTree = make_unique<Tree>(*testCaseTree);
    for (auto & [alpha, beta, ooc] : oneOfContainer) {
        auto& [alpha_gamma, beta_gamma] = ooc.at(0);
        if(not alreadyDistinguished(fsm, bestTestCaseTree.get(), alpha, beta, inputAlphabetSize)) {
            bestTestCaseTree->add(alpha_gamma);
            bestTestCaseTree->add(beta_gamma);
        }
    }
    
    const size_t maxNumberOfLoops = 200000;
    size_t numberOfLoops = 1;
    
    for ( const auto& [alpha, beta, cont] : oneOfContainer ) {
        numberOfLoops *= cont.size();
        if ( numberOfLoops > maxNumberOfLoops ) {
            break;
        }
    }
    
    if ( numberOfLoops > maxNumberOfLoops ) {
        
        for (auto & [alpha, beta, ooc] : oneOfContainer) {
            if(not alreadyDistinguished(fsm, testCaseTree.get(), alpha, beta, inputAlphabetSize)) {
                auto [best_alpha_gamma, best_beta_gamma] = ooc.at(0);
                size_t insertionCosts =
                testCaseTree->getInsertionCost(best_alpha_gamma) +
                testCaseTree->getInsertionCost(best_beta_gamma);
                
                for ( size_t t = 1; 0 < insertionCosts && t < ooc.size(); t++ ) {
                    auto [other_alpha_gamma, other_beta_gamma] = ooc.at(t);
                    size_t costs =
                    testCaseTree->getInsertionCost(other_alpha_gamma) +
                    testCaseTree->getInsertionCost(other_beta_gamma);

                    if ( costs < insertionCosts ) {
                        insertionCosts = costs;
                        best_alpha_gamma = other_alpha_gamma;
                        best_beta_gamma = other_beta_gamma;
                    }
                }

                if ( insertionCosts > 0) {
                    testCaseTree->add(best_alpha_gamma);
                    testCaseTree->add(best_beta_gamma);
                }
            }
            
        }
    }
    else {
        
        while ( haveNewIndexVec(indexVec,oneOfContainer) ) {
            
#if 0
            cout << " indexVec = ";
            for (size_t i = 0; i < indexVec.size(); i++ ) cout << " " << indexVec.at(i);
            cout << endl;
#endif
            
            unique_ptr<Tree> anotherTestCaseTree = make_unique<Tree>(*testCaseTree);
            for ( size_t ooc = 0; ooc < oneOfContainer.size(); ooc++ ) {
                size_t idx = indexVec.at(ooc);
                auto& [alpha_gamma, beta_gamma] = std::get<2>(oneOfContainer.at(ooc)).at(idx);
                anotherTestCaseTree->add(alpha_gamma);
                anotherTestCaseTree->add(beta_gamma);
            }
            size_t otherNumLeaves = anotherTestCaseTree->getNumberOfLeaves();
            size_t otherSize = anotherTestCaseTree->size();
            size_t bestNumLeaves = bestTestCaseTree->getNumberOfLeaves();
            size_t bestSize = bestTestCaseTree->size();
            
            if ( otherNumLeaves < bestNumLeaves ||
                otherSize < bestSize ) {
                bestNumLeaves = otherNumLeaves;
                bestSize = otherSize;
                bestTestCaseTree = move(anotherTestCaseTree);
            }
            
        }
        
        
    }

    size_t numLeaves = testCaseTree->getNumberOfLeaves();
    size_t size = testCaseTree->size();
    size_t bestNumLeaves = bestTestCaseTree->getNumberOfLeaves();
    size_t bestSize = bestTestCaseTree->size();
    if(bestNumLeaves < numLeaves || bestSize < size) {
        testCaseTree = move(bestTestCaseTree);
    }
    
}

void HMethod::processOneTraceInsertions(const Trace& alpha, const Trace& beta, Traces& distTraces) {
    
    if ( distTraces.size() == 1 ) {
        // No choice - use this distinguishing trace
        Trace& gamma = distTraces.at(0);
        Trace alpha_gamma = alpha;
        alpha_gamma.append(gamma);
        Trace beta_gamma = beta;
        beta_gamma.append(gamma);
        testCaseTree->add(alpha_gamma);
        testCaseTree->add(beta_gamma);
    }
     
}

void HMethod::processDistinguishingTraces(const Trace& alpha, const Trace& beta, Traces& distTraces) {
    
    if ( distTraces.size() == 1 ) return;
    
    // Check for zero insertion costs - then we have nothing to insert into the test case tree
    vector< pair<Trace,Trace> > candidatesContainer;
    
    size_t insertionCosts = 4;
    for ( size_t i = 0; 0 < insertionCosts && i < distTraces.size(); i++ ) {
        Trace& gamma = distTraces.at(i);
        Trace alpha_gamma = alpha;
        alpha_gamma.append(gamma);
        Trace beta_gamma = beta;
        beta_gamma.append(gamma);
        size_t costs =
        testCaseTree->getInsertionCost(alpha_gamma) +
        testCaseTree->getInsertionCost(beta_gamma);
        if ( costs < insertionCosts ) {
            insertionCosts = costs;
        }
        pair<Trace,Trace> p(alpha_gamma,beta_gamma);
        candidatesContainer.push_back(p);
    }
    if ( insertionCosts > 0 ) {
        // Have positive insertion costs
        // We do not insert into the test case tree, but
        // add the candidate traces into the oneOfContainer, to be
        // processed later.
        oneOfContainer.emplace_back( alpha, beta, candidatesContainer );
    }
    
}

bool HMethod::differs(const Strings& str0, const Strings& str1) {
    if ( str0.size() != str1.size() ) return true;
    set<string> set0(str0.begin(), str0.end());
    set<string> set1(str1.begin(), str1.end());
    return (set0 != set1);
}

void HMethod::getShortestDistinguishingTraces(Ofsm& fsm,
                                              Index s0,
                                              Index s1,
                                              size_t inputAlphaSize,
                                              Trace& currentTrace,
                                              Traces& allShortestDisTraces_s0_s1,
                                              size_t k) {
    
    optional<Index>*** transitionTable = fsm.getTransitionTable();
    
    if ( k == 0 ) {
        allShortestDisTraces_s0_s1.push_back(currentTrace);
        return;
    }

    
    // Try out all input/output pairs to find all shortest distinguishing traces
    for ( Index x = 0; x < fsm.getInputAlphabetSize(); x++ ) {
        for ( Index y = 0; y < fsm.getOutputAlphabetSize(); y++ ) {
            optional<Index> s0Target = transitionTable[s0][x][y];
            optional<Index> s1Target = transitionTable[s1][x][y];
                        
            // Are s0Target and s1Target (k-1)-distinguishable?
            if ( (k == 1 && (s0Target.has_value()) != (s1Target.has_value())) ||
                 (k > 1 && s0Target.has_value() && s1Target.has_value() &&
                  state2classMaps.at(k-1)[s0Target.value()] != state2classMaps.at(k-1)[s1Target.value()]) ) {
                currentTrace.append(x);
                // Recursive call to complete this trace
                // Target states can only be nullopt iff k == 1, which means that they are ignored in the recursive call anyways.
                // Therefore, they can be 0 in that case.
                getShortestDistinguishingTraces(fsm,s0Target.value_or(0),s1Target.value_or(0),inputAlphaSize,currentTrace,allShortestDisTraces_s0_s1,k-1);
                currentTrace.pop_back();
            }
        }
    }
    
    
    
}

void HMethod::getShortestDistinguishingTraces(Ofsm& fsm,
                                              Index s0,
                                              Index s1,
                                              size_t inputAlphaSize,
                                              Trace& currentTrace,
                                              Traces& allShortestDisTraces_s0_s1) {
        
    size_t ell;
    for ( ell = 1; ell < state2classMaps.size(); ell++ ) {
        // ell is defined as the lowest index so that qi and qj are separated
        // in state2classMaps.at(ell). Such an ell must exist, since fsm is minimised.
        if ( state2classMaps.at(ell)[s0] != state2classMaps.at(ell)[s1] ) {
            break;
        }
    }
    if ( ell == state2classMaps.size() )
        throw runtime_error("ERROR cannot distinguish states "
                            + to_string(s0) + ", " + to_string(s1));
    
    
    getShortestDistinguishingTraces(fsm,s0,s1,inputAlphaSize,currentTrace,allShortestDisTraces_s0_s1,ell);
    
#if 0
    cout << "Distinguish states " << s0 << " and " << s1 << endl;
    for ( const auto& tr : allShortestDisTraces_s0_s1 ) {
        cout << testsuite->trace2string(tr) << endl;
    }
#endif
    
}


void HMethod::getShortestDistinguishingTraces(Dfsm& fsm,
                                              Index s0,
                                              Index s1,
                                              size_t inputAlphaSize,
                                              Trace& currentTrace,
                                              Traces& allShortestDisTraces_s0_s1,
                                              size_t k) {

    optional<Index>*** dfsmTable = fsm.getDfsmTable();

    if ( k == 0 ) {
        allShortestDisTraces_s0_s1.push_back(currentTrace);
        return;
    }
    
    // Try out all inputs to find all shortest distinguishing traces
    for ( Index x = 0; x < fsm.getInputAlphabetSize(); x++ ) {
        optional<Index> s0Target = dfsmTable[s0][x][0];
        optional<Index> s1Target = dfsmTable[s1][x][0];
        
        if ( not s0Target.has_value() || not s1Target.has_value() ) continue;
        
        // Are s0Target and s1Target (k-1)-distinguishable?
        if ( (k == 1 && dfsmTable[s0][x][1].value() != dfsmTable[s1][x][1].value()) ||
            (k > 1 && state2classMaps.at(k-1)[s0Target.value()] != state2classMaps.at(k-1)[s1Target.value()]) ) {
            currentTrace.append(x);
            // Recursive call to complete this trace
            getShortestDistinguishingTraces(fsm,s0Target.value(),s1Target.value(),inputAlphaSize,currentTrace,allShortestDisTraces_s0_s1,k-1);
            currentTrace.pop_back();
        }
    }
    
}

void HMethod::getShortestDistinguishingTraces(Dfsm& fsm,
                                              Index s0,
                                              Index s1,
                                              size_t inputAlphaSize,
                                              Trace& currentTrace,
                                              Traces& allShortestDisTraces_s0_s1) {
    
    size_t ell;
    for ( ell = 1; ell < state2classMaps.size(); ell++ ) {
        // ell is defined as the lowest index so that qi and qj are separated
        // in state2classMaps.at(ell). Such an ell must exist, since fsm is minimised.
        if ( state2classMaps.at(ell)[s0] != state2classMaps.at(ell)[s1] ) {
            break;
        }
    }
    if ( ell == state2classMaps.size() )
        throw runtime_error("ERROR cannot distinguish states "
                            + to_string(s0) + ", " + to_string(s1));
    
    
    getShortestDistinguishingTraces(fsm,s0,s1,inputAlphaSize,currentTrace,allShortestDisTraces_s0_s1,ell);
    
#if 0
    cout << "Distinguish states " << s0 << " and " << s1 << endl;
    for ( const auto& tr : allShortestDisTraces_s0_s1 ) {
        cout << testsuite->trace2string(tr) << endl;
    }
#endif
    
}

void HMethod::getDistinguishingTraces(Fsm& fsm,
                                      Index s0,
                                      Index s1,
                                      size_t depth,
                                      size_t inputAlphaSize,
                                      Trace& currentTrace,
                                      Traces& allDisTraces_s0_s1) {
    
    if ( depth == 0 ) return;

#if 0
    cout << "Distinguish states " << s0 << " and " << s1 << " depth " << depth << endl;
#endif
    
    for ( size_t x = 0; x < inputAlphaSize; x++ ) {
        currentTrace.append(x);
        Strings ioTraces_s0;
        Strings ioTraces_s1;
        fsm.getIOTracesFromState(currentTrace,ioTraces_s0,s0);
        fsm.getIOTracesFromState(currentTrace,ioTraces_s1,s1);
        
        if ( differs(ioTraces_s0,ioTraces_s1) ) {
            allDisTraces_s0_s1.push_back(currentTrace);
#if 0
            cout << ioTraces_s0.back() << endl;
            cout << ioTraces_s1.back() << endl;
#endif
            // Do not go deeper, try next x
        }
        else {
            // CurrentTrace is not a distinguishing trace.
            set<Index> targets_s0 = fsm.getTargets(currentTrace,s0);
            set<Index> targets_s1 = fsm.getTargets(currentTrace,s1);
            if ( targets_s0.size() == 1 &&
                targets_s1.size() == 1 &&
                *targets_s0.begin() == *targets_s1.begin() ) {
                // If we reach the same target state, no extension of this trace
                // can ever become a distinguishing trace
                ;
            }
            else {
                // Depth-first search
                getDistinguishingTraces(fsm,s0,s1,depth-1,inputAlphaSize,currentTrace,allDisTraces_s0_s1);
            }
        }
        // Try next x
        currentTrace.pop_back();
    }
    
}



void HMethod::visit(Fsm &fsm) {
    
    // Must have a completely specified FSM
    if ( ! fsm.isCompletelySpecified() ) {
        throw runtime_error("This H method implementation only works with completely specified OFSMs or DFSMs");
    }
    
    // First, a transformation to prime machine is needed in any case
    unique_ptr<Fsm> primeFsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
    
    // Now visit the prime machine
    primeFsm->accept(*this);
    
    
}

void HMethod::visit(Ofsm &fsm) {
    
    if ( ! fsm.isCompletelySpecified() ) {
        throw runtime_error("This H method implementation only works with completely specified OFSMs or DFSMs");
    }
    
    if ( fsm.isMinimal() ) {
        
        size_t numberOfStates = fsm.size();
        size_t inputAlphaSize = fsm.getInputAlphabetSize();
        
        // Calculate OFSM tables just once - will be used by every call to
        // getShortestDistinguishingTraces()
        fsm.calcOFSMTables(classMap, state2classMaps);
        
        Traces** dist = new Traces*[numberOfStates];
        for (size_t i = 0; i < numberOfStates; ++i)
            dist[i] = new Traces[numberOfStates];

        for ( Index s0 = 0; s0 < numberOfStates; s0++ ) {
            for ( Index s1 = s0+1; s1 < numberOfStates; s1++ ) {
                Trace currentTrace;
                getShortestDistinguishingTraces(fsm, s0, s1, inputAlphaSize, currentTrace, dist[s0][s1]);
            }
        }
        
        createStateCover(fsm);
        
        auto stateCoverAsTree = make_unique<Tree>(*testCaseTree);
        
        // Store all traces from state cover
        Traces stateCoverTraces = testCaseTree->getAllTestCases();
        
        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates+1,
                                       testsuite->getFsm().getInputAlphabetSize(),
                                       enumeratedTraces);
        
        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);

        // Cut test case tree with FSM language to only have defined traces in the tree
        testCaseTree = cutTreeWithFSMLanguage(*testCaseTree, fsm);
        
        // Store all traces of stateCover.EnumeratedTraces
        Traces stateCover_enumeratedTraces = testCaseTree->getAllTestCases();
        
        // Step 1a.
        for ( size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++ ) {
            Trace alpha = stateCoverTraces.at(tr1);
            // Due to nondeterminism, we may have several target states reached via alpha
            set<Index> statesAfterAlpha = fsm.getTargets(alpha);
            
            for ( size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++ ) {
                Trace beta = stateCoverTraces.at(tr2);
                set<Index> statesAfterBeta = fsm.getTargets(beta);
                
                // Now loop over pairs of states from statesAfterAlpha x statesAfterBeta
                for ( const Index& a : statesAfterAlpha ) {
                    for ( const Index& b : statesAfterBeta ) {
                        
                        if ( a == b ) continue;
                        
                        Index s0 = (a < b) ? a : b;
                        Index s1 = (a < b) ? b : a;
                        
                        if ( dist[s0][s1].empty() ) {
                            throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                to_string(s0) + "," + to_string(s1));
                        }
                        processOneTraceInsertions(alpha,beta,dist[s0][s1]);
                    }
                }
                
            }
        }
        
        // Step 2a.
        for ( const auto& alpha : stateCoverTraces ) {
            // Due to nondeterminism, we may have several target states reached via alpha
            set<Index> statesAfterAlpha = fsm.getTargets(alpha);
            
            for ( const auto& beta : stateCover_enumeratedTraces ) {
                
                // Skip if beta in state cover
                if ( stateCoverAsTree->getInsertionCost(beta) == 0 ) {
                    continue;
                }
                
                set<Index> statesAfterBeta = fsm.getTargets(beta);
                
                for ( const Index& a : statesAfterAlpha ) {
                    for ( const Index& b : statesAfterBeta ) {
                        
                        if ( a == b ) continue;
                        
                        Index s0 = (a < b) ? a : b;
                        Index s1 = (a < b) ? b : a;
                        
                        if ( dist[s0][s1].empty() ) {
                            throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                to_string(s0) + "," + to_string(s1));
                        }
                        processOneTraceInsertions(alpha,beta,dist[s0][s1]);
                    }
                }
                
            }
        }
        
        
        // Step 3a.
        for ( const auto& v : stateCoverTraces ) {
            
            for ( size_t uPrimeLen = 1; uPrimeLen < 1+numAddStates; uPrimeLen++ ) {
                
                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               testsuite->getFsm().getInputAlphabetSize(),
                                               uPrimeTraces);
                
                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               testsuite->getFsm().getInputAlphabetSize(),
                                               uTraces);
                
                for ( const auto& uPrime : uPrimeTraces ) {
                    for ( const auto& u : uTraces ) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);
                        
                        set<Index> statesAfter_v_uPrime = fsm.getTargets(v_uPrime);
                        set<Index> statesAfter_v_uPrime_u = fsm.getTargets(v_uPrime_u);
                        
                        for ( const Index& a : statesAfter_v_uPrime ) {
                            for ( const Index& b : statesAfter_v_uPrime_u ) {
                                
                                if ( a == b ) continue;
                                
                                Index s0 = (a < b) ? a : b;
                                Index s1 = (a < b) ? b : a;
                                
                                if ( dist[s0][s1].empty() ) {
                                    throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                        to_string(s0) + "," + to_string(s1));
                                }
                                processOneTraceInsertions(v_uPrime,v_uPrime_u,dist[s0][s1]);
                            }
                        }
                        
                    }
                }
            }
            
        }
        
        
        // Step 1b.
        for ( size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++ ) {
            Trace alpha = stateCoverTraces.at(tr1);
            // Due to nondeterminism, we may have several target states reached via alpha
            set<Index> statesAfterAlpha = fsm.getTargets(alpha);
            
            for ( size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++ ) {
                Trace beta = stateCoverTraces.at(tr2);
                set<Index> statesAfterBeta = fsm.getTargets(beta);
                
                // Now loop over pairs of states from statesAfterAlpha x statesAfterBeta
                for ( const Index& a : statesAfterAlpha ) {
                    for ( const Index& b : statesAfterBeta ) {
                        
                        if ( a == b ) continue;
                        
                        Index s0 = (a < b) ? a : b;
                        Index s1 = (a < b) ? b : a;
                        
                        if ( dist[s0][s1].empty() ) {
                            throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                to_string(s0) + "," + to_string(s1));
                        }
                        processDistinguishingTraces(alpha,beta,dist[s0][s1]);
                    }
                }
                
            }
        }
        
        // Step 2b.
        for ( const auto& alpha : stateCoverTraces ) {
            // Due to nondeterminism, we may have several target states reached via alpha
            set<Index> statesAfterAlpha = fsm.getTargets(alpha);
            
            for ( const auto& beta : stateCover_enumeratedTraces ) {
                
                // Skip if beta in state cover
                if ( stateCoverAsTree->getInsertionCost(beta) == 0 ) {
                    continue;
                }
                
                set<Index> statesAfterBeta = fsm.getTargets(beta);
                
                for ( const Index& a : statesAfterAlpha ) {
                    for ( const Index& b : statesAfterBeta ) {
                        
                        if ( a == b ) continue;
                        
                        Index s0 = (a < b) ? a : b;
                        Index s1 = (a < b) ? b : a;
                        
                        if ( dist[s0][s1].empty() ) {
                            throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                to_string(s0) + "," + to_string(s1));
                        }
                        processDistinguishingTraces(alpha,beta,dist[s0][s1]);
                    }
                }
                
            }
        }
        
        // Step 3b.
        for ( const auto& v : stateCoverTraces ) {
            
            for ( size_t uPrimeLen = 1; uPrimeLen < 1+numAddStates; uPrimeLen++ ) {
                
                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               testsuite->getFsm().getInputAlphabetSize(),
                                               uPrimeTraces);
                
                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               testsuite->getFsm().getInputAlphabetSize(),
                                               uTraces);
                
                for ( const auto& uPrime : uPrimeTraces ) {
                    for ( const auto& u : uTraces ) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);
                        
                        set<Index> statesAfter_v_uPrime = fsm.getTargets(v_uPrime);
                        set<Index> statesAfter_v_uPrime_u = fsm.getTargets(v_uPrime_u);
                        
                        for ( const Index& a : statesAfter_v_uPrime ) {
                            for ( const Index& b : statesAfter_v_uPrime_u ) {
                                
                                if ( a == b ) continue;
                                
                                Index s0 = (a < b) ? a : b;
                                Index s1 = (a < b) ? b : a;
                                
                                if ( dist[s0][s1].empty() ) {
                                    throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                        to_string(s0) + "," + to_string(s1));
                                }
                                processDistinguishingTraces(v_uPrime,v_uPrime_u,dist[s0][s1]);
                            }
                        }
                        
                    }
                }
            }
            
        }

        
        // Add missing trace candidates, optimising the size of the test case tree
        addBestCandidateTraces(fsm, inputAlphaSize);
        
        // Check final version of the test case tree
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();

        for (Index i = 0; i < numberOfStates; ++i)
            delete[] dist[i];
        delete[] dist;
    }
    else {
        
        // Need to transform to prime OFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
        // Now visit the prime OFSM
        minimisedOfsm->accept(*this);
        
    }
    
    
}

void HMethod::visit(Dfsm &fsm) {
    
    if ( ! fsm.isCompletelySpecified() ) {
        throw runtime_error("This H method implementation only works with completely specified DFSMs");
    }
    
    if ( fsm.isMinimal() ) {
        
        size_t numberOfStates = fsm.size();
        size_t inputAlphaSize = fsm.getInputAlphabetSize();
        
        // Calculate OFSM tables just once - will be used by every call to
        // getShortestDistinguishingTraces()
        fsm.calcOFSMTables(classMap, state2classMaps);
        
        Traces** dist = new Traces*[numberOfStates];
        for (Index i = 0; i < numberOfStates; ++i)
            dist[i] = new Traces[numberOfStates];

        for ( Index s0 = 0; s0 < numberOfStates; s0++ ) {
            for ( Index s1 = s0+1; s1 < numberOfStates; s1++ ) {
                Trace currentTrace;
                getDistinguishingTraces(fsm, s0, s1, numberOfStates - 1, inputAlphaSize, currentTrace, dist[s0][s1]);

            }
        }
        
        createStateCover(fsm);
        
        auto stateCoverAsTree = make_unique<Tree>(*testCaseTree);
        
        // Store all traces from state cover
        Traces stateCoverTraces = testCaseTree->getAllTestCases();
        assert(stateCoverTraces.size() == numberOfStates);
        
        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates+1,
                                       testsuite->getFsm().getInputAlphabetSize(),
                                       enumeratedTraces);
        
        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);

        // Cut test case tree with FSM language to only have defined traces in the tree
        testCaseTree = cutTreeWithFSMLanguage(*testCaseTree, fsm);
        
        // Store all traces of stateCover.EnumeratedTraces
        Traces stateCover_enumeratedTraces = testCaseTree->getAllTestCases();
        
        // Step 1a.
        // Add all alpha.gamma, beta.gamma where alpha, beta in V
        // and gamma distinguishes s0-after-alpha, s0-after-beta
        // (if alpha.gamma or beta.gamma are already in testCaseTree, addition
        // will not lead to a new test case).
        // For 1a, perform only one-trace-insertions
        for ( size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++ ) {
            Trace alpha = stateCoverTraces.at(tr1);
            optional<Index> s0AfterAplhaId = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for ( size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++ ) {
                Trace beta = stateCoverTraces.at(tr2);
                                
                optional<Index> s0AfterBetaId = fsm.getTarget(fsm.getInitialStateId(), beta);
                // If reached states are different, look for gamma
                if ( s0AfterAplhaId.value() != s0AfterBetaId.value() and
                    not alreadyDistinguished(fsm, testCaseTree.get(), alpha, beta, inputAlphaSize)) {
                    // We know that both alpha and beta are defined, as they
                    // are prefixes to state cover traces.  Then,
                    // s0AfterAplhaId and s0AfterBetaId are state IDs and the
                    // containing optionals can safely just be "dereferenced"
                    // without checking.
                    Index s0 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterAplhaId.value() : s0AfterBetaId.value();
                    Index s1 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterBetaId.value() : s0AfterAplhaId.value();
                    if ( dist[s0][s1].empty() ) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processOneTraceInsertions(alpha,beta,dist[s0][s1]);
                }
            }
        }
        
        
        
        
        
        // Step 2a.
        // Handle pairs of traces alpha, beta with alpha in state cover and
        // beta in stateCover_enumeratedTraces. Skip (alpha,beta) if
        // beta is also contained in state cover.
        // For 2a, perform only one-trace-insertions
        for ( const auto& alpha : stateCoverTraces ) {
            
            optional<Index> sAfterAlpha = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for ( const auto& beta : stateCover_enumeratedTraces ) {
                                
                // Skip if beta in state cover
                if ( stateCoverAsTree->getInsertionCost(beta) == 0 ) {
                    continue;
                }

                optional<Index> sAfterBeta = fsm.getTarget(fsm.getInitialStateId(), beta);
                if ( sAfterAlpha.value() != sAfterBeta.value() and
                     not alreadyDistinguished(fsm, testCaseTree.get(), alpha, beta, inputAlphaSize)) {
                    // We know that both alpha and beta are defined.
                    // Thus, sAfterAlpha and sAfterBeta are state IDs and the
                    // containing optionals can safely be "dereferenced" without checking.
                    Index s0 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterAlpha.value() : sAfterBeta.value();
                    Index s1 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterBeta.value() : sAfterAlpha.value();
                    
                    if ( dist[s0][s1].empty() ) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processOneTraceInsertions(alpha,beta,dist[s0][s1]);
                }
            }
        }
         
        
        // Step 3a.
        // Handle pairs of traces v.u', v.u'.u, where
        // v is from the state cover, (u'.u) is an arbitrary input trace
        // of length 2..(1+numAddStates), and both u', u are non-empty.
        // For 3a, perform only one-trace-insertions
        for ( const auto& v : stateCoverTraces ) {
            
            for ( size_t uPrimeLen = 1; uPrimeLen < 1+numAddStates; uPrimeLen++ ) {
                
                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               fsm.getInputAlphabetSize(),
                                               uPrimeTraces);
                
                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               testsuite->getFsm().getInputAlphabetSize(),
                                               uTraces);
                
                for ( const auto& uPrime : uPrimeTraces ) {
                    for ( const auto& u : uTraces ) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);
                        
                        optional<Index> sPrime = fsm.getTarget(fsm.getInitialStateId(), v_uPrime);
                        optional<Index> s = fsm.getTarget(fsm.getInitialStateId(), v_uPrime_u);
                        
                        if ( sPrime.value() != s.value() and
                            not alreadyDistinguished(fsm, testCaseTree.get(), v_uPrime, v_uPrime_u, inputAlphaSize)) {
                            Index s0 = ( sPrime.value() < s.value() ) ? sPrime.value() : s.value();
                            Index s1 = ( sPrime.value() < s.value() ) ? s.value() : sPrime.value();
                            
                            if ( dist[s0][s1].empty() ) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processOneTraceInsertions(v_uPrime,v_uPrime_u,dist[s0][s1]);
                        }
                    }
                }
                
            }
            
        }
         
        
        // Step 1b.
        // Add all alpha.gamma, beta.gamma where alpha, beta in V
        // and gamma distinguishes s0-after-alpha, s0-after-beta
        // (if alpha.gamma or beta.gamma are already in testCaseTree, addition
        // will not lead to a new test case)
        for ( size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++ ) {
            Trace alpha = stateCoverTraces.at(tr1);
            optional<Index> s0AfterAplhaId = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for ( size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++ ) {
                Trace beta = stateCoverTraces.at(tr2);
                                
                optional<Index> s0AfterBetaId = fsm.getTarget(fsm.getInitialStateId(), beta);
                // If reached states are different, look for gamma
                if ( s0AfterAplhaId.value() != s0AfterBetaId.value() and
                    not alreadyDistinguished(fsm, testCaseTree.get(), alpha, beta, inputAlphaSize)) {
                    // `alpha` and `beta` are prefixes of state cover traces
                    // and therefore always reach some state and the containing
                    // optionals can be "dereferenced" without checking.
                    Index s0 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterAplhaId.value() : s0AfterBetaId.value();
                    Index s1 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterBetaId.value() : s0AfterAplhaId.value();
                    if ( dist[s0][s1].empty() ) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processDistinguishingTraces(alpha,beta,dist[s0][s1]);
                }
            }
        }
        
        
        
        
        
        // Step 2b.
        // Handle pairs of traces alpha, beta with alpha in state cover and
        // beta in stateCover_enumeratedTraces. Skip (alpha,beta) if
        // beta is also contained in state cover.
        for ( const auto& alpha : stateCoverTraces ) {
            
            optional<Index> sAfterAlpha = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for ( const auto& beta : stateCover_enumeratedTraces ) {
                                
                // Skip if beta in state cover
                if ( stateCoverAsTree->getInsertionCost(beta) == 0 ) {
                    continue;
                }

                optional<Index> sAfterBeta = fsm.getTarget(fsm.getInitialStateId(), beta);
                if ( sAfterAlpha.value() != sAfterBeta.value() and
                    not alreadyDistinguished(fsm, testCaseTree.get(), alpha, beta, inputAlphaSize)) {
                    // `alpha` and `beta` are always defined. Thus
                    // `sAfterAlpha` and `sAfterBeta` always contain a value.
                    Index s0 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterAlpha.value() : sAfterBeta.value();
                    Index s1 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterBeta.value() : sAfterAlpha.value();
                    
                    if ( dist[s0][s1].empty() ) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processDistinguishingTraces(alpha,beta,dist[s0][s1]);
                }
            }
        }
        
        // Step 3b.
        // Handle pairs of traces v.u', v.u'.u, where
        // v is from the state cover, (u'.u) is an arbitrary input trace
        // of length 2..(1+numAddStates), and both u', u are non-empty.
        for ( const auto& v : stateCoverTraces ) {
            
            for ( size_t uPrimeLen = 1; uPrimeLen < 1+numAddStates; uPrimeLen++ ) {
                
                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               fsm.getInputAlphabetSize(),
                                               uPrimeTraces);
                
                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               fsm.getInputAlphabetSize(),
                                               uTraces);
                
                for ( const auto& uPrime : uPrimeTraces ) {
                    for ( const auto& u : uTraces ) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);
                        
                        optional<Index> sPrime = fsm.getTarget(fsm.getInitialStateId(), v_uPrime);
                        optional<Index> s = fsm.getTarget(fsm.getInitialStateId(), v_uPrime_u);
                        
                        if ( sPrime.value() != s.value() and
                            not alreadyDistinguished(fsm, testCaseTree.get(), v_uPrime, v_uPrime_u, inputAlphaSize)) {
                            Index s0 = ( sPrime.value() < s.value() ) ? sPrime.value() : s.value();
                            Index s1 = ( sPrime.value() < s.value() ) ? s.value() : sPrime.value();
                            
                            if ( dist[s0][s1].empty() ) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processDistinguishingTraces(v_uPrime,v_uPrime_u,dist[s0][s1]);
                        }
                    }
                }
                
            }
            
        }
        
        // Add missing trace candidates, optimising the size of the test case tree
        addBestCandidateTraces(fsm, inputAlphaSize);
        
        // Check final version of the test case tree
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();

        for (Index i = 0; i < numberOfStates; ++i)
            delete[] dist[i];
        delete[] dist;
    }
    else {
        
        // Need to transform to prime DFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
        // Now visit the prime DFSM
        minimisedDfsm->accept(*this);
        
    }
    
}

} // namespace libfsmtest
