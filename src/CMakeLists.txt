cmake_minimum_required(VERSION 3.20)

set(LIBFSMTEST_VERSION_MAJOR 1)
set(LIBFSMTEST_VERSION_MINOR 0)

project(libfsmtest VERSION "${LIBFSMTEST_VERSION_MAJOR}.${LIBFSMTEST_VERSION_MINOR}")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

enable_testing()

add_compile_definitions(LIBFSMTEST_VERSION_MAJOR=${LIBFSMTEST_VERSION_MAJOR})
add_compile_definitions(LIBFSMTEST_VERSION_MINOR=${LIBFSMTEST_VERSION_MINOR})
if (MSVC)
    add_compile_options(/WX /W4 /permissive-)
else()
    add_compile_options(-Wall -Wextra -pedantic -Wsign-conversion -Wconversion -Wnarrowing -Wuninitialized -Wshadow -Wfatal-errors
                        -Wold-style-cast -Wswitch-enum -Wno-unqualified-std-cast-call)
    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
      add_compile_options(-Wdocumentation)
    endif()
endif()

add_subdirectory(libfsmtest)
add_subdirectory(checker)
add_subdirectory(generator)
add_subdirectory(fsm2dot)
add_subdirectory(usage-demo)
