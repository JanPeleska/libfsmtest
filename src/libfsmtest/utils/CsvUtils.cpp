/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "CsvUtils.hpp"
#include <cassert>

using namespace std;

namespace libfsmtest {

Csv readCsv(std::istream &inputstream,
            char delimiter,
            const function<void(string &)>& applyToLine,
            const function<void(string &)>& applyToCell) {
    vector<CsvColumn> csv;

    string line;
    size_t row = 0;
    size_t numberOfColumns = 0;

    // Consume the file line-by-line.
    // Lines are delimited by a '\n' (LF) character.
    // If the file has windows-style line endings, there will be a '\r' (CR) character at the end of the line.
    while(getline(inputstream, line, '\n')) {
        if (applyToLine) applyToLine(line);

        // Ignore leftover CR
        if (line.back() == '\r') line.pop_back();

        // Ignore lines containing only newlines
        if (line.length() == 0) continue;

        // Count the amount of delimiters on this line.
        // Delimiters only separate cells from each other instead of ending cells,
        // so there are amountOfDelimiters + 1 cells on this line.
        size_t amountOfDelimiters = static_cast<size_t>(count(line.begin(), line.end(), delimiter));

        if (row == 0) {
            // This is the first row.
            // Therefore, the amount of delimiters defines the amount of columns of this file.
            numberOfColumns = amountOfDelimiters + 1;
            // Create columns
            for (size_t i = 1; i <= numberOfColumns; ++i)
                csv.emplace_back();
        } else {
            if (amountOfDelimiters != numberOfColumns - 1) {
                ostringstream error_message;
                error_message << "CSV is not in rectangular format."
                              << "Deviant row #" << row + 1 << endl
                              << "\"" << line << "\"" << endl
                              << "has " << amountOfDelimiters + 1 << " columns, first row had"
                              << numberOfColumns << " columns";

                throw runtime_error(error_message.str());
            }
        }

        istringstream lineStream(line);
        string cell;
        size_t columnIndex = 0;
        bool quote = false;
        bool lastWasDelimiter = false;

        while (auto next = lineStream.get()) {
            if(istringstream::traits_type::not_eof(next)) {
                auto nextChar = istringstream::traits_type::to_char_type(next);
                switch (nextChar) {
                    case '\"':
                        if (cell.empty()) {
                            // This is the first character of a cell.
                            // This will always indicate that the cell is enclosed in quotes.
                            quote = true;
                            break;
                        }
                        // This is not the first character of a cell.
                        if (lineStream.peek() == '\"') {
                            // This is an escaped quote, so add it to the content
                            // and remove it from the stream.
                            lineStream.get();
                            cell += nextChar;
                            break;
                        }
                        // This is neither the first, nor an escaped quote,
                        // so it should end a quoted cell
                        if (!quote)
                            throw std::runtime_error("Error while parsing CSV: Quotation mismatch in row " + to_string(row + 1) + " and column " +
                                                             to_string(columnIndex + 1));
                        // A quote got ended, so we expect the next character to be either a delimiter, EOF (=LF) or CR
                        quote = false;
                        if (lineStream.peek() != EOF && lineStream.peek() != delimiter && lineStream.peek() != '\r')
                            throw std::runtime_error("Error while parsing CSV: Missing cell end in row " + to_string(row + 1) + " and column " +
                                                             to_string(columnIndex + 1));
                        break;

                    case EOF:
                        assert(false);
                        break;
                    default:
                        if (nextChar == delimiter) {
                            if (quote) {
                                // The delimiter appeared inside a quoted cell, so it is a normal character
                                cell += nextChar;
                            } else {
                                // The delimiter ended a cell, so put it in the current column
                                if (applyToCell) applyToCell(cell);
                                csv.at(columnIndex).push_back(cell);
                                // Clear the cell and increase the column index
                                cell = "";
                                ++columnIndex;
                            }
                        } else {
                            cell += nextChar;
                        }
                        break;
                }
                lastWasDelimiter = (nextChar == delimiter);
            } else {
                if (quote) throw std::runtime_error("Error while parsing CSV: Quotation mismatch in row " +
                                                            to_string(row + 1) + " and column " + to_string(columnIndex + 1));
                if (lastWasDelimiter || !cell.empty()) {
                    if (applyToCell) applyToCell(cell);
                    csv.at(columnIndex).push_back(cell);
                    cell = "";
                    ++columnIndex;
                }
                break;
            }
        }
        ++row;
    }

    return csv;
}

void writeCsv(std::ostream &outstream, Csv &csv, const char delimiter) {
    for (size_t row = 0; row < csv.at(0).size(); row++) {
        for (size_t col = 0; col < csv.size() - 1; col++) {
            outstream << csv.at(col).at(row) << delimiter;
        }
        outstream << csv.at(csv.size() - 1).at(row) << std::endl;
    }
}

} //namespace libfsmtest
