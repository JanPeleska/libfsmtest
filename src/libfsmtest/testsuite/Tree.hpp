/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <memory>
#include <vector>
#include <optional>

#include "testsuite/TreeNode.hpp"
#include "testsuite/Trace.hpp"
#include "fsm/definitions.hpp"
#include "testsuite/definitions.hpp"


namespace libfsmtest {

class Tree {
private:
    /** The root node of this tree */
    std::unique_ptr<TreeNode> root;
    
    /** True if tree "contains" the empty trace */
    bool containsEmptyTrace;
    
public:
    /**
     * Constructor for an empty tree.
     */
    explicit Tree() : root(std::make_unique<TreeNode>(nullptr)) {containsEmptyTrace = false;}

    /**
     * Constructor for tree with a root node.
     * @param rootPtr The root node.
     */
    Tree(std::unique_ptr<TreeNode> rootPtr) : root(std::move(rootPtr)) { containsEmptyTrace = true; }

    /**
     * Copy constructor.
     * Will create a deep copy of all nodes.
     * @param other The tree to copy
     */
    Tree(const Tree& other) : root(std::make_unique<TreeNode>(*other.root))  { containsEmptyTrace = other.containsEmptyTrace; }

    /**
     * Destructor.
     */
    ~Tree() = default;

    /**
     * Getter for #root.
     * @return #root
     */
    TreeNode* getRoot() const {
        return root.get();
    }
 

    /**
     * Getter for all leaves of this tree.
     * Will traverse the tree to obtain those.
     * @return All leaves of this tree.
     */
    std::vector<TreeNode*> getLeaves() const;
    
    /** Get the number of nodes in the tree */
    size_t size() const;
    
    /** Get the number of leaves */
    size_t getNumberOfLeaves() const;

    /**
     * Add a trace to the tree.
     * @param t The trace to add.
     */
    void add(const Trace& t);
    
    
    
    /** Get all existing nodes into container */
    void getExistingNodes(std::set<TreeNode*>& existingNodesSet);

    /** Get all existing nodes */
    std::vector<TreeNode*> getExistingNodes() const;

    /**
     * Add a trace to all nodes in this tree that are
     * contained in existingNodesSet. 
     * @param t The trace to add to each node contained in nodeSet.
     */
    void addToAllNodes(const Trace& t, const std::set<TreeNode*>& nodeSet);
    
    /**
     * Add all traces to all nodes in this tree (state of the
     * tree when the operation is called).
     * @param traces Container of traces to add to each node existing when
     *          the operation is called.
     */
    void addToAllNodes(const Traces& traces);

    /**
     * Return the TreeNode after an input- or output index.
     * @param index The index after which to get the node.
     * @return nullptr, when there is no node for that index, or
     *         TreeNode for that index.
     */
    TreeNode* after(size_t index) const;
    
    /**
     * Find tree node by value
     */
    TreeNode* findByValue(std::optional<size_t> value);

    /**
     * Return the TreeNode after following a trace.
     * @see after(Trace::const_iterator, Trace::const_iterator)
     * @param t The trace to follow.
     * @return nullptr, when the trace isn't a path of this tree
     *         TreeNode after following the trace.
     */
    TreeNode* after(const Trace& t);

    /**
     * Return full traces in this tree.
     * @return All traces from root to leaves.
     */
    Traces getTestCases() const;

    /**
     * Return all traces (including prefixes) in this tree.
     * @return All traces from root to every node.
     */
    Traces getAllTestCases() const;
    
    /**
     * Calculate costs for inserting trace into tree
     * @param tr The trace whose insertion costs should be calculated.
     * @return 0 iff the trace is already contained in the tree
     *         1 iff the trace would extend an existing branch
     *         2 iff the trace would create a new branch (i.e. branch
     *               off an existing path, before its leaf is visited)
     */
    size_t getInsertionCost(const Trace& tr) const;
    
    bool checkTree() { return root->checkNodes(); }
    
    void toDot(const std::string& filename);


    /**
     * Apply a function to all nodes currently in the tree.
     * @param f Function to apply.
     */
    void applyToAllNodes(std::function<void (libfsmtest::TreeNode *)> f);
    void applyToAllNodes(std::function<void (libfsmtest::TreeNode const*)> f) const;

};

} // namespace libfsmtest



