/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

namespace libfsmtest {

using CsvColumn = std::vector<std::string>;
using Csv = std::vector<CsvColumn>;

/**
 * Reads CSV data from an input stream (e.g. a filestream).
 * Assumes the data is rectangular
 * (every column has the same amount of cells, every row has the same amount of columns).
 * @param inputstream The stream to read CSV data from.
 * @param delimiter The delimiter used to separate cells.
 * @param applyToLine An optional function to apply to each line while parsing.
 * @param applyToCell An optional function to apply to each cell while parsing.
 * @return Read data as vector of columns.
 */
Csv readCsv(std::istream &inputstream,
            char delimiter,
            const std::function<void(std::string &)>& applyToLine = nullptr,
            const std::function<void(std::string &)>& applyToCell = nullptr);

/**
 * Writes data in CSV format to an output stream.
 * Assumes the data is rectangular
 * (Every column has the same amount of rows, every row has the same amount of columns).
 * @param outstream The stream the data will be written to
 * @param csv The data to write. Has to be rectangular.
 * @param delimiter The delimiter to separate cells.
 */
void writeCsv(std::ostream &outstream, Csv &csv, char delimiter);

} // namespace libfsmtest

