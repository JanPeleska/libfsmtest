/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include "fsm/Dfsm.hpp"
#include <bitset>

namespace libfsmtest {

/**
 * Traits used for the random generation of FSMs.
 * @see RandomFsmCreator
 * Combining multiple traits is possible.
 * Not specifying one of the traits will not ensure that the negation is true.
 */
enum FsmTrait {
    MINIMAL, /*!< The machine has to be minimal, but does not necessarily contain the number of specified states. */
    INITIALLY_CONNECTED, /*!< The machine has to be initially connected */
    COMPLETELY_SPECIFIED /*!< The machine has to be completely specified */
};

class FsmTraits {
private:
    std::bitset<3> traits;

public:
    template<typename... Args>
    FsmTraits(Args... args) : traits() {
        traits[0] = ((args == MINIMAL) || ...);
        traits[1] = traits[0] || ((args == INITIALLY_CONNECTED) || ...);
        traits[2] = ((args == COMPLETELY_SPECIFIED) || ...);
    }

    bool minimal() {
        return traits[0];
    }
    bool initially_connected() {
        return traits[1];
    }
    bool completely_specified() {
        return traits[2];
    }
};

/**
 * Checks whether a given FSM fulfills all traits.
 * @param fsm The FSM to check traits on
 * @return true, if the FSM fullfills all traits from the template parameter,
 *         false, otherwise
 */
bool checkFsmTraits(Fsm& fsm, FsmTraits traits) {
    bool minimalTrait = traits.minimal() ? fsm.isMinimal() : true;
    bool initially_connectedTrait = traits.initially_connected() ? fsm.isInitiallyConnected() : true;
    bool completely_specifiedTrait = traits.completely_specified() ? fsm.isCompletelySpecified() : true;

    return minimalTrait && initially_connectedTrait && completely_specifiedTrait;
}

} // namespace libfsmtest
