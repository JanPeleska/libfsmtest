/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <deque>
#include <map>
#include <set>

#include "SPYHMethod.hpp"
#include "fsm/Dfsm.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"

using namespace std;

namespace libfsmtest {


ConvergenceGraph::ConvergenceGraph(const Dfsm& dfsmRef, const Tree& testSuiteRef)
 : dfsm(dfsmRef), testSuite(testSuiteRef), root(dfsmRef.getInitialStateId(), testSuiteRef.getRoot(), dfsmRef.getInputAlphabetSize())
{
    // add the current traces in the test suite (avoiding computation of intermediate containers)
    for (auto& n : testSuiteRef.getLeaves())
	{
		add(n->getPath());
	}
}

ConvergenceNode &ConvergenceGraph::after(const Trace& trace) {
    ConvergenceNode *node = &root;
    for (Index x : trace) {
        node = node->nextForInput[x];
    }
    return *node;
}

void ConvergenceGraph::replaceAfter(const Trace& trace, Index input, ConvergenceNode &newNode) {
    after(trace).nextForInput[input] = &newNode;
}


void ConvergenceGraph::add(const Trace& trace) 
{
    ConvergenceNode *node = &root;
    Index state = dfsm.getInitialStateId();
    // treeNode initialised as a non-null pointer to the root of the test suite
    TreeNode* treeNode = testSuite.getRoot();
    for (Index x : trace) {

        // the dfsm is assumed to be complete and any trace
        // is assumed to contain only inputs accepted by the dfsm
        state = dfsm.getDfsmTable()[state][x][0].value();

        // as is also assumed that trace is contained in the test suite,
        // treeNode remains not null
        treeNode = treeNode->after(x);

        if (!node->nextForInput[x]) {
            // newly added ConvergentNode has only {treeNode} as its convergentNodes,
            // that is, a nonempty set containing no nullpointers
            node->nextForInput[x] = &nodePool.emplace_back(state,treeNode,dfsm.getInputAlphabetSize());
        } else {
            // if a convergent node already exists, then the current prefix of
            // the trace converges with that node and should be added 
            node->nextForInput[x]->convergentNodes.insert(treeNode);
        }

        node = node->nextForInput[x];
    }
}


void ConvergenceGraph::mergeInto(ConvergenceNode &node1, ConvergenceNode &node2) {
    // if the nodes already coincide, no merge is necessary
    if (&node1 == &node2) {
        return;
    }
    
    for (auto & conv : node1.convergentNodes) {
        // convergentNodes are merged, retaining the invariant, as the convergentNodes
        // of both node1 and node2 are not empty and contain no nullpointers
        node2.convergentNodes.insert(conv);
    }
    
    for (Index x = 0; x < dfsm.getInputAlphabetSize(); ++x) {
        if (! node1.nextForInput[x]) {
            // nothing to merge into right node here
            continue;            
        }
        
        if (! node2.nextForInput[x]) {
            // "replace" empty content of right node with content of left node
            node2.nextForInput[x] = node1.nextForInput[x];
        } else {
            // both sides are not empty -> perform recursive merge
            mergeInto(*node1.nextForInput[x], *node2.nextForInput[x]);    
        }        
    }
}


void ConvergenceGraph::merge(const Trace& trace1, Index input, const Trace& trace2) 
{
    ConvergenceNode &node  = after(trace1);
    ConvergenceNode &nodeL = *node.nextForInput[input];
    ConvergenceNode &nodeR = after(trace2);

    
    node.nextForInput[input] = &nodeR;    
    mergeInto(nodeL,nodeR);
}


std::unordered_set<TreeNode*> ConvergenceGraph::getConvergentTraces(const Trace& trace) 
{
    return after(trace).convergentNodes;
}

bool ConvergenceGraph::hasLeaf(const Trace& trace) 
{
    for (auto & treeNode : getConvergentTraces(trace)) {
        if (treeNode->isLeaf())
            return true;
    }
    return false;
}





std::vector<Trace> SPYHMethod::getDistinguishingTracesOfDepth(Dfsm& fsm, Index qi, Index qj, Index d, Trace& prefix, std::vector<std::map<Index,size_t>>& state2classMaps) {
    std::vector<Trace> v;
    
    if (d == 0) {
        for ( Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
            Index yi = fsm.getDfsmTable()[qi][x][1].value();
            Index yj = fsm.getDfsmTable()[qj][x][1].value();
            
            if ( yi != yj ) {
                Trace tr(prefix);
                tr.append(x);
                v.push_back(tr);
            }
        }

        return v;
    }

    
    
    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
        
        Index qix = fsm.getDfsmTable()[qi][x][0].value();
        Index qjx = fsm.getDfsmTable()[qj][x][0].value();

        // check if the classes of qi after x and qj after x in table (d) are distinct
        if ( state2classMaps.at(d)[qix] != state2classMaps.at(d)[qjx] ) {
            Trace newPrefix(prefix);
            newPrefix.append(x);
            std::vector<Trace> w = getDistinguishingTracesOfDepth(fsm,qix,qjx,d-1,newPrefix,state2classMaps);
            v.insert(v.end(),w.begin(),w.end());
        }
    }

    return v;    
}

std::vector<std::vector<std::vector<Trace>>> SPYHMethod::getMinimalDistinguishingTracesForStatePairs(Dfsm& fsm) {
    size_t numberOfStates = fsm.size();

    std::vector<std::vector<std::vector<Trace>>> result;
    
    map< size_t, set<Index> > classMap;
    vector< map<Index,size_t> > state2classMaps;
    
    // Calculate Pk tables
    fsm.calcOFSMTables(classMap,state2classMaps);
    
    for ( Index qi = 0; qi < numberOfStates; ++qi ) {
        std::vector<std::vector<Trace>> qiResult;

        for ( Index qj = 0; qj < qi; ++qj ) {

            // obtain minimal index such that in OFSM table ell the states reside in distinct classes
            // NOTE: it is assumed here that such a class exists due to minimality of fsm
            size_t ell;
            for ( ell = 1; ell < state2classMaps.size(); ell++ ) {
                if ( state2classMaps.at(ell)[qi] != state2classMaps.at(ell)[qj] ) {
                    break;
                }
            }

            Trace emptyPrefix;
            qiResult.push_back(getDistinguishingTracesOfDepth(fsm,qi,qj,ell-1,emptyPrefix,state2classMaps));
        }

        result.push_back(qiResult);
    }
    return result;
}

std::vector<Trace> SPYHMethod::getMinimalDistinguishingTraces(Index qi, Index qj) {
    if (qi < qj) {
        return minimalDistinguishingTraces[qj][qi];
    } else {
        return minimalDistinguishingTraces[qi][qj];
    }
}


size_t SPYHMethod::estimateGrowthOfTestSuite(Dfsm& fsm, const Index u, const Index v, Index x) {
    auto dfsmTable = fsm.getDfsmTable();

    // if u and v respond differently to x, then return the minimal value 1
    // NOTE: x is assumed to be defined in both states
    if (dfsmTable[u][x][1].value() != dfsmTable[v][x][1].value())
        return 1;
    
    Index ux = dfsmTable[u][x][0].value();
    Index vx = dfsmTable[v][x][0].value();
    
    // if after application of x both states coincide or remain as before 
    // the application or "switch", then a value larger then produced by 
    // any shortest distinguishing trace is returned
    if (ux == vx
        || (ux == u && vx == v)
        || (ux == v && vx == u))
        return 2 * fsm.getStates().size();

    // otherwise compute and use the shortest distinguishing trace of u and v
    auto distTraces = getMinimalDistinguishingTraces(u,v);
    size_t minEst = distTraces.at(0).size();
    for (auto & trace : distTraces) {
        if (trace.size() < minEst) {
            minEst = trace.size();
        }
    }
    return minEst * 2 + 1;
}


std::pair<size_t,std::stack<size_t>> SPYHMethod::getPrefixOfSeparatingTrace(Dfsm& fsm, const Trace& trace1, const Trace& trace2, Tree& testSuite, ConvergenceGraph & graph) {
    
    // get shortest sequences in each class
    Trace u = trace1;
    for (auto & trace : graph.getConvergentTraces(trace1)) {
        if (trace->getPath().size() < u.size()) {
            u = trace->getPath();
        }
    }
    Trace v = trace2;
    for (auto & trace : graph.getConvergentTraces(trace2)) {
        if (trace->getPath().size() < u.size()) {
            v = trace->getPath();
        }
    }

    Index stateU = fsm.getTarget(fsm.getInitialStateId(), u).value();
    Index stateV = fsm.getTarget(fsm.getInitialStateId(), v).value();

    // get shortest distinguishing trace and use double its size as minEst
    auto distTraces = getMinimalDistinguishingTraces(stateU,stateV);

    
    size_t minEst = distTraces.at(0).size();
    for (auto & trace : distTraces) {
        if (trace.size() < minEst) {
            minEst = trace.size();
        }
    }
    minEst = 2 * minEst;

    auto convClassU = graph.getConvergentTraces(u);
    auto convClassV = graph.getConvergentTraces(v);
    auto dfsmTable = fsm.getDfsmTable();

    // start with the empty prefix
    std::stack<size_t> bestPrefix;

    if (!graph.hasLeaf(u)) 
        minEst = minEst + u.size();
    if (!graph.hasLeaf(v)) 
        minEst = minEst + v.size();

    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
        
        bool xDistinguishes = (dfsmTable[stateU][x][1].value() != dfsmTable[stateV][x][1].value());

        // if the states reached by u'x and v'x coincide, and the same output is produced on x after both u' and v', then 
        // x cannot distinguish the states reached by u' and v'
        if ((not xDistinguishes) and (dfsmTable[stateU][x][0].value() == dfsmTable[stateV][x][0].value()))
            continue;      

        // check if there exist u' in [u] and trace w such that u'xw are contained in the test suite
        bool foundExtensionForU = false;    
        Trace convUPrime;    
        for (auto & convU : convClassU) {

            // u'xw is contained in the test suite if and only if x is defined in the testsuite after u'
            if (convU->after(x) != nullptr) {
                convUPrime = convU->getPath();
                foundExtensionForU = true;   
                break;
            }
        }

        // check if there exist v' in [v] and trace w' such that v'xw' are contained in the test suite
        bool foundExtensionForV = false;    
        Trace convVPrime;    
        for (auto & convV : convClassV) {

            // v'xw is contained in the test suite if and only if x is defined in the testsuite after v'
            if (convV->after(x) != nullptr) {
                convVPrime = convV->getPath();
                foundExtensionForV = true;   
                break;
            }
        }
        

        if (foundExtensionForU && foundExtensionForV) {

            // if x produces different outputs to the states reached by u' and v', then the
            // test suite already distinguishes u and v
            if (xDistinguishes)
                return std::make_pair<size_t,std::stack<size_t>>(0,{});

            convUPrime.append(x);
            convVPrime.append(x);

            // recursive call on u'x and v'x
            auto targetPair = getPrefixOfSeparatingTrace(fsm, convUPrime, convVPrime, testSuite, graph);

            // if u'x and v'x are already distinguished, then so are u' and v'
            if (targetPair.first == 0) {
                return std::make_pair<size_t,std::stack<size_t>>(0,{});
            }

            // if the target pair is better than the current best, then use the target pair
            if (targetPair.first <= minEst) {
                minEst = targetPair.first;
                bestPrefix = targetPair.second;
                // input x must be added to the front of the best result for the target pair
                bestPrefix.push(x);
            }   

        } else if (foundExtensionForU) {

            size_t e = estimateGrowthOfTestSuite(fsm,stateU,stateV,x);   
            if (e != 1) {
                if (graph.hasLeaf(u)) {
                    e = e+1; 
                } else {
                    Trace nextU(u);
                    nextU.append(x);
                    if (!graph.hasLeaf(nextU)) {
                        e = e + 1 + u.size();
                    }                        
                }                    
            }
            if (!graph.hasLeaf(v)) {
                e = e + v.size();
            }                    
            if (e < minEst) {
                minEst = e;
                bestPrefix = std::stack<size_t>();
                bestPrefix.push(x);
            }   

        } else if (foundExtensionForV) {

            size_t e = estimateGrowthOfTestSuite(fsm,stateU,stateV,x);   
            if (e != 1) {
                if (graph.hasLeaf(v)) {
                    e = e+1; 
                } else {
                    Trace nextV(v);
                    nextV.append(x);
                    if (!graph.hasLeaf(nextV)) {
                        e = e + 1 + v.size();
                    }                        
                }                    
            }
            if (!graph.hasLeaf(u)) {
                e = e + u.size();
            }                    
            if (e < minEst) {
                minEst = e;
                bestPrefix = std::stack<size_t>();
                bestPrefix.push(x);
            }  
        }
    }
    return std::make_pair(minEst,bestPrefix); 
}





void SPYHMethod::spyhDistinguish(Dfsm& fsm, const Trace& trace, Traces const &traces, Tree& testSuite, ConvergenceGraph & graph) {
    Index u = fsm.getTarget(fsm.getInitialStateId(), trace).value();
    
    for (auto & otherTrace : traces) {
        Index v = fsm.getTarget(fsm.getInitialStateId(), otherTrace).value();
        
        // trace needs to be distinguished only from traces reaching other states
        if (u == v) continue;

        auto distPair = getPrefixOfSeparatingTrace(fsm,trace,otherTrace,testSuite,graph);
        Trace w;
        while (!distPair.second.empty()) {
            size_t x = distPair.second.top();
            distPair.second.pop();
            w.append(x);
        }

        // the test suite needs to be extended only if it does not already r-dist 
        if (distPair.first > 0) {
            
            Trace uw = trace;
            uw.append(w);
            //uw.insert(uw.end(), w.begin(), w.end());
            Trace vw = otherTrace;
            vw.append(w);
            //vw.insert(vw.end(), w.begin(), w.end());

            Index uwState = fsm.getTarget(fsm.getInitialStateId(),uw).value();
            Index vwState = fsm.getTarget(fsm.getInitialStateId(),vw).value();

            auto traceToAppend = w;
            if (uwState == vwState) {
                // Note: this case has not been described in the SPYH method,
                // but it may occur e.g. if getPrefixOfSeparatingTrace  
                // produces pair (1,x) and input x distinguishes the states
                // reached u and v, while ux and vx converge.
                // In this case, no distinguishing trace needs to be applied
                // after ux and vx.

            } else {
                // always chooses the first shortest distinguishing trace
                auto distTrace = getMinimalDistinguishingTraces(uwState,vwState).at(0);  
                traceToAppend.append(distTrace);     
            }

            // append separating sequences to 
            appendSeparatingSequence(trace,traceToAppend,testSuite,graph);
            appendSeparatingSequence(otherTrace,traceToAppend,testSuite,graph);
        }
    }    
}

void SPYHMethod::appendSeparatingSequence(const Trace& traceToAppendTo, const Trace& traceToAppend, Tree& testSuite, ConvergenceGraph & graph) {
    // get shortest u' in [traceToAppendTo]
    Trace uBest = traceToAppendTo;
    for (auto & trace : graph.getConvergentTraces(traceToAppendTo)) {
        if (trace->getPath().size() < uBest.size()) {
            uBest = trace->getPath();
        }
    }

    // note: this is initialised as -1 in the original SPYH method;
    //       below we compare idx and maxLength using >= instead of >,
    //       resulting in the same effect but enabling the use of unsigned 
    //       types for maxLength
    unsigned int maxLength = 0;
    for (auto & u : graph.getConvergentTraces(traceToAppendTo)) {
        // get length of the longest prefix w of traceToAppend such that traceToAppendTo.w is in the test suite
        unsigned int idx = 0;
        TreeNode* node = u;
        for (auto &x : traceToAppend) {
            if (node->after(x) == nullptr) break;
            node = node->after(x);
            ++idx;
        }

        // if traceToAppend has already been fully applied, nothing further needs to be done
        if (idx == traceToAppend.size())
            return;

        // update the best length only if the current length is better and requires no branching
        if (idx >= maxLength && node->isLeaf()) {
            uBest = u->getPath();
            maxLength = idx;
        }
    }    

    // append traceToAppend after uBest and insert the trace into testsuite and graph
    uBest.append(traceToAppend);
    testSuite.add(uBest);
    graph.add(uBest);
}

void SPYHMethod::distinguishFromSet(Dfsm& fsm, const Trace& u, const Trace& v, Traces const &stateCover, Traces &tracesToDistFrom, Tree& testSuite, ConvergenceGraph & graph, size_t depth) {
    // dist u 
    spyhDistinguish(fsm,u,tracesToDistFrom,testSuite,graph);

    // check whether [v] contains no traces from the state cover
    bool notReferenced = true;
    auto tracesConvergentToV = graph.getConvergentTraces(v);
    for (auto & coverTrace : stateCover) {
        for (auto & traceConvergentToV : tracesConvergentToV) {
            if (traceConvergentToV->getPath() == coverTrace) {
                notReferenced = false;
                break;
            }
        }
        if (!notReferenced) 
            break;
    }

    // if v has not been referenced already, distinguish it too
    if (notReferenced) {
        spyhDistinguish(fsm,v,tracesToDistFrom,testSuite,graph);
    }

    if (depth > 0) {
        
        tracesToDistFrom.push_back(u);
        if(notReferenced) {
            tracesToDistFrom.push_back(v);
        }

        for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
            Trace xTrace;
            xTrace.append(x);
            appendSeparatingSequence(u,xTrace,testSuite,graph);
            appendSeparatingSequence(v,xTrace,testSuite,graph);
            Trace ux = u;
            ux.append(x);
            Trace vx = v;
            vx.append(x);
            distinguishFromSet(fsm,ux,vx,stateCover,tracesToDistFrom,testSuite,graph,depth-1);
        }


        if (notReferenced) {
            tracesToDistFrom.pop_back();
        }
        tracesToDistFrom.pop_back();
    }
}

void SPYHMethod::generateTestSuite(Dfsm& fsm) {
    
    // pre-compute distinguishing traces
    minimalDistinguishingTraces = getMinimalDistinguishingTracesForStatePairs(fsm); 
    
    // collect all transitions to be verified
    std::set<Transition> transitions;
    for (auto & node : fsm.getStates()) {
        for (auto & t : node.getTransitions()) {
            transitions.insert(t);
        }
    }

    // Test suite is initialised with the state cover
    createStateCover(fsm);
    auto stateCover = testCaseTree->getAllTestCases();
    std::vector<std::decay_t<decltype(stateCover)>::iterator> stateCoverAssignment(fsm.getStates().size());
    for (auto iter = stateCover.begin(); iter != stateCover.end(); ++iter) {
        stateCoverAssignment.at(fsm.getTarget(fsm.getInitialStateId(),*iter).value()) = iter;
    }
    

    ConvergenceGraph graph(fsm, *testCaseTree);
    

    // distinguish traces in state cover and note already verified nodes
    for ( auto & trace : stateCover ) {
        spyhDistinguish(fsm,trace,stateCover,*testCaseTree,graph);
    }

    // filter all already verified transitions - i.e. all transitions
    // (s,x,y,s') such that the state cover reaches s by some u and
    // s' by u.x
    // As the state cover generated by getStateCover() is minimal,
    // this holds for all transitions along prefixes of traces in the
    // state cover.
    for (auto const &trace : stateCover) {
        Index state = fsm.getInitialStateId();
        for (auto x : trace) {
            for (auto & t : fsm.getStates().at(state).getTransitions()) {
                if (t.getInput() == x) {
                    transitions.erase(t);

                    state = t.getTargetId();
                    break;
                }
            }
        }
    }

    // sort remaining transitions
    std::vector<Transition> sortedTransitions;
    std::vector<Transition> sortedTransitionsTemp;
    std::vector<size_t> sortedTansitionIndices;
    std::vector<size_t> transitionWeights;
    size_t tIdx=0;
    for (auto& t : transitions) {
        sortedTransitionsTemp.push_back(t);
        sortedTansitionIndices.push_back(tIdx);
        ++tIdx;
        transitionWeights.push_back(stateCoverAssignment[t.getSourceId()]->size() + stateCoverAssignment[t.getTargetId()]->size());
    }
    std::sort(sortedTansitionIndices.begin(), sortedTansitionIndices.end(), [&transitionWeights](const size_t& t1, const size_t& t2) {
        return transitionWeights[t1] < transitionWeights[t2];
    });
    sortedTransitions.reserve(sortedTansitionIndices.size());
    for (auto& sortedIdx : sortedTansitionIndices) {
        sortedTransitions.push_back(sortedTransitionsTemp[sortedIdx]);
    }


    // verify all remaining transitions
    for (auto const & t : sortedTransitions) {                     

        Index source = t.getSourceId();
        Index target = t.getTargetId();
        Index x = t.getInput();


        auto &sourceTrace = *stateCoverAssignment[source];
        auto &targetTrace = *stateCoverAssignment[target];

        Trace ux(sourceTrace);
        ux.append(x);
        testCaseTree->add(ux);
        graph.add(ux);

        auto v = targetTrace;

        // create a copy of the state cover 
        decltype(stateCover) stateCoverCopy(stateCover);

        distinguishFromSet(fsm,ux,v,stateCover,stateCoverCopy,*testCaseTree,graph,numAddStates);

        graph.merge(sourceTrace,x,v);
    }
}




void SPYHMethod::visit(Fsm &/*fsm*/) {
    
    throw runtime_error("The SPYH method can only be applied to complete DFSMs");
}

void SPYHMethod::visit(Ofsm &/*fsm*/) {
    
    throw runtime_error("The SPYH method can only be applied to complete DFSMs");
}

void SPYHMethod::visit(Dfsm &fsm) {

    if ( !fsm.isCompletelySpecified() ) { 
        throw runtime_error("The SPYH method can only be applied to complete DFSMs");        
    }
    
    if ( fsm.isMinimal() ) {
        
        generateTestSuite(fsm);
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime DFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
                
        // Now visit the prime DFSM
        minimisedDfsm->accept(*this);
        
    }
    
}

} // namespace libfsmtest
