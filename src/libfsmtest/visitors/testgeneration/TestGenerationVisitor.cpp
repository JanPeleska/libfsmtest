/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <deque>
#include "TestGenerationVisitor.hpp"

using namespace std;

namespace libfsmtest {

TestGenerationVisitor::TestGenerationVisitor() :
testsuite(nullptr),
testCaseTree(nullptr)
{
    
}

void TestGenerationVisitor::testCaseTreeToTestSuite() {
    for ( const auto& tr : testCaseTree->getTestCases() ) {
        testsuite->addInputTrace(tr);
    }
}

bool TestGenerationVisitor::distinguishes(Ofsm& fsm, const Trace& trace, Index qi, Index qj) {
    Strings ioTraces_qi;
    Strings ioTraces_qj;
    
    fsm.getIOTracesFromState(trace,ioTraces_qi,qi);
    fsm.getIOTracesFromState(trace,ioTraces_qj,qj);
    
    // For OFSMs, we can get several entries in ioTraces_qi and ioTraces_qi.
    // If ioTraces_qi != ioTraces_qj, setOfTraces distinguishes both states
    if ( ioTraces_qi.size() != ioTraces_qj.size() ) return true;
    
    // Size is the same, so check that every trace of ioTraces_qi
    // is also contained in ioTraces_qj. If this is the case, trace
    // does NOT distinguish qi and qj
    for ( const auto& tqi : ioTraces_qi ) {
        bool found = false;
        for ( const auto& tqj : ioTraces_qj ) {
            if ( tqi == tqj ) {
                found = true;
                break;
            }
        }
        if ( ! found ) return true;
    }
    
    return false;
}

void TestGenerationVisitor::createStateCover(Fsm &fsm) {
    
    States states = fsm.getStates();
    Index initialStateId = fsm.getInitialStateId();
    map<Index,TreeNode*> state2node;
    
    // Create testCaseTree with root referencing to initial state of fsm
    testCaseTree = make_unique<Tree>(make_unique<TreeNode>(nullptr,initialStateId));
    state2node[initialStateId] = testCaseTree->getRoot();
    
    deque<Index> bfsQueue;
    bfsQueue.push_back(initialStateId);
    
    while ( ! bfsQueue.empty() ) {
        
        Index thisStateId = bfsQueue.front();
        bfsQueue.pop_front();
        TreeNode* thisNode = state2node[thisStateId];

        State& thisState = states.at(thisStateId);
        
        for ( const auto& tr : thisState.getTransitions() ) {
            Index x = tr.getInput();
            Index targetStateId = tr.getTargetId();
            
            // Create a new tree node if the target state has not been reached yet
            if ( state2node.find(targetStateId) == state2node.end() )  {
                TreeNode* newNode = thisNode->add(x,targetStateId);
                state2node[targetStateId] = newNode;
                bfsQueue.push_back(targetStateId);
            }
        }
    }
}

bool TestGenerationVisitor::distinguishes(Dfsm& fsm, Traces& setOfTraces, Index qi, Index qj) {
    
    Strings ioTraces_qi;
    Strings ioTraces_qj;
    
    for ( const auto& tr : setOfTraces ) {
        fsm.getIOTracesFromState(tr,ioTraces_qi,qi);
        fsm.getIOTracesFromState(tr,ioTraces_qj,qj);
        
        // For DFSMs, we only get one entry each in ioTraces_qi and ioTraces_qi
        if ( ioTraces_qi.back() != ioTraces_qj.back() ) {
            // tr distinguishes qi, qj
            return true;
        }
        
        // Not distinguished - clear containers and try next trace
        ioTraces_qi.clear();
        ioTraces_qj.clear();
    }
    
    return false;
}

bool TestGenerationVisitor::distinguishes(Ofsm& fsm, Traces& setOfTraces, Index qi, Index qj) {
        
    for ( const auto& tr : setOfTraces ) {
        if (distinguishes(fsm,tr,qi,qj)) return true;
    }
    
    return false;
}



bool TestGenerationVisitor::haveNextTrace(size_t alphaSize, Trace& tr) {
    
    Index lastInputId = alphaSize - 1;
    auto ite = tr.begin();
    
    for ( ; ite != tr.end(); ite++ ) {
        if ( *ite < lastInputId ) {
            *ite = *ite + 1;
            break;
        }
    }
    // If all tr-values are == lastInputId return
    if ( ite == tr.end() ) return false;
    
    // Reset all tr-values before ite to 0, since they were == lastInputId before
    while ( ite != tr.begin() ) {
        --ite;
        *ite = 0;
    }
    return true;
}

void TestGenerationVisitor::enumerateInputAlphabetToLength(size_t minLength,
                                                           size_t maxLength,
                                                           size_t alphaInSize,
                                                           Traces& generatedInputTraces) {
        
    for ( size_t idx = minLength; idx <= maxLength; idx++ ) {
        // Create all input traces of length idx and add them to generatedInputTraces
        
        // Initially, we take input trace of length idx which only contains zeroes.
        Trace tr;
        for ( size_t i = 0; i < idx; i++ ) {
            tr.append(0);
        }
        generatedInputTraces.push_back(tr);
        
        // Now create ALL traces of length idx
        while ( haveNextTrace(alphaInSize,tr) ) {
            generatedInputTraces.push_back(tr);
        }
        
    }
}



}
