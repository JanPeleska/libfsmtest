
# change into directory where build script resides in
# (the build script might have been called from another location)
cd $(dirname "$0")
pathToScript=$PWD

for build in "Release" "Debug"; do
        mkdir -p "build.$build";
        cd "build.$build";
        cmake -DCMAKE_BUILD_TYPE=$build "${pathToScript}/src";
        cd ..;
done
 
