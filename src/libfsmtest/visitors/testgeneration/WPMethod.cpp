/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <deque>
#include <map>
#include <functional>

#include "WPMethod.hpp"
#include "fsm/Dfsm.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"

using namespace std;

namespace libfsmtest {

void WPMethod::selectStateIdentificationSetsFromCharacterisationSet(Ofsm& fsm, const Traces& characterisationSet, std::unordered_map<Index,Traces>& stateIdentificationSets) {
    
    stateIdentificationSets.clear();
    for (Index q = 0; q < fsm.size(); ++q) {
        stateIdentificationSets[q] = {};
    }
    
    // Initialise 2x2 matrix indexed over states with false
    bool** distinguished = new bool*[fsm.size()];
    for(Index i = 0; i < fsm.size(); ++i)
        distinguished[i] = new bool[fsm.size()]();
    
    for (Index qi = 0; qi < fsm.size(); ++qi) {
        
        for (auto& trace : characterisationSet) {
            
            bool distinguishedFromAll = true;
            for (Index qj = qi+1; qj < fsm.size(); ++qj) {
                
                // if qi and qj are already distinguished, then no addition is necessary
                if (distinguished[qi][qj]) continue;
                
                if (distinguishes(fsm,trace,qi,qj)) {
                    distinguished[qi][qj] = true;
                    distinguished[qj][qi] = true;
                    stateIdentificationSets[qi].push_back(trace);
                    stateIdentificationSets[qj].push_back(trace);                    
                } else {
                    // qi is not yet distinguished from qj by any trace
                    // considered so far; thus more traces have to be
                    // considered
                    distinguishedFromAll = false;
                }
            }
            
            // if qi has been distinguished from all qj by the traces
            // considered so far, no further traces need to be considered
            if (distinguishedFromAll) break;
        }
    }

    for(Index i = 0; i < fsm.size(); ++i)
        delete[] distinguished[i];
    delete[] distinguished;
}


void WPMethod::visit(Fsm &fsm) {
    
    // First, a transformation to prime machine is needed in any case
    unique_ptr<Fsm> primeFsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
    
    // @debug output of prime machine
    ToDotFileVisitor dot("prime-of-nonObservable.dot");
    primeFsm->accept(dot);
    dot.writeToFile();
    
    // Now visit the prime machine
    primeFsm->accept(*this);
    
    
}

void WPMethod::visit(Ofsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        createStateCover(fsm);
        
        testCaseTree->toDot("StateCover-nonobservable.dot");
        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates+1,
                                       testsuite->getFsm().getInputAlphabetSize(),
                                       enumeratedTraces);
        
        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);
        
        // Compute charactersation set
        Traces characterisationSet;
        createCharacterisationSet(fsm,characterisationSet);
        
        // Compute state identification sets
        std::unordered_map<Index,Traces> stateIdentificationSets;
        selectStateIdentificationSetsFromCharacterisationSet(fsm,characterisationSet,stateIdentificationSets);
        
        // Let X be the input alphabet of the Fsm, V its state cover, n its size and numAddStates = m.
        // Furthermore let W the characterisation set and W'_q the state identification set of state q.
        // Then the WP test suite consists of two components:
        //   WP1 = V . X^(<=m-n) . W
        //   WP2 = x.W'_q for each x in V . X^(m-n+1) and each q reached by x
        // The current test suite tree corresponds to
        //   V . X^(<=m-n+1)
        // Thus, WP1 is created by appending W after every inner node of the test suite tree,
        // and WP2 is created by appending the required W'_q after every leaf of the tree. 
        
        std::function<void(TreeNode*)> addComponent = [&fsm,&stateIdentificationSets,&characterisationSet](TreeNode* node)->void {
            if (node->isLeaf()) {
                auto trace = node->getPath();
                for (Index q : fsm.getTargets(trace)) {
                    // append W'_q
                    for (auto& traceInIdentificationSet : stateIdentificationSets[q]) {
                        node->add(traceInIdentificationSet);
                    }
                }
            } else {
                // append W
                for (auto& traceInCharacterisationSet : characterisationSet) {
                    node->add(traceInCharacterisationSet);
                }
            }
        };
        testCaseTree->applyToAllNodes(addComponent);        
        
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime OFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
        
        // Debug output
        ToDotFileVisitor dot("Minimised.dot");
        minimisedOfsm->accept(dot);
        dot.writeToFile();
        
        
        // Now visit the prime OFSM
        minimisedOfsm->accept(*this);
        
    }
    
    
}

void WPMethod::visit(Dfsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        createStateCover(fsm);
        
        testCaseTree->toDot("StateCover-nonobservable.dot");
        
        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates+1,
                                       testsuite->getFsm().getInputAlphabetSize(),
                                       enumeratedTraces);
        
        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);
        
        // Compute charactersation set
        Traces characterisationSet;
        createCharacterisationSet(fsm,characterisationSet);
        
        // Compute state identification sets
        std::unordered_map<Index,Traces> stateIdentificationSets;
        selectStateIdentificationSetsFromCharacterisationSet(fsm,characterisationSet,stateIdentificationSets);
        
        // Construction analogous to Ofsm case
        std::function<void(TreeNode*)> addComponent = [&fsm,&stateIdentificationSets,&characterisationSet](TreeNode* node)->void {
            if (node->isLeaf()) {
                auto trace = node->getPath();
                for (Index q : fsm.getTargets(trace)) {
                    // append W'_q
                    for (auto& traceInStateIdentificationSet : stateIdentificationSets[q]) {
                        node->add(traceInStateIdentificationSet);
                    }
                }
            } else {
                // append W
                for (auto& traceInCharacterisationSet : characterisationSet) {
                    node->add(traceInCharacterisationSet);
                }
            }
        };
        testCaseTree->applyToAllNodes(addComponent);        
        
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime DFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
        
        // Debug output
        ToDotFileVisitor dot("Minimised.dot");
        minimisedDfsm->accept(dot);
        dot.writeToFile();
        
        
        // Now visit the prime DFSM
        minimisedDfsm->accept(*this);
        
    }
    
}

} // namespace libfsmtest
