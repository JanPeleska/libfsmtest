/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once
#include "utils/compatibility.hpp"

#include <list>
#include <string>
#include <vector>

#include "fsm/definitions.hpp"

namespace libfsmtest {

/**
 * Class representing a sequence from an input or output alphabet.
 */
class Trace {
private:
    /** The list of indices for an input or output alphabet */
    std::list<Index> trace;

public:
    using iterator = std::list<Index>::iterator;
    using const_iterator = std::list<Index>::const_iterator;
    using reverse_iterator = std::list<Index>::reverse_iterator;
    using const_reverse_iterator = std::list<Index>::const_reverse_iterator;

    /**
     * Default constructor
     */
    Trace() = default;

    /**
     * Range constructor
     */
    Trace(Trace::iterator const &begin, Trace::iterator const &end);
    Trace(Trace::const_iterator const &begin, Trace::const_iterator const &end);

    template<typename ForwardIter1, typename ForwardIter2>
    Trace(ForwardIter1 begin, ForwardIter2 end)
      : trace(begin, end) { }


    /**
    * Constructor via initializer_list
    * @param l Initializer list to create the list from.
    */
    Trace(std::initializer_list<Index> l) : trace(l) {}

    /**
     * Append an index to this trace.
     * @param e The index to append.
     */
    void append(const Index e);
    /**
     * Append another list to this trace.
     * @param other The list to append.
     */
    void append(const std::list<Index>& other);
    /**
     * Append an initializer list to this trace.
     * @param l The initializer list to append.
     */
    void append(std::initializer_list<Index> l);
    /**
     * Append another trace to this trace.
     * @param other The trace to append.
     */
    void append(const Trace& other);

    /**
     * Prepend an index to this trace.
     * @param e The index to prepend.
     */
    void prepend(Index e);
    /**
     * Prepend another list to this trace.
     * @param other The list to prepend.
     */
    void prepend(const std::list<Index>& other);
    /**
     * Prepend an initializer list to this trace.
     * @param l The initializer list to prepend.
     */
    void prepend(std::initializer_list<Index> l);
    /**
     * Prepend another trace to this trace.
     * @param other The trace to prepend.
     */
    void prepend(const Trace& other);
    
    /** Delete last element */
    void pop_back() { trace.pop_back(); }
    
    /** Delete first element */
    void pop_front() { trace.pop_front(); }

    /** Peek first element */
    Index front() const { return trace.front(); }
    /** Peek last element */
    Index back() const { return trace.back(); }

    /**
     * Check whether the given trace is a prefix of this trace.
     * @param other The given trace.
     * @return true, if the given trace is a prefix of this,
     *         false, otherwise.
     */
    bool hasPrefix(const Trace& other) const;

    bool isPrefixOf(const Trace& other) const;

    /**
     * Check whether this trace is empty (has length 0)
     * @return true, when there are no indices in this trace,
     *         false, otherwise.
     */
    bool isEmpty() const {
        return trace.empty();
    }
    /**
     * Returns a read/write iterator to the first element of this trace.
     * @see std::list::begin()
     * @return A read/write iterator
     */
    iterator begin() {return trace.begin();}
    /**
     * Returns a read/write iterator one past the last element of this trace.
     * @see std::list::end()
     * @return A read/write iterator
     */
    iterator end() {return trace.end();}
    /**
     * Returns a read-only iterator to the first element of this trace.
     * @see std::list::cbegin()
     * @return A read-only iterator
     */
    const_iterator begin() const {return trace.begin();}
    /**
     * Returns a read-only iterator one past the last element of this trace.
     * @see std::list::cend()
     * @return A read-only iterator
     */
    const_iterator end() const {return trace.end();}
    /**
     * Returns a read-only iterator to the first element of this trace.
     * @see std::list::cbegin()
     * @return A read-only iterator
     */
    const_iterator cbegin() const {return trace.cbegin();}
    /**
     * Returns a read-only iterator one past the last element of this trace.
     * @see std::list::cend()
     * @return A read-only iterator
     */
    const_iterator cend() const {return trace.cend();}

    /**
     * Returns a read/write iterator to the first element of the reversed trace.
     * @see std::list::rbegin()
     * @return A read/write iterator
     */
    reverse_iterator rbegin() {return trace.rbegin();}
    /**
     * Returns a read/write iterator one past the last element of the reversed trace.
     * @see std::list::rend()
     * @return A read/write iterator
     */
    reverse_iterator rend() {return trace.rend();}
    /**
     * Returns a read-only iterator to the first element of the reversed trace.
     * @see std::list::cbegin()
     * @return A read-only iterator
     */
    const_reverse_iterator rbegin() const {return trace.rbegin();}
    /**
     * Returns a read-only iterator one past the last element of the reversed trace.
     * @see std::list::rcend()
     * @return A read-only iterator
     */
    const_reverse_iterator rend() const {return trace.rend();}
    /**
     * Returns a read-only iterator to the first element of the reversed trace.
     * @see std::list::crbegin()
     * @return A read-only iterator
     */
    const_reverse_iterator crbegin() const {return trace.crbegin();}
    /**
     * Returns a read-only iterator one past the last element of the reversed trace.
     * @see std::list::crend()
     * @return A read-only iterator
     */
    const_reverse_iterator crend() const {return trace.crend();}

    /** 
     * Returns the size (length) of the trace.
     * @return The size of the trace.
     */
    size_t size() const {return trace.size();}

    /**
     * Compute all nonempty prefixes of this trace.
     * @param proper If true, then the trace itself is not contained in the result.
     * @return all nonempty prefixes of this trace.
     */
    std::vector<Trace> getNonemptyPrefixes(bool proper = false) const;

    /**
     * Translates the trace from internal to some external representation
     * according to a given alphabet.
     * @param alphabet The alphabet to use.
     * @return A vector of strings that has the same length as this trace but
     *  where for every index `n` the symbol in the result is the symbol from
     *  `alphabet` when indexed by the element at index `n`.
     */
    Strings translate(Strings const &alphabet) const;

    /**
	 * Check whether or not, the two traces are the same sequence of inputs
	 * @param trace1 The first trace
	 * @param trace2 The second trace
	 * @return True if they are the same, false otherwise
	 */
	friend bool operator==(Trace const & trace1, Trace const & trace2);

    friend bool operator<(Trace const &trace1, Trace const &trace2);
};

/**
 * Shorthand definition for better readability.
 */
using Traces = std::vector<Trace>;

} // namespace libfsmtest

