/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>

#include "ToFsmFileVisitor.hpp"
#include "fsm/Dfsm.hpp"


namespace libfsmtest {

ToFsmFileVisitor::ToFsmFileVisitor(const std::string& prefix) {
    this->fileName = prefix + ".fsm";
    inFileName = prefix + ".in";
    outFileName = prefix + ".out";
    statesFileName = prefix + ".state";
}

void ToFsmFileVisitor::visit(Fsm &fsm) {
    gatherData(fsm);
}

void ToFsmFileVisitor::visit(Ofsm &ofsm) {
    gatherData(ofsm);
}

void ToFsmFileVisitor::visit(Dfsm &dfsm) {
    gatherData(dfsm);
}

void ToFsmFileVisitor::gatherData(Fsm &fsm) {
    initialState = fsm.getInitialStateId();
    presentationLayer = std::make_unique<PresentationLayer>(fsm.getPresentationLayer());

    for_each(fsm.getStates().begin(),
             fsm.getStates().end(),
             [this](auto& state) { state.accept(*this); } );
}

void ToFsmFileVisitor::visit(State &state) {
    for_each(state.getTransitions().begin(),
             state.getTransitions().end(),
             [this](auto& tr) { tr.accept(*this); } );
}

void ToFsmFileVisitor::visit(Transition &transition) {
    CsvColumn& preState = fsmData.at(0);
    CsvColumn& input = fsmData.at(1);
    CsvColumn& output = fsmData.at(2);
    CsvColumn& postState = fsmData.at(3);

    if (transition.getSourceId() == initialState) {
        /**
         * This is a transition of the initial state,
         * so it gets inserted at the front with an offset
         * to account for already added transitions from the initial state
         */
        preState.emplace(preState.begin() + offsetInitialStateTransitions, std::to_string(transition.getSourceId()));
        input.emplace(input.begin() + offsetInitialStateTransitions, std::to_string(transition.getInput()));
        output.emplace(output.begin() + offsetInitialStateTransitions, std::to_string(transition.getOutput()));
        postState.emplace(postState.begin() + offsetInitialStateTransitions, std::to_string(transition.getTargetId()));

        ++offsetInitialStateTransitions;
    } else {
        preState.emplace_back(std::to_string(transition.getSourceId()));
        input.emplace_back(std::to_string(transition.getInput()));
        output.emplace_back(std::to_string(transition.getOutput()));
        postState.emplace_back(std::to_string(transition.getTargetId()));
    }
}

void ToFsmFileVisitor::writeToFile() {
    std::ofstream fsmFile(fileName);
    if (!fsmFile.is_open()) throw std::runtime_error("Failed to open output file");
    writeCsv(fsmFile, fsmData, ' ');

    std::ofstream inFile(inFileName);
    if (!inFile.is_open()) throw std::runtime_error("Failed to open output file");
    for (const auto& input : presentationLayer->inputAlphabet) {
        inFile << input << std::endl;
    }

    std::ofstream outFile(outFileName);
    if (!outFile.is_open()) throw std::runtime_error("Failed to open output file");
    for (const auto& output : presentationLayer->outputAlphabet) {
        outFile << output << std::endl;
    }

    std::ofstream stateFile(statesFileName);
    if (!stateFile.is_open()) throw std::runtime_error("Failed to open output file");
    for (const auto& state : presentationLayer->stateNames) {
        stateFile << state << std::endl;
    }
}

} // namespace libfsmtest
