/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <cstdlib>
#include <memory>
#include <optional>

#include "fsm/Ofsm.hpp"

namespace libfsmtest {

class Dfsm : public Ofsm {
    
protected:
    
    /**
     * dfsmTable is created by the constructor. It can be used like an array
     * dfsmTable[s][x][0] = s', dfsmTable[s][x][1] = y,
     * where s' is the poststate reached from state s under input x. If the
     * Dfsm is partial, and s does not have an emanating transition for input x,
     * dfsmTable[s][x][0] = std::nullopt and dfsmTable[s][x][1] is undefined.
     * If the post-state s' exists, dfsmTable[s][x][1] = y is the output
     * associated with transition  s --- x/y ---> s'.
     */
    std::optional<Index>*** dfsmTable;
    
      
public:
            
    Dfsm(const std::string& fsmName,
         States stateVec,
         PresentationLayer pl,
         Index initialState);
    
    /** copy constructor */
    Dfsm(Dfsm const &dfsm);
    
    ~Dfsm() override;
    
    void accept(FsmVisitor& v) override { v.visit(*this); }

    void calcOFSMTables(std::map< size_t, std::set<Index> >& classMap,
                                std::vector< std::map<Index,size_t> >& state2classMaps) const override;
    
    void createDfsmTable();

    void resetDfsmTable(); 
    
    std::optional<Index>*** getDfsmTable() const { return dfsmTable; }
    
    /**
     * Return true iff DFSM  is initially connected.
     *
     */
    bool isInitiallyConnected() const override;
    
    /**
     * Creates a copy of the object.
     * @return A unique_ptr pointing to the created clone
     */
    std::unique_ptr<Fsm> clone() const override;
    
    /**
     * Creates an object of the same type as this one but with different
     * attributes.
     * @param fsmName Optional parameter. If set, the created FSM will be named
     *  according to the parameter. If not set, the created FSM will be named
     *  the same as this one.
     * @param states Optional parameter. If set, the created FSM will have the
     *  set of states defined by the parameter as its state set. If not set, the
     *  created FSM will have the same state space as this one.
     * @param pl Optional parameter. If set, the created FSM will have the
     *  presentation layer given as the argument as its presentation layer. If
     *  not set, the created FSM will have the same presentation layer as this
     *  one.
     * @param initialStateId Optional parameter. If set, the created FSM will
     *  have the initial state set according to the given argument. If not set,
     *  the created FSM will have the same initial state ID as this one.
     */
    std::unique_ptr<Fsm> modify(std::optional<std::string> fsmName,
                                        std::optional<States> states,
                                        std::optional<PresentationLayer> pl,
                                        std::optional<Index> initialStateId) const override;

    /**
     * Obtain the ID of the state reached from a given state by a given input trace.
     * @param sourceId ID of the state to apply the trace to.
     * @param trace The input trace to apply.
     * @return If the trace is defined in the source state, then the ID of the
     *         state reached by the trace.
     *         Otherwise std::nullopt.
     */
    std::optional<Index> getTarget(Index sourceId, const Trace& trace) const;
    
    std::string getTraceAndStates(const Trace& tr) const;
};

}
