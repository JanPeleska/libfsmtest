/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstdlib>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <deque>
#include <map>
#include <optional>
#include <cassert>
#include <utility>
#include <set>
#include <unordered_set>

#include "HMethod2.hpp"
#include "fsm/Dfsm.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"

using namespace std;

namespace libfsmtest {




void HMethod2::getDistinguishingTraces(Dfsm& fsm, Trace** dist) {
    
    size_t numberOfStates = fsm.size();
    size_t inputAlphaSize = fsm.getInputAlphabetSize();
    optional<Index>*** dfsmTable = fsm.getDfsmTable();

    // Handle all pairs distinguishable by a single input.
    // These are all pairs of states assigned to distinct classes in OFSM table 1.
    for ( Index s0 = 0; s0 < numberOfStates; ++s0 ) {
        for ( Index s1 = s0+1; s1 < numberOfStates; ++s1 ) {
            
            if (state2classMaps[1][s0] == state2classMaps[1][s1]) {
                continue; // s0 and s1 are not distinguishable by a single input
            }

            
            for ( Index x = 0; x < inputAlphaSize; ++x ) {

                if ( (not dfsmTable[s0][x][1].has_value()) && (not dfsmTable[s1][x][1].has_value()) ) {
                    // x does not distinguish s0 and s1 if it is defined in neither of them
                    continue;
                }

                if ( dfsmTable[s0][x][1].has_value()
                     && dfsmTable[s1][x][1].has_value()
                     && (dfsmTable[s0][x][1].value() == dfsmTable[s1][x][1].value())) {
                    // x does not distinguish s0 and s1 if is defined in both of them and produces
                    // the same response
                    continue;
                }


                Trace dt;
                dt.append(x);
                dist[s0][s1] = dt;
                dist[s1][s0] = dt;                    
                break;
            }
        }
    }

    // Step through OFSM tables at index k=2 and above.
    // For each table, find all states that differ in that table but not the previous table.
    // For each such pair, find an input x leading to a pair of states differing in the previous table.
    // By the construction of OFSM tables, the previous table is the first table that this latter pair differs in.
    // Construct a distinguishing trace for the former pair by appending x to the front of the distinguishing 
    // trace of the latter pair.
    for ( size_t k = 2; k < state2classMaps.size(); ++k ) {
        for ( Index s0 = 0; s0 < numberOfStates; ++s0 ) {
            for ( Index s1 = s0+1; s1 < numberOfStates; ++s1 ) {

                if (! dist[s0][s1].isEmpty()) {
                    continue; // s0 and s1 are already distinguished by a shorter trace 
                }
                
                if (state2classMaps[k][s0] == state2classMaps[k][s1]) {
                    continue; // s0 and s1 are not distinguishable by a trace of length k
                }

                
                for ( Index x = 0; x < inputAlphaSize; ++x ) {
                    Index s0xy = dfsmTable[s0][x][0].value();
                    Index s1xy = dfsmTable[s1][x][0].value();

                    if ( state2classMaps[k-1][s0xy] == state2classMaps[k-1][s1xy] ) {
                        continue; // the states reached by x are not distinguishable
                                  // by a trace of length k-1
                    }

                    Trace dt;
                    dt.append(x);
                    dt.append(dist[s0xy][s1xy]);
                    dist[s0][s1] = dt;
                    dist[s1][s0] = dt;                    
                    break;
                }
            }
        }
    }    
}


void HMethod2::getDistinguishingTraces(Ofsm& fsm, Trace** dist) {
    
    size_t numberOfStates = fsm.size();
    size_t inputAlphaSize = fsm.getInputAlphabetSize();
    size_t outputAlphaSize = fsm.getOutputAlphabetSize();
    optional<Index>*** transitionTable = fsm.getTransitionTable();

    // Handle all pairs distinguishable by a single input.
    // These are all pairs of states assigned to distinct classes in OFSM table 1.
    for ( Index s0 = 0; s0 < numberOfStates; ++s0 ) {
        for ( Index s1 = s0+1; s1 < numberOfStates; ++s1 ) {
            
            if (state2classMaps[1][s0] == state2classMaps[1][s1]) {
                continue; // s0 and s1 are not distinguishable by a single input
            }
            
            for ( Index x = 0; x < inputAlphaSize; ++x ) {
                for ( Index y = 0; y < outputAlphaSize; y++ ) {
                    if ( (transitionTable[s0][x][y].has_value()) == (transitionTable[s1][x][y].has_value()) ) {
                        continue; //  x/y does not distinguish s0 and s1
                    }

                    Trace dt;
                    dt.append(x);
                    dist[s0][s1] = dt;
                    dist[s1][s0] = dt;                    
                    goto foundSingleDistinguishingInput;
                }
            }

            foundSingleDistinguishingInput: ;
        }
    }

    // Step through OFSM tables at index k=2 and above.
    // For each table, find all states that differ in that table but not the previous table.
    // For each such pair, find an IO pair x/y leading to a pair of states differing in the previous table.
    // By the construction of OFSM tables, the previous table is the first table that this latter pair differs in.
    // Construct a distinguishing trace for the former pair by appending x/y to the front of the distinguishing 
    // trace of the latter pair.
    for ( size_t k = 2; k < state2classMaps.size(); ++k ) {
        for ( Index s0 = 0; s0 < numberOfStates; ++s0 ) {
            for ( Index s1 = s0+1; s1 < numberOfStates; ++s1 ) {

                if (! dist[s0][s1].isEmpty()) {
                    continue; // s0 and s1 are already distinguished by a shorter trace 
                }
                
                if (state2classMaps[k][s0] == state2classMaps[k][s1]) {
                    continue; // s0 and s1 are not distinguishable by a trace of length k
                }
                
                for ( Index x = 0; x < inputAlphaSize; ++x ) {
                    for ( Index y = 0; y < outputAlphaSize; y++ ) {
                        if ( (! transitionTable[s0][x][y].has_value()) && (! transitionTable[s1][x][y].has_value()) ) {
                            continue; // x/y is not defined in either s0 or s1
                        }

                        // x/y must now be defined in both s0 and s1, as otherwise they 
                        // would already have been distinguished in table 1 by x
                        Index s0xy = transitionTable[s0][x][y].value();
                        Index s1xy = transitionTable[s1][x][y].value();

                        if ( state2classMaps[k-1][s0xy] == state2classMaps[k-1][s1xy] ) {
                            continue; // the states reached by x/y are not distinguishable
                                      // by a trace of length k-1
                        }

                        Trace dt;
                        dt.append(x);
                        dt.append(dist[s0xy][s1xy]);
                        dist[s0][s1] = dt;
                        dist[s1][s0] = dt;                    
                        goto foundDistinguishingInput;
                    }
                }

                foundDistinguishingInput: ;
            }
        }
    }   
}



void HMethod2::findCheapestDistTrace(Dfsm& fsm, TreeNode* n0, Index s0, TreeNode* n1, Index s1, Trace& bestTrace, size_t& bestAdditionalInsertions, size_t& bestAdditionalBranches, Trace** dist) {

    Trace t0;
    t0.append(1);
    Trace t1;
    t1.append(3);
    if (n0 != nullptr && n0->getPath()==t0 && n1 != nullptr && n1->getPath()==t1 ) {
        cout << "";
    }

    optional<Index>*** dfsmTable = fsm.getDfsmTable();

    // find input x that is prefix of a cheapest dist. trace
    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {

        // check whether n0 is a leaf, in which case adding x would not branch
        size_t b0L = (n0 == nullptr || n0->isLeaf()) ? 0 : 1;  
        // check whether n0 already has an outgoing edge labelled x
        size_t b0C = (n0 != nullptr && n0->after(x) != nullptr) ? 0 : 1;
        size_t b0 = b0L < b0C ? b0L : b0C;

        size_t b1L = (n1 == nullptr || n1->isLeaf()) ? 0 : 1;  
        size_t b1C = (n1 != nullptr && n1->after(x) != nullptr) ? 0 : 1;
        size_t b1 = b1L < b1C ? b1L : b1C;

        size_t b = b0 + b1;

        Index s0x = dfsmTable[s0][x][0].value();
        Index y0x = dfsmTable[s0][x][1].value();
        Index s1x = dfsmTable[s1][x][0].value();
        Index y1x = dfsmTable[s1][x][1].value();

        if (y0x != y1x) {
            // x distinguishes s0 and s1
            if (b == 0 || b <= bestAdditionalBranches) {
                // if x requires fewer branches than the best choice found so far, update the choice;
                // also update if it requires the same number of branches, as no dist. trace shorter 
                // than a single input can be found
                Trace trace;
                trace.append(x);
                bestTrace = trace;
                bestAdditionalInsertions = b0C + b1C;
                bestAdditionalBranches = b;

                // try the next input
                continue;
            }
        }

        if (s0x == s1x) {
            // if x reaches the same state from s0 and s1 while producing the same output,
            // x cannot be prefix of a dist. trace of s0 and s1
            continue;
        }

        // continue based on the subtrees reached from n0, n1 by x
        TreeNode* n0x = n0 == nullptr ? nullptr : n0->after(x);
        TreeNode* n1x = n1 == nullptr ? nullptr : n1->after(x);

        if (n0x == nullptr && n1x == nullptr) {

            // if neither n0 nor n1 has a subtree for x, minimum cost is incurred by
            // extending both with x.w for a minimum length dist. trace of s0 and s1,
            // which requires |x.w| insertions

            Trace w = dist[s0x][s1x];
            size_t l = 2 * (1 + w.size());

            if (b < bestAdditionalBranches || (b == bestAdditionalBranches && l < bestAdditionalInsertions)) {
                // update the choice only if either x.w incurs fewer branches or if it
                // incurs the same number of branches but fewer insertions
                Trace trace;
                trace.append(x);
                trace.append(w);
                bestTrace = trace;
                bestAdditionalInsertions = l;
                bestAdditionalBranches = b;
            }

            // try the next input
            continue;
        }

        // at this point, at most one of n0x or n1x may be a nullptr;
        // to avoid duplicate code for each of the two cases where exactly one is a nullptr, 
        // we swap if n1x is a nullptr, ensuring that at most n0x is a nullptr
        if (n1x == nullptr) {
            swap(n0x, n1x);
            swap(s0x, s1x);
            swap(b0, b1); // Note: b0L/b1L and b0C/b1C are not swapped, as they are not required below
        }

        // search for a cheapest dist trace with prefix x via recursion
        Trace bestTraceX = dist[s0x][s1x];
        size_t bestAdditionalInsertionsX = 0;
        size_t bestAdditionalBranchesX = 3;
        findCheapestDistTrace(fsm, n0x, s0x, n1x, s1x, bestTraceX, bestAdditionalInsertionsX, bestAdditionalBranchesX, dist);

        // if n0 does not have a subtree for x, this incurs additional cost
        if (n0x == nullptr) {
            // as n0 has no subtree for x, an additional insertion is required
            bestAdditionalInsertionsX += 1;
            // inserting x after n0 may also incur additional branching
            bestAdditionalBranchesX += b0;
        }

        if (bestAdditionalBranchesX < bestAdditionalBranches  
            || (bestAdditionalBranchesX == bestAdditionalBranches && bestAdditionalInsertionsX < bestAdditionalInsertions)) {
            // update the previous choice if continuing along x wither incurs fewer branches or 
            // incurs the same number of branches but fewer insertions
            Trace trace;
            trace.append(x);
            trace.append(bestTraceX);
            bestTrace = trace;
            bestAdditionalInsertions = bestAdditionalInsertionsX;
            bestAdditionalBranches = bestAdditionalBranchesX;  
        }
    }
}


void HMethod2::findCheapestDistTracePN(Ofsm& fsm, TreeNode* n0, Index s0, TreeNode* n1, Index s1, Trace& bestTrace, size_t& bestAdditionalInsertions, size_t& bestAdditionalBranches, Trace** dist) {

    optional<Index>*** transitionTable = fsm.getTransitionTable();

    // find input x that is prefix of a cheapest dist. trace
    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {

        // check whether n0 is a leaf, in which case adding x would not branch
        size_t b0L = (n0 == nullptr || n0->isLeaf()) ? 0 : 1;
        // check whether n0 already has an outgoing edge labelled x
        size_t b0C = (n0 != nullptr && n0->after(x) != nullptr) ? 0 : 1;
        size_t b0 = b0L < b0C ? b0L : b0C;

        size_t b1L = (n1 == nullptr || n1->isLeaf()) ? 0 : 1;
        size_t b1C = (n1 != nullptr && n1->after(x) != nullptr) ? 0 : 1;
        size_t b1 = b1L < b1C ? b1L : b1C;

        size_t b = b0 + b1;

        // consider all outputs
        for (Index y = 0; y < fsm.getOutputAlphabetSize(); ++y) {

            optional<Index> s0opt = transitionTable[s0][x][y];
            optional<Index> s1opt = transitionTable[s1][x][y];

            if (s0opt.has_value() != s1opt.has_value()) {
                // x/y occurs in only one of L(s0), L(s1)
                // hence x distinguishes s0 and s1
                if (b == 0 || b <= bestAdditionalBranches) {
                    // if x requires fewer branches than the best choice found so far, update the choice;
                    // also update if it requires the same number of branches, as no dist. trace shorter
                    // than a single input can be found
                    Trace trace;
                    trace.append(x);
                    bestTrace = trace;
                    bestAdditionalInsertions = b0C + b1C;
                    bestAdditionalBranches = b;
                }

                // try the next IO pair
                continue;
            }

            if ((not s0opt.has_value()) && (not s1opt.has_value())) {
                // x/y occurs in neither L(s0) nor L(s1)
                // hence x/y is not prefix of a dist. trace of s0 and s1
                continue;
            }

            // now x/y must occur in both L(s0) and L(s1)
            Index s0x = s0opt.value();
            Index s1x = s1opt.value();

            if (s0x == s1x) {
                // x/y reaches the same state from s0 and s1 and
                // hence x/y cannot be prefix of a dist. trace of s0 and s1
                continue;
            }

            // continue based on the subtrees reached from n0, n1 by x
            TreeNode* n0x = n0 == nullptr ? nullptr : n0->after(x);
            TreeNode* n1x = n1 == nullptr ? nullptr : n1->after(x);

            if (n0x == nullptr && n1x == nullptr) {

                // if neither n0 nor n1 has a subtree for x, minimum cost is incurred by
                // extending both with x.w for a minimum length dist. trace of s0 and s1,
                // which requires |x.w| insertions

                Trace w = dist[s0x][s1x];
                size_t l = 2 * (1 + w.size());

                if (b < bestAdditionalBranches || (b == bestAdditionalBranches && l < bestAdditionalInsertions)) {
                    // update the choice only if either x.w incurs fewer branches or if it
                    // incurs the same number of branches but fewer insertions
                    Trace trace;
                    trace.append(x);
                    trace.append(w);
                    bestTrace = trace;
                    bestAdditionalInsertions = l;
                    bestAdditionalBranches = b;
                }

                // try the next IO pair
                continue;
            }

            // at this point, at most one of n0x or n1x may be a nullptr;
            // to avoid duplicate code for each of the two cases where exactly one is a nullptr,
            // we swap if n1x is a nullptr, ensuring that at most n0x is a nullptr
            if (n1x == nullptr) {
                swap(n0x, n1x);
                swap(s0x, s1x);
                swap(b0, b1); // Note: b0L/b1L and b0C/b1C are not swapped, as they are not required below
            }

            // search for a cheapest dist trace with prefix x via recursion,
            // starting from the states reached via x/y
            Trace bestTraceX = dist[s0x][s1x];
            size_t bestAdditionalInsertionsX = 0;
            size_t bestAdditionalBranchesX = 3;
            findCheapestDistTracePN(fsm, n0x, s0x, n1x, s1x, bestTraceX, bestAdditionalInsertionsX, bestAdditionalBranchesX, dist);

            // if n0 does not have a subtree for x, this incurs additional cost
            if (n0x == nullptr) {
                // as n0 has no subtree for x, an additional insertion is required
                bestAdditionalInsertionsX += 1;
                // inserting x after n0 may also incur additional branching
                bestAdditionalBranchesX += b0;
            }

            if (bestAdditionalBranchesX < bestAdditionalBranches
                || (bestAdditionalBranchesX == bestAdditionalBranches && bestAdditionalInsertionsX < bestAdditionalInsertions)) {
                // update the previous choice if continuing along x wither incurs fewer branches or
                // incurs the same number of branches but fewer insertions
                Trace trace;
                trace.append(x);
                trace.append(bestTraceX);
                bestTrace = trace;
                bestAdditionalInsertions = bestAdditionalInsertionsX;
                bestAdditionalBranches = bestAdditionalBranchesX;
            }

        }
    }
}



void HMethod2::addCheapestDistTrace(Dfsm& fsm, pair<TreeNode*,Index> p0, pair<TreeNode*,Index> p1, Trace** dist) {
    TreeNode* n0 = p0.first;
    Index s0 = p0.second;
    TreeNode* n1 =p1.first;
    Index s1 = p1.second;
    
    // initial choice: pre-computed dist. trace
    Trace bestTrace = dist[s0][s1];
    size_t bestAdditionalInsertions = 0;
    size_t bestAdditionalBranches = 3;  // as at most two new branches may be created in the test suite,
                                        // initialising this as 3 ensures that any subsequent choice is better

    // find a cheapest dist. trace of s0 and s1 to insert after n0 and n1
    findCheapestDistTrace(fsm, n0, s0, n1, s1, bestTrace, bestAdditionalInsertions, bestAdditionalBranches, dist);

    // insert the trace
    n0->add(bestTrace);
    n1->add(bestTrace);
}

void HMethod2::addCheapestDistTracePN(Ofsm& fsm, pair<TreeNode*,Index> p0, pair<TreeNode*,Index> p1, Trace** dist) {
    TreeNode* n0 = p0.first;
    Index s0 = p0.second;
    TreeNode* n1 =p1.first;
    Index s1 = p1.second;

    // initial choice: pre-computed dist. trace
    Trace bestTrace = dist[s0][s1];
    size_t bestAdditionalInsertions = 0;
    size_t bestAdditionalBranches = 3;  // as at most two new branches may be created in the test suite,
    // initialising this as 3 ensures that any subsequent choice is better

    // find a cheapest dist. trace of s0 and s1 to insert after n0 and n1
    findCheapestDistTracePN(fsm, n0, s0, n1, s1, bestTrace, bestAdditionalInsertions, bestAdditionalBranches, dist);

    // insert the trace
    n0->add(bestTrace);
    n1->add(bestTrace);
}


void HMethod2::distinguishFromSet(Dfsm& fsm, pair<TreeNode*,Index> p, set<pair<TreeNode*,Index>>& toDist, size_t depth, Trace** dist) {
    
    TreeNode* n0 = p.first;
    Index s0 = p.second;
    optional<Index>*** dfsmTable = fsm.getDfsmTable();

    // distinguish s from all elements of toDist
    for (auto& distPair : toDist) {
        if (s0 == distPair.second) {
            continue; // n0 and n1 converge in s0 and hence cannot be distinguished
        }
        addCheapestDistTrace(fsm, p, distPair, dist);
    }

    // continue only if further inputs are to be applied
    if (depth == 0) return;

    // perform a recursive step for each input, adding p to toDist if necessary
    auto insertionResult = toDist.insert(p);
    bool toDistAlreadyContainsP = !insertionResult.second;  
    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
        TreeNode* nx = n0->after(x);
        Index sx = dfsmTable[s0][x][0].value();
        pair<TreeNode*,Index> px = make_pair(nx,sx);
        distinguishFromSet(fsm, px, toDist, depth-1, dist);
    }
    if (!toDistAlreadyContainsP) {
        toDist.erase(p);
    }
}


void HMethod2::distinguishFromSetPN(Ofsm& fsm, pair<TreeNode*,Index> p, set<pair<TreeNode*,Index>>& toDist, size_t depth, Trace** dist) {

    TreeNode* n0 = p.first;
    Index s0 = p.second;
    optional<Index>*** transitionTable = fsm.getTransitionTable();

    // distinguish s from all elements of toDist
    for (auto& distPair : toDist) {
        if (s0 == distPair.second) {
            continue; // n0 and n1 converge in s0 and hence cannot be distinguished
        }
        addCheapestDistTracePN(fsm, p, distPair, dist);
    }

    // continue only if further inputs are to be applied
    if (depth == 0) return;

    // perform a recursive step for each input, adding p to toDist if necessary
    auto insertionResult = toDist.insert(p);
    bool toDistAlreadyContainsP = !insertionResult.second;
    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
        TreeNode* nx = n0->after(x);
        for (Index y = 0; y < fsm.getOutputAlphabetSize(); ++y) {
            optional<Index> s0opt = transitionTable[s0][x][y];
            if (not s0opt.has_value()) {
                // if x/y does not reach any state, then it does not need to be
                // considered further
                continue;
            }
            pair<TreeNode*,Index> px = make_pair(nx,s0opt.value());
            distinguishFromSetPN(fsm, px, toDist, depth-1, dist);
        }
    }
    if (!toDistAlreadyContainsP) {
        toDist.erase(p);
    }
}



bool HMethod2::satisfiesHCondition(Ofsm& fsm, vector<pair<Trace,Index>>& cover, Tree& suite, size_t additionalStates) {

    // check that the cover is valid
    for (auto& p : cover) {
        auto tgts = fsm.getTargets(p.first);
        if (tgts.count(p.second) == 0) {
            cout << "ERROR in state cover pair (";
            for (auto& x : p.first) cout << x;
            cout << "," << p.second << ") -- trace does not reach target" << endl;
            return false;
        }
    }

    // find nodes for the state cover
    vector<pair<TreeNode*,Index>> v;
    for (auto& p : cover) {
        TreeNode* n = suite.after(p.first);
        if (n == nullptr) {
            cout << "ERROR in state cover pair (";
            for (auto& x : p.first) cout << x;
            cout << "," << p.second << ") -- trace is not contained in test suite" << endl;
            return false;
        }
        v.emplace_back(n,p.second);
    }

    // check whether all extensions are in the test suite
    Traces extensions;
    enumerateInputAlphabetToLength(1,
                                   additionalStates+1,
                                   fsm.getInputAlphabetSize(),
                                   extensions);
    for ( auto& p : v ) {
        for ( auto& w : extensions ) {
            TreeNode *nw = p.first->after(w);
            if (nw == nullptr) {
                cout << "ERROR missing extension ";
                for (auto &x : w) cout << x;
                cout << " after state cover trace ";
                for (auto &x : p.first->getPath()) cout << x;
                cout << " reaching " << p.second << endl;
                return false;
            }
        }
    }




    // sets of pairs to separate via the h condition
    vector<pair<pair<TreeNode*,Index>, pair<TreeNode*,Index>>> a;
    vector<pair<pair<TreeNode*,Index>, pair<TreeNode*,Index>>> b;
    vector<pair<pair<TreeNode*,Index>, pair<TreeNode*,Index>>> c;

    // create A = V x V
    for ( size_t i = 0; i < v.size(); ++i ) {
        for ( size_t j = i + 1; j < v.size(); ++j ) {
            // note: there is no need to consider other states reached by traces in the state cover
            a.push_back(make_pair(v[i],v[j]));
        }
    }


    // create B = {(v,v'.w) | v,v' : V, 0 < |w| <= m-n+1 }
    for ( auto& p : v ) {
        for ( auto& w : extensions ) {
            // not null by previous check
            TreeNode *nw = p.first->after(w);
            // note: due to nondeterminism, a trace may reach distinct targets
            // and hence incur multiple pairs in B
            for (auto& sw : fsm.getTargets(w,p.second)) {
                if (p.second != sw) {
                    // only diverging traces need to be separated
                    b.emplace_back(p,make_pair(nw,sw));
                }
            }
        }
    }



    // create C = {(v.w', v.w) | v : V, 0 < |w'| < |w| <= m-n+1, w' : pref(w) }
    // this is somewhat more elaborate, as we are not necessarily interested in all pairs
    // (s0,s1) reached by (v.w', v.w), since for each s0 only those s1 are relevant that may
    // be reached from it via the suffix of w of length |w|-|w'|.
    function<void(pair<TreeNode*,Index>, vector<std::pair<TreeNode*,Index>>&, size_t)> addDistPairs = [&fsm,&c,&addDistPairs](pair<TreeNode*,Index> p, vector<std::pair<TreeNode*,Index>>& prefixes, size_t depth)->void {
        for (auto& distPair : prefixes) {
            if (p.second != distPair.second) {
                // only diverging traces need to be separated
                c.emplace_back(distPair,p);
            }
        }

        // stop if the desired depth has been reached
        if (depth == 0) return;

        // mark p as to be separated by longer extensions
        prefixes.push_back(p);
        for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
            // not null by previous check
            TreeNode* nxy = p.first->after(x);

            for (Index y = 0; y < fsm.getOutputAlphabetSize(); ++y) {
                optional<Index> sOpt = fsm.getTransitionTable()[p.second][x][y];
                if (not sOpt.has_value()) {
                    // extension with x/y is not required if this is not in L(p.second)
                    continue;
                }
                Index sxy = sOpt.value();
                pair<TreeNode*,Index> pxy = make_pair(nxy,sxy);
                addDistPairs(pxy,prefixes,depth-1);
            }
        }
        prefixes.pop_back();
        return;
    };
    for ( auto& p : v ) {
        vector<std::pair<TreeNode*,Index>> prefixes;
        addDistPairs(p, prefixes, additionalStates+1);
    }


    // check for each pair ((n0,s0),(n1,s1)) in A,B,C whether the test suite
    // contains a distinguishing trace of s0 and s1 in the intersection of the
    // subtrees rooted at n0 and n1
    auto checkDist = [&fsm](pair<pair<TreeNode*,Index>, pair<TreeNode*,Index>>& toDist) {
        TreeNode* n0 = toDist.first.first;
        Index s0 = toDist.first.second;
        TreeNode* n1 = toDist.second.first;
        Index s1 = toDist.second.second;

        Trace d;
        bool dExists = false;

        // try to find a trace in the intersection of the subtrees rooted at n0 and n1
        // that distinguishes s0 and s1
        std::deque<std::pair<TreeNode*,TreeNode*>> worklist;
        worklist.emplace_back(n0, n1);
        while(not worklist.empty()) {
            auto [left, right] = worklist.front();
            Trace trace = left->getPath(n0);
            auto io1 = fsm.getIOTracesFromState(trace,s0,false);
            auto io2 = fsm.getIOTracesFromState(trace,s1,false);

            if ( io1 != io2 ) {
                d = trace;
                dExists = true;
                break;
            }

            for(size_t i = 0; i < fsm.getInputAlphabetSize(); ++i) {
                if(left->after(i) != nullptr && right->after(i) != nullptr) {
                    worklist.emplace_back(left->after(i), right->after(i));
                }
            }
            worklist.pop_front();
        }

        if (not dExists) {
            cout << "(";
            for ( auto& x : n0->getPath() ) cout << x;
            cout << ",";
            for ( auto& x : n1->getPath() ) cout << x;
            cout << ") reaching (" << s0 << "," << s1 << ") is NOT distinguished -- ERROR" << endl;;
        } 
        return dExists;
    };

    for ( auto toDist : a ) {
        if (not checkDist(toDist)) {
            //cout << "error occured in set A" << endl;
            return false;
        }
    }

    for ( auto toDist : b ) {
        if (not checkDist(toDist)) {
            //cout << "error occured in set B" << endl;
            return false;
        }
    }

    for ( auto toDist : c ) {
        if (not checkDist(toDist)) {
            //cout << "error occured in set C" << endl;
            return false;
        }
    }
    return true;
}










void HMethod2::visit(Fsm &fsm) {
    
    // First, a transformation to prime machine is needed in any case
    unique_ptr<Fsm> primeFsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
    
    // Now visit the prime machine
    primeFsm->accept(*this);
}

void HMethod2::visit(Ofsm &fsm) {

    if ( not fsm.isMinimal() ) {
        // Need to transform to prime OFSM (i.e. minimised)
        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm, "_MIN");

        // Now visit the prime OFSM
        minimisedDfsm->accept(*this);
        return;
    }

    size_t numberOfStates = fsm.size();

    // ######## Preparation ########
    // select for each pair of states a single distinguishing trace of minimal length

    fsm.calcOFSMTables(classMap, state2classMaps);
    Trace** dist = new Trace*[numberOfStates];
    for (Index i = 0; i < numberOfStates; ++i)
        dist[i] = new Trace[numberOfStates];
    getDistinguishingTraces(fsm,dist);


    // ######## Initial Test Suite ########
    // compute the initial test suite consisting of a state cover extended
    // with all traces up to length m-n+1

    createStateCover(fsm);
    // collect elements in the state cover to extend from
    // note: no particular heuristic is applied in selecting a trace reaching some state q if
    //       multiple such traces exist in the state cover
    set<pair<TreeNode*,Index>> v;
    unordered_set<Index> foundStates;
    auto stateCover = testCaseTree->getAllTestCases();
    for (auto iter = stateCover.begin(); iter != stateCover.end(); ++iter) {
        set<Index> reachedStates = fsm.getTargets(*iter);
        for (Index reachedState : reachedStates) {
            if (foundStates.count(reachedState) == 0) {
                foundStates.insert(reachedState);
                TreeNode* reachedNode = testCaseTree->after(*iter);
                v.insert(make_pair(reachedNode,reachedState));
            }
        }
    }

    // ######## Extension ########
    // extend the state cover with all traces up to length m-n+1
    Traces enumeratedTraces;
    enumerateInputAlphabetToLength(1,
                                   numAddStates+1,
                                   testsuite->getFsm().getInputAlphabetSize(),
                                   enumeratedTraces);
    testCaseTree->addToAllNodes(enumeratedTraces);

    // ######## Separation ########
    // separate traces as required by the H-Condition
    for (auto &p: v) {
        set<pair<TreeNode *, Index>> x(v);
        distinguishFromSetPN(fsm, p, x, numAddStates + 1, dist);
    }

    // ######## Conversion ########
    testCaseTreeToTestSuite();

    // cleanup
    for (Index i = 0; i < numberOfStates; ++i) {
        delete[] dist[i];
    }
    delete[] dist;

    // completeness check
    #ifndef NDEBUG
        vector<pair<Trace,Index>> vv;
        for (auto& p : v) vv.emplace_back(p.first->getPath(),p.second);
        bool isComplete = satisfiesHCondition(fsm, vv, *testCaseTree, numAddStates);
        cout << "COMPLETENESS CHECK: " << (isComplete ? "PASS" : "FAIL") << endl;
    #endif
}







void HMethod2::visit(Dfsm &fsm) {

    if ( not fsm.isMinimal() ) {
        // Need to transform to prime DFSM (i.e. minimised)
        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm, "_MIN");

        // Now visit the prime DFSM
        minimisedDfsm->accept(*this);
        return;
    }

    size_t numberOfStates = fsm.size();

    // ######## Preparation ########
    // select for each pair of states a single distinguishing trace of minimal length

    fsm.calcOFSMTables(classMap, state2classMaps);
    Trace** dist = new Trace*[numberOfStates];
    for (Index i = 0; i < numberOfStates; ++i)
        dist[i] = new Trace[numberOfStates];
    getDistinguishingTraces(fsm,dist);


    // ######## Initial Test Suite ########
    // compute the initial test suite consisting of a state cover extended
    // with all traces up to length m-n+1

    createStateCover(fsm);
    // collect elements in the state cover to extend from
    set<pair<TreeNode*,Index>> v;
    unordered_set<Index> foundStates; // TODO: if createStateCover is minimal, it is not necessary to ensure
                                      //       that for each state exactly a single element is added to v
    auto stateCover = testCaseTree->getAllTestCases();
    for (auto iter = stateCover.begin(); iter != stateCover.end(); ++iter) {
        Index reachedState = fsm.getTarget(fsm.getInitialStateId(),*iter).value();
        if (foundStates.count(reachedState) == 0) {
            foundStates.insert(reachedState);
            TreeNode* reachedNode = testCaseTree->after(*iter);
            v.insert(make_pair(reachedNode,reachedState));
        }
    }

    // ######## Extension ########
    // extend the state cover with all traces up to length m-n+1
    Traces enumeratedTraces;
    enumerateInputAlphabetToLength(1,
                                   numAddStates+1,
                                   testsuite->getFsm().getInputAlphabetSize(),
                                   enumeratedTraces);
    testCaseTree->addToAllNodes(enumeratedTraces);

    // ######## Separation ########
    // separate traces as required by the H-Condition
    // apply a specialised version for complete DFSMs if possible, else fall back to the
    // general version for arbitrary OFSMs
    if ( fsm.isCompletelySpecified() ) {
        for (auto &p: v) {
            set<pair<TreeNode *, Index>> x(v);
            distinguishFromSet(fsm, p, x, numAddStates+1, dist);
        }
    } else {
        for (auto &p: v) {
            set<pair<TreeNode *, Index>> x(v);
            distinguishFromSetPN(fsm, p, x, numAddStates + 1, dist);
        }
    }

    // ######## Conversion ########
    testCaseTreeToTestSuite();

    // cleanup
    for (Index i = 0; i < numberOfStates; ++i) {
        delete[] dist[i];
    }
    delete[] dist;


    // completeness check
    #ifndef NDEBUG
        vector<pair<Trace,Index>> vv;
        for (auto& p : v) vv.emplace_back(p.first->getPath(),p.second);
        bool isComplete = satisfiesHCondition(fsm, vv, *testCaseTree, numAddStates);
        cout << "COMPLETENESS CHECK: " << (isComplete ? "PASS" : "FAIL") << endl;
    #endif
}

} // namespace libfsmtest
