#include "testsuite/cutTreeWithFSMLanguage.hpp"

#include "fsm/Fsm.hpp"

#include <stack>
#include <algorithm>
#include <cassert>

using namespace std;

namespace libfsmtest {

/**
 * Function to create a tree that contains all traces from a given tree that
 * are also in the defined input traces of a given FSM.
 */
unique_ptr<Tree> cutTreeWithFSMLanguage(Tree const &tree, Fsm const &fsm) {
    // Creating the tree the resulting traces shall be contained in
    std::unique_ptr<Tree> result = make_unique<Tree>();

    // Define a stack for recursion elimination Each element of the stack
    // contains a pair of tree nodes, a state from FSM fsm and an input index
    // The pairs of tree nodes are nodes that correspond to each other, i.e.
    // they are reached by the same input path and the node from the tree
    // `result` has only children that correspond to some children of the
    // corresponding node in the `tree` tree. The state in the stack element is
    // a state in `fsm` that is reachable by the input sequence leading to the
    // TreeNodes. The input id is the next input id to process.
    typedef tuple<TreeNode*, TreeNode*, State, Index> StackFrame;
    stack<StackFrame> stack;

    // The initial stack element contains both tree roots, the FSM's initial
    // state and the first input id
    stack.push(make_tuple(tree.getRoot(), result->getRoot(), fsm.getStates().at(fsm.getInitialStateId()), 0));

    // Determining the upper bound of the input ids
    size_t const inputAlphabetSize = fsm.getPresentationLayer().inputAlphabet.size();
    // While there are stack frames on the stack to process
    while(not stack.empty()) {
        // Get the elements from the top stack frame
        auto &[refTreeNode, resultTreeNode, fsmState, nextInput] = stack.top();

        // If the stack frame is not fully processed, i.e. if there are still
        // input IDs to check
        if(nextInput < inputAlphabetSize) {
            // and if the state has an outgoing transition with the next input id
            // and if the reference tree node has a child node reachable by that input id
            if(fsmState.hasTransition(nextInput) and refTreeNode->after(nextInput) != nullptr) {
                // Get all transitions with that input
                Transitions transitionsForInput;
                copy_if(fsmState.getTransitions().cbegin(),
                        fsmState.getTransitions().cend(),
                        back_inserter(transitionsForInput),
                        [nextInput=nextInput] (auto const &transition) {
                    return transition.getInput() == nextInput;
                });
                // Note: We are using a workaround for the above lambda to
                // capture nextInput. The current clang compiler (12.0) does
                // not pull nextInput from the enclosing scope as structured
                // bindings cannot be referenced in lambda captures according
                // to a design paper for C++17 that Clang seems to implement
                // while gcc and msvc don't. In C++20 this restriction is
                // lifted again. For this code however, we use an init
                // statement, declaring a variable local to the lambda and
                // initialising it with the structured binding, which is legal
                // and works for all (major and tested) compilers.

                // Get the child node corresponding to the input
                TreeNode *nextRefTreeNode = refTreeNode->after(nextInput);
                TreeNode *nextResultTreeNode = resultTreeNode->after(nextInput);
                // If that node does not exist in the result tree yet, create it
                // Note that it can already exist in nondeterministic FSMs
                if(nextResultTreeNode == nullptr) {
                    nextResultTreeNode = resultTreeNode->add(nextInput);
                    nextResultTreeNode->setValue(nextRefTreeNode->getValue());
                }

                // Push the new node with its corresponding node and a
                // corresponding FSM state to the stack for processing. The
                // input id enumeration starts at 0 for each stack frame.
                for(auto const &transition : transitionsForInput) {
                    stack.push(make_tuple(nextRefTreeNode, nextResultTreeNode, fsm.getStates().at(transition.getTargetId()), 0));
                }
            } else {
                // However, if either the FSM state does not have a transition
                // for some input or the reference tree's node does not have a
                // child node for the input
                ; //Do nothing. We just skip this input and don't copy the subtree
            }
            //Increment the input for the current stack frame
            ++nextInput;
        } else {
            // However, if the last input has been processed, pop the top of the stack
            stack.pop();
        }
    }

    // Check the results. This is for checking during development only
    std::vector<TreeNode const*> pathsInLanguage;
    std::vector<TreeNode const*> pathsNotInLanguage;

    auto sortNodes = [&pathsInLanguage,&pathsNotInLanguage,&result](TreeNode const *node) {
        if(result->after(node->getPath()) != nullptr) {
            pathsInLanguage.push_back(node);
        } else {
            pathsNotInLanguage.push_back(node);
        }
    };

    tree.applyToAllNodes(sortNodes);
    assert(all_of(pathsInLanguage.cbegin(), pathsInLanguage.cend(), [&fsm](TreeNode const *node) {
        return not fsm.getTargets(node->getPath()).empty();
    }));
    assert(none_of(pathsNotInLanguage.cbegin(), pathsNotInLanguage.cend(), [&fsm](TreeNode const *node) {
        return not fsm.getTargets(node->getPath()).empty();
    }));

    return result;
}


}
