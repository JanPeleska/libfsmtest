/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <unordered_set>
#include <stack>
#include <vector>
#include <memory>

#include "testsuite/Tree.hpp"
#include "visitors/FsmVisitor.hpp"
#include "visitors/TestGenerationFrame.hpp"

namespace libfsmtest {


// Type of nodes in the convergence graph
struct ConvergenceNode {
    // traces converging in this node
    std::unordered_set<TreeNode*> convergentNodes;
    // nextForInput[x] is the convergence node reached by input x 
    std::vector<ConvergenceNode*> nextForInput;
    // state of the underlying DFSM corresponding to this node
    Index state;

    bool isLeaf = true;

    ConvergenceNode(ConvergenceNode const &other) = default;
    ConvergenceNode(Index node,TreeNode* convergentNode, size_t numInputs) {
        state = node;
        convergentNodes.insert(convergentNode);
        nextForInput.resize(numInputs, nullptr);
    }
};

/** 
 * A graph storing information on which traces in a test suite
 * have been established to converge.
 * 
 * Invariant: For any ConvergenceNode n in the graph, n.convergentNodes
 *            is not empty and contains no nullpointers.
 */
class ConvergenceGraph
{
private:
    const Dfsm& dfsm;
    const Tree& testSuite;

    // The root node of the graph
    ConvergenceNode root;

    // Pool of nodes allocated. Not all of these are guaranteed to be
    // referenced in the graph at every point in time. Using an std::deque to
    // keep all references to nodes in the pool valid if the pool size is
    // increased.
    std::deque<ConvergenceNode> nodePool;

    // assumes that trace is aready defined in the graph
    ConvergenceNode &after(const Trace& trace);

    // assumes that trace followed by input is aready defined in the graph
    void replaceAfter(const Trace& trace, Index input, ConvergenceNode &newNode);

    // merge two nodes 
    void mergeInto(ConvergenceNode &node1, ConvergenceNode &node2);

public:

    /**
     * Create an new convergence graph storing all traces in the test suite
     * as separate classes.
     */
	ConvergenceGraph(const Dfsm& dfsm, const Tree& testSuite);

    /**
     * Add a trace to the convergence graph.
     * Assumes that trace is already contained in the test suite.
     */
    void add(const Trace& trace);

    /**
     * Merge (trace1.input) and (trace2) into a single class.
     * 
     * Assumes that trace1.input and trace2 are already contained in the graph.
     */
    void merge(const Trace& trace1, Index input, const Trace& trace2);

    /**
     * Obtain all traces convergent to trace that are contained in the test suite.
     * 
     * Invariant: the returned set does not contain nullpointers.
     * 
     * Assumes that trace is already contained in the graph.
     */
    std::unordered_set<TreeNode*> getConvergentTraces(const Trace& trace);

    /**
     * Check whether there exists a trace convergent to the input trace
     * that is maximal in the test suite.
     * 
     * Assumes that trace is already contained in the graph.
     */
    bool hasLeaf(const Trace& trace);
	
};

/**
 * Class implementing the SPYH-Method for test generation.
 */
class SPYHMethod : public TestGenerationVisitor {

protected:

    /**
     * The maximal number of additional states,
     * which the implementation DFSM in minimised
     * form may have, when compared to the reference
     * model in minimised form.
     */
    const size_t numAddStates;

    // a vector v such that for any pair qi,qj of state indices of fsm with qi > qj,
    // v[qi][qj] contains all minimum length distinguishing traces for the states
    std::vector<std::vector<std::vector<Trace>>> minimalDistinguishingTraces;

    // get the minimum length distinguishing traces for qi and qj
    std::vector<Trace> getMinimalDistinguishingTraces(Index qi, Index qj);

    // compute all distinguishing traces for qi and qj of length d+1
    static std::vector<Trace> getDistinguishingTracesOfDepth(Dfsm& fsm, Index qi, Index qj, size_t d, Trace& prefix, std::vector<std::map<size_t,size_t>>& state2classMaps);
    
    // compute a vector v such that for any pair qi,qj of state indices of fsm with qi > qj,
    // v[qi][qj] contains all minimum length distinguishing traces for the states
    static std::vector<std::vector<std::vector<Trace>>> getMinimalDistinguishingTracesForStatePairs(Dfsm& fsm);    
    
    // corresponds to method Distinguish of the original description of the SPYH method
    void spyhDistinguish(Dfsm& fsm, const Trace& trace, Traces const &traces, Tree& testSuite, ConvergenceGraph & graph);
    
    // corresponds to method GetPrefixOfSepSeq of the original description of the SPYH method
    std::pair<size_t,std::stack<size_t>> getPrefixOfSeparatingTrace(Dfsm& fsm, const Trace& trace1, const Trace& trace2, Tree& testSuite, ConvergenceGraph & graph);
    
    // corresponds to method EstimateGrowthOfT of the original description of the SPYH method
    size_t estimateGrowthOfTestSuite(Dfsm& fsm, Index u, Index v, Index input);
    
    // corresponds to method AppendSeparatingSequence of the original description of the SPYH method
    void appendSeparatingSequence(const Trace& traceToAppendTo, const Trace& traceToAppend, Tree& testSuite, ConvergenceGraph & graph);
    
    // corresponds to method DistinguishFromSet of the original description of the SPYH method
    void distinguishFromSet(Dfsm& fsm, const Trace& trace1, const Trace& trace2, Traces const &stateCover, Traces &tracesToDistFrom, Tree& testSuite, ConvergenceGraph & graph, size_t depth);

    /**
     * Generate an m-complete test suite for fsm.
     */ 
    void generateTestSuite(Dfsm& fsm);

public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation DFSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     */
    SPYHMethod(const size_t numAddStatesParam) :
        TestGenerationVisitor(),
        numAddStates(numAddStatesParam)
    {

    }

    /**
     * Not supported.
     * The current implementation of the SPYH method is limited to complete DFSMs.
     */
    void visit(Fsm & fsm) override;

    /** 
     * Not supported.
     * The current implementation of the SPYH method is limited to complete DFSMs. 
     */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual SPYH-Method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     *             If this fsm is not completely specified, then a runtime error is thrown,
     *             as the current implementation of the SPYH method is limited to complete DFSMs.
     */
    void visit(Dfsm & dfsm) override;
    
};

} // namespace libfsmtest
