/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <vector>
#include <deque>
#include <memory>
#include <stdexcept>

#include "ToInitiallyConnectedTransformer.hpp"

using namespace std;

namespace libfsmtest {


void ToInitiallyConnectedTransformer::visit(Fsm& fsmToTransform) {
    
    States statesConnected;
    Strings stateNamesConnected;
    
    transformToInitiallyConnected(fsmToTransform,statesConnected,stateNamesConnected);

    PresentationLayer modifiedPl(fsmToTransform.getPresentationLayer());
    modifiedPl.stateNames = stateNamesConnected;
    
    transformedFsm = make_unique<Fsm>(fsmToTransform.getName() + nameSuffix,
                                      statesConnected,
                                      modifiedPl,
                                      0 /* Initial states of connected machine has id 0 */);
}


void ToInitiallyConnectedTransformer::visit(Ofsm& fsmToTransform) {
    
    States statesConnected;
    Strings stateNamesConnected;
    
    transformToInitiallyConnected(fsmToTransform,statesConnected,stateNamesConnected);

    PresentationLayer modifiedPl(fsmToTransform.getPresentationLayer());
    modifiedPl.stateNames = stateNamesConnected;

    transformedFsm = make_unique<Ofsm>(fsmToTransform.getName() + nameSuffix,
                                       statesConnected,
                                       modifiedPl,
                                       0 /* Initial states of connected machine has id 0 */);
    
}



void ToInitiallyConnectedTransformer::visit(Dfsm& fsmToTransform) {
    
    States statesConnected;
    Strings stateNamesConnected;
    
    transformToInitiallyConnected(fsmToTransform,statesConnected,stateNamesConnected);
    
    // Presentation layer for connected state machine
    PresentationLayer modifiedPl(fsmToTransform.getPresentationLayer());
    modifiedPl.stateNames = stateNamesConnected;
    
    transformedFsm = make_unique<Dfsm>(fsmToTransform.getName() + nameSuffix,
                                       statesConnected,
                                       modifiedPl,
                                       0 /* Initial states of connected machine has id 0 */);
}


void ToInitiallyConnectedTransformer::transformToInitiallyConnected(Fsm& fsm,
                                                                         States& statesConnected,
                                                                         Strings& stateNamesConnected) {
    
    
    
    // Initial state number of the connected machine
    Index initialStateIdConnected = 0;
    
    // Map from the connected FSM's state ids to this FSM's state ids
    map<Index,Index> connectedId2thisId;
    connectedId2thisId[initialStateIdConnected] = fsm.getInitialStateId();
    
    // Inverse map from this FSM's state ids to the connected FSM's state ids
    map<Index,Index> thisId2connectedId;
    thisId2connectedId[fsm.getInitialStateId()] = initialStateIdConnected;
    
    // This set will finally contain all ids of the unreachable states.
    // Initially, ALL state ids but that of the initial state are contained
    // in this set.
    set<Index> unreachableStateIds;
    for ( Index n = 0; n < fsm.getStates().size(); n++ ) {
        if ( n != fsm.getInitialStateId() ) unreachableStateIds.insert(n);
    }
    
    // Perform a BFS, starting at the initial state, and remove
    // the state ids of each visited state.
    deque<Index> bfsQueue;
    bfsQueue.push_back(fsm.getInitialStateId());
    
    while ( ! bfsQueue.empty() ) {
        Index thisId = bfsQueue.front();
        bfsQueue.pop_front();
        
        for ( auto& tr : fsm.getStates()[thisId].getTransitions() ) {
            Index tgtId = tr.getTargetId();
            if ( unreachableStateIds.find(tgtId) != unreachableStateIds.end() ) {
                unreachableStateIds.erase(tgtId);
                bfsQueue.push_back(tgtId);
            }
        }
    }
    // Now the states whose ids are still in unreachableStateIds are
    // unreachable from the initial state.
    // We complete the map from this FSM's reachable state ids to the new FSM's state ids
    Index idConnected = 0;
    for ( Index n = (fsm.getInitialStateId() + 1) % fsm.getStates().size();
         n != fsm.getInitialStateId();
         n = (n + 1) % fsm.getStates().size() ) {
        if ( unreachableStateIds.find(n) == unreachableStateIds.end() ) {
            // fsm.getStates()[n] is reachable
            connectedId2thisId[++idConnected] = n;
            thisId2connectedId[n] = idConnected;
        }
    }
    
    // Create new states with new transitions for the initially connected FSM
    statesConnected.reserve(connectedId2thisId.size());
    for ( auto& p : connectedId2thisId ) {
        State& state = fsm.getStates().at(p.second);
        // Create the new state for the connected FSM
        statesConnected.emplace_back(state.getName(),p.first);
        // Register the external state name in the presentation layer
        stateNamesConnected.push_back(state.getName());
        // Now add transitions
        for ( auto& tr : state.getTransitions() ) {
            statesConnected.at(p.first).getTransitions().emplace_back(p.first,
                                                                      tr.getInput(),
                                                                      tr.getOutput(),
                                                                      thisId2connectedId[tr.getTargetId()]);
        }
    }
    
}

}
