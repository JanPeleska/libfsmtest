/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>

#include "TreeNode.hpp"

using namespace std;

namespace libfsmtest {

size_t TreeNode::nextNodeId;

bool TreeNode::isLeaf() const {
    return children.empty();
}

void TreeNode::calculateLeaves(vector<TreeNode *> &leaves) {
    if (isLeaf()) {
        leaves.push_back(this);
    } else {
        for (const auto& [index, child] : children) {
            child->calculateLeaves(leaves);
        }
    }
}

size_t TreeNode::getNumberOfLeaves() const {
    if ( isLeaf() ) return 1;
    size_t count = 0;
    for ( const auto& [index, child] : children ) {
        count += child->getNumberOfLeaves();
    }
    return count;
}

size_t TreeNode::size() const {
    size_t count = 1;
    for ( const auto& [index, child] : children ) {
        count += child->size();
    }
    return count;
}


TreeNode::TreeNode(const TreeNode &other) {
    id = nextNodeId++;
    parent = nullptr;
    value = other.value;
    for (const auto& [index, child] : other.children) {
        const auto& [position,_] = children.emplace(index, make_unique<TreeNode>(*child));
        position->second->setParent(this);
    }
}

void TreeNode::setParent(TreeNode *newParent) {
    TreeNode::parent = newParent;
}

void TreeNode::setValue(optional<size_t> newValue) {
    TreeNode::value = newValue;
}

TreeNode *TreeNode::add(size_t ioIndex, optional<size_t> childValue) {
    
    auto indexIte = children.find(ioIndex);
    
    if ( indexIte == children.end() ) {
        children[ioIndex] = make_unique<TreeNode>(this,childValue);
    }
    else {
        children[ioIndex]->setValue(childValue);
    }
    return children[ioIndex].get();

}

void TreeNode::add(const Trace &t) {
    TreeNode::add(t.begin(), t.end());
}

void TreeNode::add(Trace::const_iterator start, Trace::const_iterator end) {
    if (start == end) {
        // End of a trace is reached,
        return;
    }

    // get current index
    size_t index = *start;

    // Try to create a new child for the current index or use already existing
    auto indexIte = children.find(index);
    if ( indexIte == children.end() ) {
        children[index] = make_unique<TreeNode>(this);
    }
        children[index]->add(++start,end);
    
}


void TreeNode::getExistingNodes(std::set<TreeNode*>& existingNodesSet) {
    existingNodesSet.insert(this);
    for ( const auto& p : children ) {
        p.second->getExistingNodes(existingNodesSet);
    }
}


void TreeNode::addToAllNodes(const Trace &t, const set<TreeNode*>& existingNodes) {
    
    // Do not traverse new nodes
    if ( existingNodes.find(this) == existingNodes.end() ) return;
    
    for (const auto& [key, child] : children) {
        child->addToAllNodes(t,existingNodes);
    }
    add(t);
}

TreeNode *TreeNode::after(size_t index) const {
    auto pos = children.find(index);

    if (pos != children.end()) {
        return pos->second.get();
    }

    return nullptr;
}

TreeNode *TreeNode::after(const Trace &t) {
    return after(t.begin(), t.end());
}

TreeNode *TreeNode::after(Trace::const_iterator start, Trace::const_iterator end) {
    if (start != end) {
        // get current index and increment iterator
        size_t index = *start;

        // look for a node with that index
        auto pos = children.find(index);
        if (pos != children.end()) {
            // Follow the rest of the trace
            return pos->second->after(++start, end);
        }
        // Trace hasn't ended, but there is no node for the current index
        return nullptr;
    }

    // The trace has ended, this is the first node after following the trace.
    return this;
}

Trace TreeNode::getPath() const {
    Trace t;
    TreeNode* currentParent = this->parent;
    const TreeNode* current = this;
    while (currentParent != nullptr) {
        optional<size_t> idx = currentParent->getIndex(current);
        if ( idx.has_value() ) {
            t.prepend(idx.value());
        }
        else {
            throw runtime_error("ERROR idx = -1 in operation TreeNode::getPath()");
        }
        current = currentParent;
        currentParent = current->getParent();
    }
    return t;
}

Trace TreeNode::getPath(TreeNode const *subtreeRoot) const {
    Trace t;
    TreeNode* currentParent = this->parent;
    const TreeNode* current = this;
    while (current != subtreeRoot) {
        optional<size_t> idx = currentParent->getIndex(current);
        if ( idx.has_value() ) {
            t.prepend(idx.value());
        }
        else {
            throw runtime_error("ERROR idx = -1 in operation TreeNode::getPath()");
        }
        current = currentParent;
        currentParent = current->getParent();
    }
    return t;
}

optional<size_t> TreeNode::getIndex(const TreeNode *node) const {
    
    for ( const auto& p : children ) {
        if ( p.second.get() == node ) {
            return p.first;
        }
    }
    return nullopt;

}

TreeNode *TreeNode::getParent() const {
    return parent;
}

void TreeNode::traverseForTraces(Traces &traces, Trace const &path) const {
    // for each edge (x,t) add path.x to traces and
    // perform a recursive call on t with updated 
    // prefix path.x 
    for ( const auto& p : children ) {
        Trace newPrefix(path);
        newPrefix.append(p.first);
        traces.push_back(newPrefix);
        p.second->traverseForTraces(traces,newPrefix);
    }
}

TreeNode* TreeNode::findByValue(optional<size_t> v) {
    
    if ( this->value.value() == v.value() ) return this;
    
    for ( const auto& p : children ) {
        TreeNode* n = p.second->findByValue(v);
        if ( n != nullptr ) return n;
    }
    
    return nullptr;
    
}

bool TreeNode::checkNodes() {
    
    for ( const auto& p : children ) {
        if ( this != p.second->getParent() ) {
            throw runtime_error("ERROR illegal parent pointer for TreeNode " + to_string(p.first));
        }
        p.second->checkNodes();
    }
    
    return true;
}

void TreeNode::toDot(ofstream& outStream) {
    
    outStream << id << R"([fontname="helvetica",label=")" << id << " value = " ;
    if(value.has_value()) {
        outStream << *value;
    } else {
        outStream << -1;
    }
    outStream << "\"];" << endl;
    
    for ( const auto& p : children ) {
        outStream << id << " -> " << p.second->getNodeId()
        << R"([fontname="helvetica",label=")"<< p.first << "\"];" << endl;
        outStream << p.second->getNodeId() << " -> " << id << ";" << endl;
    }
    
    for ( const auto& p : children ) {
        p.second->toDot(outStream);
    }
    
}

void TreeNode::applyRecursively(const std::function<void (libfsmtest::TreeNode *)>& f, const std::set<TreeNode*>& existingNodes) {

    // Do not traverse new nodes
    if ( existingNodes.find(this) == existingNodes.end() ) return;
    
    for (const auto& [key, child] : children) {
        child->applyRecursively(f,existingNodes);
    }
    f(this);
}
void TreeNode::applyRecursively(const std::function<void (libfsmtest::TreeNode const*)>& f, const std::set<TreeNode const*>& existingNodes) const {

    // Do not traverse new nodes
    if ( existingNodes.find(this) == existingNodes.cend() ) return;
    
    for (const auto& [key, child] : children) {
        child->applyRecursively(f,existingNodes);
    }
    f(this);
}

std::vector<size_t> TreeNode::getLabelsOfOutgoingEdges() const {
    std::vector<size_t> result;
    result.reserve(children.size());
    for (const auto& [key, child] : children) {
        result.push_back(key);
    }
    return result;
}

 


} // namespace libfsmtest
