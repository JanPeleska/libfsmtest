/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <deque>
#include <queue>
#include <set>
#include <algorithm>

#include "StrongStateCountingMethod.hpp"
#include "fsm/Ofsm.hpp"
#include "fsm/Dfsm.hpp"
#include "fsm/Fsm.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "creators/transformers/ToObservableTransformer.hpp"
#include "creators/transformers/ToInitiallyConnectedTransformer.hpp"

using namespace std;

namespace libfsmtest { 

Index StrongStateCountingMethod::getTargetOfIOTrace(Ofsm& fsm, Index sourceId, IOTrace& trace) const {
    Index currentState = sourceId;

    auto inputIter = trace.input.begin();
    auto outputIter = trace.output.begin();

    auto transitionTable = fsm.getTransitionTable();

    for (; inputIter != trace.input.end() && outputIter != trace.output.end(); ++inputIter, ++outputIter) {
        Index x = *inputIter;
        Index y = *outputIter;

        currentState = transitionTable[currentState][x][y].value();
    }

    return currentState;
}

Tree StrongStateCountingMethod::sharedExtensions(Tree& tree, const Trace& t1, const Trace& t2) const {

    Tree result;

    std::queue<TreeNode*> todoAfter1;
    std::queue<TreeNode*> todoAfter2;
    std::queue<TreeNode*> todoTree;
    todoAfter1.push(tree.after(t1));
    todoAfter2.push(tree.after(t2));
    todoTree.push(result.getRoot());

    while (!todoAfter1.empty()) {
        auto after1 = todoAfter1.front();
        todoAfter1.pop();
        auto after2 = todoAfter2.front();
        todoAfter2.pop();
        auto node = todoTree.front();
        todoTree.pop();

        for (auto x1 : after1->getLabelsOfOutgoingEdges()) {
            auto x2s = after2->getLabelsOfOutgoingEdges();
            if ( std::find(x2s.begin(), x2s.end(), x1) == x2s.end() ) {
                // x1 is not a shared edge
                continue;
            }

            auto newNode = node->add(x1);
            auto newAfter1 = after1->after(x1);
            auto newAfter2 = after2->after(x1);

            todoAfter1.push(newAfter1);
            todoAfter2.push(newAfter2);
            todoTree.push(newNode);
        }
    }

    return result;
}




void StrongStateCountingMethod::calcDeterministicallyReachingSequences(Ofsm& fsm) {
    dReachingSequences = std::unordered_map<Index, Trace>();

    // create a copy of the fsm with an additional sink state
    States newStates;
    Index sinkId = fsm.getInitialStateId();
    for (auto& q : fsm.getStates()) {
        State s(q.getName(),q.getId());
        for (auto& t : q.getTransitions()) {
            // set all transition outputs to 0
            s.addTransition(t.getInput(),0,t.getTargetId());
        }
        newStates.push_back(s);

        if (q.getId() > sinkId) sinkId = q.getId();
    }
    sinkId += 1;
    newStates.emplace_back("_bot",sinkId);
    for (auto& s : newStates) {
        for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
            // complete s by adding transitions to the sink state
            if (!s.hasTransition(x)) {
                s.addTransition(x,0,sinkId);
            }
        }
    }

    auto m = make_unique<Fsm>("", newStates, fsm.getPresentationLayer(), fsm.getInitialStateId());
    
    // Make m initially connected
    if ( ! m->isInitiallyConnected() ) {
        auto fsmIc = transformFsm<ToInitiallyConnectedTransformer>(m.get());
        m = move(fsmIc);
    }

    // make m observable, which effectively determinises the automaton obtained by dropping the outputs, as all outputs are 0
    unique_ptr<Fsm> mObs = transformFsm<ToObservableTransformer>(m.get());

    // labels of states of mObs corresponding to singletons of states in fsm
    unordered_map<Index, string> node2AutomatonLabel;
    for ( auto& q : fsm.getStates()) {

        set<Index> expectedLabel;
        expectedLabel.insert(q.getId());
        string nodeName = fsm.labelString(expectedLabel);
        node2AutomatonLabel[q.getId()] = nodeName;
    }
        
    // get deterministically reaching sequences via breadth first search,
    // where a sequence reaching {q} in mObs d-reaches q in fsm
    std::queue<Index> todo;
    std::unordered_set<Index> visitedStates;
    std::queue<Trace> prevSequences;
    todo.push(mObs->getInitialStateId());
    prevSequences.push(Trace ());
    while (!todo.empty()) {
        State& curNode = mObs->getStates().at(todo.front());
        todo.pop();
        Trace dReachingSequence = prevSequences.front();
        prevSequences.pop();

        if (visitedStates.count(curNode.getId()) > 0) {
            continue;
        }
        visitedStates.insert(curNode.getId());

        // check if current node is a singleton
        for ( auto& q : fsm.getStates()) {
            if (curNode.getName() == node2AutomatonLabel[q.getId()]) {
                dReachingSequences[q.getId()] = dReachingSequence;
                break;
            } 
        }

        for (auto transition : curNode.getTransitions()) {
            todo.push(transition.getTargetId());

            Trace nextSeq(dReachingSequence);
            nextSeq.append(transition.getInput());
            prevSequences.push(nextSeq);
        }
        
    }
}


void StrongStateCountingMethod::calcRDistinguishingGraph(Ofsm& fsm) {

    rDistGraph = std::unordered_map<std::pair<Index,Index>, std::pair<std::optional<Index>,std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>>, IntPairHash, IntPairEqual>();

    auto maxState = fsm.getStates().size();

    std::vector<std::pair<Index,Index>> todo;
    std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>> emptyTargets = make_shared<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>();
    for (Index i = 0; i < maxState; ++i) {
        auto ni = fsm.getStates().at(i);
        auto definedInputsI = ni.getDefinedInputs();

        for (Index j = i+1; j < maxState; ++j) {
            auto nj = fsm.getStates().at(j);

            std::pair<Index, Index> pair = std::make_pair(ni.getId(),nj.getId());

            if (definedInputsI != nj.getDefinedInputs()) {
                // nodes are r(0)-distinguishable if their sets of defined inputs differ
                // thus no input needs to be applied (marked as std::nullopt)
                auto value = std::pair<std::optional<size_t>,std::shared_ptr<std::unordered_set<std::pair<size_t,size_t>, IntPairHash, IntPairEqual>>>(std::nullopt,emptyTargets);
                rDistGraph[pair] = value;

                // also add reversed pair
                std::pair<Index, Index> pairR = std::make_pair(nj.getId(),ni.getId());
                rDistGraph[pairR] = value;
            } else {
                todo.push_back(pair);
            }
        }
    }

    bool reachedFixpoint = false;

    while (!reachedFixpoint) {
        // to be set to false again if result is updated in this iteration
        reachedFixpoint = true;

        std::vector<std::pair<Index,Index>> todoNext;

        for (std::pair<Index, Index> nodePair : todo) {            

            if (rDistGraph.count(nodePair) == 1) {
                // skip pairs that have already been added
                continue;
            }

            auto n1 = fsm.getStates().at(nodePair.first);
            auto n2 = fsm.getStates().at(nodePair.second);
            
            for (Index x : n1.getDefinedInputs()) {
                std::unordered_set<Index> outputs1 = n1.getResponsesToInput(x);
                
                std::unordered_set<Index> sharedOutputs;
                for (Index y : n2.getResponsesToInput(x)) {
                    if (outputs1.count(y) > 0) {
                        sharedOutputs.insert(y);
                    }
                }

                auto targets = make_shared<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>();

                bool distinguishesAllSharedOutputs = true;
                for (Index y : sharedOutputs) {

                    // as x is defined in both nodes (as they are in todo and hence not r(0)-distinguishable)
                    // and y is a shared output, both nodes must exhibit a transition for x/y
                    Index t1;
                    for (auto& t : n1.getTransitions()) {
                        if (t.getInput() == x && t.getOutput() == y) {
                            t1 = t.getTargetId();
                            break;
                        }
                    }
                    Index t2;
                    for (auto& t : n2.getTransitions()) {
                        if (t.getInput() == x && t.getOutput() == y) {
                            t2 = t.getTargetId();
                            break;
                        }
                    }

                    auto targetPair = std::pair<Index,Index>(t1,t2); 
                    targets->insert(targetPair);

                    if (rDistGraph.count(targetPair) == 0) {
                        distinguishesAllSharedOutputs = false;
                        break;
                    }
                }

                if (distinguishesAllSharedOutputs) {
                    // the pair can be distinguished by applying x as first input
                    rDistGraph[nodePair] = std::pair<std::optional<Index>,std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>>(x, targets);
                    // also add reversed pair
                    auto pairR = std::pair<Index,Index>(n2.getId(),n1.getId());
                    rDistGraph[pairR] = std::pair<std::optional<Index>,std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>>(x, targets);

                    // result has been updated
                    reachedFixpoint = false;
                    break;
                } else {
                    todoNext.push_back(nodePair);
                }
            }
        }
        todo = todoNext;
    }
}



void StrongStateCountingMethod::calcRDistinguishingTrees(Ofsm& fsm) {
    calcRDistinguishingGraph(fsm);
    rDistTrees = std::unordered_map<std::pair<Index,Index>, std::shared_ptr<Tree>, IntPairHash, IntPairEqual>();
    
    std::function<std::shared_ptr<Tree>(std::pair<Index,Index>)> helper = [this,&helper](std::pair<Index,Index> pair)->std::shared_ptr<Tree> {
        auto res = make_shared<Tree>();
        auto entry = rDistGraph.find(pair);
        optional<Index> x = entry->second.first;
        auto targets = entry->second.second;

        if (not x.has_value()) {
            return res;
        }
 
        if (targets->empty()) {
            Trace tr;
            tr.append(x.value());
            res->add(tr);
        }

        for (auto targetPair : *targets) {
            auto targetTree = helper(targetPair);
            for (auto trace : targetTree->getTestCases()) {
                trace.prepend(x.value());   
                res->add(trace);
            }
        }

        return res;
    };

    for (const auto& entry : rDistGraph) {
        rDistTrees[entry.first] = helper(entry.first);
    }
}

void StrongStateCountingMethod::calcMaximalRDistinguishableSets(Ofsm& fsm) {
    maximalRDistSets = std::unordered_set<std::unordered_set<Index>, IntSetHash>();

    for (auto& node : fsm.getStates()) {
        std::unordered_set<Index> set;
        set.insert(node.getId());

        bool fixpointReached = false;
        while (!fixpointReached) {
            fixpointReached = true;
            for (const auto& otherNode : fsm.getStates()) {
                if (set.count(otherNode.getId()) != 0) continue;

                bool isRdFromAllCurrentNodes = true;
                for (Index nodeToBeRD : set) {
                    auto pair = std::pair<Index,Index>(nodeToBeRD,otherNode.getId());
                    if (rDistTrees.count(pair) == 0) {
                        isRdFromAllCurrentNodes = false;
                        break;
                    }
                }
                if (isRdFromAllCurrentNodes) {
                    set.insert(otherNode.getId());
                    fixpointReached = false;
                }            
            }
        }

        maximalRDistSets.insert(set);
    }
}

void StrongStateCountingMethod::calcAllMaximalRDistinguishableSets(Ofsm& fsm) {
    maximalRDistSets = std::unordered_set<std::unordered_set<Index>, IntSetHash>();

    std::function<std::unordered_set<std::unordered_set<Index>, IntSetHash>(size_t)> helper = [this,&fsm,&helper](size_t n)->std::unordered_set<std::unordered_set<Index>, IntSetHash> {
        if (n == 0) {
            std::unordered_set<std::unordered_set<Index>, IntSetHash> result;
            std::unordered_set<Index> emptySet;
            std::unordered_set<Index> singleton;
            singleton.insert(fsm.getStates().at(n).getId());
            result.insert(singleton);
            result.insert(emptySet);
            return result;
        }

        auto node = fsm.getStates().at(n);
        auto sets = helper(n-1);

        auto result = std::unordered_set<std::unordered_set<Index>, IntSetHash>(sets);
        for (const auto& set : sets) {
            bool isRdFromAllCurrentNodes = true;
            for (auto otherNode : set) {
                auto pair = std::pair<Index,Index>(node.getId(),otherNode);
                if (rDistTrees.count(pair) == 0) {
                    isRdFromAllCurrentNodes = false;
                    break;
                }
                           
            }
            if (isRdFromAllCurrentNodes) {
                auto setWithNode = std::unordered_set<Index>(set);
                setWithNode.insert(node.getId());
                result.insert(setWithNode);
            } 
        }
        return result;
    };

    auto candidates = helper(fsm.getStates().size()-1);

    for (const auto& candidate : candidates) {
        bool isMaximal = true;
        for (const auto& otherCandidate : candidates) {
            if (candidate == otherCandidate) continue;
            
            bool isProperSubset = true;
            for (auto elem : candidate) {
                if (otherCandidate.count(elem) == 0) {
                    isProperSubset = false;
                    break;
                }
            }
            if (isProperSubset) {
                isMaximal = false;
                break;
            }
        }
        if (isMaximal) {
            maximalRDistSets.insert(candidate);
        }
    }
}





std::unordered_map<std::pair<Index,Index>, std::shared_ptr<Tree>, IntPairHash, IntPairEqual> StrongStateCountingMethod::getRDistinguishingTrees() const {
    return rDistTrees;
}

std::unordered_map<Index, Trace> StrongStateCountingMethod::getDeterministicallyReachingSequences() const {
    return dReachingSequences;
}

std::unordered_map<std::pair<Index,Index>, std::pair<std::optional<Index>,std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>>, IntPairHash, IntPairEqual> StrongStateCountingMethod::getRDistGraph() const {
    return rDistGraph;
}

std::unordered_set<std::unordered_set<Index>, IntSetHash> StrongStateCountingMethod::getMaximalRDistinguishableSets() {
    return maximalRDistSets;
}

std::vector<std::pair<std::shared_ptr<std::unordered_set<Index>>,ssize_t>> StrongStateCountingMethod::getTerminationTuples(size_t m) {
    std::vector<std::pair<std::shared_ptr<std::unordered_set<Index>>,ssize_t>> result;

    for (const auto& rdSet : maximalRDistSets) {
        size_t dr = 0;
        for (auto node : rdSet) {
            if (dReachingSequences.count(node) != 0) {
                ++ dr;
            }
        }
        result.emplace_back(make_shared<std::unordered_set<Index>>(rdSet),m-dr+1);
    }

    return result;
}


std::vector<std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<Index>>>>> StrongStateCountingMethod::calcTraversalSet(Ofsm& fsm, Index node, size_t m) {

    std::vector<std::pair<std::shared_ptr<std::unordered_set<Index>>,ssize_t>> terminationTuples = getTerminationTuples(m);
    std::vector<ssize_t> initialMissingVisits;
    for (auto & terminationTuple : terminationTuples) {
        initialMissingVisits.push_back(terminationTuple.second);
    }

    std::function<std::vector<std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<Index>>>>>(Index, std::vector<ssize_t>)> helper = [&fsm,&terminationTuples,&helper](Index nodeInLambda, std::vector<ssize_t> missingVisits)->std::vector<std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<Index>>>>> {

        std::vector<std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<Index>>>>> resultInLambda;
        std::vector<std::shared_ptr<std::unordered_set<Index>>> terminatingSets;
        for (size_t i = 0; i < missingVisits.size(); ++i) {
            if (missingVisits[i] <= 0) {
                terminatingSets.push_back(terminationTuples[i].first);
            }
        }

        // terminate if all required visits have been performed for some set
        if (!terminatingSets.empty()) {
            IOTrace emptyTrace;
            emptyTrace.input = Trace();
            emptyTrace.output = Trace();
            std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<Index>>>> singleEntry = std::make_pair(emptyTrace,terminatingSets);
            resultInLambda.push_back(singleEntry);
            return resultInLambda;
        }

        for (auto& transition : fsm.getStates().at(nodeInLambda).getTransitions()) {
            // update visits based on current target
            std::vector<ssize_t> nextMissingVisits;
            for (size_t i = 0; i < missingVisits.size(); ++i) {
                if (terminationTuples[i].first->count(transition.getTargetId()) == 0) {
                    nextMissingVisits.push_back(missingVisits[i]);
                } else {
                    nextMissingVisits.push_back(missingVisits[i]-1);
                }
            }

            auto targetResult = helper(transition.getTargetId(), nextMissingVisits);
            for (const auto& entry : targetResult) {
                // prepend results for the transition target by the transitions IO
                IOTrace curTrace = IOTrace(entry.first);
                curTrace.input.prepend(transition.getInput());
                curTrace.output.prepend(transition.getOutput());
                resultInLambda.emplace_back(curTrace,entry.second);
            }
        }
        return resultInLambda;
    };

    return helper(node,initialMissingVisits);
}


bool StrongStateCountingMethod::isRDistinguishedBy(Ofsm& fsm, const Index qi, const Index qj, const TreeNode* w) const {

    auto thisNode = fsm.getStates().at(qi);
    auto otherNode = fsm.getStates().at(qj);

    unordered_set<Index> thisDefinedInputs = thisNode.getDefinedInputs();

    // nodes are r(0)-distinguishable if their sets of defined inputs differ
    if (thisDefinedInputs != otherNode.getDefinedInputs()) {
        return true;
    }

    // nodes are r(1)-distinguished by an input on which they share no output
    for (Index x : w->getLabelsOfOutgoingEdges()) {
        if (thisDefinedInputs.find(x) == thisDefinedInputs.end()) {
            continue; // encountered an input not defined in either node, which
                      // hence trivially has no shared response but is also not
                      // useful in distinguishing the nodes as it cannot be 
                      // practically applied -> it is hence discarded
        }
        std::unordered_set<Index> thisOutputs = thisNode.getResponsesToInput(x);
        
        bool noSharedOutputs = true;
        for (Index y : otherNode.getResponsesToInput(x)) {
            if (thisOutputs.find(y) != thisOutputs.end()) {
                noSharedOutputs = false;
                break;
            }
        }

        if (noSharedOutputs) {
            return true;
        }
    }

    // nodes are r(k+1)-distinguishable if they share an input x such that any shared response y
    // to x reaches a pair of states that are r(k)-distinguishable.
    for (Index x : w->getLabelsOfOutgoingEdges()) {
        if (thisDefinedInputs.find(x) == thisDefinedInputs.end()) {
            continue; 
        }
        std::unordered_set<Index> thisOutputs = thisNode.getResponsesToInput(x);
        
        std::unordered_set<Index> sharedOutputs;
        for (Index y : otherNode.getResponsesToInput(x)) {
            if (thisOutputs.find(y) != thisOutputs.end()) {
                sharedOutputs.insert(y);
            }
        }

        bool distinguishesAllSharedOutputs = true;
        for (Index y : sharedOutputs) {
            // as x is defined in both nodes (as the are not r(0)-distinguishable)
            // and y is a shared output, both nodes must exhibit a transition for x/y
            Index thisTarget = fsm.getTransitionTable()[qi][x][y].value();
            Index otherTarget = fsm.getTransitionTable()[qj][x][y].value();

            if (! (isRDistinguishedBy(fsm,thisTarget,otherTarget,w->after(x)))) {
                // the inputs of w following input x are not sufficient to r-distinguish 
                // the nodes reached via x/y and hence input x need not be considered further
                distinguishesAllSharedOutputs = false;
                break;
            }
        }

        if (distinguishesAllSharedOutputs) {
            return true;
        }
    }

    return false;
}




std::shared_ptr<Tree> StrongStateCountingMethod::augmentToRDistSet(Ofsm& fsm, Index n1, Index n2, const TreeNode* currentlyAppliedSequences) {
    std::shared_ptr<Tree> result = make_shared<Tree>();

    // do nothing if the currently applied sequences are already sufficient to r-distinguish n1 and n2
    if (isRDistinguishedBy(fsm,n1,n2,currentlyAppliedSequences)) {
        return result;
    } 

    // this implementation currently uses no heuristic, just applied the pre-calculated r-dist set for n1 and n2
    return rDistTrees[std::make_pair(n1,n2)];
}


void StrongStateCountingMethod::initialiseTestSuiteTree(Ofsm& fsm, size_t m) {
    testCaseTree = std::make_unique<Tree>();

    for (const auto& drEntry : dReachingSequences) {
        Index node = drEntry.first;
        auto v = drEntry.second;

        testCaseTree->add(v);

        auto travSet = calcTraversalSet(fsm,node,m);

        for (auto& trp : travSet) {
            auto afterV = testCaseTree->after(v);
            afterV->add(trp.first.input);
        }
    }
}

void StrongStateCountingMethod::updateTestSuite(Ofsm& fsm, const Index node, const std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<size_t>>>>& nextElementOfD, Tree& currentTestSuite) {
    // this implementation performs no heuristic in choosing which elements to consider
    auto trace = nextElementOfD.first;
    // nextElementOfD.second is not empty by assumption
    auto rdSet = nextElementOfD.second.back(); 
    
    auto vTrace = dReachingSequences[node];

    std::unordered_set<Index> drrdNodes; // nodes in the rdSet that are also d-reachable
    for (const auto& drEntry : dReachingSequences) {
        if (rdSet->count(drEntry.first) != 0) {
            drrdNodes.insert(drEntry.first);
        }
    }

    // check all pairs of prefixes of trace and the statecover
    // first consider all prefixes of trace against its proper prefixes and against the state cover
    // note: prefixes of trace need to be applied AFTER the d-reaching sequence of node
    
    auto inputPrefixes = trace.input.getNonemptyPrefixes();
    auto outputPrefixes = trace.output.getNonemptyPrefixes();
    inputPrefixes.emplace_back(); // add empty sequence to the prefix list
    outputPrefixes.emplace_back();
    for (size_t i = 0; i < inputPrefixes.size(); ++i) {
        IOTrace trace1;
        trace1.input = inputPrefixes[i];  
        trace1.output = outputPrefixes[i];  

        Trace preTrace1(trace1.input);
        preTrace1.prepend(vTrace);      

        // as trace is assumed to be in the language of node and trace1
        // is a proper prefix of trace, trace1 is also in the language of node
        Index target1 = getTargetOfIOTrace(fsm,node,trace1); 

        if (rdSet->count(target1) == 0) {
            continue; // skip transitions that reach states not in the rdSet 
        }; 

        for (size_t j = i+1; j < inputPrefixes.size(); ++j) {
            IOTrace trace2;
            trace2.input = inputPrefixes[j];  
            trace2.output = outputPrefixes[j];  
            
            // analogous to trace1, trace2 is also in the language of node
            // and hence a target state is reached
            Index target2 = getTargetOfIOTrace(fsm,node,trace2);

            Trace preTrace2(trace2.input);
            preTrace2.prepend(vTrace);

            if (target1 == target2 || rdSet->count(target2) == 0) {
                // reaches same target as trace1 or a non-rd target
                continue;
            }

            auto sharedTraceExtensions = sharedExtensions(currentTestSuite, preTrace1, preTrace2);

            auto testSuiteExtension = augmentToRDistSet(fsm, target1, target2, sharedTraceExtensions.getRoot());

            for (const auto& tr : testSuiteExtension->getTestCases()) {
                currentTestSuite.after(preTrace1)->add(tr);
                currentTestSuite.after(preTrace2)->add(tr);
            }
        }

        for (Index drrdNode : drrdNodes) {
            auto drTrace = dReachingSequences[drrdNode];

            if (target1 == drrdNode) {
                continue;
            }            

            auto sharedTraceExtensions = sharedExtensions(currentTestSuite, preTrace1, drTrace);
            
            auto testSuiteExtension = augmentToRDistSet(fsm, target1, drrdNode, sharedTraceExtensions.getRoot());
            
            for (const auto& tr : testSuiteExtension->getTestCases()) {
                currentTestSuite.after(preTrace1)->add(tr);
                currentTestSuite.after(drTrace)->add(tr);
            }
        }
    }

    // second consider pairs of sequences in the state cover
    // translating drrdNodes to a vector to avoid mirrored comparisons
    std::vector<Index> drrdNodesVector(drrdNodes.begin(), drrdNodes.end());
    for (size_t i = 0; i < drrdNodesVector.size()-1; ++i) {
        Index drrdNode1 = drrdNodesVector[i];
        auto drTrace1 = dReachingSequences[drrdNode1];

        for (size_t j = i+1; j < drrdNodesVector.size(); ++j) {
            Index drrdNode2 = drrdNodesVector[j];
            auto drTrace2 = dReachingSequences[drrdNode2];

            if (drrdNode1 == drrdNode2) {
                continue;
            }            

            auto sharedTraceExtensions = sharedExtensions(currentTestSuite, drTrace1, drTrace2);

            auto testSuiteExtension = augmentToRDistSet(fsm, drrdNode1, drrdNode2, sharedTraceExtensions.getRoot());

            for (const auto& tr : testSuiteExtension->getTestCases()) {
                currentTestSuite.after(drTrace1)->add(tr);
                currentTestSuite.after(drTrace2)->add(tr);
            }
        }
    }
}


void StrongStateCountingMethod::generateTestSuite(Ofsm& fsm, size_t m) {

    calcDeterministicallyReachingSequences(fsm);
    calcRDistinguishingTrees(fsm);
    if (calculateAllMaximalRDistinguishableSets) {
        calcAllMaximalRDistinguishableSets(fsm);
    } else {
        calcMaximalRDistinguishableSets(fsm);
    }

    initialiseTestSuiteTree(fsm,m);      

    // for all d-reachable states s ...
    for (const auto& drEntry : dReachingSequences) {
        Index node = drEntry.first;
        auto travSet = calcTraversalSet(fsm,node,m);

        // ... and all (s,trace,rdSets) in Tr(s,m) ...
        for (const auto& trEntry : travSet) {

            // ... update the test suite via on-the-fly extension with r-distinguishing sets

            // note that testCaseTree has been initialised with initialiseTestSuite(fsm, m)
            // and that trEntry is computed via calcTraversalSet on node, thus satisfying
            // both assumptions on updateTestSuite
            updateTestSuite(fsm, node, trEntry, *testCaseTree);
        }
    }
}




void StrongStateCountingMethod::visit(Fsm &fsm) {

    // the strong state counting method cannot be applied
    // to non-obersvable and the classical methods of
    // transforming the fsm into an observable FSM do
    // not in general preserve strong reduction
    if (!fsm.isObservable())
        throw runtime_error("Strong state counting cannot be applied to non-observable FSMs.");
    
    
    // First, a transformation to prime machine is needed in any case
    unique_ptr<Fsm> primeFsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
    
    // Now visit the prime machine
    primeFsm->accept(*this);    
}



void StrongStateCountingMethod::visit(Ofsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        size_t m = fsm.getStates().size() + numAddStates;
        generateTestSuite(fsm, m);
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime OFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");

        // Now visit the prime OFSM
        minimisedOfsm->accept(*this);        
    }    
}

void StrongStateCountingMethod::visit(Dfsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        size_t m = fsm.getStates().size() + numAddStates;
        generateTestSuite(fsm, m);
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime DFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
                
        // Now visit the prime DFSM
        minimisedOfsm->accept(*this);        
    }    
}


} // namespace libfsmtest
