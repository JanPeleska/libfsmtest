/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <random>
#include <deque>
#include <map>

#include <iostream>

#include "creators/transformers/ToMinimisedTransformer.hpp"
#include "FsmTraits.hpp"

namespace libfsmtest {

template<typename Machine>
class Generator {
private:
    /**
     * To be specizalized for FSM, OFSM and DFSM to implement random generation.
     * @see Generator::generate
     * @return The generated FSM
     */
    static std::unique_ptr<Fsm> specializedGenerate(PresentationLayer, const std::string&, FsmTraits, size_t);

public:
    /**
     * Generates a new FSM according to the specified type Machine
     * and respecting the given traits.
     * @param presentationLayer The PresentationLayer to be used in the generated FSM. Defines the number of states
     * @param fsmName The name for the generated FSM.
     * @param traits The traits the generated FSM should fulfill. See FsmTrait
     * @param seed The seed the random generation is based on. Can be used to recreate the same generation.
     * @return The generated FSM.
     */
    static std::unique_ptr<Fsm> generate(PresentationLayer presentationLayer, const std::string& fsmName, FsmTraits traits, size_t seed = std::random_device()()) {
        std::unique_ptr<Fsm> fsm = specializedGenerate(std::move(presentationLayer), fsmName, traits, seed);

        if (traits.minimal()) {
            fsm = transformFsm<ToMinimisedTransformer>(fsm.get());
        }

        return fsm;
    }
};

template<>
std::unique_ptr<Fsm> Generator<Fsm>::specializedGenerate(PresentationLayer presentationLayer, const std::string& fsmName, FsmTraits traits, size_t seed) {
    Index numberOfStates = presentationLayer.stateNames.size();
    Index numberOfInputs = presentationLayer.inputAlphabet.size();
    Index numberOfOutputs = presentationLayer.outputAlphabet.size();

    std::cerr << seed << std::endl;
    std::mt19937_64 gen(seed);
    std::uniform_int_distribution<Index> distributeOutputs(0, numberOfOutputs - 1);
    std::uniform_int_distribution<Index> distributeStates(0, numberOfStates - 1);
    std::uniform_int_distribution<Index> distributeInputs(0, numberOfInputs - 1);

    States states;
    states.reserve(numberOfStates);

    std::vector<bool> reachedStates(numberOfStates);
    reachedStates[0] = true;

    for (Index source = 0; source < numberOfStates; ++source) {
        states.emplace_back(std::to_string(source), source);

        Index inputsToAdd = traits.completely_specified() ? numberOfInputs : distributeInputs(gen) + 1;
        std::vector<bool> definedInputs(numberOfInputs);

        if (traits.initially_connected()) {
            // Use the next ID of a state as first candidate
            Index target = source + 1;
            // Find the next, not yet reached state.
            // All previous states have already been reached by now.
            while (target != numberOfStates && reachedStates[target]) {
                target = target + 1;
            }
            // Only add a transition if a new state has to be reached.
            if (target != numberOfStates) {
                Index input = distributeInputs(gen);
                Index output = distributeOutputs(gen);

                states.at(source).getTransitions().emplace_back(
                    source, input, output, target);

                reachedStates[target] = true;
                definedInputs[input] = true;
                inputsToAdd--;
            }
        }

        for (Index j = 0; j < inputsToAdd; ++j) {
            Index input = distributeInputs(gen);
            // This is ran at most numberOfInputs times,
            // therefore there has to be one input that is not yet defined.
            while (definedInputs[input]) {
                // input is already defined, take another one
                input = (input + 1) % numberOfInputs;
            }

            for (Index i = 0; i <= distributeOutputs(gen); ++i) {
                Index output = distributeOutputs(gen);
                Index target = distributeStates(gen);

                states.at(source).getTransitions().emplace_back(
                    source, input, output, target);

                reachedStates[target] = true;
            }
            definedInputs[input] = true;
        }
    }

    return std::make_unique<Fsm>(fsmName, std::move(states), std::move(presentationLayer), 0);
};

template<>
std::unique_ptr<Fsm> Generator<Ofsm>::specializedGenerate(PresentationLayer presentationLayer, const std::string& fsmName, FsmTraits traits, size_t seed) {
    Index numberOfStates = presentationLayer.stateNames.size();
    Index numberOfInputs = presentationLayer.inputAlphabet.size();
    Index numberOfOutputs = presentationLayer.outputAlphabet.size();

    std::mt19937_64 gen(seed);
    std::uniform_int_distribution<Index> distributeOutputs(0, numberOfOutputs - 1);
    std::uniform_int_distribution<Index> distributeStates(0, numberOfStates - 1);
    std::uniform_int_distribution<Index> distributeInputs(0, numberOfInputs - 1);

    States states;
    states.reserve(numberOfStates);

    std::vector<bool> reachedStates(numberOfStates);
    reachedStates[0] = true;

    for (Index source = 0; source < numberOfStates; ++source) {
        states.emplace_back(std::to_string(source), source);

        Index inputsToAdd = traits.completely_specified() ? numberOfInputs : distributeInputs(gen) + 1;

        std::vector<bool> definedInputs(numberOfInputs);
        std::vector<std::vector<bool>> definedOutputs(numberOfInputs, std::vector<bool>(numberOfOutputs));

        if (traits.initially_connected()) {
            // Use the next ID of a state as first candidate
            Index target = source + 1;
            // Find the next, not yet reached state.
            // All previous states have already been reached by now.
            while (target != numberOfStates && reachedStates[target]) {
                target = target + 1;
            }
            // Only add a transition if a new state has to be reached.
            if (target != numberOfStates) {
                Index input = distributeInputs(gen);
                Index output = distributeOutputs(gen);

                states.at(source).getTransitions().emplace_back(
                    source, input, output, target);

                definedInputs[input] = true;
                definedOutputs[input][output] = true;

                reachedStates[target] = true;
                inputsToAdd--;
            }
        }

        for (Index j = 0; j < inputsToAdd; ++j) {
            Index input = distributeInputs(gen);
            // This is ran at most numberOfInputs times,
            // therefore there has to be one input that is not yet defined.
            while (definedInputs[input]) {
                // input is already defined, take another one
                input = (input + 1) % numberOfInputs;
            }

            for (Index i = 0; i <= distributeOutputs(gen); ++i) {
                Index output = distributeOutputs(gen);
                while (definedOutputs[input][output]) {
                    // output is already defined for this input, take another one
                    output = (output + 1) % numberOfOutputs;
                }

                Index target = distributeStates(gen);

                states.at(source).getTransitions().emplace_back(
                    source, input, output, target);

                definedOutputs[input][output] = true;
                reachedStates[target] = true;
            }
            definedInputs[input] = true;
        }
    }

    return std::make_unique<Ofsm>(fsmName, std::move(states), std::move(presentationLayer), 0);
}

template<>
std::unique_ptr<Fsm> Generator<Dfsm>::specializedGenerate(PresentationLayer presentationLayer, const std::string& fsmName, FsmTraits traits, size_t seed) {
    Index numberOfStates = presentationLayer.stateNames.size();
    Index numberOfInputs = presentationLayer.inputAlphabet.size();
    Index numberOfOutputs = presentationLayer.outputAlphabet.size();

    std::mt19937_64 gen(seed);
    std::uniform_int_distribution<Index> distributeOutputs(0, numberOfOutputs - 1);
    std::uniform_int_distribution<Index> distributeStates(0, numberOfStates - 1);
    std::uniform_int_distribution<Index> distributeInputs(0, numberOfInputs - 1);

    States states;
    states.reserve(numberOfStates);

    std::vector<bool> reachedStates(numberOfStates);
    reachedStates[0] = true;

    for (Index source = 0; source < numberOfStates; ++source) {
        states.emplace_back(std::to_string(source), source);

        Index transitionsToAdd = traits.completely_specified() ? numberOfInputs : distributeInputs(gen) + 1;

        std::vector<bool> definedInputs(numberOfInputs);

        if (traits.initially_connected()) {
            // Use the next ID of a state as first candidate
            Index target = source + 1;
            // Find the next, not yet reached state.
            // All previous states have already been reached by now.
            while (target != numberOfStates && reachedStates[target]) {
                target = target + 1;
            }
            // Only add a transition if a new state has to be reached.
            if (target != numberOfStates) {
                Index input = distributeInputs(gen);
                Index output = distributeOutputs(gen);

                states.at(source).getTransitions().emplace_back(
                    source, input, output, target);

                definedInputs[input] = true;
                reachedStates[target] = true;
                transitionsToAdd--;
            }
        }

        for (Index j = 0; j < transitionsToAdd; ++j) {
            Index input = distributeInputs(gen);
            // This runs at most numberOfInputs times,
            // therefore there has to be one input that is not yet defined.
            while (definedInputs[input]) {
                // input is already defined, take another one
                input = (input + 1) % numberOfInputs;
            }

            Index target = distributeStates(gen);
            Index output = distributeOutputs(gen);

            states.at(source).getTransitions().emplace_back(
                source, input, output, target);

            definedInputs[input] = true;
            reachedStates[target] = true;
        }
    }

    return std::make_unique<Dfsm>(fsmName, std::move(states), std::move(presentationLayer), 0);
}


} // namespace libfsmtest
