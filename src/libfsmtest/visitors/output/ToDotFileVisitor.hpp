/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <iostream>     // std::cout, std::ios
#include <sstream>      // std::ostringstream
#include <memory>

#include "ToFileVisitor.hpp"
#include "fsm/definitions.hpp"
#include "fsm/PresentationLayer.hpp"

namespace libfsmtest {


/**
 * Class for writing a DFSMs or FSMs to GraphViz format (also called dot-format)
 */
class ToDotFileVisitor : public ToFileVisitor {
private:

    std::ostringstream dotAsString;
    std::ostream *outputStream;
    
    void createDotAsString(Fsm& fsm);
    
    std::unique_ptr<PresentationLayer> presentationLayer;

public:
    /**
     * Constructor.
     * @see visit(Fsm&)
     * @see visit(Dfsm&)
     * @see writeToFile()
     * @param fileNameParam The name of the file to write to.
     */
    ToDotFileVisitor(const std::string& fileNameParam) 
        : outputStream(nullptr) {
        this->fileName = fileNameParam;
    }

    ToDotFileVisitor(std::ostream &outStream) 
        : outputStream(&outStream) { }

    /*
     * Produce dot file for FSM
     * Starts gathering data of a FSM.
     * Only called indirectly.
     * @see Fsm::accept()
     */
    void visit(Fsm& fsm) override;
    
    /*
     * Produce dot file for OFSM
     * Starts gathering data of a OFSM.
     * Only called indirectly.
     */
    void visit(Ofsm& ofsm) override;
    /**
     * Starts gathering data of a DFSM.
     * Only called indirectly. This uses the
     * same method createDotAsString() as the Fsm-visit method.
     * @param dfsm The DFSM to gather data from.
     */
    void visit(Dfsm& dfsm) override;
    /**
     * Starts gathering data of a State.
     * Only called indirectly.
     * @see State::accept()
     * @param state The State to gather data from.
     */
    void visit(State& state) override;
    /**
     * Starts gathering data of a Transition.
     * Only called indirectly.
     * @see Transition::accept()
     * @param transition The Transition to gather data from.
     */
    void visit(Transition& transition) override;

    /**
     * Writes out gathered data to the given file.
     * Has to be called after this visitor has been accepted on the DFSM.
     */
    void writeToFile() override;

};

} // namespace libfsmtest
