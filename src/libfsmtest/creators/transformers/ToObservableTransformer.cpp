/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <memory>
#include <vector>
#include <deque>
#include <algorithm>
#include <stdexcept>

#include "ToObservableTransformer.hpp"

using namespace std;

namespace libfsmtest {


void ToObservableTransformer::visit(Fsm& fsmToTransform) {
    
    if ( ! fsmToTransform.isInitiallyConnected() ) {
        throw runtime_error("ERROR: machine to be made observable must be initially connected.");
    }
    
    
    // List to be filled with the new states to be created
    // for the observable FSM
    States nodeLst;
    
    // Breadth first search list, containing the
    // new FSM node ids, still to be processed by the algorithm
    deque<Index> bfsLst;
    
    // Map id of a newly created node to the set of node ids from the
    // original FSM, comprised in the new FSM state
    map<Index, set<Index>> node2NodeLabel;
    
    // Set of node ids from the original FSM, comprised in the
    // current new node
    set<Index> theNodeLabel;
    
    // For the first step of the algorithm, the initial
    // state of the original FSM is the only state comprised
    // in the initial state of the new FSM
    theNodeLabel.insert(fsmToTransform.getInitialStateId());
    
    
    // Create a new presentation layer which has
    // the same names for the inputs and outputs as
    // the old presentation layer, but still an EMPTY vector
    // of node names.
    vector<string> obsState2String;
    PresentationLayer obsPl(
        fsmToTransform.getPresentationLayer().inputAlphabet,
        fsmToTransform.getPresentationLayer().outputAlphabet,
        obsState2String);
    
    // id to be taken for the next state of the new
    // observable FSM to be created.
    Index id = 0;
    
    // The initial state of the new FSM is labelled with
    // the set containing just the initial state of the old FSM
    string nodeName = fsmToTransform.labelString(theNodeLabel);
    
    nodeLst.emplace_back(nodeName,id);
    Index q0 = id++;
    
    bfsLst.push_back(q0);
    node2NodeLabel[q0] = theNodeLabel;
    
    // The node label is added to the presentation layer,
    // as name of the initial state
    obsPl.stateNames.push_back(nodeName);
    
    
    
    // Loop while there is at least one node in the BFS list.
    // Initially, the list contains just the new initial state
    // of the new FSM.
    while (!bfsLst.empty())
    {
        // Pop the first node from the list
        Index q = bfsLst.front();
        bfsLst.pop_front();
        
        Index maxInput = fsmToTransform.getPresentationLayer().inputAlphabet.size() - 1;
        Index maxOutput = fsmToTransform.getPresentationLayer().outputAlphabet.size() - 1;
        
        // Nested loop over all input/output labels that
        // might be associated with one or more outgoing transitions
        for (Index x = 0; x <= maxInput; ++ x)
        {
            for (Index y = 0; y <= maxOutput; ++ y)
            {
                
                // Clear the set of node labels that may
                // serve as node name for the target node to be
                // created (or already exists). This target node
                // comprises all nodes of the original FSM that
                // can be reached from q under a transition labelled
                // with lbl
                theNodeLabel.clear();
                
                // Loop over all node ids of the original FSM which
                // are comprised by q
                for (auto& n : node2NodeLabel.at(q))
                {
                    // For each node id comprised by q, check
                    // its outgoing transitions, whether they are
                    // labelled by lbl
                    for_each(fsmToTransform.getStates().at(n).getTransitions().cbegin(),
                             fsmToTransform.getStates().at(n).getTransitions().cend(),
                             [&](auto& tr) {
                        if ( tr.getInput() == x && tr.getOutput() == y ) theNodeLabel.insert(tr.getTargetId());
                    });
                }
                
                // Process only non-empty label sets.
                // An empty label set means that no transition labelled
                // by (x,y)) exists.
                if (!theNodeLabel.empty())
                {
                    
                    optional<Index> tgtNodeId;
                    
                    // Loop over the node-2-label map and check
                    // if there exists already a node labelled
                    // by theNodeLabel. If this is the case,
                    // it will become the target node reached from q
                    // under (x,y))
                    for ( auto& u : node2NodeLabel ) {
                        if ( u.second == theNodeLabel ) {
                            tgtNodeId = u.first;
                            break;
                        }
                    }
                    
                    if (not tgtNodeId.has_value())
                    {
                        // We need to create a new target node, to be reached
                        // from q under lbl
                        nodeName = fsmToTransform.labelString(theNodeLabel);
                        nodeLst.emplace_back(nodeName,id);
                        tgtNodeId = id++;
                        bfsLst.push_back(tgtNodeId.value());
                        node2NodeLabel[tgtNodeId.value()] = theNodeLabel;
                        obsPl.stateNames.push_back(nodeName);
                    }
                    
                    // Create the transition from q to tgtNode
                    nodeLst.at(q).getTransitions().emplace_back(q,x,y,tgtNodeId.value());
                }
            }
        }
    }
    
    
    transformedFsm = make_unique<Ofsm>(fsmToTransform.getName() + nameSuffix,
                                       nodeLst,
                                       move(obsPl),
                                       0 /* Initial states of connected machine has id 0 */);
    
    this->lastStateMapping = node2NodeLabel;
}


void ToObservableTransformer::visit(Ofsm& fsmToTransform) {
    
    // fsm is already observable - just create a copy without changes,
    // but add suffix to name.
    transformedFsm = make_unique<Ofsm>(fsmToTransform.getName() + nameSuffix,
                                       fsmToTransform.getStates(),
                                       fsmToTransform.getPresentationLayer(),
                                       fsmToTransform.getInitialStateId());
    
}



void ToObservableTransformer::visit(Dfsm& fsmToTransform) {
    
    // fsm is already observable - just create a copy without changes,
    // but add suffix to name.
    transformedFsm = make_unique<Dfsm>(fsmToTransform.getName() + nameSuffix,
                                       fsmToTransform.getStates(),
                                       fsmToTransform.getPresentationLayer(),
                                       fsmToTransform.getInitialStateId());
}

map<Index, set<Index>> const &ToObservableTransformer::getLastStateMapping() const {
    return this->lastStateMapping;
}

}
