/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <set>
#include <deque>
#include <iostream>
#include <cassert>
#include <utility>
#include <algorithm>

#include "Fsm.hpp"

using namespace std;


namespace libfsmtest {

Fsm::Fsm(string fsmName,
         States stateVec,
         const PresentationLayer& pl,
         Index initialState) :
name(std::move(fsmName)),
states(std::move(stateVec)),
presentationLayer(pl),
initialStateId(initialState) {
    
    if ( initialState >= states.size() )
        throw runtime_error("ERROR: " + name + "Initial state number too large.");
    
}

Index Fsm::getInitialStateId() const {
    return initialStateId;
}

bool Fsm::isObservable() const {
    return std::all_of(states.begin(), states.end(), [](const auto& s){
        return s.isObservable();
    });
}

bool Fsm::isDeterministic() const {
    return std::all_of(states.begin(), states.end(), [](const auto& s){
        return s.isDeterministic();
    });
}

bool Fsm::isCompletelySpecified() const {
    
    size_t inputAlphabetSize = presentationLayer.inputAlphabet.size();

    return std::all_of(states.begin(), states.end(), [inputAlphabetSize](const auto& s){
        return s.isCompletelySpecified(inputAlphabetSize);
    });
}


string Fsm::labelString(const set<Index>& lbl) const {
    
    string s = "{ ";
    
    bool isFirst = true;
    for (auto& n : lbl)
    {
        if (!isFirst)
        {
            s += ",";
        }
        isFirst = false;
        
        if ( states.at(n).getName() == to_string(states.at(n).getId()) ) {
            s += states.at(n).getName();
        }
        else {
            s += states.at(n).getName() + "(" + to_string(states.at(n).getId()) + ")";
        }
    }
    
    s += " }";
    return s;
    
}


void Fsm::resetStepping() {
    homingStates.clear();
    homingStates.insert(initialStateId);
}

bool Fsm::step(Index x, Index y, string& iotrace) {
    
    set<Index> newPostStates;
    bool inputIsDefined = false;
    
    for ( auto& id : homingStates ) {
        State& theState = states.at(id);
        for ( auto& tr : theState.getTransitions() ) {
            if ( tr.getInput() == x ) {
                inputIsDefined = true;
                if ( tr.getOutput() == y ) {
                    newPostStates.insert(tr.getTargetId());
                }
            }
        }
    }
    
    if ( ! iotrace.empty() ) {
        iotrace += ", ";
    }
    
    if ( newPostStates.empty() ) {
        if ( inputIsDefined ) {
            iotrace += presentationLayer.inputAlphabet.at(x)
            + "/ <ERROR>"
            + presentationLayer.outputAlphabet.at(y);
        }
        else {
            iotrace += "<UNDEFINED>" + presentationLayer.inputAlphabet.at(x);
        }
        return false;
    }
    
    homingStates.clear();
    homingStates.insert(newPostStates.begin(),newPostStates.end());
    iotrace += presentationLayer.inputAlphabet.at(x)
    + "/"
    + presentationLayer.outputAlphabet.at(y);
    
    return true;
    
}


bool Fsm::step(const string& xAsString, const string& yAsString, string& iotrace) {
    
    set<Index> newPostStates;
    bool inputIsDefined = false;
    
    auto x = presentationLayer.getInputId(xAsString);
    auto y = presentationLayer.getOutputId(yAsString);
    
    if ( ! iotrace.empty() ) {
        iotrace += ", ";
    }
    
    if ( not x.has_value() ) {
        iotrace += "<UNDEFINED>" + xAsString + "/ <ERROR>";
        return false;
    }
    if ( not y.has_value() ) {
        iotrace += xAsString + "/ <ERROR: UNDEFINED>" + yAsString;
        return false;
    }
    
    //FIXME: Isn't this a duplicate of Fsm::step(Index,Index,string&)?
    for ( auto& id : homingStates ) {
        State& theState = states.at(id);
        for ( auto& tr : theState.getTransitions() ) {
            if ( tr.getInput() == x.value() ) {
                inputIsDefined = true;
                if ( tr.getOutput() == y.value() ) {
                    newPostStates.insert(tr.getTargetId());
                }
            }
        }
    }

    if ( newPostStates.empty() ) {
        if ( inputIsDefined ) {
            iotrace += presentationLayer.inputAlphabet.at(x.value())
            + "/ <ERROR>"
            + presentationLayer.outputAlphabet.at(y.value());
        }
        else {
            iotrace += "<UNDEFINED>" + presentationLayer.inputAlphabet.at(x.value());
        }
        return false;
    }
    
    homingStates.clear();
    homingStates.insert(newPostStates.begin(),newPostStates.end());
    iotrace += presentationLayer.inputAlphabet.at(x.value())
    + "/"
    + presentationLayer.outputAlphabet.at(y.value());
    
    return true;
    
}

void Fsm::getIOTracesInternal(Index stateId,
                              Trace::const_iterator& ite,
                              Trace::const_iterator endIte,
                              Strings& ioTraces,
                              bool addTargetState) {
    string theNewTrace;
    
    // End of input trace reached ?
    if ( ite == endIte ) {
        --ite;
        theNewTrace = ioTraces.back();
        if ( ! theNewTrace.empty() ) {
            // Remove the last io for backtracking
            size_t pos = theNewTrace.rfind(',');
            if ( pos == string::npos ) {
                theNewTrace.clear();
            }
            else {
                theNewTrace.erase(pos);
            }
            ioTraces.push_back(theNewTrace);
        }
        return;
    }
    
    // Input trace has at least one element.
    // This is the state to which the input shall be applied
    State& thisState = states.at(stateId);
    
    // Check all transitions whether they are labelled by input *ite.
    // For nondeterministic FSMs, several transitions might fire on this input.
    // For partial FSMs, it could be the case that no
    // transition is labelled by input *ite.
    for ( auto& tr : thisState.getTransitions() ) {
        if ( *ite == tr.getInput() ) {
            Index y = tr.getOutput();
            if ( ! ioTraces.back().empty() ) {
                ioTraces.back() += ", ";
            }
            // Add IO in external representation
            string xAsString = (*ite < getInputAlphabetSize()) ? presentationLayer.inputAlphabet.at(*ite) : to_string(*ite);
            string yAsString = (y < getOutputAlphabetSize()) ? presentationLayer.outputAlphabet.at(y) : to_string(y);
            ioTraces.back() += xAsString + "/" + yAsString;
            
            if ( addTargetState ) {
                ioTraces.back() += " [" + states[tr.getTargetId()].getName() + "]";
            }
                    
            // From the transitions target state, proceed with the the next input (if any)
            getIOTracesInternal(tr.getTargetId(), ++ite, endIte, ioTraces, addTargetState);
            
            // Continue in depth-first-search manner with another transition
            // that may fire on the same input
        }
    }
    
    if ( ! ioTraces.back().empty() ) {
        size_t pos = ioTraces.back().rfind(',');
        if ( pos == string::npos ) {
            ioTraces.back().clear();
        }
        else {
            ioTraces.back().erase(pos);
        }
    }
    else {
        ioTraces.pop_back();
    }
    
    --ite;

}

void Fsm::getIOTraces(const Trace& inputTrace, Strings& ioTraces) {
    getIOTracesFromState(inputTrace, ioTraces, initialStateId);
}

void Fsm::getIOTracesFromState(const Trace& inputTrace, Strings& ioTraces, Index startState) {

    std::vector<IOTrace> traces = getIOTracesFromState(inputTrace, startState, true);

    const auto inputToString = [&](Index id){return presentationLayer.inputAlphabet.at(id);};
    const auto outputToString = [&](Index id){return presentationLayer.outputAlphabet.at(id);};

    for (const auto& trace : traces) {
        std::stringstream line;

        // Inputs and Outputs are appended together
        assert(trace.input.size() == trace.output.size());

        auto i = trace.input.cbegin();
        auto o = trace.output.cbegin();
        while (i != trace.input.cend() && o != trace.output.cend()) {
            bool isFirst = i == trace.input.cbegin();
            line << (isFirst ? "" : ", ")
                 << inputToString(*i)
                 << '/'
                 << outputToString(*o);

            ++i;
            ++o;
        }

        ioTraces.push_back(line.str());
    }
}

std::vector<IOTrace> Fsm::getIOTraces(const Trace& inputTrace, bool includeUndefined) const {
    return getIOTracesFromState(inputTrace, initialStateId, includeUndefined);
}

std::vector<IOTrace> Fsm::getIOTracesFromState(const Trace& inputTrace, Index startState, bool includeUndefined) const {
    // Container for the results
    std::vector<IOTrace> result;
    // List of states to handle now
    std::vector<std::tuple<State const*, IOTrace>> currentStates = { {&states.at(startState), {}}};
    // Iterating the input trace
    for(auto input : inputTrace) {
        // The states to handle the next input in
        std::vector<std::tuple<State const*, IOTrace>> nextStates;

        // For each state to handle in this iteration...
        for(auto const &[state, traceUpTillNow] : currentStates) {
            bool inputDefined = false;
            // ... iterate all transitions
            for(auto const &transition : state->getTransitions()) {
                // ... and check whether the input matches the symbol to handle now
                if(transition.getInput() == input) {
                    inputDefined = true;
                    // If it matches, create a new trace from the trace that
                    // led to this state and where the transition input and
                    // output are added to the back
                    IOTrace newTrace = traceUpTillNow;
                    newTrace.input.append(input);
                    newTrace.output.append(transition.getOutput());
                    // Then add the target state and the trace reaching that
                    // state to the list of states to handle in the next
                    // iteration of the input trace iteration loop
                    nextStates.emplace_back(make_tuple(&this->getStates().at(transition.getTargetId()), newTrace));
                }
            }
            if(includeUndefined and not inputDefined) {
                result.push_back(traceUpTillNow);
            }
        }

        currentStates = move(nextStates);
        nextStates.clear();
    }
    // Extract the traces
    transform(currentStates.begin(), currentStates.end(), back_inserter(result), [](auto &&elem) {
        return move(get<1>(elem));
    });
    return result;
    
}


set<Index> Fsm::getTargets(const Trace& inputTrace, Index fromState) const {
    
    set<Index> currentTargets;
    currentTargets.insert(fromState);

    for (auto x : inputTrace) {
        set<Index> nextTargets;
        for (auto q : currentTargets) {
            for (auto t : states.at(q).getTransitions()) {
                if (t.getInput() == x) {
                    nextTargets.insert(t.getTargetId());
                }
            }
        }
        currentTargets = nextTargets;
        if ( currentTargets.empty() ) break;
    }
    return currentTargets;
    
}


set<Index> Fsm::getTargets(const Trace& inputTrace) const {
    return getTargets(inputTrace,initialStateId);
}

size_t Fsm::getInputAlphabetSize() const {
    return presentationLayer.inputAlphabet.size();
}

size_t Fsm::getOutputAlphabetSize() const {
    return presentationLayer.outputAlphabet.size();
}

unique_ptr<Fsm> Fsm::clone() const {
    return make_unique<Fsm>(*this);
}

bool Fsm::isInitiallyConnected() const {
    
    set<Index> initiallyConnectedStates;
    
    // Initial state is always reachable
    initiallyConnectedStates.insert( initialStateId );
    
    // Breadth-first-search queue
    deque<Index> bfsQueue;
    bfsQueue.push_back(initialStateId);
    
    while ( ! bfsQueue.empty() ) {
        
        Index currentStateId = bfsQueue.front();
        bfsQueue.pop_front();
        const State& thisState = states.at(currentStateId);
        
        for ( const auto& tr : thisState.getTransitions() ) {
            Index targetStateId = tr.getTargetId();
            if ( initiallyConnectedStates.find(targetStateId) == initiallyConnectedStates.end() ) {
                initiallyConnectedStates.insert(targetStateId);
                bfsQueue.push_back(targetStateId);
            }
        }
        
    }
    
    // Initially connected iff cardinality of initiallyConnectedNodes
    // equals number of states
    return ( initiallyConnectedStates.size() == size() );
}

std::unique_ptr<Fsm> Fsm::modify(std::optional<std::string> fsmName,
                                 std::optional<States> newStates,
                                 std::optional<PresentationLayer> pl,
                                 std::optional<Index> newInitialStateId) const {
    if(not fsmName.has_value()) {
        fsmName = this->name;
    }
    if(not newStates.has_value()) {
        newStates = this->states;
    }
    if(not pl.has_value()) {
        pl = presentationLayer;
    }
    if(not newInitialStateId.has_value()) {
        newInitialStateId = this->initialStateId;
    }
    return make_unique<Fsm>(fsmName.value(), newStates.value(), move(pl.value()), newInitialStateId.value());
}

} // namespace libfsmtest
