/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "Mutator.hpp"
#include "fsm/Fsm.hpp"
#include "fsm/Ofsm.hpp"
#include "fsm/Dfsm.hpp"

namespace libfsmtest {

void Mutator::visit(Fsm &fsmToMutate) {
    // We cannot use Fsm::clone(), since adding a transition to a DFSM might result in an FSM.
    transformedFsm = std::make_unique<Fsm>(fsmToMutate.getName(),
                                           States(fsmToMutate.getStates().begin(), fsmToMutate.getStates().end()),
                                           fsmToMutate.getPresentationLayer(),
                                           fsmToMutate.getInitialStateId());

    for (size_t i = 0; i < mutationsToApply; ++i) {
        applyMutation();
    }

    transformedFsm = transformedFsm->clone();
}

void Mutator::visit(Ofsm &ofsm) {
    visit(static_cast<Fsm&>(ofsm));
}

void Mutator::visit(Dfsm &dfsm) {
    visit(static_cast<Fsm&>(dfsm));
}

} // namespace libfsmtest
