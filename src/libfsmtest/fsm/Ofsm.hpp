/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <cstdlib>
#include <memory>
#include <string>
#include <set>
#include <map>
#include <vector>

#include "fsm/Fsm.hpp"

namespace libfsmtest {

/**
 * Observable FSMs.
 */
class Ofsm : public Fsm {
    
protected:
    
    /**
     * 3-dimensional table created by constructor with dimensions
     *                 [number of states][size of input alphabet][size of output alphabet]
     *                 This table is used by calcOFSMTables(), isInitiallyConnected() and
     *                 other operations.
     * transitionTable[s][x][y] == s' contains the non-negative state number s' of the post state
     *                 of s under the transition labelled by x/y. If no such transition exists,
     *                 transitionTable[s][x][y] == std::nullopt
     */
    std::optional<Index>*** transitionTable;

public:
    
    /**
     * Low-level constructor - should only be used by
     * factory methods
     * @param fsmName Name of the FSM to be created
     * @param stateVec Vector of states (pred-defined with all
     *                 emanating transitions)
     * @param pl The presentation layer
     * @param initialState Number of the initial state (this equals
     *                     the index of the initial state in the
     *                     state vector) Runtime exception is thrown if
     *                     initialState is out of range.
     */
    Ofsm(const std::string& fsmName,
        States stateVec,
        PresentationLayer pl,
        Index initialState);
    
    
    
    /**
     * Copy constructor
     */
    Ofsm(const Ofsm& fsm);
    
    /** Virtual destructor */
    ~Ofsm() override;
    
    virtual bool isObservable() const { return true; }
    
    
    /**
     * Calculate the OSFM tables for this observable FSM. This method should be
     * applied on a minimised machine for calculating a characterisation set or
     * state identification sets.
     *
     * @param classMap On return, classMap[c] contains the set of states of the
     *                 (potentially unminimised) FSM associated with state  equivalence class c.
     *                 If the machine is already minimised, it contains exactly one element.
     * @param state2classMaps On return, this vector contains the successive mappings of
     *                 states to the classes they are associated with. The map state2classMaps[0]
     *                 maps every state into the same class 0. The map
     *                 state2classMaps.back() contains the final mapping from states to
     *                 state equivalence classes, as needed for minimisation.
     *
     * @note calcOFSMTables() will throw a runtime exception if the FSM is not observable.
     */
    void calcOFSMTables(std::map< size_t, std::set<Index> >& classMap,
                                std::vector< std::map<Index, size_t> >& state2classMaps) const override;
    
    /**
     * Return true iff observable FSM is initially connected.
     *
     */
    bool isInitiallyConnected() const override;
    
    /**
     *  Return true iff observable FSM is minimal.
     *  This operation calls calcOFSMTables() to check whether the FSM is minimal.
     *  If the FSM is not initially connected, false is returned (since the unreachble
     *  state is superfluous). This is checked by analysing whether the fsmTable has
     *  every state as post-state of at least one transition. If the FSM is initially
     *  connected, true is returned iff classMap has  size states.size().
     */
    bool isMinimal() const override;
    
    void createTransitionTable();
    
    void resetTransitionTable(); 
    
    std::optional<Index>*** getTransitionTable() { return transitionTable; }
    
    void accept(FsmVisitor& v) override { v.visit(*this); }

    /**
     * Creates a copy of the object.
     * @return A unique_ptr pointing to the created clone
     */
    std::unique_ptr<Fsm> clone() const override;

    /**
     * Creates an object of the same type as this one but with different
     * attributes.
     * @param fsmName Optional parameter. If set, the created FSM will be named
     *  according to the parameter. If not set, the created FSM will be named
     *  the same as this one.
     * @param states Optional parameter. If set, the created FSM will have the
     *  set of states defined by the parameter as its state set. If not set, the
     *  created FSM will have the same state space as this one.
     * @param pl Optional parameter. If set, the created FSM will have the
     *  presentation layer given as the argument as its presentation layer. If
     *  not set, the created FSM will have the same presentation layer as this
     *  one.
     * @param initialStateId Optional parameter. If set, the created FSM will
     *  have the initial state set according to the given argument. If not set,
     *  the created FSM will have the same initial state ID as this one.
     */
    std::unique_ptr<Fsm> modify(std::optional<std::string> fsmName,
                                        std::optional<States> states,
                                        std::optional<PresentationLayer> pl,
                                        std::optional<Index> initialStateId) const override;

};

}
