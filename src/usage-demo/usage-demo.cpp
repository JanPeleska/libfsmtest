/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cassert>
#include <string>

#include "libfsmtest.hpp"

using namespace std;
using namespace libfsmtest;

#ifndef RESOURCEPATH
#define RESOURCEPATH string("../../resources/")
#endif


static void demo_transformToObservableFSM() {
        
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"nonObservable",
                                                        "NO");
    
    ToDotFileVisitor dotFsm("FNO1.dot");
    fsm->accept(dotFsm);
    dotFsm.writeToFile();
    
    // Transform to initially connected FSM
    auto fsmIc = transformFsm<ToInitiallyConnectedTransformer>(fsm.get());
    
    auto fsmObs = transformFsm<ToObservableTransformer>(fsmIc.get());
    
    ToDotFileVisitor dot("FO1.dot");
    fsmObs->accept(dot);
    dot.writeToFile();

}

static void demo_transformToInitiallyConnectedFSM() {
    
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"nonObservable.fsm",
                                                        "NO");
    
    assert ( ! fsm->isInitiallyConnected() );
    
    ToDotFileVisitor dotFsm("FNO2.dot");
    fsm->accept(dotFsm);
    dotFsm.writeToFile();

    FsmPtr fsmIc = transformFsm<ToInitiallyConnectedTransformer>(fsm.get());
    
    assert ( fsmIc->isInitiallyConnected() );
    
    ToDotFileVisitor dot("FNO_IC2.dot");
    fsmIc->accept(dot);
    dot.writeToFile();

}

static void demo_FsmFromFileCreator_Dfsm() {
    FsmPtr dfsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"f.fsm",
                                                         "F");
    
    ToDotFileVisitor dotFsm("f.dot");
    dfsm->accept(dotFsm);
    dotFsm.writeToFile();
}

static void demo_minimise() {
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"NN.fsm",
                                                        "NN");
    
    ToDotFileVisitor dotFsm("NN.dot");
    fsm->accept(dotFsm);
    dotFsm.writeToFile();

    FsmPtr fsmMin = transformFsm<ToMinimisedTransformer>(fsm.get());
    
    ToDotFileVisitor doFsmMin("NN_MIN.dot");
    fsmMin->accept(doFsmMin);
    doFsmMin.writeToFile();
    
}

static void demo_minimise_dfsm() {
    
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"fsmGillA7.fsm",
                                                        "fsmGillA7");
    ToDotFileVisitor dotFsm("fsmGillA7.dot");
    fsm->accept(dotFsm);
    dotFsm.writeToFile();
    
    FsmPtr fsmMin = transformFsm<ToMinimisedTransformer>(fsm.get());
    
    ToDotFileVisitor doFsmMin("fsmGillA7_MIN.dot");
    fsmMin->accept(doFsmMin);
    doFsmMin.writeToFile();
    
}


static void demo_TestSuite() {
    
    TestSuite ts(RESOURCEPATH+"suiteNN1",RESOURCEPATH+"NN");
    
    cout << "Test Suite NN1" << endl;
    
    for ( const auto& tr : ts.getInputTraces() ) {
        cout << tr << endl;
    }
    
    ts.writeToFile();
    
    Fsm& fsm = ts.getFsm();
    
    ToDotFileVisitor dot("NN-TS.dot");
    fsm.accept(dot);
    dot.writeToFile();
    
    Traces inputTraces = ts.getInputTracesRaw();
    for ( auto& tr : inputTraces ) {
        string iotrace;
        fsm.resetStepping();
        cout << "New Test Cases" << endl;
        for ( Index& x : tr ) {
            if ( ! fsm.step(x,1,iotrace) ) {
                cout << "FAIL after " << iotrace << endl;
                goto nextTestCase;
            }
        }
        cout << "PASS: " << iotrace << endl;
      nextTestCase:
        ;
    }
    
    Trace tr;
    tr.append(1);
    tr.append(1);
    tr.append(1);
    tr.append(0);
    tr.append(0);
    tr.append(1);
    
    Strings iotraces;
    fsm.getIOTraces(tr,iotraces);
    cout << "IO traces" << endl;
    for ( auto& trStr : iotraces ) {
        cout << trStr << endl;;
    }
    
}

static void demo_wMethod() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"f.fsm",
                                                        "f");
    ToDotFileVisitor dot("f.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the W-Method
    size_t numAddStates = 0;
    TestGenerationFrame<WMethod> genFrame("SUITE-W-0", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_tMethod() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"f.fsm",
                                                        "f");
    TestGenerationFrame<TMethod> genFrame("SUITE-T", move(fsm));

    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}


static void demo_wMethod_fsm() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"nonObservable.fsm",
                                                        "NO");
    ToDotFileVisitor dot("nonObservable.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the W-Method
    size_t numAddStates = 2;
    TestGenerationFrame<WMethod> genFrame("SUITE-W-3", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_transformToPrimeMachine() {
    
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"nonObservable.fsm",
                                                        "NO");
    
    FsmPtr primeFsm = transformFsm<ToPrimeTransformer>(fsm.get());
    
    ToDotFileVisitor dot("FNO_PRIME.dot");
    primeFsm->accept(dot);
    dot.writeToFile();
    
}

static void demo_transformToPrimeMachineGdc() {
    
    auto dfsm = createFsm<FsmFromRawCreator>(RESOURCEPATH+"garage_partial.fsm",
                                             RESOURCEPATH+"garage.state",
                                             RESOURCEPATH+"garage.in",
                                             RESOURCEPATH+"garage.out",
                                             "GDC");
    
    auto dfsmComplete = transformFsm<ToAutoCompleteWithSelfLoopTransformer>(dfsm.get(), "null");
    
    auto dfsmMin = transformFsm<ToPrimeTransformer>(dfsmComplete.get());
    
    ToDotFileVisitor dot("GDC_MINIMISED.dot");
    dfsmMin->accept(dot);
    dot.writeToFile();
    
}

static void demo_wpMethod() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"f.fsm",
                                                        "f");
    ToDotFileVisitor dot("f.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the Wp-Method
    size_t numAddStates = 0;
    TestGenerationFrame<WPMethod> genFrame("SUITE-WP-2", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_wpMethod_fsm() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"nonObservable.fsm",
                                                        "NO");
    ToDotFileVisitor dot("nonObservable.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the Wp-Method
    size_t numAddStates = 2;
    TestGenerationFrame<WPMethod> genFrame("SUITE-WP-3", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_strongStateCountingMethod() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"strong-reduction/m_ex.fsm",
                                                        "m_ex");
    ToDotFileVisitor dot("m_ex.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the Strong State Counting Method
    size_t numAddStates = 0;
    TestGenerationFrame<StrongStateCountingMethod> genFrame("SUITE-SSC-m_ex", move(fsm), numAddStates, true);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_classicalStateCountingMethod() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"reduction/m.fsm",
                                                        "m");
    ToDotFileVisitor dot("m.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the State Counting Method
    size_t numAddStates = 0;
    // TODO: pass true as second argument.
    TestGenerationFrame<ClassicalStateCountingMethod> genFrame("SUITE-SC-m", move(fsm), numAddStates, true);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_spyhMethod() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"spyh-example/m_ex.fsm",
                                                        "m_spy");
    ToDotFileVisitor dot("m_spy.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    // Create the test generation framework with the SPYH-Method
    size_t numAddStates = 1;
    TestGenerationFrame<SPYHMethod> genFrame("SUITE-SPYH-m_ex", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}


static void demo_randomCompletelySpecifiedDfsm() {
    // Create a random DFSM with 5 states, 7 inputs, 4 outputs
    FsmPtr fsm = createFsm<RandomFsmCreator<Dfsm, COMPLETELY_SPECIFIED>>(5u, 7u, 4u, "random");
    cout << "Random completely specified DFSM: " << (fsm->isCompletelySpecified() and fsm->isDeterministic()) << endl;
    ToDotFileVisitor dot("random.dot");
    fsm->accept(dot);
    dot.writeToFile();
}

static void demo_autoCompletion() {
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"incomplete.fsm",
                                                        "INCOMPLETE");
    ToDotFileVisitor dot("incomplete.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    auto selfLooped = transformFsm<ToAutoCompleteWithSelfLoopTransformer>(fsm.get(), "_null");
    auto errorStated = transformFsm<ToAutoCompleteWithErrorStateTransformer>(fsm.get(), "_error", "_null", "ERROR");

    ToDotFileVisitor dotSelfLooped("selfLooped.dot");
    ToDotFileVisitor dotErrorStated("errorStated.dot");

    selfLooped->accept(dotSelfLooped);
    errorStated->accept(dotErrorStated);

    dotSelfLooped.writeToFile();
    dotErrorStated.writeToFile();
} 
   
static void demo_FsmFromFileCreator_onlyFSM() {
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH + "M0.fsm");

    ToDotFileVisitor dot("M0.dot");
    fsm->accept(dot);
    dot.writeToFile();

    ToFsmFileVisitor visitor("M0");
    fsm->accept(visitor);
    visitor.writeToFile();
}


static void demo_garage() {
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"garage.fsm",
                                                        "GDC");
    ToDotFileVisitor dot("GDC.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    // Create the test generation framework with the Wp-Method
    size_t numAddStates = 0;
    TestGenerationFrame<WPMethod> genFrame("SUITE-GDC-WP", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
}

static void demo_HMethod() {
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"garage.fsm",
                                                        "GDC");
    ToDotFileVisitor dot("GDC.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    // Create the test generation framework with the H-Method
    size_t numAddStates = 1;
    TestGenerationFrame<HMethod> genFrame("SUITE-GDC-H", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
}

static void demo_HMethod_nondet() {
    
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"nonObservable.fsm",
                                                        "NO");
    ToDotFileVisitor dot("NO.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    // Create the test generation framework with the H-Method
    size_t numAddStates = 1;
    TestGenerationFrame<HMethod> genFrame("SUITE-NO-H", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
    
    
    
    
    
}

static void demo_HWPMethod() {
    // Read FSM from file
    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"garage.fsm",
                                                        "GDC");
    ToDotFileVisitor dot("GDC.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    // Create the test generation framework with the Wp-Method
    size_t numAddStates = 1;
    TestGenerationFrame<WPMethod> genFrame("SUITE-GDC-H-WP", move(fsm), numAddStates);
    
    // Generate the test suite and write it to file
    genFrame.generateTestSuite();
    genFrame.writeToFile();
}

 

static void demo_csvInput() {

    FsmPtr fsm = createFsm<FsmFromFileCreator>(RESOURCEPATH+"garage-door-controller.csv",
                                                        "garage-door-controller",
                                                        make_unique<ToAutoCompleteWithSelfLoopTransformer>(nullptr, "null"));
    
    ToDotFileVisitor dot("garage-door-controller.dot");
    fsm->accept(dot);
    dot.writeToFile();
    
    
    
    size_t numAddStates = 1;
    TestGenerationFrame<WPMethod> genFrame("SUITE-GDC-WP", move(fsm), numAddStates);
    
    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
    
}
static void demo_FsmFromCsv() {
    FsmPtr dfsm = createFsm<FsmFromCsvCreator>(RESOURCEPATH + "FSBRTSX.csv");

    ToDotFileVisitor dot("FSBRTSX.dot");
    dfsm->accept(dot);
    dot.writeToFile();

    ToFsmFileVisitor visitor("FSBRTSX");
    dfsm->accept(visitor);
    visitor.writeToFile();

    ToCsvFileVisitor csv("FSBRTSX.csv");
    dfsm->accept(csv);
    csv.writeToFile();
}

static void demo_randomCompletelySpecifiedFsm() {
    // Create a random FSM with 5 states, 4 inputs, 4 outputs
    FsmPtr fsm = createFsm<RandomFsmCreator<Fsm, COMPLETELY_SPECIFIED>>(5u, 4u, 4u, "random");

    cout << "Random Completely Specified FSM: " << fsm->isCompletelySpecified() << endl;

    ToDotFileVisitor dot("randomFsm.dot");
    fsm->accept(dot);
    dot.writeToFile();

    FsmPtr fsm_rm = transformFsm<RandomTransitionRemoval>(fsm.get(), 5u);
    ToDotFileVisitor dot2("randomFsmRm.dot");
    fsm_rm->accept(dot2);
    dot2.writeToFile();
}


static void demo_SHMethod() {
        
    // Read reference model
    auto referenceFsm = createFsm<FsmFromFileCreator>(RESOURCEPATH + "Safety-complete-H-method/FSBRTSX.csv");
     
    // Read abstracted machine
    auto abstractionFsm = createFsm<FsmFromFileCreator>(RESOURCEPATH + "Safety-complete-H-method/FSBRTSX-ABS.csv");
    
    size_t numAdditionalStates = 0;
    TestGenerationFrame<SHMethod> genFrame("SAFETY-H-METHOD-FSBRTSX",
                                                          move(referenceFsm),
                                                          numAdditionalStates,
                                                          move(abstractionFsm));

    genFrame.generateTestSuite();
    genFrame.writeToFile();
    
    cout << "Safety-H Test generation completed." << endl;
    cout << "Safety-H Number of test cases: " << genFrame.getTestSuite()->getNumberOfTestCases() << endl;
    cout << "Safety-H Total length        : " << genFrame.getTestSuite()->getTotalLength() << endl;
    cout << "Safety-H Test case file      : " << genFrame.getTestSuite()->getName() + ".txt" << endl;

    
}




int main(int /* argc */, char** /* argv[] */) {
    cout << "Usage demo for the libfsmtest " << version.major << "." << version.minor << endl;
    
    demo_transformToObservableFSM();
    
    demo_FsmFromFileCreator_Dfsm();
    
    demo_transformToInitiallyConnectedFSM();
    
    demo_minimise();
    
    demo_minimise_dfsm();
    
    demo_TestSuite();
    
    demo_wMethod();
    
    // Apply W-Method to non-observable, not initially connected
    // FSM, so that the prime machine has to be produced first (internally in WMethod)
    demo_wMethod_fsm();
    
    demo_transformToPrimeMachine();
    
    demo_transformToPrimeMachineGdc();

    demo_wpMethod();
    
    demo_wpMethod_fsm();

    demo_strongStateCountingMethod();

    demo_classicalStateCountingMethod();

    demo_spyhMethod();

    demo_randomCompletelySpecifiedDfsm();

    demo_FsmFromFileCreator_onlyFSM();
    
    demo_autoCompletion();
    
    demo_garage();
    
    demo_HMethod();
    
    demo_HMethod_nondet();
    
    demo_SHMethod();
    
    demo_HWPMethod();
    
    demo_FsmFromCsv();

    demo_randomCompletelySpecifiedFsm();
    
    demo_csvInput();

    demo_tMethod();
    
    
    
    return 0;
}
