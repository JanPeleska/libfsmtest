/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <string>
#include <memory>
#include <utility>
#include "visitors/FsmVisitor.hpp"
#include "creators/FsmCreator.hpp"

namespace libfsmtest {

/**
 * Abstract class for all sorts of visitors transforming machines
 * into new machines
 */
class Transformer : public FsmVisitor, FsmCreator {
private:
    /** The Fsm to operate on */
    Fsm* fsm;

protected:
    /** The transformed Fsm/Ofsm/Dsfm */
    std::unique_ptr<Fsm> transformedFsm;

    /** The suffix to be attached to the name of the transformed machine */
    std::string nameSuffix;

public:

    Transformer(Fsm *fsmToTransform, std::string nameSuffixParam) :
    fsm(fsmToTransform), transformedFsm(nullptr), nameSuffix(std::move(nameSuffixParam)) { }
    
    virtual ~Transformer() override = default;

    std::unique_ptr<Fsm> createFsm() override {
        fsm->accept(*this);
        return move(transformedFsm);
    }

    void setFsm(Fsm *fsmParam) {
        Transformer::fsm = fsmParam;
    }

};

template<typename Transformer, typename... Args>
std::unique_ptr<Fsm> transformFsm(Args&& ...args) {
    Transformer transformer(std::forward<Args>(args)...);
    return transformer.createFsm();
}

} // namespace libfsmtest
