/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>
#include <stdexcept>

#include "fsm/Dfsm.hpp"
#include "ToCsvFileVisitor.hpp"

namespace libfsmtest {

void ToCsvFileVisitor::visit(Fsm& fsm) {
    // first cell is empty
    csv.emplace_back().emplace_back("");
    // insert input alphabet
    pl = std::make_unique<PresentationLayer>(fsm.getPresentationLayer());
    for (const auto& entry : pl->inputAlphabet) {
        csv.emplace_back().emplace_back(entry);
    }

    initialState = fsm.getInitialStateId();

    for_each(fsm.getStates().begin(),
             fsm.getStates().end(),
             [this](auto& state) { state.accept(*this); } );

    // Add empty cells for non-specified transitions
    std::for_each(csv.begin() + 1, csv.end(), [&](auto& col){
        while(col.size() < csv.front().size()) {
            col.emplace_back();
        }
    });
}

void ToCsvFileVisitor::visit(Ofsm& ofsm) {
    this->visit(static_cast<Fsm&>(ofsm));
}

void ToCsvFileVisitor::visit(Dfsm &dfsm) {
    this->visit(static_cast<Fsm&>(dfsm));
}

void ToCsvFileVisitor::visit(State& state) {
    if (state.getId() == initialState) {
        // initial state is always in the second row
        csv.at(0).emplace(csv.at(0).begin() + 1, state.getName());
    } else {
        // otherwise they are placed in the order of appearance
        csv.at(0).emplace_back(state.getName());
    }

    for_each(state.getTransitions().begin(),
             state.getTransitions().end(),
             [this](auto& transition) { transition.accept(*this);});
}

void ToCsvFileVisitor::visit(Transition& transition) {
    CsvColumn& column = csv.at(transition.getInput() + 1);

    // Fill up the column we are in if necessary
    while (column.size() <= transition.getSourceId() + 1) {
        column.emplace_back();
    }

    // This method will be called in the same order as #visit(State& state)
    // so the same order is ensured.
    std::stringstream cell;

    // There might already be other transitions for this input, so eventually prepend those to the new cell
    if (transition.getSourceId() == initialState) {
        cell << column.at(1);
    } else {
        cell << column.at(transition.getSourceId() + 1);
    }
    if (!cell.str().empty()) {
        cell << ", ";
    }

    cell << pl->stateNames.at(transition.getTargetId());
    cell << "/";
    cell << pl->outputAlphabet.at(transition.getOutput());

    if (transition.getSourceId() == initialState) {
        // transition starts at the initial state, thus in the second row
        column.at(1) =  cell.str();
    } else {
        // transition starts at the state with index getSourceId, so has to be inserted in getSourceId + 1 row
        column.at(1 + transition.getSourceId()) = cell.str();
    }
}

void ToCsvFileVisitor::writeToFile() {
    std::ofstream output(fileName);

    if (!output.is_open()) throw std::runtime_error("Failed to open file");

    writeCsv(output, csv, ';');
}

}
