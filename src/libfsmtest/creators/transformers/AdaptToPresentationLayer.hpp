
/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <utility>

#include "Transformer.hpp"

namespace libfsmtest {

/**
 * Class to recode an FSM to use another, equivalent presentation layer.
 */
class AdaptToPresentationLayer : public Transformer {
private:
    /**
     * The presentation layer the recoded FSM should use
     */
    const PresentationLayer& other;

    /**
     * The newly created states with respect to #other.
     * Will be filled in #visit(Fsm&).
     */
    States states;

    /**
     * Checks whether two presentation layers are adaptable to another, thus,
     * both contain the same content but in different order.
     * @param lhs The presentation layer to check adaptability for
     * @param rhs The presentation layer to adapt, i.e. specifying the target ordering
     * @return true, if the content is the same but in different order,
     *         false, otherwise.
     */
    static bool arePresentationLayersAdaptable(const PresentationLayer &lhs, const PresentationLayer &rhs) ;

public:
    /**
     * Constructor.
     * Won't start checking whether the adaption can work or recoding the FSM.
     *
     * @param fsmToTransform The FSM that should be recoded to use #other
     * @param otherPresentationLayer The presentation layer the FSM should use after recoding
     * @param nameSuffixParam An optional suffix to append to the name of the FSM.
     */
    AdaptToPresentationLayer(Fsm* fsmToTransform, const PresentationLayer& otherPresentationLayer, std::string nameSuffixParam = "_OPL") : Transformer(fsmToTransform, std::move(nameSuffixParam)), other(otherPresentationLayer) {}

    void visit(Fsm& fsm) override;
    void visit(Ofsm& ofsm) override;
    void visit(Dfsm& dfsm) override;

    void visit(State& state) override;
};

} // namespace libfsmtest

