/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "utils/compatibility.hpp"

#include "creators/input/FsmFromFileCreator.hpp"
#include "creators/transformers/ToAutoCompletedTransformer.hpp"
#include "fsm/Fsm.hpp"
#include "testsuite/TestSuite.hpp"
#include "testsuite/Trace.hpp"
#include "version.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "visitors/output/ToFsmFileVisitor.hpp"
#include "visitors/testgeneration/ClassicalStateCountingMethod.hpp"
#include "visitors/testgeneration/HMethod2.hpp"
#include "visitors/testgeneration/SHMethod.hpp"
#include "visitors/testgeneration/SPYHMethod.hpp"
#include "visitors/testgeneration/StrongStateCountingMethod.hpp"
#include "visitors/testgeneration/WMethod.hpp"
#include "visitors/testgeneration/WPMethod.hpp"
#include <cstdlib>
#include <iostream>
#include <memory>

using namespace std;
using namespace libfsmtest;

static unique_ptr<Fsm> referenceFsm = nullptr;
static unique_ptr<Fsm> abstractionFsm = nullptr;

static size_t numAdditionalStates = 0;

typedef enum {
    wMethod,
    wpMethod,
    hMethod,
    spyhMethod,
    classicalStateCountingMethod,
    strongStateCountingMethod,
    safetyhMethod
} generationMethod_t;

static generationMethod_t selectedTestGenerationMethod = wMethod;

static string testSuiteName;

[[noreturn]] static void printUsageAndExit(char* progName) {
    cerr << "libfsmtest " << version.major << "." << version.minor << " checker" << endl;
    cerr << "Usage: " << progName << " <options> <test suite name> <Path to reference model> [<Path to abstraction model>]" << endl;
    cerr << "Options: -a <number of additional states>" << endl;
    cerr << "         -cself : This selects application of the auto-completion transformation with self loops" << endl;
    cerr << "         -cerror : This selects application of the auto-completion transformation with error state" << endl;
    cerr << "         -null <null string>: Name of the null output (only with -cself or -cerror)" << endl;
    cerr << "         -errorout <error output string>: Name of the error output (only with -cerror)" << endl;
    cerr << "         -errorstate <error state string>: Name of the error state (only with -cerror)" << endl;
    cerr << "         -w : This selects the W Method for test generation (default)" << endl;
    cerr << "         -wp : This selects the Wp Method for test generation" << endl;
    cerr << "         -h : This selects the H Method for test generation" << endl;
    cerr << "         -spyh : This selects the H Method for test generation" << endl;
    cerr << "         -csc : This selects the classical (non adaptive) state counting Method for test generation" << endl;
    cerr << "         -ssc : This selects the strong state counting Method for test generation" << endl;
    cerr << "         -sh : This selects the Safety-complete H Method for test generation - it needs an abstraction model in addition to the reference model" << endl;
    exit(1);
}

static void parseArguments(int argc, char **argv) {
    
    if(argc < 3) printUsageAndExit(argv[0]);
    
    int argIdx = 1;
    
    bool needTestSuiteName = true;
    bool needReferenceFsm = false;
    bool needAbstractionFsm = false;
    
    bool selfAutoCompletion = false;
    bool errorAutoCompletion = false;
    
    string nullString("_null");
    string errorOutString("_error");
    string errorStateString("ERROR");
    
    while ( argIdx < argc ) {
        
        if ( string(argv[argIdx]) == "-a" ) {
            if ( argIdx > argc-2 ) printUsageAndExit(argv[0]);
            auto num = stoi(argv[++argIdx]);
            if(num < 0) {
                cerr << "Only non-negative numbers allowed for option '-a'." << endl;
                printUsageAndExit(argv[0]);
            }
            numAdditionalStates = static_cast<size_t>(num);
        }
        else if ( string(argv[argIdx]).at(0) == '-' &&
                 string(argv[argIdx]).at(1) == 'a') {
            // special case where number follows -a without blank
            string numStr = string(argv[argIdx]).substr(2);
            auto num = stoi(numStr);
            if(num < 0) {
                cerr << "Only non-negative numbers allowed for option '-a'." << endl;
                printUsageAndExit(argv[0]);
            }
            numAdditionalStates = static_cast<size_t>(num);
        }
        else if ( string(argv[argIdx]) == "-cself" ) {
            selfAutoCompletion = true;
            errorAutoCompletion = false;
        }
        else if ( string(argv[argIdx]) == "-cerror" ) {
            selfAutoCompletion = false;
            errorAutoCompletion = true;
        }
        else if ( string(argv[argIdx]) == "-null" ) {
            if ( argIdx > argc-2 ) printUsageAndExit(argv[0]);
            nullString = string(argv[++argIdx]);
        }
        else if ( string(argv[argIdx]) == "-errorout" ) {
            if ( argIdx > argc-2 ) printUsageAndExit(argv[0]);
            errorOutString = string(argv[++argIdx]);
        }
        else if ( string(argv[argIdx]) == "-errorstate" ) {
            if ( argIdx > argc-2 ) printUsageAndExit(argv[0]);
            errorStateString = string(argv[++argIdx]);
        }
        else if ( string(argv[argIdx]) == "-w" ) {
            selectedTestGenerationMethod = wMethod;
        }
        else if ( string(argv[argIdx]) == "-wp" ) {
            selectedTestGenerationMethod = wpMethod;
        }
        else if ( string(argv[argIdx]) == "-h" ) {
            selectedTestGenerationMethod = hMethod;
        }
        else if ( string(argv[argIdx]) == "-spyh" ) {
            selectedTestGenerationMethod = spyhMethod;
        }
        else if ( string(argv[argIdx]) == "-csc" ) {
            selectedTestGenerationMethod = classicalStateCountingMethod;
        }
        else if ( string(argv[argIdx]) == "-ssc" ) {
            selectedTestGenerationMethod = strongStateCountingMethod;
        }
        else if ( string(argv[argIdx]) == "-sh" ) {
            selectedTestGenerationMethod = safetyhMethod;
        }
        else if ( string(argv[argIdx]).at(0) == '-' ) {
            cerr << "Illegal option " << string(argv[argIdx]) << endl;
            printUsageAndExit(argv[0]);
        }
        else if ( needTestSuiteName ) {
            testSuiteName = string(argv[argIdx]);
            needTestSuiteName = false;
            needReferenceFsm = true;
        }
        else if ( needReferenceFsm ) {
            // Create reference FSM from file
            
            // If selected, create an auto completion transformer
            unique_ptr<ToAutoCompleteTransformer> transformer = nullptr;
            if ( selfAutoCompletion ) {
                transformer = make_unique<ToAutoCompleteWithSelfLoopTransformer>(nullptr, nullString);
            }
            else if ( errorAutoCompletion ) {
                transformer = make_unique<ToAutoCompleteWithErrorStateTransformer>(nullptr,
                                                                                   nullString,
                                                                                   errorOutString,
                                                                                   errorStateString);
            }
            
            // argv[argIdx] contains path/file basename to the reference FSM
            // For fsm name, we only want the basename, without path prefix of file extension suffix.
            string fsmName(argv[argIdx]);
            
            size_t pos = fsmName.rfind('/');
            if ( pos != string::npos ) {
                fsmName.erase(0,pos+1);
            }
            pos = fsmName.rfind('.');
            if ( pos != string::npos) {
                fsmName.erase(pos);
            }
            
            referenceFsm = createFsm<FsmFromFileCreator>(argv[argIdx],fsmName,move(transformer));
            
            needReferenceFsm = false;
            if ( selectedTestGenerationMethod == safetyhMethod ) {
                needAbstractionFsm = true;
            }
            
            ToDotFileVisitor dot(testSuiteName+".dot");
            referenceFsm->accept(dot);
            dot.writeToFile();
            
        }
        else if ( needAbstractionFsm ) {
            // Create abstraction FSM from file
            
            // If selected, create an auto completion transformer
            unique_ptr<ToAutoCompleteTransformer> transformer = nullptr;
            if ( selfAutoCompletion ) {
                transformer = make_unique<ToAutoCompleteWithSelfLoopTransformer>(nullptr, nullString);
            }
            else if ( errorAutoCompletion ) {
                transformer = make_unique<ToAutoCompleteWithErrorStateTransformer>(nullptr,
                                                                                   nullString,
                                                                                   errorOutString,
                                                                                   errorStateString);
            }
            
            // Prepare FSM name as done for the reference FSM
            string fsmName(argv[argIdx]);
            size_t pos = fsmName.rfind('/');
            if ( pos != string::npos ) {
                fsmName.erase(0,pos+1);
            }
            pos = fsmName.rfind('.');
            if ( pos != string::npos) {
                fsmName.erase(pos);
            }
            
            abstractionFsm = createFsm<FsmFromFileCreator>(argv[argIdx],fsmName,move(transformer));
            needAbstractionFsm = false;
        }
        else {
            printUsageAndExit(argv[0]);
        }
        
        argIdx++;
    }

    if( needAbstractionFsm ) {
        throw runtime_error("No abstraction FSM given.");
    }
    if( needReferenceFsm ) {
        throw runtime_error("No reference FSM given.");
    }
}

template<typename Method, typename... Args>
void generateTestSuiteWith(Args... args) {
    TestGenerationFrame<Method> genFrame(testSuiteName, move(referenceFsm), std::forward<Args>(args)...);

    genFrame.generateTestSuite();
    genFrame.writeToFile();

#if 0
    for ( const auto& tr : genFrame.getTestSuite()->getInputTracesRaw() ) {
        Strings ioTraces;
        genFrame.getTestSuite()->getFsm().getIOTraces(tr,ioTraces,true);
        for ( const auto& ioTr : ioTraces ) {
            cout << ioTr << endl;
        }
    }
#endif

    cout << "Test generation completed." << endl;
    cout << "Number of test cases: " << genFrame.getTestSuite()->getNumberOfTestCases() << endl;
    cout << "Total length        : " << genFrame.getTestSuite()->getTotalLength() << endl;
    cout << "Test case file      : " << genFrame.getTestSuite()->getName() + ".txt" << endl;
    cout << "Reference model     : " << ((genFrame.getTestSuite()->getFsm().isDeterministic()) ? "deterministic" : "nondeterministic") << endl;
}

/**
 * Generate test suite, using the classes of the libfsmtest
 */
int main(int argc, char **argv) {
    
    parseArguments(argc,argv);

    switch ( selectedTestGenerationMethod ) {
        case wMethod:
            generateTestSuiteWith<WMethod>(numAdditionalStates);
            break;
        case wpMethod:
            generateTestSuiteWith<WPMethod>(numAdditionalStates);
            break;
        case hMethod:
            generateTestSuiteWith<HMethod2>(numAdditionalStates);
            break;
        case spyhMethod:
            generateTestSuiteWith<SPYHMethod>(numAdditionalStates);
            break;
        case classicalStateCountingMethod:
            generateTestSuiteWith<ClassicalStateCountingMethod>(numAdditionalStates);
            break;
        case strongStateCountingMethod:
            generateTestSuiteWith<StrongStateCountingMethod>(numAdditionalStates);
            break;
        case safetyhMethod:
            generateTestSuiteWith<SHMethod>(numAdditionalStates, std::move(abstractionFsm));
            break;
        default:
            printUsageAndExit(argv[0]);
    }

    return 0;
}
