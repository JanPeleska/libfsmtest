/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "creators/transformers/ToAutoCompletedTransformer.hpp"
#include "fsm/Fsm.hpp"
#include "fsm/Ofsm.hpp"
#include "fsm/Dfsm.hpp"
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <utility>

namespace libfsmtest {

ToAutoCompleteTransformer::ToAutoCompleteTransformer(Fsm *fsmToTransform)
 : Transformer(fsmToTransform, "_COMPLETED")
 {
 }

void ToAutoCompleteTransformer::visit(Fsm& /*fsm*/) {
    // For each state, the set of inputs is determined for which there is no
    // outgoing transition.
    // This is done by creating a set of all inputs, iterating over the
    // transitions of a state and erasing all encountered inputs from the set.

    // Creating a vector of the same size as the input alphabet
    vector<Index> allInputs(this->transformedFsm->getInputAlphabetSize());
    // Filling the input vector with integers ascending from 0 (i.e. the input IDs)
    iota(allInputs.begin(), allInputs.end(), 0);

    for(auto &state : this->transformedFsm->getStates()) {
        // Creating a set of all inputs
        unordered_set<Index> inputsToHit(allInputs.cbegin(), allInputs.cend());

        // For each transition of the state under consideration, the input
        // ID is erased from the set of input IDs
        for(auto const &transition : state.getTransitions()) {
            inputsToHit.erase(transition.getInput());
        }

        // For each input that has not yet been erased, the result of
        // getCompletingTransition() is added to the set of transitions of the
        // state under consideration
        for(auto const &unhitInput : inputsToHit) {
            state.getTransitions().push_back(this->getCompletingTransition(this->transformedFsm->getPresentationLayer(), state, unhitInput));
        }
    }
    this->transformedFsm = this->transformedFsm->modify(this->transformedFsm->getName(),
                                                        this->transformedFsm->getStates(),
                                                        this->transformedFsm->getPresentationLayer(),
                                                        this->transformedFsm->getInitialStateId());
}
void ToAutoCompleteTransformer::visit(Ofsm& fsmToTransform) {
    // Do the same as for FSMs
    this->visit(static_cast<Fsm&>(fsmToTransform));
}
void ToAutoCompleteTransformer::visit(Dfsm& fsmToTransform) {
    // Do the same as for FSMs
    this->visit(static_cast<Fsm&>(fsmToTransform));
}

// Function to return the index of the first entry in a container that matches
// a string. If there is no such entry, the size of the container is returned.
template<typename Container, typename Fn>
static size_t findIDs(Container &&c, Fn &&nameExtractor, string const &name) {
    // Find the iterator for the element matching name. If there is no such
    // element, c.cend() is returned.
    auto const iter = std::find_if(c.cbegin(), c.cend(), [&](auto const &element) {
        return (nameExtractor(element) == name);
    });
    // Calculate the distance from c.cbegin() to the iterator. That distance is
    // the index of the element matching name or alternatively, if there is no
    // such element, the size of c.
    return static_cast<size_t>(distance(c.cbegin(), iter));
}
template<typename Container, typename Fn>
static map<string, size_t> findIDs(Container &&c, Fn &&nameExtractor, vector<string> names) {
    size_t lowestNewID = c.size();
    sort(names.begin(), names.end());
    map<string, size_t> result;
    for(auto const &name : names) {
        if(auto id = findIDs(c, nameExtractor, name); c.size() <= id) {
            result[name] = lowestNewID++;
        } else {
            result[name] = id;
        }
    }
    return result;
}

Transition ToAutoCompleteWithSelfLoopTransformer::getCompletingTransition(PresentationLayer const &presentationLayer, State const &s, Index input) {
    // Return a self-loop with the output ID being the completion output ID
    return Transition(s.getId(), input, findIDs(presentationLayer.outputAlphabet, [](auto const &outSym) { return outSym; }, this->completionOutput), s.getId());
}
ToAutoCompleteWithSelfLoopTransformer::ToAutoCompleteWithSelfLoopTransformer(Fsm *fsmParam, string completionOutputParam)
 : ToAutoCompleteTransformer(fsmParam),
   // Determining the ID of the output to use for completing transitions. If
   // there is no such output, the completion ID will be the size of the output
   // alphabet.
   completionOutput(std::move(completionOutputParam))
{
}

void ToAutoCompleteWithSelfLoopTransformer::visit(Fsm &fsmToTransform) {
    auto completionId = findIDs(fsmToTransform.getPresentationLayer().outputAlphabet, [](auto sym) { return sym; }, completionOutput);


    PresentationLayer modifiedPl = fsmToTransform.getPresentationLayer();
    // If the ID of the output to be used for completing transitions is equal
    // or greater to the size of the output alphabet, the output does not yet
    // exist and shall be inserted into the alphabet
    if(fsmToTransform.getOutputAlphabetSize() <= completionId) {
        modifiedPl.outputAlphabet.push_back(completionOutput);
    }

    // Creating a clone of the fsm to be transformed.
    this->transformedFsm = fsmToTransform.modify(nullopt, nullopt, modifiedPl, nullopt);

    ToAutoCompleteTransformer::visit(fsmToTransform);
}
void ToAutoCompleteWithSelfLoopTransformer::visit(Ofsm &ofsm) {
    this->visit(static_cast<Fsm&>(ofsm));
}
void ToAutoCompleteWithSelfLoopTransformer::visit(Dfsm &dfsm) {
    this->visit(static_cast<Fsm&>(dfsm));
}

Transition ToAutoCompleteWithErrorStateTransformer::getCompletingTransition(PresentationLayer const &presentationLayer, State const &s, Index input) {
    Index errorStateId = findIDs(presentationLayer.stateNames, [](auto const &outSym) { return outSym; }, this->errorState);
    // If the source state is not the error state ...
    if(s.getId() != errorStateId) {
        Index completionId = findIDs(presentationLayer.outputAlphabet, [](auto const &outSym) { return outSym; }, this->completionOutput);
        // ... return a transition to the completing state with the appropriate output
        return Transition(s.getId(), input, completionId, errorStateId);
    } else {
        Index errorStateCompletionId = findIDs(presentationLayer.outputAlphabet, [](auto const &outSym) { return outSym; }, this->errorStateCompletionOutput);
        // ... else return a self-loop transition with the appropriate output
        return Transition(errorStateId, input, errorStateCompletionId, errorStateId);
    }
}
ToAutoCompleteWithErrorStateTransformer::ToAutoCompleteWithErrorStateTransformer(Fsm *fsmToTransform, string completionOutputParam, string errorStateOutput, string errorStateName)
 : ToAutoCompleteTransformer(fsmToTransform),
   // Determining the ID of the output to use for completing transitions for
   // regular states. If there is no such output, the completion ID will be the
   // size of the output alphabet.
   completionOutput(std::move(completionOutputParam)),
   // Determining the ID of the state to use for completing transitions. If
   // there is no such state, the ID will be the size of the set of states.
   errorState(std::move(errorStateName)),
   // Determining the ID of the output to use for completing transitions for
   // the completing state. If there is no such output, the completion ID will
   // be the size of the output alphabet.
   errorStateCompletionOutput(std::move(errorStateOutput))
{
}

void ToAutoCompleteWithErrorStateTransformer::visit(Fsm &fsmToTransform) {
    auto completionOutputMap = findIDs(fsmToTransform.getPresentationLayer().outputAlphabet, [](auto sym) { return sym; }, {this->completionOutput, this->errorStateCompletionOutput});
    Index completionId = completionOutputMap.at(this->completionOutput);
    Index errorStateCompletionId = completionOutputMap.at(this->errorStateCompletionOutput);
    Index errorStateId = findIDs(fsmToTransform.getPresentationLayer().stateNames, [](auto const &outSym) { return outSym; }, this->errorState);

    // Creating a clone of the fsm to be transformed. This clone can then be
    // modified.
    this->transformedFsm = fsmToTransform.clone();
    PresentationLayer modifiedPl = fsmToTransform.getPresentationLayer();

    // If the ID of the output to be used for completing transitions for
    // regular states is equal or greater to the size of the output alphabet,
    // the output does not yet exist and shall be inserted into the alphabet
    if(fsmToTransform.getPresentationLayer().outputAlphabet.size() <= completionId) {
        modifiedPl.outputAlphabet.push_back(completionOutput);
    }
    // If the ID of the output to be used for completing transitions for the
    // completing state is equal or greater to the size of the output alphabet,
    // the output does not yet exist and shall be inserted into the alphabet
    if(fsmToTransform.getPresentationLayer().outputAlphabet.size() <= errorStateCompletionId) {
        modifiedPl.outputAlphabet.push_back(this->errorStateCompletionOutput);
    }
    // If the ID of the state to be used as target for completing transitions is equal
    // or greater to the size of the set of states, the state does not yet
    // exist and shall be inserted into the state set
    if(fsmToTransform.getStates().size() <= errorStateId) {
        States states = fsmToTransform.getStates();
        states.push_back(State(this->errorState, errorStateId));
        modifiedPl.stateNames.push_back(this->errorState);
        this->transformedFsm = this->transformedFsm->modify(nullopt, states, modifiedPl, nullopt);
    }

    this->transformedFsm = this->transformedFsm->modify(nullopt, nullopt, modifiedPl, nullopt);

    ToAutoCompleteTransformer::visit(fsmToTransform);
}

void ToAutoCompleteWithErrorStateTransformer::visit(Ofsm &ofsm) {
    this->visit(static_cast<Fsm&>(ofsm));
}
void ToAutoCompleteWithErrorStateTransformer::visit(Dfsm &dfsm) {
    this->visit(static_cast<Fsm&>(dfsm));
}

}


