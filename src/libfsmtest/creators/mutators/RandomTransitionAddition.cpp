/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <random>
#include "RandomTransitionAddition.hpp"

namespace libfsmtest {

bool RandomTransitionAddition::applyMutation() {
    size_t numberOfStates = transformedFsm->size();

    if (numberOfStates == 0) return false;

    size_t numberOfInputs = transformedFsm->getInputAlphabetSize();
    size_t numberOfOutputs = transformedFsm->getOutputAlphabetSize();

    std::random_device rd;
    std::default_random_engine gen(rd());
    std::uniform_int_distribution<size_t> distributeStates(0, numberOfStates - 1);
    std::uniform_int_distribution<size_t> distributeInput(0, numberOfInputs - 1);
    std::uniform_int_distribution<size_t> distributeOutput(0, numberOfOutputs - 1);

    Index source = distributeStates(gen);
    Index input = distributeInput(gen);
    Index output = distributeOutput(gen);
    Index target = distributeStates(gen);

    transformedFsm->getStates().at(source).addTransition(input, output, target);

    return true;
}

} // namespace libfsmtest

