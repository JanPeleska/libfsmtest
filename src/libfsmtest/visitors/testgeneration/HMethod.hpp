/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <tuple>
#include "testsuite/Tree.hpp"
#include "visitors/testgeneration/TestGenerationVisitor.hpp"

namespace libfsmtest {

/**
 * Class implementing the H-Method for test generation.
 */
class HMethod : public TestGenerationVisitor {

protected:

    /**
     * The maximal number of additional states,
     * which the implementation DFSM in minimised
     * form may have, when compared to the reference
     * model in minimised form.
     */
    const size_t numAddStates;
    
    /** OFSM tables calculated from the minimised reference model */
    std::map< size_t, std::set<Index> > classMap;
    std::vector< std::map<Index,size_t> > state2classMaps;
    
    std::vector< std::tuple<Trace, Trace, std::vector< std::pair<Trace,Trace> > > > oneOfContainer;
    
    void getShortestDistinguishingTraces(Ofsm& fsm,
                                         Index s0,
                                         Index s1,
                                         size_t inputAlphaSize,
                                         Trace& currentTrace,
                                         Traces& allShortestDisTraces_s0_s1);
    
    void getShortestDistinguishingTraces(Ofsm& fsm,
                                         Index s0,
                                         Index s1,
                                         size_t inputAlphaSize,
                                         Trace& currentTrace,
                                         Traces& allShortestDisTraces_s0_s1,
                                         size_t k);
    
    void getShortestDistinguishingTraces(Dfsm& fsm,
                                         Index s0,
                                         Index s1,
                                         size_t inputAlphaSize,
                                         Trace& currentTrace,
                                         Traces& allShortestDisTraces_s0_s1,
                                         size_t k);
    
    void getShortestDistinguishingTraces(Dfsm& fsm,
                                         Index s0,
                                         Index s1,
                                         size_t inputAlphaSize,
                                         Trace& currentTrace,
                                         Traces& allShortestDisTraces_s0_s1);
    
    void getDistinguishingTraces(Fsm& fsm,
                                 Index s0,
                                 Index s1,
                                 size_t depth,
                                 size_t inputAlphaSize,
                                 Trace& currentTrace,
                                 Traces& allDisTraces_s0_s1);
    
    /** Return true iff two containers of strings differ */
    static bool differs(const Strings& s1, const Strings& s2);
    
    void processDistinguishingTraces(const Trace& alpha, const Trace& beta, Traces& distTraces);
    void processOneTraceInsertions(const Trace& alpha, const Trace& beta, Traces& distTraces);

    void addBestCandidateTraces(Fsm &fsm, size_t inputAlphabetSize);
    
    static bool haveNewIndexVec(std::vector<size_t>& indexVec,
                         const std::vector< std::tuple< Trace, Trace, std::vector< std::pair<Trace,Trace> > > >& oneOfContainer);
    
public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation DFSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     */
    HMethod(const size_t numAddStatesParam) :
    TestGenerationVisitor(),
    numAddStates(numAddStatesParam)
    {

    }

    /**
     * Method implementing the actual H-Method for test generation on a FSM.
     * Only called indirectly, see Fsm::accept()
     * @param fsm The FSM to generate the test suite for.
     */
    void visit(Fsm & fsm) override;

    /** Generation for observable FSMs */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual H-Method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     */
    void visit(Dfsm & dfsm) override;
    
};

} // namespace libfsmtest
