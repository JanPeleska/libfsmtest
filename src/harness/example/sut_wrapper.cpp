#include <string>
#include <map>

using namespace std;

// Include header files of library to be tested
#include "gdclib.hpp"

/**
 *   Helper data structures and functions for
 *   SUT test wrapper
 */

map<string,gdc_inputs_t> fsmIn2gdcIn = {
    {"e1",e1}, {"e2",e2}, {"e3",e3}, {"e4",e4}
};
map<gdc_outputs_t,string> gdcOut2fsmOut = {
    {nop,"null"}, {a1,"a1"}, {a2,"a2"}, {a3,"a3"}, {a4,"a4"}
};


using namespace std;


void sut_init() {
    
    // Maps are already defined above
     
    // Initialise SUT, if required, by calling
    // initialisation functions or initialising
    // global SUT variables
    gdc_reset();
    
}

void sut_reset() {
    gdc_reset();
}


const string sut(const string& input) {
    
    string fsmOutputEvent;
    
    map<string,gdc_inputs_t>::iterator inputIte = fsmIn2gdcIn.find(input);
    if ( inputIte == fsmIn2gdcIn.end() ) return fsmOutputEvent;
    
    gdc_outputs_t y = gdc( inputIte->second );
    
    map<gdc_outputs_t,string>::iterator outputIte = gdcOut2fsmOut.find(y);
    if ( outputIte == gdcOut2fsmOut.end() ) return fsmOutputEvent;
    
    fsmOutputEvent = outputIte->second;
    
    return fsmOutputEvent;
    
}
