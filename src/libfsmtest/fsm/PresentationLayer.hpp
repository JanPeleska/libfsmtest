/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <utility>
#include <vector>
#include <algorithm>
#include <sstream>
#include <optional>

#include "testsuite/Trace.hpp"
#include "definitions.hpp"

namespace libfsmtest {

struct PresentationLayer {

    Strings inputAlphabet;
    Strings outputAlphabet;
    Strings stateNames;

    PresentationLayer() = default;
    PresentationLayer(Strings i, Strings o, Strings s) :
            inputAlphabet(std::move(i)),
            outputAlphabet(std::move(o)),
            stateNames(std::move(s)) {}

    [[nodiscard]] std::optional<Index> getInputId(const std::string &input) const {
        for (size_t i = 0; i < inputAlphabet.size(); i++) {
            if (inputAlphabet.at(i) == input) return i;
        }
        return std::nullopt;
    }

    [[nodiscard]] std::optional<Index> getOutputId(const std::string &output) const {
        for (size_t i = 0; i < outputAlphabet.size(); i++) {
            if (outputAlphabet.at(i) == output) return i;
        }
        return std::nullopt;
    }

    [[nodiscard]] std::optional<Index> getStateId(const std::string &stateName) const {
        for (size_t i = 0; i < stateNames.size(); i++) {
            if (stateNames.at(i) == stateName) return i;
        }
        return std::nullopt;
    }

    [[nodiscard]] std::string inputTraceToString(const Trace &t) const {
        std::ostringstream result;
        for (auto &i: t) {
            if (result.tellp() > 0)
                result << ", ";

            if (i >= inputAlphabet.size()) {
                result << "ERROR "
                       << i;
                continue;
            } else {
                result << inputAlphabet.at(i);
            }
        }
        return result.str();
    }

    [[nodiscard]] std::string outputTraceToString(const Trace &t) const {
        std::ostringstream result;
        for (auto &i: t) {
            if (result.tellp() > 0)
                result << ", ";

            if (i >= outputAlphabet.size()) {
                result << "ERROR "
                       << i;
                continue;
            } else {
                result << outputAlphabet.at(i);
            }
        }
        return result.str();
    }
};

}
