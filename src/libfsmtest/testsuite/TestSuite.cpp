/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>
#include <iostream>
#include "creators/input/FsmFromRawCreator.hpp"
#include "testsuite/TestSuite.hpp"
#include "visitors/output/ToFsmFileVisitor.hpp"

using namespace std;

namespace libfsmtest {

TestSuite::TestSuite(const std::string& nameParam,
                     const std::string& fsmName) : name(nameParam) {
    
    // Create reference FSM from file
    fsm = createFsm<FsmFromRawCreator>(fsmName+".fsm",
                                       fsmName+".state",
                                       fsmName+".in",
                                       fsmName+".out",
                                       fsmName);
    
    const PresentationLayer& pl = fsm->getPresentationLayer();
    
    // Read input traces from text file and convert to internal traces format
    ifstream tracesFile(nameParam+".txt");
    string line;
    string thisInput;
    optional<Index> x; // Input in internal encoding

    if ( !tracesFile.is_open() ) throw runtime_error("Failed to open input traces file " + nameParam + ".txt");
    
    while ( getline(tracesFile,line) ) {
        // Skip empty lines
        if ( line.empty() ) continue;
        
        // Create a new input trace
        Trace thisTrace;

        // line contains 1 input trace
        size_t pos;
        for ( pos = line.find(',');
             pos != string::npos;
             pos = line.find(',') ) {
            thisInput = string(line,0,pos);
            trim(thisInput);
            line.erase(0,pos+1);
            x = pl.getInputId(thisInput);
            if ( not x.has_value() )
                throw runtime_error("Cannot convert input "
                                    + thisInput
                                    + " with presentation layer");
            thisTrace.append(x.value());
        }
        // Handle last input in line
        thisInput = line;
        trim(thisInput);
        x = pl.getInputId(thisInput);
        if ( not x.has_value() )
            throw runtime_error("Cannot convert input "
                                + thisInput
                                + " with presentation layer");
        thisTrace.append(x.value());
        
        // Add input trace to test suite
        addInputTrace(thisTrace);
    }
}


void TestSuite::writeToFile() {
    
    // Write FSM to file
    ToFsmFileVisitor toFile(fsm->getName()+".fsm",
                            fsm->getName()+".in",
                            fsm->getName()+".out",
                            fsm->getName()+".state");
    
    fsm->accept(toFile);
    toFile.writeToFile();
    
    // Write traces to file
    ofstream tracesFile(name+".txt");
    if ( !tracesFile.is_open() )
        throw runtime_error("Failed to open output traces file " + name + ".txt");
    
    for ( const auto& tr : inputTraces ) {
        tracesFile << fsm->getPresentationLayer().inputTraceToString(tr) << endl;
    }
}


Strings TestSuite::getInputTraces() const {
    Strings strVec;
    
    for ( const auto& tr : inputTraces ) {
        strVec.push_back(fsm->getPresentationLayer().inputTraceToString(tr));
    }
    
    return strVec;
}
 

size_t TestSuite::getTotalLength() const {
    size_t length = 0;
    for ( const auto& tr : inputTraces ) {
        length += tr.size();
    }
    
    return length;
}

}
