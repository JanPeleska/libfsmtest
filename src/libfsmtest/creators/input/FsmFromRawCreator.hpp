/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <utility>
#include <optional>

#include "creators/FsmCreator.hpp"

namespace libfsmtest {

/**
 * Factory for reading a FSM specification from the low-level file format.
 */
class FsmFromRawCreator : public libfsmtest::FsmCreator {
private:
    /**
     * Paths to the files of the specification.
     */
    const std::string fsmName, fsmFileName, statesFileName, inputFileName, outputFileName;

    /** The states of the FSM */
    States states;

    /** The presentation layer of the FSM */
    PresentationLayer presentationLayer;

    /** The index of the initial state of the FSM */
    std::optional<Index> initialState;

    /**
     * Reads and parses the low-level file format.
     * Fills #states and #presentationLayer.
     */
    void readFiles();

    /**
     * Reads the content of the given file into a vector of lines.
     * @param fileName The file to be read.
     * @return A vector containing the lines of the file.
     */
    static std::vector<std::string> readFile(const std::string& fileName);

public:
    /**
     * Constructor.
     * Won't start reading or parsing the input.
     *
     * @param fsmFileNameParam The file containing the transitions.
     * @param statesFileNameParam The file containing the state names.
     * @param inputFileNameParam The file containing the input alphabet.
     * @param outputFileNameParam The file containing the output alphabet.
     * @param fsmNameParam Optional name to identify the FSM to be created
     */
    FsmFromRawCreator(std::string  fsmFileNameParam,
                       std::string  statesFileNameParam,
                       std::string  inputFileNameParam,
                       std::string  outputFileNameParam,
                       std::string  fsmNameParam = std::string(""))
                       :
    fsmName(std::move(fsmNameParam)),
    fsmFileName(std::move(fsmFileNameParam)),
    statesFileName(std::move(statesFileNameParam)),
    inputFileName(std::move(inputFileNameParam)),
    outputFileName(std::move(outputFileNameParam))  {}

    /**
     * Constructor.
     * Only takes one file (the specification in low-level format),
     * fills the presentation layer only with indices from that.
     * 
     * Won't start reading or parsing the input.
     *
     * @param fsmFileNameParam The file containing the transitions.
     * @param fsmNameParam Optional name to identify the FSM to be created
     */
    explicit FsmFromRawCreator(std::string fsmFileNameParam, std::string  fsmNameParam = std::string(""))
        : fsmName(std::move(fsmNameParam)), fsmFileName(std::move(fsmFileNameParam)) {}

    /**
     * Creates a FSM from the given input files. If the FSM is
     * deterministic, a pointer to a Dfsm-instance will be returned.
     * @see readFiles()
     * @return A unique pointer to the constructed FSM.
     */
    std::unique_ptr<Fsm> createFsm() override;
};

} // namespace libfsmtest



