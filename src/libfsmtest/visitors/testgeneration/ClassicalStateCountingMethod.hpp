#pragma once

#include "StrongStateCountingMethod.hpp"

namespace libfsmtest {


/**
 * Classical state counting implemented by restricting the
 * method for strong reduction testing to completely specified
 * FSMs.
 * Differs from the method for strong reduction testing by allowing
 * the transformation of non-observable FSMs to observable FSMs,
 * as this does not change the language of the FSMs and thus
 * preserves the reduction relation on completely specified FSMs.
 */
class ClassicalStateCountingMethod : public StrongStateCountingMethod {

protected:
    
public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation FSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     * @param calculateAllMaximalRDistinguishableSetsParam If 'true', then all maximal sets of pairwise 
     *                                                     r-distinguishable states of the FSM are computed.
     *                                                     Otherwise for each state of the FSM only a single
     *                                                     such set is computed.
     */
    explicit ClassicalStateCountingMethod(size_t numAddStatesParam, bool calculateAllMaximalRDistinguishableSetsParam = false) :
        StrongStateCountingMethod(numAddStatesParam,calculateAllMaximalRDistinguishableSetsParam)
    {

    }

    /**
     * Method implementing the actual State Counting method for test generation on a FSM.
     * Only called indirectly, see Fsm::accept()
     * @param fsm The FSM to generate the test suite for.
     */
    void visit(Fsm & fsm) override;

    /**
     * Method implementing the actual State Counting method for test generation on a OFSM.
     * Only called indirectly, see Ofsm::accept()
     * @param fsm The OFSM to generate the test suite for.
     */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual State Counting method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     */
    void visit(Dfsm & dfsm) override;
    
};

} // namespace libfsmtest
