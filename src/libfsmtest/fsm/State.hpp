/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <string>
#include <vector>
#include <unordered_set>

#include "fsm/Transition.hpp"
#include "visitors/FsmVisitor.hpp"

namespace libfsmtest {

/**
 * Class representing FSM states
 */
class State {
    
private:
    
    /** Internal state number */
    Index id;
    
    /** State name */
    std::string name;
    
    /** vector of outgoing transitions */
    Transitions transitions;
    
    
    
public:
    
    State(std::string  aStateName, Index theId);
    
    /**
     * Copy constructor
     */
    State(const State& state);
    
    State& operator=(const State& other) {
        return *(new State(other));
    }
    
    Transitions& getTransitions();
    Transitions const& getTransitions() const;
    
    Index getId() const { return id; }
    
    std::string getName() const { return name; }
    
    bool isDeterministic() const;
    
    /**
     * Return true iff vor every input of the specified alphabet,
     * there exists a transition triggered by this input.
     * @param inputAlphabetSize Size of the input alphabet
     * @note Events of the input alphabet are internally numbered by 0..lastInputId
     */
    bool isCompletelySpecified(size_t inputAlphabetSize) const;
    
    /**
     * Return true if the transitions of this state are
     * observable. This means that each input/output pair
     * uniquely determines the post-state.
     */
    bool isObservable() const;
    
    /** Accept FsmVisitors */
    void accept(FsmVisitor& v) { v.visit(*this); }
    
    /** Return true iff state has an outgoing transition with label x/y */
    bool hasTransition(Index x, Index y) const;

    /** Return true iff state has an outgoing transition for input x */
    bool hasTransition(Index x) const;

    /** 
     * Add a transition to this state.
     */
    void addTransition(Index inputIndex, Index outputIndex, Index targetId);

    /**
     * Remove a transition from this state.
     * Will throw an error when the transition does not exist.
     * @param offset The index of the transition in #transitions
     */
    void removeTransition(size_t offset);

    /**
     * Collect the defined inputs of this state,
     * i.e., all inputs x such that the state has 
     * an outgoing transition labelled with input x.
     */
    std::unordered_set<Index> getDefinedInputs() const;

    /**
     * Collect all outputs y such that this state
     * has a transition with input x and output y.
     */    
    std::unordered_set<Index> getResponsesToInput(Index x) const;

    
};

using States = std::vector<State>;

} // namespace libfsmtest

