/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once
#include "Transformer.hpp"
#include <string>

namespace libfsmtest {

// Forward declarations to avoid unnecessary includes
class Fsm;
class Ofsm;
class Dfsm;
class State;

using namespace std;

/**
 * Transform any machine to a completely specified machine. A completely specified machine is
 * contains a transition for each source state/input pair.
 */
class ToAutoCompleteTransformer : public Transformer {
protected:
    /**
     * Abstract function to return a transition used for completion.
     * @param s The source state to be completed for the given input
     * @param input Input ID of the input that is to be completed
     * @return A transition with s as it's source state and input as input ID. Output ID and target state are determined by the implementation.
     */
    virtual Transition getCompletingTransition(PresentationLayer const &presentationLayer, State const &s, Index input) = 0;

public:
    /**
     * Transformer constructor
     */
    ToAutoCompleteTransformer(Fsm *fsm);
    /**
     * Default virtual destructor
     */
    ~ToAutoCompleteTransformer() override = default;

    /**
     * Visitor function for the Fsm class.
     */
    void visit(Fsm& fsm) override;

    /**
     * Visitor function for the Ofsm class.
     */
    void visit(Ofsm& fsm) override;

    /**
     * Visitor function for the Dfsm class.
     */
    void visit(Dfsm& fsm) override;
};

/**
 * Transform any machine to a completely specified machine by adding a
 * transition where the source state is the target state and the output is some
 * value to be specified by the user for each input that is not specified for a
 * state.
 */
class ToAutoCompleteWithSelfLoopTransformer : public ToAutoCompleteTransformer {
private:
    /// The output ID of the output to use for completion
    string const completionOutput;
    /**
     * Implementation of the function returning a completing transition.
     * @param s The source state for which the transition shall be generated
     * @param input The input id of the input for which the transition shall be generated
     * @return A transition completing state s for input id input
     */
    Transition getCompletingTransition(PresentationLayer const &presentationLayer, State const &s, Index input) override;
public:
    /**
     * The constructor
     * @param fsm The FSM to transform
     * @param completionOutput The output to use for the generated completing
     *  transitions. If it exists in the output alphabet of fsm, that output
     *  will be used, otherwise a new one will be inserted into the output
     *  alphabet.
     */
    ToAutoCompleteWithSelfLoopTransformer(Fsm *fsm, string completionOutput);
    ~ToAutoCompleteWithSelfLoopTransformer() override = default;

    void visit(Fsm &fsm) override;
    void visit(Ofsm &ofsm) override;
    void visit(Dfsm &dfsm) override;
};

/**
 * Transform any machine to a completely specified machine by adding to each
 * state q of the machine and each input x not defined in q a transition from
 * q with input x to a fixed error state with a fixed y.
 * The choice of output y depends on whether q is the error state.
 * The error state and the outputs used in completing the machine will be added
 * to the machine if they are not already contained in its state set or output
 * alphabet, respectively. 
 */
class ToAutoCompleteWithErrorStateTransformer : public ToAutoCompleteTransformer {
private:
    /// The output ID of the output to use for completion of states other than the error state
    string const completionOutput;
    /// The state ID of the error state to transition to for all completing transitions
    string const errorState;
    /// The output ID of the output to use for completion of the error state
    string const errorStateCompletionOutput;
    /**
     * Implementation of the function returning a completing transition.
     * @param s The source state for which the transition shall be generated
     * @param input The input id of the input for which the transition shall be generated
     * @return A transition completing state s for input id input
     */
    Transition getCompletingTransition(PresentationLayer const &presentationLayer, State const &s, Index input) override;
public:
    /**
     * The constructor
     * @param fsm The FSM to transform
     * @param completingOutput The output to use for the generated completing
     *  transitions of regular states (i.e. states other than the error state). 
     *  If it does not exist in the output alphabet of fsm, it will be inserted 
     *  into the output alphabet.
     * @param errorStateOutput The output to use for the generated completing
     *  transitions of the error state. 
     *  If it does not exist in the output alphabet of fsm, it will be inserted 
     *  into the output alphabet.
     * @param errorStateName The state to use as the target state for the
     *  generated completing transitions. 
     *  If it does not exist in the state set of fsm, it will be inserted into the state set.
     */
    ToAutoCompleteWithErrorStateTransformer(Fsm *fsm, string completingOutput, string errorStateOutput, string errorStateName);
    ~ToAutoCompleteWithErrorStateTransformer() override = default;

    void visit(Fsm &fsm) override;
    void visit(Ofsm &ofsm) override;
    void visit(Dfsm &dfsm) override;
};

}


