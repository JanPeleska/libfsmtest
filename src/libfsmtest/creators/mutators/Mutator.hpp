/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include "creators/transformers/Transformer.hpp"

namespace libfsmtest {

/**
 * Abstract class to create FSM mutants.
 */
class Mutator : public Transformer {
protected:
    /** The number of mutations that should be applied to the FSM */
    size_t mutationsToApply;

    /**
     * Applies a single mutation to the FSM.
     * @return true, when the mutation was applied,
     *         false, when the mutation could not be applied.
     */
    virtual bool applyMutation() = 0;

    /**
     * Constructor, only to be used by derived classes.
     * @param fsmParam The FSM the mutant should be created from.
     * @param mutationsToApplyParam The number of mutations that should be applied.
     * @param nameSuffixParam Optional, will be appended to the name of the FSM.
     */
    Mutator(Fsm* fsmParam, size_t mutationsToApplyParam, const std::string& nameSuffixParam = "_MUT")
        : Transformer(fsmParam, nameSuffixParam), mutationsToApply(mutationsToApplyParam) {}

public:
    /**
     * Starts mutating an FSM.
     * Only called indirectly.
     * @see Fsm::accept()
     * @see Transformer::createFsm()
     * @param fsmToMutate The FSM to mutate.
     */
    void visit(Fsm& fsmToMutate) override;
    /**
     * Starts mutating an OFSM.
     * Only called indirectly.
     * @see Ofsm::accept()
     * @see Transformer::createFsm()
     * @param ofsm The OFSM to mutate.
     */
    void visit(Ofsm& ofsm) override;

    /**
     * Starts mutating an DFSM.
     * Only called indirectly.
     * @see Dfsm::accept()
     * @see Transformer::createFsm()
     * @param dfsm The DFSM to mutate.
     */
    void visit(libfsmtest::Dfsm& dfsm) override;
};

} // namespace libfsmtest
