/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <set>
#include <map>
#include <deque>
#include <iostream>
#include <algorithm>
#include <utility>
#include <cassert>

#include "Ofsm.hpp"

using namespace std;


namespace libfsmtest {

void Ofsm::createTransitionTable() {
    
    // transitionTable maps each state s to a Matrix mapping inputs/outputs x/y to post state
    // s' = transitionTable[s][x][y]. If there is no transition from s for label x/y then
    // transitionTable[s][x][y] == nullopt
    if(transitionTable == nullptr) {
        transitionTable = new optional<Index>**[states.size()];
        for ( size_t p = 0; p < states.size(); p++ ) {
            transitionTable[p] = new optional<Index>*[presentationLayer.inputAlphabet.size()];
            for ( size_t q = 0; q < presentationLayer.inputAlphabet.size(); q++ ) {
                transitionTable[p][q] = new optional<Index>[presentationLayer.outputAlphabet.size()];
            }
        }
    }

    // Now overwrite nullopt by defined post states for all transitions
    for ( size_t s = 0; s < states.size(); s++ ) {
        State& theState = states.at(s);
        for ( const auto& tr : theState.getTransitions() ) {
            if(states.size() <= tr.getTargetId()) {
                throw runtime_error("ERROR: Invalid transition: Target state ID too large.");
            }
            if(presentationLayer.inputAlphabet.size() <= tr.getInput()) {
                throw runtime_error("ERROR: Invalid input ID.");
            }
            if(presentationLayer.outputAlphabet.size() <= tr.getOutput()) {
                throw runtime_error("ERROR: Invalid output ID.");
            }
            transitionTable[s][tr.getInput()][tr.getOutput()] = tr.getTargetId();
        }
    }
    
}


void Ofsm::resetTransitionTable() {
    if(transitionTable != nullptr) {
        for ( size_t p = 0; p < this->states.size(); p++ ) {
            for ( size_t q = 0; q < this->presentationLayer.inputAlphabet.size(); q++ ) {
                delete[] transitionTable[p][q];
            }
            delete[] transitionTable[p];
        }
        delete[] transitionTable;
    }
    transitionTable = nullptr;
}

Ofsm::Ofsm(const string& fsmName,
                     States stateVec,
                     PresentationLayer pl,
                     size_t initialState) :
Fsm(fsmName,std::move(stateVec),move(pl),initialState),
transitionTable(nullptr)
{
    createTransitionTable();
}

Ofsm::Ofsm(const Ofsm& fsm) : Fsm(fsm), transitionTable(nullptr) {
    createTransitionTable();
}

Ofsm::~Ofsm() {
    resetTransitionTable();
}
 
 
void Ofsm::calcOFSMTables(map< size_t, set<Index> >& classMap,
                          vector< map<Index,size_t> >& state2classMaps) const {
        
    // Initially, we only have one class, containing all states
    classMap[0] = set<Index>();
    for ( Index s = 0; s < states.size(); s++ ) {
        classMap[0].insert(s);
    }
    
    // This map maps states to their current class
    map<Index,size_t> state2class;
    // Initially, all states are in class 0
    for ( Index s = 0; s < states.size(); s++ ) {
        state2class[s] = 0;
    }
    
    state2classMaps.push_back(state2class);
    
    
    // Calculate the equivalence classes
    bool haveNewClass;
    do {
        
        haveNewClass = false;
        size_t numClasses = classMap.size();
        size_t nextClass = numClasses;
        for ( size_t c = 0; c < numClasses; c++ ) {
            Index representant = *classMap[c].cbegin();
            // Check if class number c should be split
            auto sIte = classMap[c].begin();
            for ( ++sIte;
                 sIte != classMap[c].end();
                 sIte++ ) { // loop_c
                Index otherStateId = *sIte;
                for ( Index x = 0; x < presentationLayer.inputAlphabet.size(); x++ ) {
                    for ( Index y = 0; y < presentationLayer.outputAlphabet.size(); y++ ) {
                        optional<Index> representantPostStateId = transitionTable[representant][x][y];
                        optional<Index> otherPostStateid = transitionTable[otherStateId][x][y];
                        
                        // representant and otherStateId are NOT equivalent if
                        // (1) They have different values under x/y in the transitionTable (This is implied by them being mapped to different classes)
                        // (2) Either one of the values is -1 (no post state defined for x/y), or
                        // (3) Both post states are defined, but are located in different classes
                        if ( (representantPostStateId.has_value() != otherPostStateid.has_value()
                              or (representantPostStateId.has_value() and state2class[representantPostStateId.value()] != state2class[otherPostStateid.value()]))) { // By short-circuit logic, both variables contain values when this line is evaluated.
                            haveNewClass = true;
                            // The new class has number nextClass and representative otherStateId
                            classMap[nextClass] = set<Index> { otherStateId };
                            // Erase otherStateId from its old class
                            sIte = classMap[c].erase(sIte);
                            // Check whether other elements of classMap[c] should also be moved into the new class
                            for ( auto sIte2 = sIte; sIte2 != classMap[c].end();
                                  /* sIte2 updated at end of loop */ ) {
                                for ( Index a = 0; a < presentationLayer.inputAlphabet.size(); a++ ) {
                                    for ( Index b = 0; b < presentationLayer.outputAlphabet.size(); b++ ) {
                                        otherPostStateid = transitionTable[otherStateId][a][b];
                                        optional<Index> sItPostStateid = transitionTable[*sIte2][a][b];
                                        
                                        // Jump if not equivalent
                                        if ( otherPostStateid != sItPostStateid &&
                                            (not otherPostStateid.has_value() || not sItPostStateid.has_value() ||
                                             state2class[otherPostStateid.value()] != state2class[sItPostStateid.value()]) ) { // By short-circuit logic, both variables contain values when this line is evaluated.
                                            goto notEquivalent;
                                        }
                                    }
                                }
                                // *sIte2 and otherStateId are equivalent, put *sIte2 into
                                // otherStateId's class and remove it from classMap[c]
                                classMap[nextClass].insert(*sIte2);
                                sIte2 = classMap[c].erase(sIte2);
                                continue;
                                
                                // We jump here if *sIte and otherStateId are NOT equivalent
                                notEquivalent:
                                sIte2++;
                                ;
                            }
                            
                            // We are done with the new class, increment nextClass
                            nextClass++;
                           
                            // Set the iterator of loop c to the new next element after
                            // the representative
                            sIte = classMap[c].begin();
                            
                            // start next do-while cycle
                            goto loop_c_end;
                        }
                    }
                }
            loop_c_end:
            ;
            }
        }
        
        // update the state2class map
        for ( size_t d = 0; d < classMap.size(); d++ ) {
            for ( auto& id : classMap[d] ) {
                state2class[id] = d;
            }
        }
        state2classMaps.push_back(state2class);
        
#if 0
        cout << "state2class" << endl;
        for ( int i = 0; i < state2class.size(); i++ ) {
            cout << i << " --> " << state2class.at(i) << endl;
        }
        
        cout << "classMap" << endl;
        for ( size_t c = 0; c < classMap.size(); c++ ) {
            cout << c << " --> {";
            for ( auto& id : classMap[c] ) {
                cout << id << ",";
            }
            cout << "}" << endl;
        }
#endif
        
    } while (haveNewClass);
    
}


 

 
bool Ofsm::isMinimal() const {
    
    if ( ! isInitiallyConnected() ) return false;
    
    // A vector of state2class maps, created during execution of OFSM table algorithm
    vector< map<Index,size_t> > state2classMaps;
    // The map from class ids in 0,1.. to sets of states ids whose states
    // are members of that class
    map< size_t, set<Index> > classMap;
    
    // Calculate the equivalence classes. This fills the three variables
    // fsmTable,classMap,state2classMaps
    calcOFSMTables(classMap,state2classMaps);
    
    // This OFSM is minimal iff classMap has the same number
    // of elements as states.
    return ( classMap.size() == states.size() );
    
}

bool Ofsm::isInitiallyConnected() const {
    
    // On termination, initiallyConnectedStates contains the ids of
    // all initially connected states
    set<Index> initiallyConnectedStates;
    
    //  Breadth-first-search queue
    deque<Index> bfsQueue;
    
    // Initial state is always reachable
    initiallyConnectedStates.insert( initialStateId );
    bfsQueue.push_back(initialStateId);
    
    while ( ! bfsQueue.empty() ) {
        Index currentStateId = bfsQueue.front();
        bfsQueue.pop_front();
        
        // Capture all nodes reachable from current node
        for ( Index x = 0; x < presentationLayer.inputAlphabet.size(); x++ ) {
            for ( Index y = 0; y < presentationLayer.outputAlphabet.size(); y++ ) {
                optional<Index> targetStateId = transitionTable[currentStateId][x][y];
                // If the target state exists and has not already been identified
                // as initially connected ...
                if ( targetStateId.has_value() &&
                     initiallyConnectedStates.find(targetStateId.value()) == initiallyConnectedStates.end() ) {
                    // ... insert into set of initially connected states and ...
                    initiallyConnectedStates.insert(targetStateId.value());
                    // ... append it to the bfsQueue
                    bfsQueue.push_back(targetStateId.value());
                }
            }
        }
    }
    
    // Initially connected iff cardinality of initiallyConnectedNodes
    // equals number of states
    return ( initiallyConnectedStates.size() == size() );
}

unique_ptr<Fsm> Ofsm::clone() const {
    return make_unique<Ofsm>(*this);
}

std::unique_ptr<Fsm> Ofsm::modify(std::optional<std::string> fsmName,
                                  std::optional<States> newStates,
                                  std::optional<PresentationLayer> pl,
                                  std::optional<Index> newInitialStateId) const {
    if(not fsmName.has_value()) {
        fsmName = this->name;
    }
    if(not newStates.has_value()) {
        newStates = this->states;
    }
    if(not pl.has_value()) {
        pl = presentationLayer;
    }
    if(not newInitialStateId.has_value()) {
        newInitialStateId = this->initialStateId;
    }
    return make_unique<Ofsm>(fsmName.value(), newStates.value(), move(pl.value()), newInitialStateId.value());
}

 
} // namespace libfsmtest

