/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <memory>
#include <vector>
#include <deque>
#include <stdexcept>

#include "ToMinimisedTransformer.hpp"
#include "ToInitiallyConnectedTransformer.hpp"
#include "ToObservableTransformer.hpp"



using namespace std;

namespace libfsmtest {


bool ToMinimisedTransformer::minimiseConnected(Ofsm& fsm,
                                                    States& statesMin,
                                                    Strings& stateNamesMin) {
    
    // A vector of state2class maps, created during execution of OFSM table algorithm
    vector< map<Index,size_t> > state2classMaps;
    // The map from class ids in 0,1.. to sets of states ids whose states
    // are members of that class
    map< size_t, set<Index> > classMap;
    
    // Calculate the equivalence classes. This fills the three variables
    // fsmTable,classMap,state2classMaps
    fsm.calcOFSMTables(classMap,state2classMaps);

    if(classMap.size() == fsm.size()) {
        return false;
    }
    
    // The final state-to-class map
    map<Index,size_t>& state2class = state2classMaps.back();
    
    // Create the states of the minimised machine: each class gives rise to one state.
    // The class of the initial state is the new initial state.
    size_t classOfInitialState = state2class[fsm.getInitialStateId()];
    map<size_t,Index> class2newStateId;
    statesMin.reserve(classMap.size());
    Index newStateId = 0;
    statesMin.emplace_back(fsm.labelString(classMap[classOfInitialState]),newStateId);
    class2newStateId[classOfInitialState] = newStateId;
    
    for ( size_t s = (classOfInitialState + 1)%classMap.size();
         s != classOfInitialState; s = (s + 1)%classMap.size() ) {
        statesMin.emplace_back(fsm.labelString(classMap[s]),++newStateId);
        class2newStateId[s] = newStateId;
    }
    
    // Every transition of the original machine gives rise to a transition between source and target equivalence
    // classes (= states in the new machine). Do not add transitions whose x/y label already exists.
    for ( auto& state : fsm.getStates() ) {
        Index newSourceStateId = class2newStateId[ state2class[state.getId()] ];
        for ( auto& tr : state.getTransitions() ) {
            Index x = tr.getInput();
            Index y = tr.getOutput();
            if ( ! statesMin[newSourceStateId].hasTransition(x,y) ) {
                Index newTargetStateId = class2newStateId[ state2class[ tr.getTargetId() ] ];
                statesMin[newSourceStateId].getTransitions().emplace_back(newSourceStateId,x,y,newTargetStateId);
            }
        }
    }
    
    // Insert state names of minimised machine into stateNamesMin
    for ( auto& newState : statesMin ) {
        stateNamesMin.push_back(newState.getName());
    }
    
    return true;
}

bool ToMinimisedTransformer::minimiseConnected(Dfsm& fsm,
                                                    States& statesMin,
                                                    Strings& stateNamesMin) {

    // A vector of state2class maps, created during execution of OFSM table algorithm
    vector< map<Index,size_t> > state2classMaps;
    // The map from class ids in 0,1.. to sets of states ids whose states
    // are members of that class
    map< size_t, set<Index> > classMap;
    
    // Calculate the equivalence classes. This fills the two variables
    // classMap,state2classMaps
    fsm.calcOFSMTables(classMap,state2classMaps);

    if(classMap.size() == fsm.size()) {
        return false;
    }
    
    // The final state-to-class map
    map<Index,size_t>& state2class = state2classMaps.back();
    
    // Create the states of the minimised machine: each class gives rise to one state.
    // The class of the initial state is the new initial state.
    size_t classOfInitialState = state2class[fsm.getInitialStateId()];
    map<size_t,Index> class2newStateId;
    statesMin.reserve(classMap.size());
    Index newStateId = 0;
    statesMin.emplace_back(fsm.labelString(classMap[classOfInitialState]),newStateId);
    class2newStateId[classOfInitialState] = newStateId;
    
    for ( size_t s = (classOfInitialState + 1)%classMap.size();
         s != classOfInitialState; s = (s + 1)%classMap.size() ) {
        statesMin.emplace_back(fsm.labelString(classMap[s]),++newStateId);
        class2newStateId[s] = newStateId;
    }
    
    // Every transition of the original machine gives rise to a transition between source and target equivalence
    // classes (= states in the new machine). Do not add transitions whose x/y label already exists.
    for ( auto& state : fsm.getStates() ) {
        Index newSourceStateId = class2newStateId[ state2class[state.getId()] ];
        for ( auto& tr : state.getTransitions() ) {
            Index x = tr.getInput();
            Index y = tr.getOutput();
            if ( ! statesMin[newSourceStateId].hasTransition(x,y) ) {
                Index newTargetStateId = class2newStateId[ state2class[ tr.getTargetId() ] ];
                statesMin[newSourceStateId].getTransitions().emplace_back(newSourceStateId,x,y,newTargetStateId);
            }
        }
    }
    
    // Insert state names of minimised machine into stateNamesMin
    for ( auto& newState : statesMin ) {
        stateNamesMin.push_back(newState.getName());
    }

    return true;
}

void ToMinimisedTransformer::visit(Fsm& fsmToTransform) {
    // FSM must be observable and initially connected
    if ( ! fsmToTransform.isInitiallyConnected() ) throw runtime_error("ERROR: machine to be minimised must be initially connected.");
    throw runtime_error("ERROR: machine to be minimised must be observable.");
    
}




void ToMinimisedTransformer::visit(Ofsm& fsmToTransform) {
    
    States statesMin;
    Strings stateNamesMin;
    
    if ( ! fsmToTransform.isInitiallyConnected() ) throw runtime_error("ERROR: OFSM must be initially connected for minimisation.");
    
    if(minimiseConnected(fsmToTransform,statesMin,stateNamesMin)) {
        
        // Presentation layer for minimised state machine
        PresentationLayer plMin(fsmToTransform.getPresentationLayer().inputAlphabet,
                                       fsmToTransform.getPresentationLayer().outputAlphabet,
                                       stateNamesMin);
        
        
        transformedFsm = make_unique<Ofsm>(fsmToTransform.getName() + nameSuffix,
                                           statesMin,
                                           plMin,
                                           0 /* Initial states of minimised machine has id 0 */);
    } else {
        transformedFsm = fsmToTransform.clone();
    }
}



void ToMinimisedTransformer::visit(Dfsm& fsmToTransform) {
    
    States statesMin;
    Strings stateNamesMin;
    
    if ( ! fsmToTransform.isInitiallyConnected() ) throw runtime_error("ERROR: DFSM must be initially connected for minimisation.");
    
    if(minimiseConnected(fsmToTransform,statesMin,stateNamesMin)) {

        // Presentation layer for minimised state machine
        PresentationLayer plMin(fsmToTransform.getPresentationLayer().inputAlphabet,
                                       fsmToTransform.getPresentationLayer().outputAlphabet,
                                       stateNamesMin);
        
        
        transformedFsm = make_unique<Dfsm>(fsmToTransform.getName() + nameSuffix,
                                           statesMin,
                                           plMin,
                                           0 /* Initial states of minimised machine has id 0 */);
    } else {
        transformedFsm = fsmToTransform.clone();
    }
}



}
