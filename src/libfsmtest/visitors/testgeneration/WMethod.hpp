/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include "testsuite/Tree.hpp"
#include "visitors/testgeneration/TestGenerationVisitor.hpp"

namespace libfsmtest {

/**
 * Class implementing the W-Method for test generation.
 */
class WMethod : public TestGenerationVisitor {

protected:

    /**
     * The maximal number of additional states,
     * which the implementation DFSM in minimised
     * form may have, when compared to the reference
     * model in minimised form.
     */
    const size_t numAddStates;
    
    
    virtual void createCharacterisationSet(Dfsm& fsm, Traces& characterisationSet);
    virtual void createCharacterisationSet(Ofsm& fsm, Traces& characterisationSet);


public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation DFSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     */
    WMethod(const size_t numAddStatesParam) :
    TestGenerationVisitor(),
    numAddStates(numAddStatesParam)
    {

    }

    /**
     * Method implementing the actual W-Method for test generation on a FSM.
     * Only called indirectly, see Fsm::accept()
     * @param fsm The FSM to generate the test suite for.
     */
    void visit(Fsm & fsm) override;

    /** Generation for observable FSMs */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual W-Method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     */
    void visit(Dfsm & dfsm) override;
    
};

} // namespace libfsmtest
