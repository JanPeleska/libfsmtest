/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <memory>
#include <string>
#include <set>
#include <map>
#include <utility>
#include <vector>
#include <stdexcept>
#include <optional>

#include "visitors/FsmVisitor.hpp"
#include "State.hpp"
#include "definitions.hpp"
#include "PresentationLayer.hpp"
#include "testsuite/Trace.hpp"
#include "testsuite/definitions.hpp"

namespace libfsmtest {

/**
 * Most general class of finite state machines (Mealy Machines).
 */
class Fsm {
    
protected:
    
    std::string name;
    
    States states;
    
    PresentationLayer presentationLayer;
    
    Index initialStateId;
    
    /** State set for stepping method */
    std::set<Index> homingStates;
    
    void getIOTracesInternal(Index stateId,
                             Trace::const_iterator& ite,
                             Trace::const_iterator endIte,
                             Strings& ioTraces,
                             bool addTargetState = false);
        
public:
    
    /**
     * Low-level constructor - should only be used by
     * factory methods
     * @param fsmName Name of the FSM to be created
     * @param stateVec Vector of states (pred-defined with all
     *                 emanating transitions)
     * @param pl The presentation layer
     * @param initialState Number of the initial state (this equals
     *                     the index of the initial state in the
     *                     state vector) Runtime exception is thrown if
     *                     initialState is out of range.
     */
    Fsm(std::string fsmName,
        States stateVec,
        const PresentationLayer& pl,
        Index initialState);
    
    /**
     * Copy constructor
     */
    Fsm(const Fsm& fsm) = default;

    /**
     * Copy assignment operator
     */
    Fsm& operator=(const Fsm& other) = default;

    /** Virtual destructor */
    virtual ~Fsm() = default;
    
    std::string getName() const { return name; }
    
    void setName(std::string theName) { name = std::move(theName); }
    
    States& getStates() { return states; }
    States const& getStates() const { return states; }
    
    Index getInitialStateId() const;

    [[nodiscard]] PresentationLayer const& getPresentationLayer() const { return presentationLayer; }
    
    bool isObservable() const;
    
    bool isDeterministic() const;
    
    bool isCompletelySpecified() const;
    
    /**
     * The size of an FSM is the number of its states.
     */
    size_t size() const { return states.size(); }
     
    /**
     * Accept any kind of FsmVisitor
     */
    virtual void accept(FsmVisitor& v) { v.visit(*this); }
    
    
    /**
     * This operation throws an exception, because it requires
     *  observable FSMs (Ofsm instances)
     */
    virtual void calcOFSMTables(std::map< size_t, std::set<Index> >& /*classMap*/,
                                std::vector< std::map<Index, size_t> >& /*state2classMaps*/) const
    { throw std::runtime_error("ERROR: calcOFSMTables() called on Fsm instance (should be Ofsm)."); }
    
    /**
     *  This operation throws an exception, because it requires
     *  observable FSMs (Ofsm instances)
     */
    virtual bool isInitiallyConnected() const;
    
    /**
     *  This operation throws an exception, because it requires
     *  observable FSMs (Ofsm instances)
     */
    virtual bool isMinimal() const { throw std::runtime_error("ERROR: isMinimal() called on Fsm instance (should be Ofsm)."); }
    
    /**
     * For a given set of State ids, return
     * a string representation { id1, id2, ..., idlast }
     * as a set of state numbers.
     */
    std::string labelString(const std::set<Index>& lbl) const;
    
    /**
     * Start stepping - this resets the homingState set and
     * initialises it with the initial state.
     */
    void resetStepping();
    
    /**
     * Perform one step with specified input and output x/y.
     * This may lead to one or more post states.
     * If no post state exists for x/y with the current
     * homing set, an error is returned.
     *
     * If at least one post state exists, the homing set is updated accordingly.
     */
    bool step(Index x, Index y, std::string& iotrace);
    
    /** Step function with string inputs/outputs according to presentation layer */
    bool step(const std::string& x, const std::string& y, std::string& iotrace);
    
    /**
     *  Get all possible IO traces generated by a given input trace,
     *  starting at the initial state of this FSM.
     *  Each IO trace is formatted in external presentation layer format
     *  as a string. For nondeterministic machines several IO traces
     *  may be associated with one input trace.
     */
    void getIOTraces(const Trace& inputTrace, Strings& ioTraces);

    /**
     *  Get all possible IO traces generated by a given input trace,
     *  starting state number stateId of this FSM.
     *  Each IO trace is formatted in external presentation layer format
     *  as a string. For nondeterministic machines several IO traces
     *  may be associated with one input trace.
     */
    void getIOTracesFromState(const Trace& inputTrace,
                              Strings& ioTraces,
                              Index fromState);

    /**
     *  Get all possible IO traces generated by a given input trace, starting
     *  in state number stateId of this FSM.  For nondeterministic machines
     *  several IO traces may be associated with one input trace.
     *  @param inputTrace The input trace to apply
     *  @param includeUndefined Whether the result should contain
     *   "aborted"/incomplete traces for each time where some prefix of
     *   inputTrace leads to a state where the remaining trace suffix is
     *   undefined.
     *  @return Returns the set of traces produced by this machine if
     *   inputTrace is applied in fromState. Contains only traces of the same
     *   length as inputTrace if includeUndefined is false, else may contain
     *   shorter traces that stop in a state where some suffix of inputTrace is
     *   undefined.
     */
    std::vector<IOTrace> getIOTraces(const Trace& inputTrace,
                                     bool includeUndefined) const;

    /**
     *  Get all possible IO traces generated by a given input trace, starting
     *  in state number stateId of this FSM.  For nondeterministic machines
     *  several IO traces may be associated with one input trace.
     *  @param inputTrace The input trace to apply
     *  @param fromState The ID of the state to apply the input trace to
     *  @param includeUndefined Whether the result should contain
     *   "aborted"/incomplete traces for each time where some prefix of
     *   inputTrace leads to a state where the remaining trace suffix is
     *   undefined.
     *  @return Returns the set of traces produced by this machine if
     *   inputTrace is applied in fromState. Contains only traces of the same
     *   length as inputTrace if includeUndefined is false, else may contain
     *   shorter traces that stop in a state where some suffix of inputTrace is
     *   undefined.
     */
    std::vector<IOTrace> getIOTracesFromState(const Trace& inputTrace,
                                              Index fromState,
                                              bool includeUndefined) const;

    
    /**
     * Collect all targets that can be reached by a given input trace.
     * @param inputTrace The input trace.
     * @return The IDs of all states that can be reached by the input trace.
     */
    std::set<Index> getTargets(const Trace& inputTrace) const;
    
    /**
     * Collect all targets that can be reached by a given input trace
     * and a given start state.
     * @param inputTrace The input trace.
     * @param fromState The ID of the state to apply the input trace to
     * @return The IDs of all states that can be reached by the input trace.
     */
    std::set<Index> getTargets(const Trace& inputTrace, Index fromState) const;

    /**
     * Return the size of the input alphabet of this fsm.
     */
    size_t getInputAlphabetSize() const;

    /**
     * Return the size of the output alphabet of this fsm.
     */
    size_t getOutputAlphabetSize() const;

    /**
     * Creates a copy of the object.
     * @return A unique_ptr pointing to the created clone
     */
    virtual std::unique_ptr<Fsm> clone() const;

    /**
     * Creates an object of the same type as this one but with different
     * attributes.
     * @param fsmName Optional parameter. If set, the created FSM will be named
     *  according to the parameter. If not set, the created FSM will be named
     *  the same as this one.
     * @param states Optional parameter. If set, the created FSM will have the
     *  set of states defined by the parameter as its state set. If not set, the
     *  created FSM will have the same state space as this one.
     * @param pl Optional parameter. If set, the created FSM will have the
     *  presentation layer given as the argument as its presentation layer. If
     *  not set, the created FSM will have the same presentation layer as this
     *  one.
     * @param initialStateId Optional parameter. If set, the created FSM will
     *  have the initial state set according to the given argument. If not set,
     *  the created FSM will have the same initial state ID as this one.
     */
    virtual std::unique_ptr<Fsm> modify(std::optional<std::string> fsmName,
                                        std::optional<States> states,
                                        std::optional<PresentationLayer> pl,
                                        std::optional<Index> initialStateId) const;
};

}
