
/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include "Mutator.hpp"

namespace libfsmtest {

/**
 * Mutator to randomly change the target of transitions.
 */
class RandomTransitionReroute : public Mutator {
protected:
    /**
     * Randomly selects a transition and changes its target.
     * @see Mutator::applyMutation()
     * @return true, when a target was changed
     *         false, otherwise
     */
    bool applyMutation() override;

public:
    /**
     * Constructor.
     * @param fsmToMutate The FSM to mutate.
     * @param transitionsToReroute The number of transitions the targets should be mutated of.
     * @param nameSuffixParam Optional, will be appended to the name of the FSM.
     */
    RandomTransitionReroute(Fsm* fsmToMutate, size_t transitionsToReroute, const std::string& nameSuffixParam = "_RR") :
        Mutator(fsmToMutate, transitionsToReroute, nameSuffixParam) {}

};

} // namespace libfsmtest

