/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>
#include <iostream>
#include <deque>
#include <utility>

#include "Tree.hpp"

using namespace std;

namespace libfsmtest {

vector<TreeNode *> Tree::getLeaves() const {
    vector<TreeNode*> leaves;
    root->calculateLeaves(leaves);
    return leaves;
}

size_t Tree::getNumberOfLeaves() const {
    return root->getNumberOfLeaves();
}

size_t Tree::size() const {
    return root->size();
}

void Tree::add(const Trace &t) {
    root->add(t);
}


void Tree::getExistingNodes(std::set<TreeNode*>& existingNodesSet) {
    root->getExistingNodes(existingNodesSet);
}

vector<TreeNode*> Tree::getExistingNodes() const {
    deque<TreeNode*> worklist;
    vector<TreeNode*> result;
    worklist.push_back(root.get());
    while(not worklist.empty()) {
        auto node = worklist.front();
        auto labels = node->getLabelsOfOutgoingEdges();
        for(auto label : labels) {
            worklist.push_back(node->after(label));
        }
        result.push_back(node);
        worklist.pop_front();
    }
    return result;
}

void Tree::addToAllNodes(const Trace &t, const set<TreeNode*>& existingNodesSet) {
    if ( t.size() > 0 && existingNodesSet.find(root.get()) != existingNodesSet.end() ) {
        containsEmptyTrace = false;
    }
    root->addToAllNodes(t,existingNodesSet);
}

void Tree::addToAllNodes(const Traces& traces) {
    
    if ( traces.empty() ) return;
    
    std::set<TreeNode*> existingNodesSet;
    root->getExistingNodes(existingNodesSet);
    
    containsEmptyTrace = false;
    for ( const auto& tr : traces ) {
        if ( tr.isEmpty() ) {
            containsEmptyTrace = true;
            break;
        }
    }
    
    for ( const auto& t : traces ) {
        root->addToAllNodes(t,existingNodesSet);
    }
}

TreeNode *Tree::after(size_t index) const {
    return root->after(index);
}

TreeNode *Tree::after(const Trace &t) {
    return root->after(t);
}

Traces Tree::getTestCases() const {
    Traces traces;
    for (const auto& leaf : getLeaves()) {
        traces.push_back(leaf->getPath());
    }
    return traces;
}

Traces Tree::getAllTestCases() const {
    Traces traces;
    Trace trace;
    if ( containsEmptyTrace ) traces.push_back(trace);
    root->traverseForTraces(traces, trace);
    return traces;
}

 

TreeNode* Tree::findByValue(optional<size_t> value) {
    return root->findByValue(value);
}

void Tree::toDot(const string& filename) {
    ofstream outStream(filename);
    if (!outStream.is_open()) throw runtime_error("Failed to open output file " + filename);

    outStream << "digraph g { " << endl;
    
    root->toDot(outStream);
    
    outStream << "}" << endl;
}

void Tree::applyToAllNodes(std::function<void (libfsmtest::TreeNode *)> f) {
    std::set<TreeNode*> existingNodesSet;
    root->getExistingNodes(existingNodesSet);
    
    root->applyRecursively(std::move(f),existingNodesSet);
}

void Tree::applyToAllNodes(std::function<void (libfsmtest::TreeNode const*)> f) const {
    std::set<TreeNode*> existingNodesSet;
    root->getExistingNodes(existingNodesSet);
    
    root->applyRecursively(std::move(f),existingNodesSet);
}

size_t Tree::getInsertionCost(const Trace& tr) const {
    
    size_t cost = 0;
    TreeNode* currentNode = root.get();
    
    for ( const auto& x : tr ) {
        
        bool isLeaf = currentNode->isLeaf();
        currentNode = currentNode->after(x);
        
        if ( currentNode == nullptr ) {
            cost = (isLeaf) ? 1 : 2;
            break;
        }
        // The prefix of tr up to and including x is inside the tree
        // Check the next trace element
    }
    return cost;
}


} // namespace libfsmtest
