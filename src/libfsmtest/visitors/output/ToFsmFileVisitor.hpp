/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <utility>
#include <memory>

#include "ToFileVisitor.hpp"
#include "utils/CsvUtils.hpp"
#include "fsm/definitions.hpp"
#include "fsm/PresentationLayer.hpp"

namespace libfsmtest {

class ToFsmFileVisitor : public ToFileVisitor {
private:
    /** Additional files needed for low-level format */
    std::string inFileName, outFileName, statesFileName;

    Csv fsmData {CsvColumn(), CsvColumn(), CsvColumn(), CsvColumn()};
    std::unique_ptr<PresentationLayer> presentationLayer;
    int offsetInitialStateTransitions = 0;

    /**
     * Starts traversing the fsm to extract data.
     * @param fsm The fsm to traverse.
     */
    void gatherData(Fsm& fsm);

public:
    /**
     * Constructor.
     * Assumes it is given a path or filename without ending.
     * Will create the four files prefix{.fsm, .in, .out, .state}.
     * @param prefix The name of the files to write to after appending endings.
     */
    ToFsmFileVisitor(const std::string& prefix);

    /**
     * Constructor.
     * @param fsmFileName The name of the file to write the fsm definition to.
     * @param inputFileName The name of the file to write the input alphabet to.
     * @param outputFileName The name of the file to write the output alphabet to.
     * @param fileOfStates The name of the file to write the state names to.
     */
    ToFsmFileVisitor(const std::string& fsmFileName, std::string  inputFileName,
                     std::string  outputFileName, std::string  fileOfStates) :
                     inFileName(std::move(inputFileName)), outFileName(std::move(outputFileName)), statesFileName(std::move(fileOfStates)) {
        this->fileName = fsmFileName;
    }

    /*
     * Starts gathering data of a FSM.
     * Only called indirectly.
     * @see Fsm::accept()
     */
    void visit(Fsm& fsm) override;

    /*
     * Starts gathering data of a OFSM.
     * Only called indirectly.
     */
    void visit(Ofsm& ofsm) override;

    /**
     * Starts gathering data of a DFSM.
     * Only called indirectly.
     * @param dfsm The DFSM to gather data from.
     */
    void visit(Dfsm& dfsm) override;

    /**
     * Starts gathering data of a State.
     * Only called indirectly.
     * @see State::accept()
     * @param state The State to gather data from.
     */
    void visit(State& state) override;

    /**
     * Starts gathering data of a Transition.
     * Only called indirectly.
     * @see Transition::accept()
     * @param transition The Transition to gather data from.
     */
    void visit(Transition& transition) override;

    /**
     * Writes out gathered data to the given file.
     * Has to be called after this visitor has been accepted on the FSM.
     */
    void writeToFile() override;
};

} // namespace libfsmtest
