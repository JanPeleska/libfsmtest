\chapter{Using Generator, Checker, and Test Harness -- the Workflow}\label{chap:workflow}
% ============================================================================




% ============================================================================
\section{A Sample Test Campaign}\label{sec:gdcexample}

Throughout this chapter, we work with a simple   model-based testing campaign, using an example originally introduced by 
Paul C.~Jorgensen in~\cite{jorgensen2017}. 


% ----------------------------------------------------------------
\subsection{Reference Model Description}\label{sec:gdcmodel}

 
The \emph{garage door controller (GDC)} 
is a computer managing the up and down movement of a garage door via an electric motor, 
as shown in the 
overview diagram in Fig.~\ref{fig:gdccontext}. The GDC outputs commands {\tt a1, a2, a3, a4}
to the motor, initiating down movement, up movement, stopping the motor, and
reversing its down movement into up movement, respectively. As inputs, the GDC receives a command
``button pressed'' ({\tt e1}) from a remote control device, and two
events ``door reaches position down'' ({\tt e2}) and 
  ``door reaches position up'' ({\tt e3}) from two door position sensors.
Additionally, a safety device is integrated by means of 
a light sensor which sends an event ``light beam crossed'' ({\tt e4}) when something 
moves underneath the garage door while the door is closing.



% .................................................................
\begin{figure}[H]
% \hspace*{-10mm}
 \begin{center}
\includegraphics[width=\textwidth]{figures/gdc-context.pdf} 
\end{center}
%\vspace*{-20mm}
\caption{Garage door controller and its operational environment.}
 \label{fig:gdccontext}
 \end{figure}
% ................................................................


The expected behaviour of the GDC is modelled by the FSM in Fig.~\ref{fig:gdcfsm}.
In the initial state {\sf Door\_Up}, the door is expected to be in the UP position, and the ``button pressed'' event {\tt e1} from the remote control triggers a ``Start down movement'' command {\tt a1} to 
the motor. The GDC transits to state {\sf Door\_closing}. In this  state, an input
{\tt e4} from the light sensor leads to an {\tt a4} command to the motor, with the effect that
the down movement of the door is reversed to up movement. This leads to state {\sf Door\_opening}.
During down movement in state {\sf Door\_closing}, another occurrence of the {\tt e1}-event leads to
a ``Stop movement'' command {\tt a3} to the motor, and the controller transits to state {\sf Door\_stopped\_going\_down}.
From there, the downward movement is resumed (output {\tt a1}), 
as soon as another {\tt e1}-command is given.



% .................................................................
\begin{figure}[H]
% \hspace*{-10mm}
 \begin{center}
\includegraphics[width=\textwidth]{figures/gdc-fsm.png} 
\end{center}
%\vspace*{-20mm}
\caption{Behaviour of the garage door controller, modelled by a DFSM.}
 \label{fig:gdcfsm}
 \end{figure}
% ................................................................



When the door sensor signals that the door has reached the down position ({\tt e2}),
the motor is stopped with command {\tt a3}, and the controller transits into state {\sf Door\_down}.
From  this state, another {\tt e1}-event triggers the analogous actions for moving the door up, until  the UP position is reached. During the UP-movement, 
inputs from the light sensor do not have any effect.

In Fig.~\ref{fig:dfsmexcelb}, the same DFSM is modelled by means of a transition table.

The missing transitions in each state have the ``self-loop-with null outputs'' interpretation, as explained 
in Section~\ref{sec:autocomptrans}. Therefore, the DFSM is meant to be completely specified. When reading the 
DFSM from input files where the self-loop transitions are missing, the auto completion  transformer
{\tt ToAutoCompleteWithSelfLoopTransformer} introduced in Section~\ref{sec:autocomptrans}
needs to be applied.


 
\newpage

% .................................................................
\begin{figure}
% \hspace*{-10mm}
 \begin{center}
\includegraphics[width=1.1\textwidth]{figures/gdc-table.png} 
\end{center}
%\vspace*{-20mm}
\caption{Tabular format for modelling DFSMs.}
 \label{fig:dfsmexcelb}
 \end{figure}
% ................................................................





Note that the DFSM in Fig.~\ref{fig:gdcfsm} is not minimal; it has been represented in
this   form to optimise its readability. The equivalent minimised machine is shown
in Fig.~\ref{fig:gdcfsmmin}. This has been constructed using the prime machine transformer
described in Section~\ref{sec:primetrans}.
The output graph shown in 
Fig.~\ref{fig:gdcfsmmin} has been created by using the  
{\tt ToDotFileVisitor} described in Chapter~\ref{chap:tofilevisitors}. This produces files in the 
so-called {\tt .dot}-format, from which the 
GraphViz\footnote{\href{http://www.graphviz.org}{http://www.graphviz.org}} tool creates graph  representations.


In main program file {\tt usage\_demo.cpp}, these transformations have been programmed in procedure
{\tt demo\_transformToPrimeMachineGdc()}.


% .................................................................
\begin{figure}[H]
% \hspace*{-10mm}
 \begin{center}
\includegraphics[width=\textwidth]{figures/GDC_MINIMISED.pdf} 
\end{center}
%\vspace*{-20mm}
\caption{Minimised, auto-completed DFSM, equivalent to the GDC model from Fig.~\ref{fig:gdcfsm}.}
 \label{fig:gdcfsmmin}
 \end{figure}
% ................................................................


 
%The FSM states, as shown in this figure, have the meaning
%
%\begin{center}
%\begin{tabular}{c|c}\\
%State & Description
%\\\hline
%{\tt s1} & Initial state {\tt Door Up}    \\ 
%{\tt s2} & {\tt Door down}    \\ 
%{\tt s3} & {\tt Door stopped going down}    \\ 
%{\tt s4} & {\tt Door stopped going up}    \\ 
%{\tt s5} &  {\tt Door closing}   \\ 
%{\tt s6} &  {\tt Door opening}   \\ 
%\end{tabular}
%\end{center}

 
% ----------------------------------------------------------------
\subsection{GDC System Under Test}\label{sec:gdcinterface}

A sample implementation in \cpp\ is given in the FSM Library, directory
{\tt src/harness/example}, in file {\tt gdclib.cpp}; the public operation 
interfaces  are specified in {\tt gdclib.hpp} as follows.

\lstset{language=C,numbers=left,basicstyle=\footnotesize}
\begin{lstlisting}
typedef enum {
    e1,
    e2,
    e3,
    e4
} gdc_inputs_t;

typedef enum {
    nop,
    a1,
    a2,
    a3,
    a4
} gdc_outputs_t;

extern void gdc_reset();
extern gdc_outputs_t gdc(gdc_inputs_t x);
\end{lstlisting}

The GDC   expects its inputs in enumeration format \verb+gdc_inputs_t+
and returns actions to the motor in format \verb+gdc_outputs_t+. The implementation in
{\tt gdclib.cpp} follows the state machine programming 
paradigm and is straightforward, so that no further comments are needed.

In Section~\ref{sec:harness} it is shown how test suites generated from the GDC model discussed in Section~\ref{sec:gdcmodel} can be executed against this \cpp\ application, using the test harness provided by \fsml.





% ============================================================================
\newpage
\section{Using the Test Generator}\label{sec:generator}

Users only interested in generating test suites with the existing methods available in the \fsml\ class library do not need to
write their own programs with instances of test generation frame and test generation visitors, as described in Chapter~\ref{chap:tsgen}. Instead, they can invoke the program {\tt generator} which is created from the library and source file
\begin{verbatim}
src/generator/generator.cpp
\end{verbatim}
when building the \fsml\ class library\footnote{Recall from {\tt README.md} that the executables reside in sub-directories of  the build directories {\tt build.Debug}, {\tt build.Release}, or {\tt xcodebuild.Debug}. For example, the generator program has path 
{\tt build.Release/generator/generator}.}.

The {\tt generator} program is invoked from its directory as follows
\begin{lstlisting}
./generator  [<options>] \
              <test suite name> \
              <Path to reference model> \
             [<Path to abstraction model>]
\end{lstlisting}

Parameter \verb+<test suite name>+ specifies the name of the test suite to be generated. On termination of the
{\tt generator}, the test cases file containing the input sequences to be exercised on the system under test is then named 
\begin{lstlisting}
     <test suite name>.txt
\end{lstlisting}
and stored in the directory from where {\tt generator} has been invoked.

Parameter \verb+<Path to reference model>+ gives the path and basename of the reference model. The generator uses
the {\tt FsmFromFileCreator} described in Section~\ref{sec:fromfilereader} internally, so file extensions and file types are determined automatically.

Optional parameter \verb+<Path to abstraction model>+  gives the path and basename of the abstraction model. This
is only used when applying the safety-complete H method (see Section~\ref{sec:abstraction}).

The generator produces a test suite according to the following  options.
\begin{enumerate}
\item If no options are provided, the W-Method is applied with 0 additional states assumed for the implementation
(see variable {\tt numAddStates} in Listing~\ref{algo:genparadigm}).

\item \verb+-a<number>+ sets the value of additional states to be assumed for the implementation to \verb+<number>+. This option is used in combination with the options selecting the test generation method
described below.

\item \verb+-cself+ specifies that an auto completion transformation should be applied on the FSM input files with self loops and null outputs, as described in Section~\ref{sec:autocomptrans}. This option is used in combination with the options selecting the test generation method described below.

\item \verb+-cerror+ specifies that an auto completion transformation should be applied on the FSM input files with transition to an error state and error output, as described in Section~\ref{sec:autocomptrans}. This option is used in combination with the options selecting the test generation method described below.

\item \verb+-null <null string>+ specifies the name of the null output, as used in an auto-completed FSM's presentation layer. This option is only useful if one of the options \verb+-cself+ of  \verb+-cerror+
have been provided as well. If this option is not provided, the default value   {\tt \_null} will be chosen for the  null output.

\item \verb+-errorout <error output string>+ specifies the name of the error output, as used in an auto-completed FSM's presentation layer. This option is only useful if   the option   \verb+-cerror+
has been provided as well. If this option is not provided, the default value   {\tt \_error} will be chosen for the  error output.

\item \verb+-errorstate <error state string>+ specifies the name of the error state, as used in an auto-completed FSM's presentation layer. This option is only useful if   the option   \verb+-cerror+
has been provided as well. If this option is not provided, the default value   {\tt ERROR} will be chosen for the  error output.
 
\end{enumerate}

The following options select the test generation method to be used; only one of them may be chosen when invoking the generator.
\begin{enumerate}
\item {\tt -w} -- selection of the W-method
\item {\tt -wp} -- selection of the Wp-method
\item {\tt -h} -- selection of the H-method
\item {\tt -spyh} -- selection of the SPYH method
\item {\tt -csc} -- selection of the classical state counting method
\item {\tt -ssc} -- selection of the strong state counting method
\item {\tt -sh} -- selection of the safety-complete H-method.\newline This is the only option requiring the additional parameter \verb+<Path to abstraction model>+.
\end{enumerate}


\begin{example}{ex:generator}
For generating a test suite for the garage door controller (GDC) described in Section~\ref{sec:gdcexample}, we first note that raw file specifications of the GDC exist in the {\tt resources/} directory, files
\begin{verbatim}
   garage.fsm, garage.in, garage.out, garage.state
\end{verbatim}
The fsm-file contains all transitions for a complete DFSM, so no auto-completion directives are required when calling the generator. Assume that we wish to  use the H-Method for test suite generation under the hypothesis that the software under test has at most 2 extra states.\footnote{More precisely, we assume that the unknown minimised DFSM representing the true behaviour of the software under test has at most 2 additional states in comparison to the minimised GDC reference model.} The name of the test suite should be {\tt ``SUITE-GDC-H-2''}. Activating the generator from its build directory, say,
\begin{verbatim}
   libfsmtest/build.Release/generator/
\end{verbatim}
requires command
\begin{lstlisting}
./generator  -h -a2 "SUITE-GDC-H-2" ../../resources/garage
\end{lstlisting}
On termination, the output
\begin{verbatim}
Test generation completed.
Number of test cases: 277
Total length        : 1493
Test case file      : SUITE-GDC-H-2.txt
\end{verbatim}
is written to the console. The test case file contains 277~lines, each line representing the inputs for one test case. The {\tt "Total length"} is number of input events summed up over all test cases in the suite. 

The generator creates the following files in the directory from where it has been activated:
\begin{enumerate}
\item File {\tt SUITE-GDC-H-2.txt} containing the test cases. Its first lines look like this (recall that 
{\tt e1, e2, \dots} are  inputs to the GDC, as described Section~\ref{sec:gdcexample}):
\begin{verbatim}
e1, e1, e1, e1, e1
e1, e1, e1, e2, e1
. . .
e1, e2, e1, e1, e1, e1
e1, e2, e1, e1, e1, e2
e1, e2, e1, e1, e2, e1
e1, e2, e1, e1, e3, e1
. . .
\end{verbatim}

\item Files 
\begin{verbatim}
   garage.fsm, garage.in, garage.out, garage.state
\end{verbatim}
because these represent the reference FSM which is part of the test suite, where it serves as the test oracle.
\end{enumerate}
When using the generated test suite in the checker or test harness, described in Section~\ref{sec:checker} and Section~\ref{sec:harness}, respectively, these files have to be supplied to the respective tool.
%When using the generated test suite in the checker described in Section~\ref{sec:checker}, the test case file and the FSM files in raw format need to be copied into the directory from where the checker is activated (or relative or absolute paths need to be used when invoking the checker).
%When using the generated test suite in the test harness described in Section~\ref{sec:harness}, these files also have to be made available to the harness when activating the test execution.
\end{example}



% ============================================================================
\newpage
\section{Using the Checker}\label{sec:checker}

Users who intend to apply a test suite to a given FSM do not need to write their own programs to do so. Instead, they can invoke the program \texttt{checker} which is implemented in the source file
%
\begin{verbatim}
  src/checker/checker.cpp
\end{verbatim}
%
and generated when building the \texttt{libfsmtest}. Currently this checker supports test suite application with respect to testing for language equivalence. 

The \texttt{checker} program is invoked from its directory as follows:
%
\begin{verbatim}
./checker [options]
          <Path to the test suite>
          <Path to the reference model>
          <Path to the FSM to test>
\end{verbatim}
%
The parameter \texttt{<Path to the test suite>} specifies the file where the test
suite to be applied is read from. For reference on test suite generation see
Section~\ref{sec:generator}.

Parameter \texttt{<Path to the reference model>} gives the path and basename of
the reference model. The checker uses the \texttt{FsmFromFileCreator} described
in Section~\ref{sec:fromfilereader} internally, so file extensions and file
types are determined automatically.

The path and basename of the model to apply the test suite to has to be given
as the parameter \texttt{<Path to the FSM to test>}. As with \texttt{<Path to
the reference model>}, the checker uses the \texttt{FsmFromFileCreator}, so the
file extensions and file types are determined automatically.

With these three mandatory parameters, the checker tries to read all given
necessary files and throws an exception if any of those are not
available\footnote{Note that the current implementation will warn about an
unsupported file format if any of the given FSMs could not be read, even if
that is due to missing files.}.

If all files could be read successfully, a final check ensures that both the
reference FSM and the FSM to test have the same input and output
alphabets\footnote{The current implementation even requires both the input and
output alphabet pairs to define the symbols in the same order.}.

Possible options for the checker are the options \texttt{-cself},
\texttt{-cerror}, \texttt{-null}, \texttt{-errorout} and \texttt{-errorstate}
as described in Section~\ref{sec:generator}. These options control whether, which and
how an auto completion transformer shall be applied to the SUT model before checking
it.

When all parameters have been parsed successfully, the input sequences in the
test suite will be evaluated on both the reference FSM and the FSM to test.
For both FSMs and for each input sequence in the test suite the set of produced
output traces is determined, and differences in these sets are examined.
For each output sequence produced by one of the FSMs that is not produced by
the other FSM in response to the same input sequence, the checker determines
the output sequence with the longest common prefix and prints both sequences.
\begin{verbatim}
FAILURE: SUT implements unspecified output trace 0,0,2,2 
         for input trace 0,0,1,1
         Closest match diverges at step 4: 1
FAILURE: SUT does not implement output trace 0,2,1,1 
         for input trace 0,1,1,1
         Closest match diverges at step 3: 2,2
         (Expected: 1,1)
FAILURE: SUT implements unspecified output trace 0,2,2,2 
         for input trace 0,1,1,1
         Closest match diverges at step 3: 1,1
FAILURE: SUT does not implement output trace 1,0,2,0,2 
         for input trace 1,0,1,0,1
\end{verbatim}
However, if the sets of output sequences produced by both machines agree for
all input sequences in the test suite, the checker notes this as \texttt{PASS}
and exits.

\begin{example}{ex:checker}
For checking an implementation of the garage door controller (GDC) described in
Section~\ref{sec:gdcexample}, we note again that raw file specifications of the
GDC exist in the {\tt resources/} directory, files
\begin{verbatim}
   garage.fsm, garage.in, garage.out, garage.state
\end{verbatim}
Furthermore, we will use the test suite generated in
Section~\ref{sec:generator}, \texttt{``SUITE-GDC-H-2``} to check a mutated
implementation. To generate this mutation we copy the GDC FSM files, rename the
copies to
\begin{verbatim}
   garage-mutant.fsm, garage-mutant.in,
   garage-mutant.out, garage-mutant.state
\end{verbatim}
and modify the \texttt{garage-mutant.fsm} file. In this example, we remove the
transition from the state \texttt{Door\_stopped\_going\_down} to the state
\texttt{Door\_closing} and change the output on the self-loop of
\texttt{Door\_Up} triggered by input \texttt{e3} from \texttt{null} to
\texttt{a3}.

Assuming the current working directory contains the checker executable and that
the \texttt{garage}, \texttt{garage-mutant} and test suite files are in the
directory \texttt{/tmp}, we invoke the checker as follows
\begin{lstlisting}
./checker /tmp/SUITE-GDC-H-2 /tmp/garage /tmp/garage-mutant
\end{lstlisting}
On termination, the checker prints numerous lines beginning with
\texttt{FAILURE:}, as the SUT clearly is not equal to the GDC but in the fault
domain.
The first lines read as follows:
\begin{lstlisting}
FAILURE: SUT does not implement output trace "a1,a3,a1,a3,a1" for input trace "e1,e1,e1,e1,e1"
         Closest match diverges at step 3: ""
         (Expected: "a1,a3,a1")
FAILURE: SUT implements unspecified output trace "a1,a3" for input trace "e1,e1,e1,e1,e1"
         Closest match diverges at step 3: "a1,a3,a1"
\end{lstlisting}
The first failure here shows the missing transition: When executing the input
sequence \texttt{e1,e1,e1}, we expect the output sequence \texttt{a1,a3,a1}.
However, due to the missing transition in the mutant, the mutants execution
stops after the second input.
The \emph{closest match} mentioned by the checker is the sequence with the
sequence produced by the SUT with the longest prefix common with the expected
output sequence. Beginning at step 3, i.e. after the second \texttt{e1} input,
the SUT produces an empty trace, which is indicated by the second line.
The third line shows the output sequence that was expected at that position.

The second failure shows the same fault but from the other perspective: The SUT
produces an unexpected output sequence \texttt{a1,a3} and the closest match is
the output sequence in the set of expected output sequences that shares the
longest prefix with the produced sequence.

Further down the list of failures, we see the following lines:
\begin{lstlisting}
FAILURE: SUT does not implement output trace "a1,a4,a3,null,a1,a3" for input trace "e1,e4,e3,e3,e1,e1"
         Closest match diverges at step 4: "a3,a1,a3"
         (Expected: "null,a1,a3")
FAILURE: SUT implements unspecified output trace "a1,a4,a3,a3,a1,a3" for input trace "e1,e4,e3,e3,e1,e1"
         Closest match diverges at step 4: "null,a1,a3"
\end{lstlisting}
These reflect the mutated output: The expected output at that position in the
execution sequence would have been the output \texttt{null}, whereas the SUT
produces the output \texttt{a3}.
\end{example}

% ============================================================================
\newpage
\section{Using the Test Harness}\label{sec:harness}


A {\bf test harness} is a program which exercises a given test suite on a software under test (SUT). The \fsml\ class library comes with a test harness  which allows to execute test suites generated by means of one of the FSM-based methods described above against a \cpp\ library consisting of one or more operations to be tested. The re-usable test harness requires  \emph{input refinement} (each input alphabet value of the test case needs to be mapped to a concrete SUT operation call with input parameter values and presets of attributes)
and \emph{output abstraction} (the effect of each operation call on return value, reference parameters and attributes needs to be abstracted to the corresponding value of the reference FSM's output alphabet).



To support this, the test harness operates with a \emph{SUT wrapper}. This is a \cpp-source frame to be completed for each test campaign, offering a function    with fixed signature {\tt std::string sut(const std::string\& x)} to the test harness for calling the SUT. For each input to be exercised on the SUT in a test step, the test harness calls {\tt sut(x)}, where {\tt x} is the input alphabet value as string. The wrapper maps {\tt x}  to concrete input data (parameters and attributes) of the SUT and calls the associated SUT  operation.  
The SUT response is abstracted by the wrapper to an output alphabet value which is returned as string from 
call {\tt sut(x)} to the   harness. The harness checks SUT reactions by simulating the test suite's  reference FSM in back-to-back fashion and comparing outputs. In Fig.~\ref{fig:harnessconfig}, the interplay between harness, wrapper and SUT is depicted.

The harness is contained in \fsml\ as file
\begin{verbatim}
libfsmtest/src/harness/harness.cpp
\end{verbatim}
It needs to be compiled and linked with the SUT, but there should be no need in general to make any adaptations in this file. 


% .................................................................
\begin{figure}[H]
% \hspace*{-10mm}
 \begin{center}
\includegraphics[width=\textwidth]{figures/testharness.pdf} 
\end{center}
%\vspace*{-20mm}
\caption{Test harness, SUT wrapper, and software under test.}
 \label{fig:harnessconfig}
 \end{figure}
% ................................................................

The wrapper source frame is provided by file
\begin{verbatim}
libfsmtest/src/harness/sut_wrapper.cpp
\end{verbatim}
This file needs to be edited; the source frame is shown in Listing~\ref{algo:wrappera}. As can be seen in the source frame, the following code needs to be included.

\begin{enumerate}
\item Include-directives to SUT-specific header files, so that the software under test can be invoked from the wrapper, and the attributes to be preset can be accessed (line~4 in Listing~\ref{algo:wrappera}). 

\item Data structures (typically maps) of functions for input refinement and output abstraction need to be
inserted (line~10 in Listing~\ref{algo:wrappera}).

\item The SUT has to be initialised in the body of function {\tt void sut\_init()}. If the SUT software does not consist of a static class, it is usually required to instantiate SUT objects here and make pointers to these objects available in the global wrapper data.

\item The body of function {\tt void sut\_reset()} needs to be filled in. The code to be inserted here should
re-initialise the SUT, so that a new test case can be applied to the SUT residing again in its initial state.

\item The body of the function {\tt const string sut(const string\& input)} needs to be provided. Here, the 
input data
refinement is performed and the associated SUT operation is called. The returned data and changed attributes are abstracted to the return string {\tt fsmOutputEvent} and returned to the calling harness.
\end{enumerate}



\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={SUT wrapper source frame.},
label=algo:wrappera]
#include <string>

// Include header files of library to be tested
// #include "..."

/** Helper data structures and functions for
 *  SUT test wrapper */
// ...
using namespace std;
void sut_init() {
    // initialise wrapper data structures
    // for mapping FSM inputs to SUT inputs
    // and vice versa
    // ...
    // Initialise SUT, if required, by calling
    // initialisation functions or initialising
    // global SUT variables
    // ...
}

void sut_reset() {
    // Insert code suitable for resetting SUT into
    // its initial state
    // ...
}

const string sut(const string& input) {
    string fsmOutputEvent;
    // Transform FSM input event passed as string
    // 'input' to SUT input variable settings and
    // global variables settings
    // ...    
    // Call the SUT function addressed by the FSM input event
    // with the input parameter values defined before
    // ...    
    // Convert the return value, the (in-)out-parameter values,
    // and the global SUT variables to the FSM output event
    // represented as string fsmOutputEvent
    // ...   
    // return output event in string representation
    return fsmOutputEvent;   
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}




 



\begin{example}{ex:gdcharness}
In directory 
\begin{verbatim}
libfsmtest/src/harness/example
\end{verbatim}
an example test suite has been provided for testing the garage door controller (GDC) code which is 
contained there in files {\tt gdclib.cpp, .hpp}. The reference DFSM is the result of a generator call (see Section~\ref{sec:generator}) and resides in files {\tt GDC.fsm, .in, .out, .state}. The test cases are contained in {\tt SUITE-GDC.txt}.

The SUT wrapper for this test application is shown in Listing~\ref{algo:wrappergdc}.
In lines 10---15, this  wrapper applies a particularly simple, but frequently 
applicable variant of input refinement and 
output abstraction. To analyse this, 
recall from the GDC software interface specified in Section~\ref{sec:gdcinterface} that inputs to the GDC are specified as enumeration values {\tt e1,\dots,e4} and outputs by enumeration values {\tt nop,a1,\dots,a4}. 
The test harness uses inputs encoded by the presentation layer of the reference DFSM (see file {\tt GDC.in}). This encoding uses strings {\tt "e1",\dots,"e4"}. As a consequence, input refinement simply requires a mapping of input alphabet strings to associated enumeration values.  This   is realised in lines 10---12 as map {\tt fsmIn2gdcIn}.
Conversely, the GDC software return values  {\tt nop,a1,\dots,a4} need to be mapped to output alphabet values
of the reference DFSM (file {\tt GDC.out}) which has the string values
{\tt "null", "a1",\dots,"a4"}. This is realised by map {\tt gdcOut2fsmOut} in lines 13---15.

With these mappings at hand, the implementation of the {\tt sut()} wrapper function shown in lines 23---34
is straightforward: the input alphabet value {\tt input} is transformed by means of map {\tt fsmIn2gdcIn} into
an enumeration value which is used as input argument of the SUT function  {\tt gdc()}. The return value {\tt y} 
 of this SUT call is transformed by means of map {\tt  gdcOut2fsmOut} into a string of the DFSM output alphabet
 and used as return value of the wrapper function {\tt sut()}.
 

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={SUT wrapper for the garage door controller example.},
label=algo:wrappergdc]
#include <string>
#include <map>
using namespace std;
// Include header files of library to be tested
#include "gdclib.hpp"
/**
 *   Helper data structures and functions for
 *   SUT test wrapper
 */
map<string,gdc_inputs_t> fsmIn2gdcIn = {
    {"e1",e1}, {"e2",e2}, {"e3",e3}, {"e4",e4}
};
map<gdc_outputs_t,string> gdcOut2fsmOut = {
    {nop,"null"}, {a1,"a1"}, {a2,"a2"}, {a3,"a3"}, {a4,"a4"}
};
using namespace std;
void sut_init() {
    gdc_reset();
}
void sut_reset() {
    gdc_reset();
}
const string sut(const string& input) {    
    string fsmOutputEvent;    
    map<string,gdc_inputs_t>::iterator 
      inputIte = fsmIn2gdcIn.find(input);
    if ( inputIte == fsmIn2gdcIn.end() ) return fsmOutputEvent;    
    gdc_outputs_t y = gdc( inputIte->second );    
    map<gdc_outputs_t,string>::iterator 
      outputIte = gdcOut2fsmOut.find(y);
    if ( outputIte == gdcOut2fsmOut.end() ) return fsmOutputEvent;    
    fsmOutputEvent = outputIte->second;
    return fsmOutputEvent;
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

\newpage
Using the {\tt harness/example} sub-directory as working directory, the  harness   is now compiled and linked together with wrapper and SUT using command 
\begin{lstlisting}
c++ -std=c++17 -o harness  \
    -I./ -I../../libfsmtest \
    *.cpp ../harness.cpp 
    ../../../build.Release/libfsmtest/libfsmtest.a  -lc++
\end{lstlisting}

Command {\tt c++} is a link to the \cpp\ compiler. The {\tt -std} option indicates that \cpp\ 2017 syntax 
is admissible (some compilers use an older \cpp\ standard if this option is missing; this might lead to compile errors). Options {\tt -I\dots} indicate where to look for \cpp\ header files. In line~3 all cpp-files 
in the local directory (wrapper and SUT code) are referenced, as well as the harness source code residing 
in the directory above. In line~4, the \fsml\ library and the \cpp\ standard library are made available  to the other object files.
As a result of the compilation and linking process,
 the executable {\tt harness} is created which can be invoked with different test suites and reference DFSMs to be executed against the GDC software under test. For the {\tt SUITE-GDC.txt} test cases, the execution command is 
\begin{lstlisting}
./harness SUITE-GDC GDC 
\end{lstlisting}
which leads to result
\begin{lstlisting}
PASS: e1/a1, e1/a3, e1/a1
PASS: e1/a1, e2/a3, e1/a2, e1/a3
PASS: e1/a1, e2/a3, e1/a2, e2/null
PASS: e1/a1, e2/a3, e2/null, e1/a2
PASS: e1/a1, e2/a3, e3/null, e1/a2
PASS: e1/a1, e2/a3, e4/null, e1/a2
PASS: e1/a1, e3/null, e1/a3
PASS: e1/a1, e3/null, e2/a3
PASS: e1/a1, e4/a4, e1/a3, e1/a2
PASS: e1/a1, e4/a4, e2/null, e1/a3
PASS: e1/a1, e4/a4, e2/null, e2/null
PASS: e1/a1, e4/a4, e3/a3, e1/a1
PASS: e1/a1, e4/a4, e4/null, e1/a3
PASS: e1/a1, e4/a4, e4/null, e2/null
PASS: e2/null, e1/a1
PASS: e3/null, e1/a1
PASS: e4/null, e1/a1
\end{lstlisting}
written to the console.




Readers are invited to experiment with different SUT implementations, 
fault injections into the {\tt gdclib.cpp} implementation, and different model variants.
He or she should keep in mind that some fault injections may increase the number of 
states in the minimised DFSM corresponding to the true   behaviour of the software under test. If this is
suspected, the parameter \verb+-a <additional states>+ has to be used for test generation with a suitable estimate (see Section~\ref{sec:generator}). Otherwise it is not guaranteed that the test suite will uncover 
every violation of language equivalence between implementation and reference model. 
\end{example}

\superskip
Note that the current version of the test harness only checks for language equivalence with deterministic reference FSMs and deterministic implementations. Nondeterminism and reduction testing, as well as support of other conformance relations (quasi reduction and strong reduction) will be included in future versions of the harness.



