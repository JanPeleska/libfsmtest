/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @file Template for C++ test harness
 */

#include "utils/compatibility.hpp"

#include <string>
#include <iostream>

#include "testsuite/TestSuite.hpp"
#include "fsm/Fsm.hpp"

using namespace std;
using namespace libfsmtest;

extern void sut_init();

extern void sut_reset();

extern const string sut(const string& input);

/**
 * Global or static variables
 */
static TestSuite* theTestSuite;
int firstTestCase = -1;
int lastTestCase = -1;


/**
 Execute one input trace of the test suite against SUT
 */
void executeTrace(string tr) {
    
    // Log the complete I/O trace up to an error occurrence
    // or until the end of tr in ioTrace
    string ioTrace;
    string inputEvent;
    string outputEvent;
    
    
    
    size_t pos = tr.find(",");
    
    while ( pos != string::npos ) {
        // Get next input event from trace
        inputEvent = tr.substr(0,pos);
        theTestSuite->trim(inputEvent);
        // Remove event from trace
        tr.erase(0,pos+1);
        
        // Call the SUT
        outputEvent = sut(inputEvent);
        
        // Check I/O against current state of the reference FSM
        // and extend the ioTrace observed so far
        if ( ! theTestSuite->getFsm().step(inputEvent,outputEvent,ioTrace) ) {
            // Test case is failed
            goto testCaseFailed;
        }
        
        pos = tr.find(",");
    }
    
    // Last element of the trace
    inputEvent = tr;
    theTestSuite->trim(inputEvent);
    outputEvent = sut(inputEvent);
    
    // Check I/O against current state of the reference FSM
    if ( theTestSuite->getFsm().step(inputEvent,outputEvent,ioTrace) ) {
        cout << "PASS: " + ioTrace << endl;
        return;
    }
    
testCaseFailed:
    cout << "FAIL after I/O trace: " << ioTrace << endl;
    
}


/**
 * Execute a given test suite against SUT
 */
void executeTestSuite() {
    int cnt = 0;
    for ( auto& trAsString : theTestSuite->getInputTraces() ) {
        if ( firstTestCase >= 0 and lastTestCase >= 0 ) {
            if ( cnt < firstTestCase ) {
                cnt++;
                continue;
            }

            if ( cnt > lastTestCase ) {
                break;
            }
        }

        // reset the FSM acting as test oracle
        theTestSuite->getFsm().resetStepping();
        // reset the SUT
        sut_reset();
        executeTrace(trAsString);
        cnt++;
    }
}


/**
 * Test harness - main program.
 * The program is invoked as
 *      harness testsuitename fsmname
 * @param argv[1] Name of the test suite (without .txt suffix). It is expected that
 *                a test suite trace file name "argv[1].txt" resides in the directory from where
 *                harness is started.
 * @param argv[2] Name of the FSM to be used as reference model. It is expected that
 *                FSM files "argv[2].fsm", "argv[2].in", "argv[2].out", "argv[2].state",
 *                resides in the directory from where harness is started.
 * @param argv[3] Number of the first test-case to be executed, greater or equal to 0.
 *                The first line in the test-suite-file has index 0.
 * @param argv[4] Number of the last test-case to be executed, greater or equal to 0.
 *                The first line in the test-suite-file has index 0.
 *
 * @note All files mentioned here are created when saving a test suite to disk,
 *       using the writeToFile() operation.
 */

int main(int argc, char** argv) {
    
    // Need test suite name as first program parameter and
    // FSM name as second parameter.
    if ( argc < 3 ) {
        fprintf(stderr,"Usage: harness <Test suite name> <FSM name> [<fist test-case>] [<last test-case>].\n");
        exit(1);
    }
    
    if ( argc == 5 ) {
        firstTestCase = atoi(argv[3]);
        lastTestCase = atoi(argv[4]);

        if (firstTestCase < 0 || lastTestCase < 0) {
            fprintf(stderr, "Error, invalid index: First test-case: %d, last test-case: %d. Index must be >= 0.\n", firstTestCase, lastTestCase);
            exit(1);
        }

        if (firstTestCase > lastTestCase) {
            fprintf(stderr, "Error: First test-case is greater than last test-case.\n");
            exit(1);
        }
    }
    
    // Read the test suite stored in files, together with its
    // reference FSM
    theTestSuite = new TestSuite(string(argv[1]), string(argv[2]));
    
    // Initialise the SUT and the I/O mapping functions of the wrapper
    sut_init();
    
    // Execute the test suite against SUT
    executeTestSuite();
    
    exit(0);
    
}
