/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <memory>
#include <vector>
#include <deque>

#include "ToMinimisedTransformer.hpp"
#include "ToInitiallyConnectedTransformer.hpp"
#include "ToObservableTransformer.hpp"
#include "ToPrimeTransformer.hpp"



using namespace std;

namespace libfsmtest {
 
void ToPrimeTransformer::visit(Fsm& fsmToTransform) {
    
    if ( ! fsmToTransform.isInitiallyConnected() ) {
        auto icFsm = transformFsm<ToInitiallyConnectedTransformer>(&fsmToTransform);
        icFsm->accept(*this);
    }
    else {
        // Create observable machine
        auto observableFsm = transformFsm<ToObservableTransformer>(&fsmToTransform, "");
        
        // Now handle the observable machine
        observableFsm->accept(*this);
    }
    
}




void ToPrimeTransformer::visit(Ofsm& fsmToTransform) {
    // We already know that fsm is observable
    std::unique_ptr<Fsm> intermediateResult;

    if ( not fsmToTransform.isInitiallyConnected() ) {
        // First make initially connected, then minimise
        intermediateResult = transformFsm<ToInitiallyConnectedTransformer>(&fsmToTransform,"");
    }
    Ofsm *fsmToMinimise = (intermediateResult.get() != nullptr ? static_cast<Ofsm*>(intermediateResult.get()) : &fsmToTransform);
    // Minimise the initially connected machine
    transformedFsm = transformFsm<ToMinimisedTransformer>(fsmToMinimise,nameSuffix);
}



void ToPrimeTransformer::visit(Dfsm& fsmToTransform) {
    // We already know that fsm is deterministic, and therefore observable
    std::unique_ptr<Fsm> intermediateResult;

    if ( not fsmToTransform.isInitiallyConnected() ) {
        // First make initially connected, then minimise
        intermediateResult = transformFsm<ToInitiallyConnectedTransformer>(&fsmToTransform,"");
    }
    Dfsm *fsmToMinimise = (intermediateResult.get() != nullptr ? static_cast<Dfsm*>(intermediateResult.get()) : &fsmToTransform);
    // Minimise the initially connected machine
    transformedFsm = transformFsm<ToMinimisedTransformer>(fsmToMinimise,nameSuffix);
}



}
