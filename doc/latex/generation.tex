\chapter{Test Suite Generation}\label{chap:tsgen}
% ==============================================================================================

\section{Test Generation Frame, Test Generation Method, and Test Suite}

To generate test suites using different methods (like W-Method, H-Method, etc.), three re-usable concepts have been implemented in  \fsml.
\begin{itemize}
\item A {\bf test generation frame} is instantiated to handle the test suite generation task in a way that is independent on the specific method. 

\item {\bf Test generation methods} are always implemented as FSM-visitors. The test generation frame receives a pointer to the test method to be used 
as a parameter of its constructor.

\item The test generation frame provides an operation to activate the test generation process.  This produces a {\bf test suite} which can be processed further in the program or written to disk.
\end{itemize}

Consider Listing~\ref{algo:genparadigm} to inspect how these concepts are applied in practise.




\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Application of test generation frame, method, and suite.},
label=algo:genparadigm]
#include "..."

using namespace std;
using namespace libfsmtest;

int main(int argc, char* argv[]) {

  // Create FSM from file
  auto fsm = createFsm<FsmFromFileCreator>("f.fsm","f");
  
  // Create test generation frame:
  // As generation method, the WMethod is selected.
  // "SUITE-W-0" is the name of the test suite.
  // The FSM is moved into the generation frame,
  // since it serves there as the reference model
  // to create test cases from.
  // This instance needs the maximal number of additional
  // states the implementation might contain. 
  int numAddStates = 0;
  TestGenerationFrame<WMethod> genFrame("SUITE-W-0",
                               move(fsm),
                               numAddStates);
    
  // Generate the test suite and write it to file
  genFrame.generateTestSuite();
  genFrame.writeToFile();
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

When method {\tt generateTestSuite()} is invoked (line~26),
the test generation frame {\tt genFrame} created in this example will activate the test 
generation internally according to the visitor pattern: the FSM-visitor instance of
class {\tt WMethod} is provided as parameter in an {\tt accept(\dots visitor instance\dots)} call
on the reference FSM. This is all hidden from users of \fsml, but must be studied before creating and adding your own test generation methods to \fsml.

The effect of the {\tt genFrame.writeToFile()} call is as follows.
\begin{enumerate}
\item The test suite consisting of a number of input traces is written into a text file named as the test suite with extension {\tt .txt}. The input events are represented in external form, as specified in the FSM's  presentation layer.
For the example in Listing~\ref{algo:genparadigm}, the demonstration program {\tt usage-demo} (see Chapter~\ref{chap:over}) produces the following test suite in file {\tt SUITE-W-0.txt}. Each line represents one test case specified by the associated inputs to be exercised on the system under test.

\begin{lstlisting}
e1, e1, e1
e1, e1, e2
e1, e2, e1, e1
e1, e2, e1, e2
e1, e2, e2, e1
e1, e2, e2, e2
e1, e2, e3, e1
e1, e2, e3, e2
e1, e2, e4, e1
e1, e2, e4, e2
e1, e3, e1
e1, e3, e2
e1, e4, e1, e1
e1, e4, e1, e2
e1, e4, e2, e1
e1, e4, e2, e2
e1, e4, e3, e1
e1, e4, e3, e2
e1, e4, e4, e1
e1, e4, e4, e2
e2, e1
e2, e2
e3, e1
e3, e2
e4, e1
e4, e2
\end{lstlisting}

\item The reference FSM is written in raw format (see Section~\ref{sec:rawformat}) to files 
\verb+<FSM-name>.fsm+, \verb+<FSM-name>.in+, \verb+<FSM-name>.out+, and  \verb+<FSM-name>.state+.
These FSM files, together with the test cases file, need to be provided to the test harness described in Chapter~\ref{chap:workflow}, when running the generated suite against a software under test. The harness will use the reference FSM as a test oracle.


\end{enumerate}

% ==========================================================================================
\section{Available Test Generation Methods}

In the current version, the following   generation methods listed in Table~\ref{tab:methods} 
have been provided, more are yet to come in the near future. All methods are implemented in files located in
\begin{verbatim}
    src/libfsmtest/visitors/testgeneration/
\end{verbatim}
Each method may pose requirements for admissible reference FSMs to fulfill. These
requirements are listed in Table~\ref{tab:method-requirements}. If the method
is called with a reference FSM that violates one of the method's application requirements,
the implementing visitor  will throw an exception.


\begin{table}[H]
\caption{Test generation methods currently available in \fsml.}
\begin{center}
\footnotesize
\begin{tabular}{|p{30mm}|p{28mm}|p{37mm}|p{20mm}|}\hline\hline
{\bf Method} & {\bf Class Name} & {\bf Files} & {\bf References} \\
\hline\hline
W-Method & {\tt WMethod} & {\tt WMethod.hpp, .cpp} & \cite{chow:wmethod}, \cite{vasilevskii1973}, \cite[Section~4.6]{PeleskaHuangLectureNotesMBT}
\\\hline
WP-Method & {\tt WPMethod} & {\tt WPMethod.hpp, .cpp} & \cite{luo_test_1994},   \cite[Section~4.8.1]{PeleskaHuangLectureNotesMBT}
\\\hline
H-Method & {\tt HMethod} & {\tt HMethod.hpp, .cpp} & \cite{DBLP:conf/forte/DorofeevaEY05},   
\cite[Section~4.7]{PeleskaHuangLectureNotesMBT}
\\\hline
SPYH-Method &  {\tt SPYHMethod} & {\tt SPYHMethod.hpp, .cpp} & \cite{DBLP:conf/icst/SouchaB18}  
\\\hline
T-Method & {\tt TMethod} & {\tt TMethod.hpp, .cpp} & \cite{t-method},   
\cite[Section~4.3]{PeleskaHuangLectureNotesMBT}
\\\hline
Safety-complete H-Method & {\tt SHMethod} & {SHMethod.hpp, .cpp} & \cite{DBLP:journals/sqj/HuangOP19},
\cite{huang2021complete}
\\\hline
Classical (non-adaptive) state counting method & {\tt ClassicalState\-CountingMethod} & {\tt ClassicalState\-Counting\-Method.hpp, .cpp}
& \cite{hierons_testing_2004}
\\\hline
Strong State Counting Method & {\tt StrongState\-Counting\-Method} & {StrongState\-Counting\-Method.hpp, .cpp} & 
\cite{DBLP:journals/corr/abs-2106-14284}
\\
\hline\hline
\end{tabular}
\normalsize
\end{center}
\label{tab:methods}
\end{table}%

\begin{table}[H]
\caption{Requirements for the test generation methods currently available in \fsml.}
\begin{center}
\footnotesize
\begin{tabular}{|p{30mm}|p{98mm}|}\hline\hline
{\bf Method} & {\bf Requirements}\\
\hline\hline
W-Method & Requires the existence of a characterisation set. This is guaranteed for completely specified reference FSMs.
\\\hline
WP-Method & Requires the existence of a characterisation set and state identification sets. This is guaranteed for completely specified reference FSMs.
\\\hline
H-Method & Requires harmonized traces. This is guaranteed for deterministic or completely specified reference FSMs.
\\\hline
SPYH-Method & Requires a completely specified and deterministic reference FSM.
\\\hline
T-Method & Requires a completely specified reference FSM.
\\\hline
Safety-complete H-Method & Requires harmonized traces. This is guaranteed for deterministic or completely specified reference FSMs.
\\\hline
Classical (non-adaptive) state counting method & Requires a completely specified reference FSM.
\\\hline
Strong State Counting Method & Reference FSMs are required to be observable, as the general approach to make any FSM observable does not preserve strong reduction.
\\
\hline\hline
\end{tabular}
\normalsize
\end{center}
\label{tab:method-requirements}
\end{table}%



When using a specific method, the test generation frame (see lines 20---22 in Listing~\ref{algo:genparadigm})
gets this method's class name as type parameter in the 
\begin{lstlisting}
TestGenerationFrame< _type_ >()
\end{lstlisting}
instantiation command. For example, when the H-Method should be used, the generation frame in
lines 20---22 of Listing~\ref{algo:genparadigm} is created with statement
\begin{lstlisting}
TestGenerationFrame<HMethod> genFrame("SUITE-H-0",// Any test suite name which makes clear that the H-Method has been used
                                     move(fsm),
                                     numAddStates);
\end{lstlisting}


% ==========================================================================================
\newpage
\section[Test Generation Methods Using Abstraction]{Test Generation Methods Using\\ Abstraction}\label{sec:abstraction}

In the context of {\bf property-oriented testing}~\cite{machado_towards_2007}, we are no longer focused on verifying a conformance relation between reference model and implementation. Instead, it has to be tested whether the SUT fulfils certain properties that are also fulfilled by the reference model. Properties are conditions about inputs, outputs, and their causal ordering. In practical applications, properties are often equivalent to, or derived from requirements to be fulfilled by the implementation. 
The most general way to specify properties is by means of a temporal logic such as LTL~\cite{van_de_pol_synchronous_2019}. This, however, is currently not yet supported by \fsml.

A slightly less general, but still quite powerful way is to specify properties by means of {\bf FSM abstractions}. The theory behind this has been investigated in~\cite{DBLP:journals/sqj/HuangOP19,huang2021complete}. Note that it is applicable to deterministic, completely specified FSMs only.
We introduce the -- quite intuitive -- concept here by means of an example.



\begin{example}{ex:alarmsystem}
Consider the completely specified DFSM $A$ shown in Fig.~\ref{fig:bigger-alarm-fsm} with input alphabet $\Sigma_I=\{c_1,\dots,c_6\}$ and output alphabet $\Sigma_O=\{ d_0, \dots, d_4 \}$. Suppose we wish to test whether the implementation satisfies the following property which is obviously fulfilled by $A$.
\begin{quote}
{\bf Property~1.}\ {\sl If the inputs are always in range $\{ c_1,c_2,c_3\}$ then the outputs will always be in range 
$\{ d_0,d_1\}$.} \hfill (*)
\end{quote}
Expressed in LTL, this property is specified by 
$$
\tg (c_1\vee c_2\vee c_3) \Rightarrow \tg(d_0\vee d_1),
$$ 
but we will not need this for the FSM abstraction approach. Instead, we specify an abstracted FSM 
$\alpha(A)$ as follows:  
\begin{enumerate}
\item The input alphabet of $\alpha(A)$ equals that of $A$, that is, $\{c_1,\dots,c_6\}$,
\item the output alphabet of $\alpha(A)$ is $\{e_0, e_1\}$, where $e_0$ stands for {``\sl $A$-output is in
$\{ d_0,d_1\}$''} and $e_1$ stands for {``\sl $A$-output is not in
$\{ d_0,d_1\}$''},

\item the states of  $\alpha(A)$ and the initial state are the same as in $A$, and
\item the transition relation $\alpha(R)$  of  $\alpha(A)$ is obtained from the transition relation $R$ of $A$ as
\begin{eqnarray*}
\alpha(R) & =  & \{ (s,x,e_0,s')~|~\exists y\in\{  d_0,d_1\} \centerdot (s,x,y,s')\in R         \} \cup {} 
\\
& & \{ (s,x,e_1,s')~|~\exists y\in\{  d_2,d_3,d_4 \} \centerdot (s,x,y,s')\in R         \}.
\end{eqnarray*}
\end{enumerate}
Intuitively speaking, $\alpha(A)$ has the same transition graph topology as $A$, and the transitions are labelled by the same inputs as in $A$. The outputs, however, are abstracted to the new values $e_0, e_1$, depending on whether the corresponding $A$-output is in $\{  d_0,d_1\}$ or not. This abstraction machine $\alpha(A)$ is shown in Fig.~\ref{fig:bigger-alarm-abs}.

Since the abstracted FSM has fewer outputs, it distinguishes fewer states than $A$: indeed, the minimised machine of $\alpha(A)$ only has two states, as shown in Fig.~\ref{fig:bigger-alarm-abs-prime}. Obviously,
$\alpha(A)$ fulfils the {\bf abstracted property}
\begin{quote}
{\bf Property~1a.}\ {\sl If the inputs are always in range $\{ c_1,c_2,c_3\}$ then the output will always be $e_0$.} \hfill (**)
\end{quote}

Now the theory developed in~\cite{DBLP:journals/sqj/HuangOP19,huang2021complete} states that we can apply the {\bf Safety-complete H-Method (SH-Method)} to derive an exhaustive test suite which is guaranteed to fail on an implementation violating property (*), because the abstraction FSM consistently abstracts this property to the one specified in (**). The SH-Method differs from the H-Method in the fact that  distinguishing traces $\gamma$ are appended to certain traces $\alpha, \beta$ already contained in the test suite only if the states reached by $\alpha$ and $\beta$, respectively, are also distinguishable in the   abstracted FSM. The ``normal'' H method appends $\gamma$ to $\alpha$ and $\beta$ already if these reach states that are distinguishable in $A$\footnote{States $q,q'$ that are distinguishable in $\alpha(A)$ are by construction also distinguishable in $A$, but not every pair of states distinguishable in $A$ is distinguishable in $\alpha(A)$.}.

As a consequence, the SH-Method may result in significantly fewer test cases than the H-Method. For the FSM example $A$ discussed here, the Safety-H-Method and the conventional H-Method produce the following numbers of test cases, depending on the maximal value $a$  of additional states assumed for the implementation.

\begin{center}
\begin{tabular}{|l||r|r|r|r|}
\hline\hline
 & $a = 0$ & $a = 1$ & $a = 2$ & $a = 3$\\
\hline\hline
SH-Method test suite size                  & 21 & 126 & 756 & 4536 \\\hline
H-Method test suite size & 28 & 158 & 982 & 5888 \\
\hline
\hline
Ratio                    & $0.75$ & $0.79$ & $0.77$ & $0.77$\\
\hline
\hline
\end{tabular}
\end{center}

\medskip
\noindent
Further examples are presented in~\cite{DBLP:journals/sqj/HuangOP19,huang2021complete}.
%
\end{example}


\begin{figure}[H]
%%\hspace*{-40mm}
\begin{center}
\input{tikz/bigger-alarm-fsm.tex}
\end{center}
%%\vspace*{-10mm}
\caption{FSM  $A$ with different regions: once state $s_2$ has been reached, the FSM will only visit states in $\{s_2, s_3, s_4\}$; it will never return to $s_0$ or~$s_1$.}
\label{fig:bigger-alarm-fsm}
\end{figure}




\begin{figure}[H]
\begin{center}
\input{tikz/bigger-alarm-abs-fsm.tex}
\end{center}
\caption{FSM abstraction $\alpha(A)$ of the original FSM $A$ shown in Fig.~\ref{fig:bigger-alarm-fsm}.}
\label{fig:bigger-alarm-abs}
\end{figure}


\begin{figure}[H]
\begin{center}
\input{tikz/bigger-alarm-abs-fsm-prime.tex}
\end{center}
\caption{Minimised FSM associated with $\alpha(A)$ from Fig.~\ref{fig:bigger-alarm-abs}.}
\label{fig:bigger-alarm-abs-prime}
\end{figure}



The abstraction concept described above 
is   implemented    by the SH-Method (class {\tt SHMethod}).
When using abstraction machines, the test generation frame is created with an additional parameter:
\begin{lstlisting}
// ... read reference FSM and abstraction FSM ...
// referenceFsm is a unique pointer to the reference FSM.
// abstractionFsm is a unique pointer to the abstraction FSM.
TestGenerationFrame<SHMethod> genFrame("SAFETY-H-METHOD-FSBRTSX",
                move(referenceFsm),
                numAdditionalStates,
                move(abstractionFsm));
                
// Generate the test suite and write it to file
genFrame.generateTestSuite();
genFrame.writeToFile();                
\end{lstlisting}

Observe that the  SH-Method is exhaustive, but not sound. This means that an implementation can fail a test suite even though it correctly implements the property for which the abstraction FSM has been created. In this case, the test suite has uncovered a violation of language equivalence, which we consider as a good thing, because in principle, the SUT should really be equivalent to the reference model, though we are currently only interested in a certain property. Test suites generated by the SH-Method will never fail for implementations that are language equivalent to the reference model. In~\cite{huang2021complete} it has been shown for a specific type of properties that 
it is possible to create complete (i.e.~exhaustive and sound) test suites that only fail if the specified property is violated. This insight, however, is of theoretical value only, because these test suites may become {\it larger} than suites establishing language equivalence.

The fact that {\it two} FSMs are required for the SH-Method deserves an explanation. In principle, it would be
possible to use the abstracted model itself as reference machine. However, the difference $a$ between the number of states in the minimised reference machine and the potential number of states in the minimised DFSM
representing the implementation behaviour would be larger than for the original reference machine. The test suite size, however, grows exponentially in $a$. Therefore, it is better to use the original machine ($A$ in the example above) with a smaller value of $a$.


Furthermore, note that it is not always the case that utilisation of an abstraction FSM will reduce the test suite size in comparison to testing for language equivalence. The following heuristics is applicable to decide this.
\begin{itemize}
\item The Safety-H-Method never produces more test cases then the H-Method.
\item If the prime machine of the FSM abstraction still has the same size as the prime machine of the reference model, then no reduction is to be expected. 
\item If all states of the reference model's prime machine can be distinguished by very few very short traces, then the test case reduction to be achieved by the Safety-H-Method can be expected to be quite small, even if the prime machine of the FSM abstraction has fewer states than that of the reference model.

\item If the reference FSM contains a region that is of no relevance for the property to be checked, and if   this region can never be left once entered, the test suite size reduction achieved by the SH-Method grows with the size of this region.

\item The ratio {\sl ``number of test cases generated by SH-Method / number of test cases generated by H-Method''} does not change significantly with the number $a$ of potential additional states in the implementation.
\end{itemize}

In any case, the test suites can be calculated beforehand, and if their size is nearly identical, it is more advisable to test for language equivalence, since this   guarantees that {\it all} properties fulfilled by the reference model are also fulfilled by the implementation.

Finally, note that the FSM abstraction and the resulting test suite created by the SH-Method are not only applicable to a single property, but to {\it all} properties captured by the same abstraction FSM.
This fact is well-known from the field of model checking. If a Kripke structure has a labelling function $L$
mapping concrete states $s$ to sets $L(s) \subseteq \text{AP}$  of atomic propositions  that are fulfilled in
this state, then the resulting Kripke structure can be used for property checking of {\it all} temporal formulas
(LTL, CTL, CTL*) over atomic propositions from $\text{AP}$~\cite{clarke_em-etal:1999a}.

\begin{example}{ex:abstraction_b}
Consider the following property of $A$ from Example~\ref{ex:alarmsystem} which is captured by the same FSM abstraction $\alpha(A)$.
\begin{quote}
{\bf Property~2.}\ {\sl After an output in $\{ d_2,d_3,d_4\}$ has been produced, there will never be
another output from $\{ d_0,d_1\}$.}  
\end{quote}
Using LTL, this property would be expressed as 
$$
\tg\big( (d_2\vee d_3 \vee d_4) \Rightarrow \tg(\neg d_0 \wedge \neg d_1) \big).
$$
This property is encoded in $\alpha(A)$ as well, since it can be expressed by
\begin{quote}
{\bf Property~2a.}\ {\sl After   output $e_1$ has been produced, there will never be
another output  $e_0$.}  
\end{quote}
The test suite created by the SH-Method for Property~1 from Example~\ref{ex:alarmsystem} is also exhaustive for Property~2.
\end{example}





%%% @todo usage example






















