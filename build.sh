pathToScript=$(readlink -f $(dirname "$0"))

for build in "Release" "Debug"; do
        mkdir "build.$build";
        cd "build.$build";
        cmake -DCMAKE_BUILD_TYPE=$build "$pathToScript/src";
        cd ..;
done
