cd $(dirname "$0")
pathToScript=$PWD

for build in "Debug"; do
        mkdir -p "xcodebuild.$build";
        cd "xcodebuild.$build";
        cmake -G Xcode -DCMAKE_BUILD_TYPE=$build "$pathToScript/src";
        cd ..;
done
