/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>
#include <algorithm>

#include "utils/CsvUtils.hpp"
#include "fsm/definitions.hpp"
#include "fsm/Transition.hpp"
#include "fsm/Dfsm.hpp"
#include "FsmFromRawCreator.hpp"

using namespace std;

namespace libfsmtest {

unique_ptr<Fsm> FsmFromRawCreator::createFsm() {
    readFiles();
    
    if(not initialState.has_value()) {
        throw runtime_error("ERROR: FSM does not have an initial state.");
    }
    for ( auto& state : states ) {
        if ( !state.isDeterministic() ) {
            // Decide whether to create an observable or arbitrary FSM
            for ( auto& s : states ) {
                if ( !s.isObservable() ) {
                    return make_unique<Fsm>(fsmName, move(states), move(presentationLayer), initialState.value());
                }
            }
            // Machine is observable, but not deterministic
            return make_unique<Ofsm>(fsmName, move(states), move(presentationLayer), initialState.value());
        }
    }
    // The FSM is deterministic, so we return a Dfsm instance
    return make_unique<Dfsm>(fsmName, move(states), move(presentationLayer), initialState.value());
}

void FsmFromRawCreator::readFiles() {
    ifstream fsm(fsmFileName);
    if (!fsm.is_open()) throw runtime_error("Failed to open fsm input file");

    auto removeTrailingWhitespace = [](string& s) {
        auto it = s.end();
        while (it != s.begin() && ::isspace(*--it));
        s.erase(it + 1, s.end());
    };
    Csv fsmData = readCsv(fsm, ' ', removeTrailingWhitespace, nullptr);

    Strings stateNames, inputAlphabet, outputAlphabet;

    if (statesFileName.empty() && inputFileName.empty() && outputFileName.empty()) {
        /**
         * second constructor was used, there are no alphabets or state names.
         * therefore use only internal format of indices.
         */

        struct Comparator {
            bool operator()(const string& lhs, const string& rhs) const {
                return stoul(lhs) < stoul(rhs);
            }
        };

        // create set with preStates from fsmData
        set<string, Comparator> stateNamesSet(fsmData.at(0).begin(), fsmData.at(0).end());
        // insert possibly different postStates from fsmData
        stateNamesSet.insert(fsmData.at(3).begin(), fsmData.at(3).end());
        // assign vector with data from set
        stateNames.assign(stateNamesSet.begin(), stateNamesSet.end());

        // Find highest indices for both alphabets
        Index highestInputIndex = stoul(*max_element(fsmData.at(1).begin(), fsmData.at(1).end(), Comparator()));
        Index highestOutputIndex = stoul(*max_element(fsmData.at(2).begin(), fsmData.at(2).end(), Comparator()));

        inputAlphabet.reserve(highestInputIndex + 1);
        for (Index i = 0; i <= highestInputIndex; ++i) {
            inputAlphabet.emplace_back(std::to_string(i));
        }

        outputAlphabet.reserve(highestOutputIndex + 1);
        for (Index i = 0; i <= highestOutputIndex; ++i) {
            outputAlphabet.emplace_back(std::to_string(i));
        }
    } else {
        stateNames = readFile(statesFileName);
        inputAlphabet = readFile(inputFileName);
        outputAlphabet = readFile(outputFileName);
    }

    states.reserve(stateNames.size());
    Index idx = 0;
    for_each(stateNames.begin(), stateNames.end(), [&](const auto& aStateName){states.emplace_back(aStateName,idx++);});


    for (size_t row = 0; row < fsmData.at(0).size(); ++row) {
        Index preState, input, output, postState;
        try {
            preState = stoul(fsmData.at(0).at(row));
        } catch (std::invalid_argument& invalid_argument) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": prestate ID (" + fsmData.at(0).at(row) + "is invalid: " + invalid_argument.what());
        } catch (std::out_of_range& outOfRange) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": prestate ID (" + fsmData.at(0).at(row) + "is invalid: " + outOfRange.what());
        }

        try {
            input = stoul(fsmData.at(1).at(row));
        } catch (std::invalid_argument& invalid_argument) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": input ID (" + fsmData.at(1).at(row) + "is invalid: " + invalid_argument.what());
        } catch (std::out_of_range& outOfRange) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": input ID (" + fsmData.at(1).at(row) + "is invalid: " + outOfRange.what());
        }

        try {
            output = stoul(fsmData.at(2).at(row));
        } catch (std::invalid_argument& invalid_argument) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": output ID (" + fsmData.at(2).at(row) + "is invalid: " + invalid_argument.what());
        } catch (std::out_of_range& outOfRange) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": output ID (" + fsmData.at(2).at(row) + "is invalid: " + outOfRange.what());
        }

        try {
            postState = stoul(fsmData.at(3).at(row));
        } catch (std::invalid_argument& invalid_argument) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": poststate ID (" + fsmData.at(3).at(row) + "is invalid: " + invalid_argument.what());
        } catch (std::out_of_range& outOfRange) {
            throw runtime_error("ERROR: At line " + to_string(row) + ": poststate ID (" + fsmData.at(3).at(row) + "is invalid: " + outOfRange.what());
        }

        if (not this->initialState.has_value()) {
            this->initialState = preState;
        }

        states.at(preState).getTransitions().emplace_back(states[preState].getId(), input, output, states[postState].getId());
    }

    presentationLayer = PresentationLayer(inputAlphabet, outputAlphabet, stateNames);
}

vector<string> FsmFromRawCreator::readFile(const string &fileName) {
    vector<string> v;

    ifstream input(fileName);
    string line;

    if (!input.is_open()) throw runtime_error("Failed to open input file");

    while (getline(input, line)) {
        line.erase(std::remove_if(line.begin(), line.end(), ::isspace), line.end());
        v.push_back(line);
    }

    return v;
}

}
