\chapter{FSM Creation}\label{chap:creation}
% ===================================================================================

Though the main classes {\tt Fsm}, {\tt Ofsm},  and {\tt Dfsm} have their own constructors, these are used only indirectly via factory methods. The \fsml\ provides factory methods for creating FSMs by reading them from various file formats,
by transformation of existing FSMs,  and by random generation. The available creator methods are described in the subsequent sections. Every creation method inherits from the abstract class {\tt FsmCreator}, see header file
\begin{quote}
\tt src/libfsmtest/creators/FsmCreator.hpp
\end{quote}

If you wish to program your own FSM creation method, you should also create a concrete class inheriting from  {\tt FsmCreator}.


% ==============================================================================================
\section{FSM Creation from Files}
\subsection{Raw File Format}\label{sec:rawformat}

The most versatile format for reading FSMs from file is the so-called {\bf raw format}.
%
\begin{itemize}
\item It allows for specification of deterministic or nondeterministic FSMs. In the nondeterministic case, the FSM may be observable or unobservable. Both deterministic and nondeterministic FSMs can be completely specified or partial.
\footnote{Please look up the definitions in~\cite{PeleskaHuangLectureNotesMBT}, if you are not familiar with these terms.}


\item It is possible to specify larger input alphabets, where not every input is processed by the FSM.
\item It is possible to specify larger output alphabets, where the FSM produces only a subset
of the output alphabet.
\end{itemize}
%
Additionally, if FSMs are the result of another automated generation process, the raw format is easier to generate automatically than the other formats accepted by the generator.

By convention, FSM definition files in raw format carry the file extension {\tt .fsm}.
Each line of an FSM definition file specifies one transition by means  
of four non-negative numbers
\begin{verbatim}
       <pre-state> <input> <output> <post-state>
\end{verbatim}
The interpretation of one transition line is: ``Starting in state {\tt <pre-state>}, the FSM may transit with input {\tt <input>} to state {\tt <post-state>}, producing
output {\tt <output>}.'' 
The states are numbered in range $0,1,2,\dots,(\text{NumberOfStates} - 1)$. The inputs are numbered in range $0,1,2,\dots,(\text{SizeOfInputAlphabet} - 1)$.
The outputs are numbered in range $0,1,2,\dots,(\text{SizeOfOutputAlphabet} - 1)$.

For every state, all outgoing transitions must be listed in consecutive lines. The initial FSM state is specified by the {\tt <pre-state>} of the first line in the file. Therefore the pre-state is not necessarily the one with number 0. This is practical when producing different FSMs from the same initial FSM by changing the initial state, but leaving all other specifications unchanged. In such a case, the block of lines starting with the new initial state is just moved to the beginning of the file.

Optionally, the {\tt .fsm}-file can be complemented by three files defining a {\bf presentation layer}; this layer specifies the external names of states, input events, and output events.
The first file associates external state names with internal state numbers.
The name of the {\tt .fsm}-file without that extension is usually treated as the {\tt FSM-Name}.
\begin{description}
\item[\tt <FSM-Name>.state] The state files are usually named by the FSM-Name with file extension {\tt .state}. The file contents consists of a single text column with  $\text{NumberOfStates}$
entries, such as
\begin{verbatim}
state0
state1
state2
...       
\end{verbatim}
This associates name ``{\tt state0}'' with state number 0, name ``{\tt state1}'' with state 
number 1 and so on. In external FSM representations, these names will be used as state names.

\item[\tt <FSM-Name>.in] The input files are usually named by the FSM-Name with file extension {\tt .in}. The file contents consists of a single text column with  $\text{SizeOfInputAlphabet}$
entries, such as
\begin{verbatim}
in0
in1
in2
...       
\end{verbatim}
This associates name ``{\tt in0}'' with input number 0, name ``{\tt in1}'' with input 
number 1 and so on. In external FSM representations, these names will be used as input names.

\item[\tt <FSM-Name>.out] The output files are usually named by the FSM-Name with file extension {\tt .out}. The file contents consists of a single text column with  
$\text{SizeOfOutputAlphabet}$
entries, such as
\begin{verbatim}
out0
out1
out2
...       
\end{verbatim}
This associates name ``{\tt out0}'' with output number 0, name ``{\tt out1}'' with output 
number 1 and so on. In external FSM representations, these names will be used as output names.
\end{description}



% ...................................................................................
\begin{figure}[H]
%%\hspace*{-40mm}
\begin{center}
\includegraphics[width=1.3\textwidth]{figures/gdc.pdf}
\end{center}
%%\vspace*{-10mm}
\caption{GraphViz representation of DFSM specified in raw format with the files shown below.}
\label{fig:dfsmgdcraw}
\end{figure}
%% ...................................................................................




For example, the state machine depicted   in Fig.~\ref{fig:dfsmgdcraw}
is specified in raw format as follows.

\paragraph{fsm-file.}
The state machine with its transitions is specified by
\begin{verbatim}
0 0 1 4 
0 1 0 0
0 2 0 0
0 3 0 0
1 0 2 5
1 1 0 1
1 2 0 1
1 3 0 1
2 0 1 4
2 1 0 2
2 2 0 2
2 3 0 2
3 0 2 5
3 1 0 3
3 2 0 3
3 3 0 3
4 0 3 2 
4 1 3 1
4 2 0 4
4 3 4 5
5 0 3 3
5 1 0 5
5 2 3 0
5 3 0 5
\end{verbatim}
 

\paragraph{state-file.}
The state file is specified by
\begin{verbatim}
Door_Up
Door_Down
Door_stopped_going_down
Door_stopped_going_up
Door_closing
Door_opening
\end{verbatim}


\noindent
\begin{minipage}[t]{.5\textwidth}
\paragraph{in-file.}
The input file is
\begin{verbatim}
e1
e2
e3
e4
\end{verbatim}
\end{minipage}
\hspace*{5mm}
\begin{minipage}[t]{.5\textwidth}
\paragraph{out-file.}
The output file is
\begin{verbatim}
null
a1
a2
a3
a4
\end{verbatim}
\end{minipage}


\bigskip
The FSM creator using raw format is implemented as class {\tt FsmFromFileCreator} with header file
\begin{quote}
\tt src/creators/input/FsmFromRawCreator.hpp
\end{quote}
%
Listing~\ref{algo:rawcreator} shows how to create an FSM from a raw input file, together with the three files for the presentation layer.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Create a FSM from raw format files.},
label=algo:rawcreator]
#include "Fsm.hpp"
#include "FsmFromRawCreator.hpp"

using namespace std;
using namespace libfsmtest;

int main(int argc, char* argv[]) {
    
  FsmFromRawCreator creator("nonObservable.fsm",
                            "nonObservable.state",
                            "nonObservable.in",
                            "nonObservable.out",
      "MY_NONOBSERVABLE_FSM"
  );
    
 // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

All classes implementing the factory methods from header file {\tt FsmCreator.hpp} need to provide one creator method {\tt std::unique\_ptr<Fsm> createFsm()}.

Note that the factory methods decide internally, whether to create instances of class {\tt Fsm} or instances of the sub-classes {\tt Ofsm} or {\tt Dfsm}. The creation result is always a unique pointer to {\tt Fsm}. 
Virtual functions and polymorphism ensure that the proper methods are called. For example, the call
{\tt bool b = fsm->isMinimal();} will  use Gill's Pk-table algorithm~\cite{gill62} when the FSM is deterministic, 
a nondeterministic variant thereof~\cite{PeleskaHuangLectureNotesMBT} if the machine is observable, and it will throw an exception if the FSM is non-observable.

Instead of manually instantiating a specific variant of an {\tt FsmCreator} and calling the factory method on that instance, the provided shorthand can be used as shown in the \autoref{algo:rawcreatorshorthand}.
Supplied with the type of {\tt FsmCreator} to be used as a template argument and the arguments for its constructor passed as parameters, it will directly return a unique pointer to {\tt Fsm}.

\begin{figure}[H]
  \rule{\textwidth}{0.5pt}
  \begin{lstlisting}[caption={Create a FSM from raw format files using the shorthand for {\tt FsmCreator}s},
  label=algo:rawcreatorshorthand]
  #include "Fsm.hpp"
  #include "FsmFromRawCreator.hpp"
  
  using namespace std;
  using namespace libfsmtest;
  
  int main(int argc, char* argv[]) {
      
    unique_ptr<Fsm> fsm = createFsm<FsmFromRawCreator>(
        "nonObservable.fsm",
        "nonObservable.state",
        "nonObservable.in",
        "nonObservable.out",
        "MY_NONOBSERVABLE_FSM"
    );
      
   // . . .
   
  }
  \end{lstlisting}
  \rule{\textwidth}{0.5pt}
  \end{figure}



% ==============================================================================================
\newpage
\subsection{CSV File Format for FSMs}\label{sec:csvformat}

FSMs can also be modelled using {\bf CSV-format}, exported from tools like
Excel or LibreOffice. 
Fig.~\ref{fig:dfsmexcel} shows a DFSM table for the Garage Door Controller example described below in Chapter~\ref{chap:workflow}.
The rules for filling out such a \emph{FSM transition table} are as follows.
\begin{enumerate}
\item The leftmost/uppermost field (A,1) is empty.
\item The first column, starting with (A,2), contains the state names, starting with the initial state in (A,2).
\item The first row, starting with (B,1), contains the identifiers of the input alphabet.
\item \begin{enumerate}
  \item For a state $s$ and input $x$, field $(s,x)$ has syntax $s'/y$. $s'$ is the post state of the transition from state $s$ on input $x$, and $y$ is the corresponding output. 
  $s'$ must be a valid state identifier occurring in the first column~A.
  \item There can be multiple outgoing transitions for state $s$ and input $x$.
  Then, field $(s,x)$ contains a comma-separated list $s_1^\prime / y_1, \ldots, s_n^\prime / y_n$ of the above syntax to specify more than one transition.
  For $s_i^\prime$ and $y_i$, the same conditions as above apply.
\end{enumerate}
\item All identifiers for states, inputs, and outputs conform to C-variable syntax: start with a character or an underscore, only characters, underscores, or numbers may follow, no spaces. These identifiers are used to construct the presentation layer: they represent the external names for states, inputs, and outputs. Internal consecutive 
non-negative numbers for these will be created internally.

\item If a table field for some state $s$ on some input $x$ is empty,
the resulting FSM will be partial. 


%
%
%
%this is interpreted as a self-loop
%$s/{\tt null}$: a transition with post-state identical to the pre-state, accompanied by a ``null output'' output ${\tt null}$ which is always inserted (with internal number 0) into the output alphabet.
%As a consequence, the DFSMs specified in this way are automatically completely specified.
%
%The convention to interpret empty table entries for a given input  as  self loops with null-output is usually called {\bf auto-completion}. Note that this has the disadvantage that it is impossible to specify partial DFSMs with the CSV format. You need to apply the raw format for this purpose.


\item {\bf The CSV format needs semicolon ``;'' as separator.} This is important when exporting from a formatted tabular file format (such as .xlsx or .ods) to CSV.
\end{enumerate}

An example of an admissible model CSV-format looks as follows, it corresponds
to the DFSM transition table shown in Fig.~\ref{fig:dfsmexcel}.

\begin{verbatim}
;e1;e2;e3;e4
Door_Up;Door_closing/a1;;;
Door_Down;Door_opening/a2;;;
Door_stopped_going_down;Door_closing/a1;;;
Door_stopped_going_up;Door_opening/a2;;;
Door_closing;Door_stopped_going_down/a3;Door_Down/a3;;Door_opening/a4
Door_opening;Door_stopped_going_up/a3;;Door_Up/a3;
\end{verbatim}
 

% .................................................................
\begin{figure}
% \hspace*{-10mm}
 \begin{center}
\includegraphics[width=1.1\textwidth]{figures/gdc-table.png} 
\end{center}
%\vspace*{-20mm}
\caption{Tabular format for modelling DFSMs.}
 \label{fig:dfsmexcel}
 \end{figure}
% ................................................................


The CSV creator is implemented as class {\tt FsmFromCsvCreator} with header file
\begin{quote}
\tt src/creators/input/FsmFromCsvCreator.hpp
\end{quote}
%
Listing~\ref{algo:csvcreator} shows how to create a FSM from a CSV file.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Create a FSM from a CSV file.},
label=algo:csvcreator]
#include "Dfsm.hpp"
#include "FsmFromCsvCreator.hpp"

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {

  unique_ptr<Fsm> fsm = createFsm<FsmFromCsvCreator>("garage-door-controller.csv");
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}



% ==============================================================================================
\newpage
\subsection[Reading FSMs from Files With Automated Format Identification]{Reading FSMs from File\\ With Automated Format Identification}\label{sec:fromfilereader}

The two FSM-from-file generators described above have been abstracted in class
\begin{quote}
\tt FsmFromFileCreator
\end{quote} 
contained in files  
\begin{quote}
\tt src/libfsmtest/creators/input/FsmFromFileCreator.hpp, .cpp
\end{quote} 
%
An instance of {\tt FsmFromFileCreator} first identifies the file
type provided by the user and then performs the FSM instantiation using the FSM-from-file creators described above.
This allows for simpler code in programs using the \fsml. In particular,   programs reading FSMs from different 
file formats need not require users to specify the format explicitly or add file identification code in the programs.


\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Create an FSM from file with automated format identification.},
label=algo:fromfilecreator]
#include "FsmFromFileCreator.hpp"
#include ". . . "

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {

 // First parameter of FsmFromFileCreator constructor:
 // basename of the file(s) to be opened
 // Secod (optional) parameter: FSM name to be used.
 unique_ptr<Fsm> fsm = createFsm<FsmFromFileCreator>("garage-door-controller",
                                                     "GDC");
 
 // ... now continue working with fsm ...
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

Consider the example in Listing~\ref{algo:fromfilecreator}.
An instance of {\tt FsmFromFileCreator} is created by   providing just the basename \verb+"garage-door-controller"+ of 
the file. The instance will then proceed as follows in the call to method {\tt createFsm}.
\begin{enumerate}
\item First, it is checked whether a file with the given basename and extension {\tt .fsm} exists. If this is the case,
the FSM will be instantiated with the help of class {\tt FsmFromRawCreator}, using the raw format. If {\tt  .in, .out, .state} files with the same basename exist as well, they will be used to create the FSM's presentation layer. If not, a presentation layer using just non-negative numbers for inputs, outputs, and states will be created internally.

\item If (1.) does not apply, the instance of {\tt FsmFromFileCreator}  looks for a file with the given basename and extension {\tt .csv}. If such a file exists, an instance of {\tt FsmFromCsvCreator} will be internally created, and the FSM will be instantiated from the CSV file.

\item Else, if file names are provided including the extension, the raw format is expected to be contained in a file with extension {\tt .fsm}, and the CSV format is expected to be contained in a file with extension {\tt .csv}. If an unknown or no extension is provided, the {\tt FsmFromFileCreator} instance tries to guess the file type from the file content.
\end{enumerate}


Recall from the description of the three FSM-from-file creators, that FSM files with incomplete transition information 
will lead to the instantiation of partial FSMs. In Section~\ref{sec:autocomptrans}, the option to perform auto-completion, that is, to create complete FSMs from partial specifications, is described. There, a third optional parameter of the  {\tt FsmFromFileCreator}-constructor is described which allows for automated application of 
an auto-completion transformation.

% ================================================================================
\newpage
\section{FSM Creation by Transformation}\label{sec:fsmtrans}

All FSM transformation classes reside in directory 
\begin{quote}
{\tt src/libfsmtest/creators/transformers}.
\end{quote}


% --------------------------------------------------------------------------------
\subsection{Auto-Completion Transformers}\label{sec:autocomptrans}
When specifying partial FSMs, inputs are enabled or disabled in a state-dependent way. If an input is enabled in a state we also say that it is \emph{defined} in that state, otherwise we say that it is \emph{undefined} in that state. There are different interpretations of inputs that are undefined in some state\footnote{A comprehensive discussion of these interpretations can be found in~\cite{DBLP:journals/corr/abs-2106-14284}.}.

\begin{enumerate}
\item The specification model is {\it incomplete}, and it is unknown what should happen for the undefined inputs in the respective states.
\item The undefined inputs {\it cannot occur} in the respective states (for example, the input corresponds to an input button of a graphical user interface which is not visible in the respective state).
\item The undefinedness of the input stands for {\it this input is ignored in this state}, so it's just a shorthand notation. This corresponds to a self loop transition labelled by this input and a {\tt null} output. 
\item The undefinedness of the input stands for {\it this input is not allowed in this state, and its occurrence will cause
an error output and a transition into an error state which is never left again}. This is another shorthand notation. All inputs occurring when the FSM is in the error state cause a {\tt null}-output.
\end{enumerate}

For the interpretations~3 and 4, two so-called {\bf auto-completion} transformers have been provided by classes
\begin{itemize}
\item {\tt ToAutoCompleteWithSelfLoopTransformer} and
\item {\tt ToAutoCompleteWithErrorStateTransformer}, 
\end{itemize}
both implemented in files 
\begin{verbatim}
  src/libfsmtest/creators/transformers/ToAutoCompletedTransformer.hpp
  src/libfsmtest/creators/transformers/ToAutoCompletedTransformer.cpp
\end{verbatim}
In Fig.~\ref{algo:autotrans}, it is shown how the transformers are instantiated and invoked.
Analog to the shorthand for generic {\tt FsmCreator}s explained in \autoref{sec:rawformat}, a shorthand for {\tt Transformer}s is provided.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Use of the auto-completion transformers.},
label=algo:autotrans]
#include "creators/transformers/ToAutoCompletedTransformer.hpp"
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {
  // ... read partial FSM from file, 
  // unique pointer fsm ...
  
  // Transform fsm to completely specified FSM, using
  // the self-loop auto-completion.
  // As external presentation of the {\tt null} output,
  // use string "_null". Type directive 'auto' means that
  // the new machine is again of type unique_ptr<Fsm>.
  ToAutoCompleteWithSelfLoopTransformer selfLooper(fsm.get(), "_null");
  auto selfLoopCompletedFsm = selfLooper.createFsm();
  
  // Transform fsm to completely specified FSM, using
  // the auto-completion with transition to an error state.
  // As external presentation of the {\tt null} output,
  // use string "_null". For the error output, use "_error".
  // As error state name, use "ERROR"
  // This time, we also use the shorthand.
  auto errorStateCompletedFsm = transformFsm<ToAutoCompleteWithErrorStateTransformer>(fsm.get(), "_error", "_null", "ERROR");
  
  // Continue working with completely specified FSMs selfLoopCompletedFsm
  // and errorStateCompletedFsm
  // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

Quite often, it is already clear when reading an FSM from a file that an auto-completion should be performed.
Therefore, the {\tt FsmFromFileCreator} offers the option to provide such a transformer when instantiating an
{\tt FsmFromFileCreator} object. This is shown in Listing~\ref{algo:createwithtrans}.


\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Use of the auto-completion transformers.},
label=algo:createwithtrans]
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {

  // We know that file garage-door-controller.csv contains a 
  // partial FSM definition, and we want the FSM to be 
  // auto-completed with self-loops and null-outputs.
  // To this end, an instance of ToAutoCompleteWithSelfLoopTransformer
  // is provided to creator as third parameter.
  // The instance of ToAutoCompleteWithSelfLoopTransformer 
  // gets a nullptr for the FSM, since this FSM first 
  // has to be read from file.
  unique_ptr<Fsm> fsm = transformFsm<FsmFromFileCreator>("garage-door-controller.csv",
            "garage-door-controller",
            make_unique<ToAutoCompleteWithSelfLoopTransformer>
                                                (nullptr, "null"));

  // ... continue working with fsm ...

}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}


% --------------------------------------------------------------------------------
\newpage
\subsection{Transformation to Prime Machines}\label{sec:primetrans}

Recall that a {\bf prime machine} is an FSM that is initially connected, observable, and minimised. 


% .................................................................................
\paragraph{Transformation to the prime machine.}
Quite often, it is desirable to
transform a given arbitrary FSM into an initially connected, observable, and minimised FSM in one step.
This task is performed by class {\tt ToPrimeTransformer} in files
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/transformers/ToPrimeTransformer.hpp
  src/libfsmtest/creators/transformers/ToPrimeTransformer.cpp
\end{verbatim}
\normalsize

This transformer converts the given FSM according to the following steps.
\begin{enumerate}
\item If necessary, the FSM is transformed into an initially connected one.
\item If necessary, the resulting machine is transformed into an observable FSM.
\item The resulting machine is minimised. 
\end{enumerate}
These steps are performed by class {\tt ToPrimeTransformer} through the individual transformers described in the paragraphs below.

Listing.~\ref{algo:toprime} shows how to use this transformation.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Transformation to prime machine.},
label=algo:toprime]
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {
  // ... read partial FSM from file, 
  // unique pointer fsm ...
  
  // Define the transformer to prime FSM,
  // providing the original FSM as a parameter to the shorthand
  // Perform the transformation and obtain unique pointer to
  // the prime FSM
  auto primeFsm = transformFsm<ToPrimeTransformer>(fsm.get());

  
  // Continue working with the prime FSM
  // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}


% .................................................................................
\paragraph{Transformation to initially connected machine.}

Recall that an FSM is {\tt initially connected} if all states can be reached from the initial states by repeated application of the transition relation. The transfomer implemented by class {\tt ToInitiallyConnectedTransformer}
in files 
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/transformers/ToInitiallyConnectedTransformer.hpp
  src/libfsmtest/creators/transformers/ToInitiallyConnectedTransformer.cpp
\end{verbatim}
\normalsize
%
performs this transformation task.


Listing.~\ref{algo:toic} shows how to use this transformation.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Transformation to an initially connected FSM.},
label=algo:toic]
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {
  // ... read partial FSM from file, 
  // unique pointer fsm ...
  
  // Define the transformer to initially connected FSM,
  // providing the original FSM as a parameter to the shorthand
  // Perform the transformation and obtain unique pointer to
  // the initially connected FSM
  auto primeFsm = createFsm<ToInitiallyConnectedTransformer>(fsm.get());
  
  // Continue working with the initially connected FSM
  // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}




% .................................................................................
\paragraph{Transformation to observableFSMs.}
Any FSM can be transformed into a language-equivalent observable one~\cite{PeleskaHuangLectureNotesMBT}.\footnote{The algorithm is applicable to both completely specified and partial machines. It is interesting to note, however, that in case of partial FSMs,
the transformed FSM is not necessarily quasi-equivalent to the original FSM~\cite{DBLP:journals/scp/Hierons19}, and not necessarily a strong reduction of the original FSM~\cite{DBLP:journals/corr/abs-2106-14284}.} To this end,
class {\tt ToObservableTransformer} in files 
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/transformers/ToObservableTransformer.hpp
  src/libfsmtest/creators/transformers/ToObservableTransformer.cpp
\end{verbatim}
\normalsize
implements the standard algorithm for the transformation of arbitrary (partial or comletely specified) FSM into observable ones with the same language. The transformer will throw an exception if the FSM is not initially connected. If the FSM is already observable, a copy will be returned by operation {\tt createFsm()} (see Listing~\ref{algo:toobs}





Listing~\ref{algo:toobs} shows how to use this transformation. 

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Transformation to an observable FSM.},
label=algo:toobs]
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {
  // ... read partial FSM from file, 
  // unique pointer fsm ...
  
  // Define the transformer to observable FSM,
  // providing the original FSM as a parameter to the shorthand
  // Perform the necessary transformations and obtain 
  // the unique pointer to the observable FSM
  auto fsmObs = createFsm<ToObservableTransformer>(fsm.get());
  
  // Continue working with the observable FSM
  // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

% .................................................................................
\paragraph{Minimising observable, initially connected machines.}

For minimising observable, initially connected machines, the transformer of 
class {\tt ToMinimisedTransformer} is used. It is implemented in files
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/transformers/ToMinimisedTransformer.hpp, .cpp
\end{verbatim}
\normalsize
This transformer throws exceptions if the FSM to be transformed is not initially connected or not observable.


Listing~\ref{algo:tomin} shows how to use this transformation. 

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Transformation to minimised FSM.},
label=algo:tomin]
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {
  // ... read partial FSM from file, 
  // unique pointer fsm ...
  
  // Define the transformer to minimised FSM,
  // providing the original FSM as a parameter to the shorthand
  // Perform the necessary transformations and obtain 
  // the unique pointer to the minimsed FSM
  auto fsmMin = createFsm<ToMinimisedTransformer>(fsm.get());
  
  // Continue working with the minimised FSM
  // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}


% =================================================================================
\newpage
\section{Random Creation and Mutation of FSMs}\label{sec:fsmrand}

There are many use cases for randomly generated FSMs.
All classes for random FSM creation and mutation of existing FSMs reside in directory 
\begin{quote}
{\tt src/libfsmtest/creators/randoms}.
\end{quote}

Every class for random creation of FSMs inherits from the class {\tt RandomFsmCreator}, which takes care of the initialisation of a presentation layer; see header file 
\begin{quote}
  {\tt src/libfsmtest/creators/randoms/RandomFsmCreator.hpp}.
\end{quote}
To implement other methods for random creation, this file should be inherited in a new concrete class.
Classes for random mutation of FSMs should inherit the already discussed {\tt Transformer} class: a basic concept of the \fsml\ class library is to never change an existing FSM, but to transform it into another one. Therefore, mutations
are created by transforming the existing FSM into a different one.


% -----------------------------------------------------------------------------------
\subsection{Creation of Random Completely Specified FSMs.}\label{sec:completefsmrand}
Recall that a FSM is {\bf completely specified} if there exists an outgoing transition for every possible input in every state.   FSMs, OFSMs,  and DFSMs can be completely specified.

\paragraph{Random Completely Specified FSM}
The class to create random completely specified FSMs can be found in files
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/randoms/RandomCompletelySpecifiedFsm.hpp, .cpp
\end{verbatim}
\normalsize

For a given numbers of states, inputs and outputs the creator will then generate a completely specified FSM.
During the generation it is also asserted that the FSM generated is initially connected.
For each state, the FSM will have at least one, at most three outgoing transitions for each input.
Listing \ref{algo:randomcsfsmcreator} shows how to use this creator.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Create a Random Completely Specified FSM.},
label=algo:randomcsfsmcreator]
#include "RandomCompletelySpecifiedFsmCreator.hpp"
#include ". . . "

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {

  // First parameter of RandomCompletelySpecifiedFsmCreator:
  // Number of states the FSM will have.
  // Second parameter: Number of inputs the alphabet will have.
  // Third parameter: Number of outputs the alphabet will have.
  // Fourth (optional) parameter: FSM name to be used.
  unique_ptr<Fsm> fsm = createFsm<RandomCompletelySpecifiedFsmCreator>(5, 7, 4, "random");
  
  // ... now continue working with fsm ...
  
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

If it is required to produce an {\it observable}, but potentially nondeterministic FSM at random, 
the machine is created as shown in Listing~\ref{algo:randomcsfsmcreator}, but then transformed into an observable one, using the transformer to observable FSMs described in Section~\ref{sec:fsmtrans}. Using FSM method {\tt isObervable()}, it should be checked whether such a transformation is needed, since the FSM generated at random may already be observable. Note, however, that the transformed OFSM will generally not have the same number of states
as requested in the random creation.


% ......................................................................
\paragraph{Random Completely Specified DFSM.}
The class to create random completely specified DFSMs can be found in files
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/randoms/RandomCompletelySpecifiedDfsm.hpp, .cpp
\end{verbatim}
\normalsize


For a given numbers of states, inputs and outputs the creator will then generate a completely specified DFSM.
Each state will have exactly one outgoing transition for each input.
Listing \ref{algo:randomcsdfsmcreator} shows how to use this creator. Recall that DFSMs are automatically observable.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Create a Random Completely Specified FSM.},
label=algo:randomcsdfsmcreator]
#include "RandomCompletelySpecifiedDfsmCreator.hpp"
#include ". . . "

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {

  // First parameter of RandomCompletelySpecifiedDfsmCreator:
  // Number of states the DFSM will have.
  // Second parameter: Number of inputs the alphabet will have.
  // Third parameter: Number of outputs the alphabet will have.
  // Fourth (optional) parameter: DFSM name to be used.
  unique_ptr<Fsm> fsm = createFsm<RandomCompletelySpecifiedDfsmCreator>(5, 7, 4, "random");
  
  // ... now continue working with dfsm ...
  
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}

% -----------------------------------------------------------------------------------
\subsection{Random Removal of Transitions}\label{sec:fsmrandalter}
The class to randomly remove transitions from FSMs can be found in files
\footnotesize
\begin{verbatim}
  src/libfsmtest/creators/mutators/RandomTransitionRemoval.hpp, .cpp
\end{verbatim}
\normalsize

Given a number of transitions to remove, this transformer will randomly choose a state that has at least one transition, then randomly choose one of those transitions and remove it.
The transformer will stop and return the altered FSM once the number of transitions to remove has been reached, or there are no transitions left to remove.
Listing~\ref{algo:randomcsdfsmcreator} shows how to use this transformer.

\begin{figure}[H]
\rule{\textwidth}{0.5pt}
\begin{lstlisting}[caption={Transformation to remove random transitions of an FSM.},
label=algo:randomremoval]
#include "creators/mutators/RandomTransitionRemoval.hpp"
#include "..."

using namespace std;
using namespace libfsmtest;

void main(int argc, char* argv[]) {
  // ... create or retrieve FSM
  // unique pointer fsm ...
  
  // Define the transformer to remove random transitions,
  // providing the original FSM to the constructor
  // as well as the number of transition to remove
  // and an (optional) suffix to append to the name.
  // Perform the necessary transformations and obtain 
  // the unique pointer to the altered FSM
  auto fsmRm = transformFsm<RandomTransitionRemoval>(fsm.get(), 5);

  // Continue working with the altered FSM
  // . . .
 
}
\end{lstlisting}
\rule{\textwidth}{0.5pt}
\end{figure}






















