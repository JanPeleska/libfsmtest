/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <deque>
#include <map>
#include <set>

#include "SHMethod.hpp"
#include "fsm/Dfsm.hpp"
#include "visitors/TestGenerationFrame.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"
#include "creators/transformers/ToObservableTransformer.hpp"
#include "testsuite/cutTreeWithFSMLanguage.hpp"

using namespace std;

namespace libfsmtest {

template<typename X, typename Y, typename Z>
map<X, Z> composeMaps(map<X, Y> const &first, map<Y, Z> const &second) {
    map<X, Z> result;
    for (auto const &firstMapElement : first) {
        result[firstMapElement.first] = second.at(firstMapElement.second);
    }
    return result;
}

template<typename X, typename Y, typename Z>
map<X, set<Z>> composeMaps(map<X, set<Y>> const &first, map<Y, Z> const &second) {
    map<X, set<Z>> result;
    for (auto const &firstMapElement : first) {
        for (auto const &firstMapElementMapped : firstMapElement.second) {
            result[firstMapElement.first].insert(second.at(firstMapElementMapped));
        }
    }
    return result;
}


void SHMethod::calculateClassMaps(Ofsm &fsm) {

    vector<map<Index, size_t> > state2classMapsFsm;
    fsm.calcOFSMTables(classMapFsm, state2classMapsFsm);
    state2classFsm = state2classMapsFsm.back();

    vector<map<Index, size_t> > state2classMapsAbsFsm;
    if (not abstractionFsm->isObservable()) {
        ToObservableTransformer transformer(abstractionFsm->clone().release());
        auto observableAbstraction = transformer.createFsm();
        auto observabilityMap = transformer.getLastStateMapping();
        observableAbstraction->calcOFSMTables(classMapAbsFsm, state2classMapsAbsFsm);
        state2classAbsFsm = composeMaps(observabilityMap, state2classMapsAbsFsm.back());
    } else {
        abstractionFsm->calcOFSMTables(classMapAbsFsm, state2classMapsAbsFsm);
        state2classAbsFsm.clear();
        for(auto [key, value] : state2classMapsAbsFsm.back()) {
            state2classAbsFsm[key].insert(value);
        }
    }

    /** Map reference model classes to abstraction model classes */
    for (const auto &p : classMapFsm) {
        size_t refClassId = p.first;
        Index representativeId = *p.second.begin();

        refClass2AbsClass[refClassId] = state2classAbsFsm[representativeId];
    }

}

void SHMethod::visit(Ofsm &fsm) {
    if (!fsm.isCompletelySpecified()) {
        throw runtime_error("This safety-complete H method implementation only works with completely specified FSMs");
    }

    if (isFirstCall) {
        isFirstCall = false;
        calculateClassMaps(fsm);
        if (!abstractionFsm->isObservable()) {
            abstractionFsm = transformFsm<ToObservableTransformer>(abstractionFsm->clone().release());
        }
        if (!abstractionFsm->isMinimal()) {
            abstractionFsm = transformFsm<ToPrimeTransformer>(abstractionFsm->clone().release());
        }

#if 1
        ToDotFileVisitor dot("abstraction-fsm.dot");
        abstractionFsm->accept(dot);
        dot.writeToFile();
#endif
    }


    if (fsm.isMinimal()) {

#if 1
        ToDotFileVisitor dot("reference-fsm-minimised.dot");
        fsm.accept(dot);
        dot.writeToFile();
#endif

        size_t numberOfStates = fsm.size();
        size_t inputAlphaSize = fsm.getInputAlphabetSize();

        // Calculate OFSM tables just once - will be used by every call to
        // getShortestDistinguishingTraces()
        // Note that classMap, state2classMaps are attributes of the parent class
        // HMethod.
        fsm.calcOFSMTables(classMap, state2classMaps);

        Traces **dist = new Traces *[numberOfStates];
        for (Index i = 0; i < numberOfStates; ++i)
            dist[i] = new Traces[numberOfStates];

        for (Index s0 = 0; s0 < numberOfStates; s0++) {
            for (Index s1 = s0 + 1; s1 < numberOfStates; s1++) {
                Trace currentTrace;
                getShortestDistinguishingTraces(fsm, s0, s1, inputAlphaSize, currentTrace, dist[s0][s1]);
            }
        }

        createStateCover(fsm);

        auto stateCoverAsTree = make_unique<Tree>(*testCaseTree);

        // Store all traces from state cover
        Traces stateCoverTraces = testCaseTree->getAllTestCases();

        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates + 1,
                                       inputAlphaSize,
                                       enumeratedTraces);

        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);

        // Cut test case tree with FSM language to only have defined traces in the tree
        testCaseTree = cutTreeWithFSMLanguage(*testCaseTree, fsm);

        // Store all traces of stateCover.EnumeratedTraces
        Traces stateCover_enumeratedTraces = testCaseTree->getAllTestCases();

        // Step 1a.
        // Add all alpha.gamma, beta.gamma where alpha, beta in V
        // and gamma distinguishes s0-after-alpha, s0-after-beta
        // (if alpha.gamma or beta.gamma are already in testCaseTree, addition
        // will not lead to a new test case)
        // For 1a, perform only one-trace-insertions
        for (size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++) {
            Trace alpha = stateCoverTraces.at(tr1);
            set<Index> s0AfterAlphaId = fsm.getTargets(alpha, fsm.getInitialStateId());
            for (size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++) {
                Trace beta = stateCoverTraces.at(tr2);

                auto s0AfterBetaId = fsm.getTargets(beta, fsm.getInitialStateId());
                for (Index alphaState : s0AfterAlphaId) {
                    for (Index betaState : s0AfterBetaId) {
                        // If reached states are different, look for gamma
                        if (alphaState != betaState) {
                            Index s0 = min(alphaState, betaState);
                            Index s1 = max(alphaState, betaState);
                            if (dist[s0][s1].empty()) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processOneTraceInsertions(alpha, beta, dist[s0][s1]);
                        }
                    }
                }
            }
        }

        // Step 2a.
        // Handle pairs of traces alpha, beta with alpha in state cover and
        // beta in stateCover_enumeratedTraces.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        // For 2a, perform only one-trace-insertions
        for (const auto &alpha : stateCoverTraces) {

            set<Index> sAfterAlpha = fsm.getTargets(alpha, fsm.getInitialStateId());
            for (const auto &beta : stateCover_enumeratedTraces) {

                // Skip if beta in state cover
                if (stateCoverAsTree->getInsertionCost(beta) == 0) {
                    continue;
                }

                set<Index> sAfterBeta = fsm.getTargets(beta, fsm.getInitialStateId());
                for (Index alphaState : sAfterAlpha) {
                    for (Index betaState : sAfterBeta) {
                        if (refClass2AbsClass[alphaState] != refClass2AbsClass[betaState]) {
                            Index s0 = min(alphaState, betaState);
                            Index s1 = max(alphaState, betaState);

                            if (dist[s0][s1].empty()) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processOneTraceInsertions(alpha, beta, dist[s0][s1]);
                        }
                    }
                }
            }
        }

        // Step 3a.
        // Handle pairs of traces v.u', v.u'.u, where
        // v is from the state cover, (u'.u) is an arbitrary input trace
        // of length 2..(1+numAddStates), and both u', u are non-empty.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        // For 3a, perform only one-trace-insertions
        for (const auto &v : stateCoverTraces) {

            for (size_t uPrimeLen = 1; uPrimeLen < 1 + numAddStates; uPrimeLen++) {

                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               inputAlphaSize,
                                               uPrimeTraces);

                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               inputAlphaSize,
                                               uTraces);

                for (const auto &uPrime : uPrimeTraces) {
                    for (const auto &u : uTraces) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);

                        set<Index> sPrime = fsm.getTargets(v_uPrime, fsm.getInitialStateId());
                        set<Index> s = fsm.getTargets(v_uPrime_u, fsm.getInitialStateId());

                        for (Index stateAfterPrime : sPrime) {
                            for (Index stateAfterPrimeAndU : s) {
                                if (refClass2AbsClass[stateAfterPrime] != refClass2AbsClass[stateAfterPrimeAndU]) {
                                    Index s0 = min(stateAfterPrime, stateAfterPrimeAndU);
                                    Index s1 = max(stateAfterPrime, stateAfterPrimeAndU);

                                    if (dist[s0][s1].empty()) {
                                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                            to_string(s0) + "," + to_string(s1));
                                    }
                                    processOneTraceInsertions(v_uPrime, v_uPrime_u, dist[s0][s1]);
                                }
                            }
                        }
                    }
                }

            }

        }



        // Step 1b.
        // Add all alpha.gamma, beta.gamma where alpha, beta in V
        // and gamma distinguishes s0-after-alpha, s0-after-beta
        // (if alpha.gamma or beta.gamma are already in testCaseTree, addition
        // will not lead to a new test case)
        for (size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++) {
            Trace alpha = stateCoverTraces.at(tr1);
            set<Index> s0AfterAlphaId = fsm.getTargets(alpha, fsm.getInitialStateId());
            for (size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++) {
                Trace beta = stateCoverTraces.at(tr2);

                set<Index> s0AfterBetaId = fsm.getTargets(beta, fsm.getInitialStateId());
                for (Index alphaState : s0AfterAlphaId) {
                    for (Index betaState : s0AfterBetaId) {
                        // If reached states are different, look for gamma
                        if (alphaState != betaState) {
                            Index s0 = min(alphaState, betaState);
                            Index s1 = max(alphaState, betaState);
                            if (dist[s0][s1].empty()) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processDistinguishingTraces(alpha, beta, dist[s0][s1]);
                        }
                    }
                }
            }
        }

        // Step 2b.
        // Handle pairs of traces alpha, beta with alpha in state cover and
        // beta in stateCover_enumeratedTraces.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        for (const auto &alpha : stateCoverTraces) {

            set<Index> sAfterAlpha = fsm.getTargets(alpha, fsm.getInitialStateId());
            for (const auto &beta : stateCover_enumeratedTraces) {

                // Skip if beta in state cover
                if (stateCoverAsTree->getInsertionCost(beta) == 0) {
                    continue;
                }

                set<Index> sAfterBeta = fsm.getTargets(beta, fsm.getInitialStateId());
                for (Index alphaState : sAfterAlpha) {
                    for (Index betaState : sAfterBeta) {
                        if (refClass2AbsClass[alphaState] != refClass2AbsClass[betaState]) {
                            Index s0 = min(alphaState, betaState);
                            Index s1 = max(alphaState, betaState);

                            if (dist[s0][s1].empty()) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processDistinguishingTraces(alpha, beta, dist[s0][s1]);
                        }
                    }
                }
            }
        }

        // Step 3b.
        // Handle pairs of traces v.u', v.u'.u, where
        // v is from the state cover, (u'.u) is an arbitrary input trace
        // of length 2..(1+numAddStates), and both u', u are non-empty.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        for (const auto &v : stateCoverTraces) {

            for (size_t uPrimeLen = 1; uPrimeLen < 1 + numAddStates; uPrimeLen++) {

                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               inputAlphaSize,
                                               uPrimeTraces);

                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               inputAlphaSize,
                                               uTraces);

                for (const auto &uPrime : uPrimeTraces) {
                    for (const auto &u : uTraces) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);

                        set<Index> sPrime = fsm.getTargets(v_uPrime, fsm.getInitialStateId());
                        set<Index> s = fsm.getTargets(v_uPrime_u, fsm.getInitialStateId());

                        for (Index stateAfterPrime : sPrime) {
                            for (Index stateAfterPrimeAndU : s) {
                                if (refClass2AbsClass[stateAfterPrime] != refClass2AbsClass[stateAfterPrimeAndU]) {
                                    Index s0 = min(stateAfterPrime, stateAfterPrimeAndU);
                                    Index s1 = max(stateAfterPrime, stateAfterPrimeAndU);

                                    if (dist[s0][s1].empty()) {
                                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                            to_string(s0) + "," + to_string(s1));
                                    }
                                    processDistinguishingTraces(v_uPrime, v_uPrime_u, dist[s0][s1]);
                                }
                            }
                        }
                    }
                }

            }

        }


        // Add missing trace candidates, optimising the size of the test case tree
        addBestCandidateTraces(fsm, inputAlphaSize);

        // Check final version of the test case tree
        if (!testCaseTree->checkTree()) throw runtime_error("Malformed test case tree");

        // Copy test case tree into test suite
        testCaseTreeToTestSuite();

        for (Index i = 0; i < numberOfStates; ++i)
            delete[] dist[i];
        delete[] dist;
    } else {

        // Need to transform to prime OFSM (i.e. initially connected, minimised)

        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm, "_MIN");

        // Now visit the prime DFSM
        minimisedOfsm->accept(*this);

    }
}

void SHMethod::visit(Dfsm &fsm) {

    if (!fsm.isCompletelySpecified()) {
        throw runtime_error("This safety-complete H method implementation only works with completely specified DFSMs");
    }

    if (isFirstCall) {
        isFirstCall = false;
        calculateClassMaps(fsm);
        if (!abstractionFsm->isMinimal()) {
            abstractionFsm = transformFsm<ToPrimeTransformer>(abstractionFsm.get());
        }

#if 0
        ToDotFileVisitor dot("abstraction-fsm.dot");
        abstractionFsm->accept(dot);
        dot.writeToFile();
#endif
    }


    if (fsm.isMinimal()) {

#if 1
        ToDotFileVisitor dot("reference-fsm-minimised.dot");
        fsm.accept(dot);
        dot.writeToFile();
#endif

        size_t numberOfStates = fsm.size();
        size_t inputAlphaSize = fsm.getInputAlphabetSize();

        // Calculate OFSM tables just once - will be used by every call to
        // getShortestDistinguishingTraces()
        // Note that classMap, state2classMaps are attributes of the parent class
        // HMethod.
        fsm.calcOFSMTables(classMap, state2classMaps);

        Traces **dist = new Traces *[numberOfStates];
        for (Index i = 0; i < numberOfStates; ++i)
            dist[i] = new Traces[numberOfStates];

        for (Index s0 = 0; s0 < numberOfStates; s0++) {
            for (Index s1 = s0 + 1; s1 < numberOfStates; s1++) {
                Trace currentTrace;
                getShortestDistinguishingTraces(fsm, s0, s1, inputAlphaSize, currentTrace, dist[s0][s1]);
            }
        }

        createStateCover(fsm);

        auto stateCoverAsTree = make_unique<Tree>(*testCaseTree);

        // Store all traces from state cover
        Traces stateCoverTraces = testCaseTree->getAllTestCases();

        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates + 1,
                                       inputAlphaSize,
                                       enumeratedTraces);

        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);

        // Cut test case tree with FSM language to only have defined traces in the tree
        testCaseTree = cutTreeWithFSMLanguage(*testCaseTree, fsm);

        // Store all traces of stateCover.EnumeratedTraces
        Traces stateCover_enumeratedTraces = testCaseTree->getAllTestCases();

        // Step 1a.
        // Add all alpha.gamma, beta.gamma where alpha, beta in V
        // and gamma distinguishes s0-after-alpha, s0-after-beta
        // (if alpha.gamma or beta.gamma are already in testCaseTree, addition
        // will not lead to a new test case)
        // For 1a, perform only one-trace-insertions
        for (size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++) {
            Trace alpha = stateCoverTraces.at(tr1);
            optional<Index> s0AfterAplhaId = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for (size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++) {
                Trace beta = stateCoverTraces.at(tr2);

                optional<Index> s0AfterBetaId = fsm.getTarget(fsm.getInitialStateId(), beta);
                // If reached states are different, look for gamma
                if (s0AfterAplhaId.value() != s0AfterBetaId.value()) {
                    // Both `alpha` and `beta` are always defined and thus so
                    // is `s0AfterAplhaId` and `s0AfterBetaId`
                    Index s0 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterAplhaId.value() : s0AfterBetaId.value();
                    Index s1 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterBetaId.value() : s0AfterAplhaId.value();
                    if (dist[s0][s1].empty()) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processOneTraceInsertions(alpha, beta, dist[s0][s1]);
                }
            }
        }

        // Step 2a.
        // Handle pairs of traces alpha, beta with alpha in state cover and
        // beta in stateCover_enumeratedTraces.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        // For 2a, perform only one-trace-insertions
        for (const auto &alpha : stateCoverTraces) {

            optional<Index> sAfterAlpha = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for (const auto &beta : stateCover_enumeratedTraces) {

                // Skip if beta in state cover
                if (stateCoverAsTree->getInsertionCost(beta) == 0) {
                    continue;
                }

                optional<Index> sAfterBeta = fsm.getTarget(fsm.getInitialStateId(), beta);
                // Both `alpha` and `beta` are always defined and thus so
                // is `sAfterAlphaId` and `sAfterBetaId`
                if (refClass2AbsClass[sAfterAlpha.value()] != refClass2AbsClass[sAfterBeta.value()]) {
                    Index s0 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterAlpha.value() : sAfterBeta.value();
                    Index s1 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterBeta.value() : sAfterAlpha.value();

                    if (dist[s0][s1].empty()) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processOneTraceInsertions(alpha, beta, dist[s0][s1]);
                }
            }
        }

        // Step 3a.
        // Handle pairs of traces v.u', v.u'.u, where
        // v is from the state cover, (u'.u) is an arbitrary input trace
        // of length 2..(1+numAddStates), and both u', u are non-empty.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        // For 3a, perform only one-trace-insertions
        for (const auto &v : stateCoverTraces) {

            for (size_t uPrimeLen = 1; uPrimeLen < 1 + numAddStates; uPrimeLen++) {

                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               inputAlphaSize,
                                               uPrimeTraces);

                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               inputAlphaSize,
                                               uTraces);

                for (const auto &uPrime : uPrimeTraces) {
                    for (const auto &u : uTraces) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);

                        optional<Index> sPrime = fsm.getTarget(fsm.getInitialStateId(), v_uPrime);
                        optional<Index> s = fsm.getTarget(fsm.getInitialStateId(), v_uPrime_u);

                        assert(sPrime.has_value());
                        assert(s.has_value());
                        if (refClass2AbsClass[sPrime.value()] != refClass2AbsClass[s.value()]) {
                            Index s0 = (sPrime.value() < s.value()) ? sPrime.value() : s.value();
                            Index s1 = (sPrime.value() < s.value()) ? s.value() : sPrime.value();

                            if (dist[s0][s1].empty()) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processOneTraceInsertions(v_uPrime, v_uPrime_u, dist[s0][s1]);
                        }
                    }
                }

            }

        }



        // Step 1b.
        // Add all alpha.gamma, beta.gamma where alpha, beta in V
        // and gamma distinguishes s0-after-alpha, s0-after-beta
        // (if alpha.gamma or beta.gamma are already in testCaseTree, addition
        // will not lead to a new test case)
        for (size_t tr1 = 0; tr1 < stateCoverTraces.size(); tr1++) {
            Trace alpha = stateCoverTraces.at(tr1);
            optional<Index> s0AfterAplhaId = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for (size_t tr2 = tr1 + 1; tr2 < stateCoverTraces.size(); tr2++) {
                Trace beta = stateCoverTraces.at(tr2);

                optional<Index> s0AfterBetaId = fsm.getTarget(fsm.getInitialStateId(), beta);
                // If reached states are different, look for gamma
                if (s0AfterAplhaId.value() != s0AfterBetaId.value()) {
                    // We know that both `s0AfterAplhaId` and `s0AfterBetaId` are always defined.
                    Index s0 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterAplhaId.value() : s0AfterBetaId.value();
                    Index s1 = (s0AfterAplhaId.value() < s0AfterBetaId.value()) ? s0AfterBetaId.value() : s0AfterAplhaId.value();
                    if (dist[s0][s1].empty()) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processDistinguishingTraces(alpha, beta, dist[s0][s1]);
                }
            }
        }

        // Step 2b.
        // Handle pairs of traces alpha, beta with alpha in state cover and
        // beta in stateCover_enumeratedTraces.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        for (const auto &alpha : stateCoverTraces) {

            optional<Index> sAfterAlpha = fsm.getTarget(fsm.getInitialStateId(), alpha);
            for (const auto &beta : stateCover_enumeratedTraces) {

                // Skip if beta in state cover
                if (stateCoverAsTree->getInsertionCost(beta) == 0) {
                    continue;
                }

                optional<Index> sAfterBeta = fsm.getTarget(fsm.getInitialStateId(), beta);
                if (refClass2AbsClass[sAfterAlpha.value()] != refClass2AbsClass[sAfterBeta.value()]) {
                    Index s0 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterAlpha.value() : sAfterBeta.value();
                    Index s1 = (sAfterAlpha.value() < sAfterBeta.value()) ? sAfterBeta.value() : sAfterAlpha.value();

                    if (dist[s0][s1].empty()) {
                        throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                            to_string(s0) + "," + to_string(s1));
                    }
                    processDistinguishingTraces(alpha, beta, dist[s0][s1]);
                }
            }
        }

        // Step 3b.
        // Handle pairs of traces v.u', v.u'.u, where
        // v is from the state cover, (u'.u) is an arbitrary input trace
        // of length 2..(1+numAddStates), and both u', u are non-empty.
        // FOR THE SAFETY-COMPLETE H-METHOD, only pairs of states are considered
        // that are also distinguishable in the abstraction model
        for (const auto &v : stateCoverTraces) {

            for (size_t uPrimeLen = 1; uPrimeLen < 1 + numAddStates; uPrimeLen++) {

                // construct all traces of length uPrimeLen
                Traces uPrimeTraces;
                enumerateInputAlphabetToLength(uPrimeLen,
                                               uPrimeLen,
                                               inputAlphaSize,
                                               uPrimeTraces);

                // construct all extensions of length uPrimeLen+1..(1+numAddStates)
                Traces uTraces;
                enumerateInputAlphabetToLength(1,
                                               1 + numAddStates - uPrimeLen,
                                               inputAlphaSize,
                                               uTraces);

                for (const auto &uPrime : uPrimeTraces) {
                    for (const auto &u : uTraces) {
                        Trace v_uPrime = v;
                        v_uPrime.append(uPrime);
                        Trace v_uPrime_u = v_uPrime;
                        v_uPrime_u.append(u);

                        optional<Index> sPrime = fsm.getTarget(fsm.getInitialStateId(), v_uPrime);
                        optional<Index> s = fsm.getTarget(fsm.getInitialStateId(), v_uPrime_u);

                        assert(sPrime.has_value());
                        assert(s.has_value());
                        if (refClass2AbsClass[sPrime.value()] != refClass2AbsClass[s.value()]) {
                            Index s0 = (sPrime.value() < s.value()) ? sPrime.value() : s.value();
                            Index s1 = (sPrime.value() < s.value()) ? s.value() : sPrime.value();

                            if (dist[s0][s1].empty()) {
                                throw runtime_error("ERROR: unexpectedly, no distinguishing trace for states " +
                                                    to_string(s0) + "," + to_string(s1));
                            }
                            processDistinguishingTraces(v_uPrime, v_uPrime_u, dist[s0][s1]);
                        }
                    }
                }

            }

        }


        // Add missing trace candidates, optimising the size of the test case tree
        addBestCandidateTraces(fsm, inputAlphaSize);

        // Check final version of the test case tree
        if (!testCaseTree->checkTree()) throw runtime_error("Malformed test case tree");

        // Copy test case tree into test suite
        testCaseTreeToTestSuite();

        for (size_t i = 0; i < numberOfStates; ++i)
            delete[] dist[i];
        delete[] dist;
    } else {

        // Need to transform to prime DFSM (i.e. initially connected, minimised)

        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm, "_MIN");

        // Now visit the prime DFSM
        minimisedDfsm->accept(*this);

    }

}

} // namespace libfsmtest
