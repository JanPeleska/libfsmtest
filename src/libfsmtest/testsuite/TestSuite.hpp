/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once
#include <map>
#include <utility>
#include <vector>
#include <memory>
#include <algorithm>
#include <cctype>
#include <locale>


#include "fsm/definitions.hpp"
#include "fsm/Fsm.hpp"
#include "testsuite/Trace.hpp"

namespace libfsmtest {


class TestSuite {
    
private:
    
    // trim from start (in place)
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
        }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(ch);
        }).base(), s.end());
    }

    /** Test suite name */
    std::string name;
    
    /** Vector of input sequences representing the suite's test cases */
    Traces inputTraces;
    
    /** Reference model to be applied when
     *  running the input traces against an SUT
     */
    std::unique_ptr<Fsm> fsm;
        
public:
    
    /**
     * Create a new test suite with specified name.
     */
    TestSuite(const std::string& nameParam,
              std::unique_ptr<Fsm> fsmPtr) : name(nameParam), fsm(move(fsmPtr)) {
        // In case the fsm does not have a well-defined name,
        // use the name of the test suite.
        if ( this->fsm->getName().empty() ) this->fsm->setName(nameParam);
    }
    
    /**
     * Create a test suite from file
     * @param name The name of the test suite. It is expected that
     *             a file 'name.txt' exists which contains all input
     *             traces in external format (presentation layer applied).
     * @param fsmName Name of the reference FSM to be used for checking
     *             the SUT reactions when running the test suite against
     *             the SUT. It is expected that files 'fsmName.fsm',
     *             'fsmName.in', 'fsmName.out', 'fsmName.state', exist for
     *             creating the FSM from these files. When displaying
     *             traces in textual format, the FSM's presentation layer
     *             is used to produce the external representation.
     */
    TestSuite(const std::string& name,
              const std::string& fsmName);
    
    /** Get test suite name */
    std::string getName() const { return name; }
    
    /**
     * Write test suite to several files, as specified in the previous constructor.
     * Formatted this way, the test suite can be "de-serialised" later using the above constructor.
     */
    void writeToFile();
    
    /** Get number of test cases */
    size_t getNumberOfTestCases() const { return inputTraces.size(); }
    
    /** Get number of test steps over all cases */
    size_t getTotalLength() const;
    
    /**
     * Add one input trace to test suite
     */
    void addInputTrace(const Trace& tr) { inputTraces.push_back(tr); }
    
    /**
     * Get raw traces
     */
    Traces getInputTracesRaw() const { return inputTraces; }
    
    /**
     * Set raw traces
     */
    void setInputTracesRaw(Traces inputTracesParam) { this->inputTraces = std::move(inputTracesParam); }
    
    /**
     *  Get input traces as strings
     */
    Strings getInputTraces() const;
    
    /** Get the reference model */
    Fsm& getFsm() { return *fsm; }
    Fsm const& getFsm() const { return *fsm; }
    

    // Auxiliary method, also needed in test harness:
    // trim string (input event) from both ends (in place)
    static inline void trim(std::string &s) {
        ltrim(s);
        rtrim(s);
    }

};

}




