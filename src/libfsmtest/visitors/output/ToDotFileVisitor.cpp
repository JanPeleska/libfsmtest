/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <fstream>
#include <algorithm>

#include "ToDotFileVisitor.hpp"
#include "fsm/Dfsm.hpp"

using namespace std;

namespace libfsmtest {

void ToDotFileVisitor::writeToFile() {
    // This `if` is intended to differentiate between the different constructors for this class.
    // For the string parameter constructor we assume `this->filename` to be set.
    // Otherwise, we assume it to be empty
    if(this->fileName.empty()) {
        (*this->outputStream) << "digraph g { " << std::endl;
        (*this->outputStream) << dotAsString.str() << std::endl << "}" << std::endl;
    } else {
        std::ofstream outStream(fileName);
        if (!outStream.is_open()) throw std::runtime_error("Failed to open output file");
        
        outStream << "digraph g { " << endl;
        
        outStream << dotAsString.str() << endl << "}" << endl;
    }
}



void ToDotFileVisitor::visit(State& state) {
    
    if ( state.getId() != initialState ) {
        // If state name equals its number , do not
        // add ( number )
        if ( state.getName() == to_string(state.getId()) ) {
            dotAsString << state.getId()
            << R"([fontname="helvetica",label=")"
            << state.getName()
            << "\"];" << endl;
        }
        else {
            dotAsString << state.getId()
            << R"([fontname="helvetica",label=")"
            << state.getName()
            << "(" << state.getId() << ")"
            << "\"];" << endl;
        }
    }
    else {
        // Special format for initial state
        if ( state.getName() == to_string(state.getId()) ) {
            dotAsString << state.getId()
            << R"([shape=doublecircle,fontname="helvetica",label=")"
            << state.getName()
            << "\"];" << endl;
        }
        else {
            dotAsString << state.getId()
            << R"([shape=doublecircle,fontname="helvetica",label=")"
            << state.getName()
            << "(" << state.getId() << ")"
            << "\"];" << endl;
        }
    }
    
    for_each(state.getTransitions().begin(),
             state.getTransitions().end(),
             [this](auto& tr) { tr.accept(*this); } );
}

void ToDotFileVisitor::visit(Transition& tr) {
    
    string inStr = presentationLayer->inputAlphabet.at(tr.getInput());
    string outStr = presentationLayer->outputAlphabet.at(tr.getOutput());
    
    if (tr.getSourceId() != tr.getTargetId() ) {
        dotAsString << tr.getSourceId()
        << " -> "
        << tr.getTargetId()
        << R"([fontname="helvetica",label=")"
        << inStr
        << "/"
        << outStr
        << "\"];" << endl;
    }
    else {
        // Leave more space for labels attached to self-loops
        dotAsString << tr.getSourceId()
        << " -> "
        << tr.getTargetId()
        << R"([fontname="helvetica",label="  )"
        << inStr
        << "/"
        << outStr
        << "  \"];" << endl;
    }
    
}

void ToDotFileVisitor::createDotAsString(Fsm& fsm) {
    
    presentationLayer = std::make_unique<PresentationLayer>(fsm.getPresentationLayer());
    
    for_each(fsm.getStates().begin(),
             fsm.getStates().end(),
             [this](auto& state) { state.accept(*this); } );
}

void ToDotFileVisitor::visit(Dfsm& dfsm)  {
    initialState = dfsm.getInitialStateId();
    createDotAsString(dfsm);
}

void ToDotFileVisitor::visit(Ofsm& ofsm)  {
    initialState = ofsm.getInitialStateId();
    createDotAsString(ofsm);
}

void ToDotFileVisitor::visit(Fsm& fsm)  {
    initialState = fsm.getInitialStateId();
    createDotAsString(fsm);
}


} //namespace libfsmtest
