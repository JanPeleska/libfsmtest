/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <memory>
#include <cassert>

#include "fsm/Fsm.hpp"
#include "testsuite/TestSuite.hpp"
#include "visitors/testgeneration/TestGenerationVisitor.hpp"

namespace libfsmtest {

template <typename T>
class TestGenerationFrame  {
    
protected:
    
    std::unique_ptr<TestSuite> testsuite;
    std::unique_ptr<T> generatingVisitor;

    TestGenerationFrame(const std::string& paramTestSuiteName,
                        std::unique_ptr<Fsm> referenceModel,
                        std::unique_ptr<T> generatingVisitorParam) : generatingVisitor(std::move(generatingVisitorParam))
    {
      testsuite = std::make_unique<TestSuite>(paramTestSuiteName, move(referenceModel));
      generatingVisitor->setTestSuite(testsuite.get());
    }

public:

    template <typename... Args>
    TestGenerationFrame(const std::string& paramTestSuiteName,
                     std::unique_ptr<Fsm> referenceModel,
                     Args&&... generatingVisitorArgs) {
        testsuite = std::make_unique<TestSuite>(paramTestSuiteName, move(referenceModel));
        generatingVisitor = std::make_unique<T>(std::forward<Args>(generatingVisitorArgs)...);
        generatingVisitor->setTestSuite(testsuite.get());
    }
    
    virtual ~TestGenerationFrame() = default;
    
    void generateTestSuite() {
        testsuite->getFsm().accept(*generatingVisitor);
    }
    
    TestSuite* getTestSuite() { return testsuite.get(); }
    
    void writeToFile() { testsuite->writeToFile(); }

    template <typename... Args>
    static TestGenerationFrame<T> create(const std::string& paramTestSuiteName,
                                         std::unique_ptr<Fsm> referenceModel,
                                         Args... generatingVisitorArgs)
    {
        return { paramTestSuiteName, std::move(referenceModel), std::make_unique<T>(std::forward<Args>(generatingVisitorArgs)...) };
    }
};

}
