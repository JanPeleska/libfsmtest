#include <string>


// Include header files of library to be tested
// #include "..."



/**
 *   Helper data structures and functions for
 *   SUT test wrapper
 */
 
// ...

using namespace std;


void sut_init() {
    
    // initialise wrapper data structures
    // for mapping FSM inputs to SUT inputs
    // and vice versa
    // ...
    
    // Initialise SUT, if required, by calling
    // initialisation functions or initialising
    // global SUT variables
    // ...
    
}

void sut_reset() {
    // Insert code suitable for resetting SUT into
    // its initial state
    // ...
}


const string sut(const string& input) {
    
    string fsmOutputEvent;
    
    // Transform FSM input event passed as string
    // 'input' to SUT input variable settings and
    // global variables settings
    // ...
    
    // Call the SUT function addressed by the FSM input event
    // with the input parameter values defined before
    // ...
    
    // Convert the return value, the (in-)out-parameter values,
    // and the global SUT variables to the FSM output event
    // represented as string fsmOutputEvent
    // ...
    
    // return output event in string representation
    return fsmOutputEvent;
    
}
