/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <deque>
#include <map>

#include "WMethod.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"

using namespace std;

namespace libfsmtest {

void WMethod::createCharacterisationSet(Dfsm& fsm, Traces& characterisationSet) {
    
    size_t numberOfStates = fsm.size();
    size_t inputAlphaSize = fsm.getInputAlphabetSize();
    
    optional<Index>*** dfsmTable = fsm.getDfsmTable();
    map< size_t, set<Index> > classMap;
    vector< map<Index,size_t> > state2classMaps;
    
    // Calculate Pk tables
    fsm.calcOFSMTables(classMap,state2classMaps);
    
    for ( Index qi = 0; qi < numberOfStates; qi++ ) {
        for ( Index qj = qi + 1; qj < numberOfStates; qj++ ) {
            
            if ( distinguishes(fsm,characterisationSet,qi,qj) ) continue;
            
            size_t ell;
            for ( ell = 1; ell < state2classMaps.size(); ell++ ) {
                if ( state2classMaps.at(ell)[qi] != state2classMaps.at(ell)[qj] ) {
                    break;
                }
            }
            if ( ell == state2classMaps.size() )
                throw runtime_error("ERROR cannot distinguish states "
                                    + to_string(qi) + ", " + to_string(qj));
            size_t k = 1;
            optional<Index> qikMin1 = qi;
            optional<Index> qjkMin1 = qj;
            optional<Index> qik, qjk;
            Trace separatingTrace;
            while ( ell > k ) {
                Index xk;
                for ( xk = 0; xk < inputAlphaSize; xk++ ) {

                    qik = dfsmTable[qikMin1.value()][xk][0];
                    qjk = dfsmTable[qjkMin1.value()][xk][0];
                    
                    if ( qik.has_value() && qjk.has_value() &&
                        state2classMaps.at(ell - k)[qik.value()] != state2classMaps.at(ell - k)[qjk.value()] ) {
                        separatingTrace.append(xk);
                        break;
                    }
                }
                if ( xk == inputAlphaSize )
                    throw runtime_error("ERROR cannot find xk for "
                                        + to_string(qi) + ", " + to_string(qj));
                
                k++;
                qikMin1 = qik;
                qjkMin1 = qjk;
            }
            // Final step for ell == k
            Index xk;
            for ( xk = 0; xk < inputAlphaSize; xk++ ) {
                qik = dfsmTable[qikMin1.value()][xk][0];
                qjk = dfsmTable[qjkMin1.value()][xk][0];
                if ( qik.has_value() && qjk.has_value() &&
                    dfsmTable[qikMin1.value()][xk][1].value() != dfsmTable[qjkMin1.value()][xk][1].value() ) {
                    separatingTrace.append(xk);
                    break;
                }
            }
            if ( xk == inputAlphaSize )
                throw runtime_error("ERROR cannot find xk for "
                                    + to_string(qikMin1.value()) + ", " + to_string(qjkMin1.value()));
            
            characterisationSet.push_back(separatingTrace);
            
        }
    }
    
}

void WMethod::createCharacterisationSet(Ofsm& fsm, Traces& characterisationSet) {

    
    size_t numberOfStates = fsm.size();
    size_t inputAlphaSize = fsm.getInputAlphabetSize();
    size_t outputAlphaSize = fsm.getOutputAlphabetSize();
    
    optional<Index>*** transitionTable = fsm.getTransitionTable();
    map< size_t, set<Index> > classMap;
    vector< map<Index,size_t> > state2classMaps;
    
    // Calculate Pk tables
    fsm.calcOFSMTables(classMap,state2classMaps);
    
    for ( Index qi = 0; qi < numberOfStates; qi++ ) {
        for ( Index qj = qi + 1; qj < numberOfStates; qj++ ) {
            
            if ( distinguishes(fsm,characterisationSet,qi,qj) ) continue;
            
            size_t ell;
            for ( ell = 1; ell < state2classMaps.size(); ell++ ) {
                if ( state2classMaps.at(ell)[qi] != state2classMaps.at(ell)[qj] ) {
                    break;
                }
            }
            if ( ell == state2classMaps.size() )
                throw runtime_error("ERROR cannot distinguish states "
                                    + to_string(qi) + ", " + to_string(qj));
            size_t k = 1;
            optional<Index> qikMin1 = qi;
            optional<Index> qjkMin1 = qj;
            optional<Index> qik, qjk;
            Trace separatingTrace;
            while ( ell > k ) {
                Index xk;
                for ( xk = 0; xk < inputAlphaSize; xk++ ) {
                    for ( Index y = 0; y < outputAlphaSize; y++ ) {
                        
                        qik = transitionTable[qikMin1.value()][xk][y];
                        qjk = transitionTable[qjkMin1.value()][xk][y];
                        
                        if ( transitionTable[qikMin1.value()][xk][y].has_value() && transitionTable[qjkMin1.value()][xk][y].has_value() &&
                            state2classMaps.at(ell - k)[qik.value()] != state2classMaps.at(ell - k)[qjk.value()] ) {
                            separatingTrace.append(xk);
                            goto foundStates;
                        }
                    }
                }
                if ( xk == inputAlphaSize )
                    throw runtime_error("ERROR cannot find xk for "
                                        + to_string(qi) + ", " + to_string(qj));
                
            foundStates:
                k++;
                qikMin1 = qik;
                qjkMin1 = qjk;
            }
            // Final step for ell == k
            Index xk;
            for ( xk = 0; xk < inputAlphaSize; xk++ ) {
                for ( Index y = 0; y < outputAlphaSize; y++ ) {
                    if ( (transitionTable[qikMin1.value()][xk][y].has_value()) != (transitionTable[qjkMin1.value()][xk][y].has_value()) ) {
                        // One state has a defined post-state under xk/y,
                        // the other post state has not.
                        separatingTrace.append(xk);
                        goto sepTraceCompleted;
                    }
                }
            }
            if ( xk == inputAlphaSize )
                throw runtime_error("ERROR cannot find xk for "
                                    + to_string(qikMin1.value()) + ", " + to_string(qjkMin1.value()));
            
        sepTraceCompleted:
            characterisationSet.push_back(separatingTrace);
            
        }
    }
    
    
}


void WMethod::visit(Fsm &fsm) {
    
    // First, a transformation to prime machine is needed in any case
    unique_ptr<Fsm> primeFsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
    
    // @debug output of prime machine
    ToDotFileVisitor dot("prime-of-nonObservable.dot");
    primeFsm->accept(dot);
    dot.writeToFile();
    
    // Now visit the prime machine
    primeFsm->accept(*this);
    
    
}

void WMethod::visit(Ofsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        createStateCover(fsm);
        
        testCaseTree->toDot("StateCover-nonobservable.dot");
        
        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates+1,
                                       testsuite->getFsm().getInputAlphabetSize(),
                                       enumeratedTraces);
        
        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);
        
        // Append charactersation set to testCaseTree
        Traces characterisationSet;
        createCharacterisationSet(fsm,characterisationSet);
        testCaseTree->addToAllNodes(characterisationSet);
        
#if 0
        cout << "Characterisation set" << endl;
        for ( const auto& tr : characterisationSet ) {
            cout << testsuite->getFsm().getPresentationLayer().inputTraceToString(tr) << endl;
        }
#endif
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime OFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
        
        // Debug output
        ToDotFileVisitor dot("Minimised.dot");
        minimisedOfsm->accept(dot);
        dot.writeToFile();
        
        
        // Now visit the prime OFSM
        minimisedOfsm->accept(*this);
        
    }
    
    
}

void WMethod::visit(Dfsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        createStateCover(fsm);
        
        Traces enumeratedTraces;
        enumerateInputAlphabetToLength(1,
                                       numAddStates+1,
                                       testsuite->getFsm().getInputAlphabetSize(),
                                       enumeratedTraces);
        
        // Append enumeratedTraces to testCaseTree
        testCaseTree->addToAllNodes(enumeratedTraces);
        
        // Append charactersation set to testCaseTree
        Traces characterisationSet;
        createCharacterisationSet(fsm,characterisationSet);
        testCaseTree->addToAllNodes(characterisationSet);
        
#if 0
        cout << "Characterisation set:" << endl;
        for ( const auto& tr : characterisationSet ) {
            cout << testsuite->trace2string(tr) << endl;
        }
#endif
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime DFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedDfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
        
        // Debug output
        ToDotFileVisitor dot("Minimised.dot");
        minimisedDfsm->accept(dot);
        dot.writeToFile();
        
        
        // Now visit the prime DFSM
        minimisedDfsm->accept(*this);
        
    }
    
}

} // namespace libfsmtest
