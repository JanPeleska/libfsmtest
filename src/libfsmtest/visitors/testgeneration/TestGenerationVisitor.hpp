/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include "fsm/Dfsm.hpp"
#include "fsm/Ofsm.hpp"
#include "testsuite/TestSuite.hpp"
#include "testsuite/Tree.hpp"
#include "visitors/FsmVisitor.hpp"

namespace libfsmtest {
 

/**
 * Abstract class declaring visitor methods to FSMs.
 */
class TestGenerationVisitor : public FsmVisitor {
    
protected:
    
    TestSuite* testsuite;
    
    /**
     * Tree structure grown incrementally during test case generation.
     * On completion, the full paths from root to any leave represent the test cases.
     */
    std::unique_ptr<Tree> testCaseTree;


    /** Copy testCaseTree into testsuite */
    void testCaseTreeToTestSuite();

    /** 
     * Check whether a given input trace distinguishes the given states.
     * @param fsm Ofsm containing the states.
     * @param trace Input trace.
     * @param qi Index of the first state.
     * @param qj Index of the second state.
     * @return True iff qi and qj are distinguishable by trace.
     */
    static bool distinguishes(Ofsm& fsm, const Trace& trace, Index qi, Index qj);

    /**
     * Creates a state cover initialises testCaseTree with it.
     */
    virtual void createStateCover(Fsm& fsm);
    
    /** 
     * Check whether a given set of input traces distinguishes the given states.
     * @param fsm Dfsm containing the states.
     * @param setOfTraces Input traces.
     * @param qi Index of the first state.
     * @param qj Index of the second state.
     * @return True iff qi and qj are distinguishable by trace.
     */
    static bool distinguishes(Dfsm& fsm, Traces& setOfTraces, Index qi, Index qj);

    /** 
     * Check whether a given set of input traces distinguishes the given states.
     * @param fsm Ofsm containing the states.
     * @param setOfTraces Input trace.
     * @param qi Index of the first state.
     * @param qj Index of the second state.
     * @return True iff qi and qj are distinguishable by trace.
     */
    static bool distinguishes(Ofsm& fsm, Traces& setOfTraces, Index qi, Index qj);

    static void enumerateInputAlphabetToLength(size_t minLength,
                                               size_t maxLength,
                                               Traces& generatedInputTraces);
    
    
    /**
     * Create next trace of same length in enumeration
     * @param alphaSize Size of the input alphabet
     * @param tr Input trace, internally updated to the successor trace
     *           if this exists.
     * @return true iff a successor trace could be found.
     */
    static bool haveNextTrace(size_t alphaSize, Trace& tr);
    
    
public:
    
    /**
     * Enumerate input traces in given length range.
     * @param minLength Minimal trace length required
     * @param maxLength Maximal trace length required
     * @param alphaInSize Size of the input alphabet
     * @param generatedInputTraces Container to be prvoded by the caller.
     *        when calling this operation, the container should be empty.
     *        On return, it contains all traces up to 'length'
     */
    static void enumerateInputAlphabetToLength(size_t minLength,
                                               size_t maxLength,
                                               size_t alphaInSize,
                                               Traces& generatedInputTraces);
    
    TestGenerationVisitor();
    
    ~TestGenerationVisitor() override = default;

    void setTestSuite(TestSuite* ts) {
        testsuite = ts;
    }
};

} // namespace libfsmtest

