/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "creators/FsmCreator.hpp"
#include "creators/transformers/Transformer.hpp"

namespace libfsmtest {

/**
 * The different file formats supported.
 */
typedef enum {
    NOT_SUPPORTED, /*!< The format of the file is not supported */
    CSV, /*!< The CSV file format to specify FSMs (see #FsmFromCsvCreator) */
    RAW, /*!< The raw file format to specify FSMs (see #FsmFromRawCreator) */

    // INTERNAL:
    NOT_RECOGNISED /*!< Internal only: the format wasn't recognised by extension or content*/
} FileFormat_t;

/**
 * Creator that recognises the different file formats supported
 * and chooses the appropriate Creator accordingly.
 */
class FsmFromFileCreator : public FsmCreator {
private:
    /** The file name to load a FSM from */
    std::string fileName;

    /** The file name to load a FSM from without its (maybe present) extension */
    std::string prefix;

    /** The name the FSM should get */
    std::string const fsmName;

    /** The Transformer that gets applied after reading */
    std::unique_ptr<Transformer> transformer;

    /**
     * Tries to recognise the format of a given file.
     * @see recogniseByExtension()
     * @see recogniseByContent()
     * @return The corresponding #FileFormat_t
     *         or FileFormat_t#NOT_SUPPORTED
     */
    FileFormat_t recogniseFormat();

    /**
     * Tries to recognise the format of a file based on the file extension.
     * @return The corresponding #FileFormat_t
     *         or FileFormat_t#NOT_RECOGNISED
     */
    FileFormat_t recogniseByExtension();

    /**
     * Tries to recognise the format of a file based on the content
     * (of the first line).
     * @see recogniseCSV(std::string)
     * @see recogniseRaw(std::string)
     * @return The corresponding #FileFormat_t
     *         or FileFormat_t#NOT_RECOGNISED
     */
    FileFormat_t recogniseByContent();

    /**
     * Checks whether a given line corresponds to the
     * first line of the CSV FSM file format.
     * @param firstLine The first line of a file
     * @return true, if the line matches the format
     *         false, otherwise
     */
    static bool recogniseCSV(const std::string& firstLine);
    /**
     * Checks whether a given line corresponds to the
     * first line of the raw FSM file format.
     * @param firstLine The first line of a file
     * @return true, if the line matches the format
     *         false, otherwise
     */
    static bool recogniseRAW(const std::string& firstLine);

    /**
     * Checks whether for a given prefix (filename without extension)
     * there are files prefix{.state, .out, .in}.
     * @param filename The prefix to check with the three extensions
     * @return true, if all three files exists,
     *         false, otherwise.
     */
    static bool checkForPresentationLayer(const std::string& filename);

public:
    /**
     * Constructor.
     * Won't start creating a Fsm.
     * @param fileName The file name to create a Fsm from.
     */
    explicit FsmFromFileCreator(std::string const& fileName, std::string  fsmName = "", std::unique_ptr<Transformer> transformer = nullptr);

    std::unique_ptr<Fsm> createFsm() override;
};

} // namespace libfsmtest

