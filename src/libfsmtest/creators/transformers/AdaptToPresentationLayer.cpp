
/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdexcept>

#include "fsm/Ofsm.hpp"
#include "fsm/Dfsm.hpp"

#include "AdaptToPresentationLayer.hpp"

using namespace std;

namespace libfsmtest {


bool AdaptToPresentationLayer::arePresentationLayersAdaptable(const PresentationLayer &lhs, const PresentationLayer &rhs) {

    // check input alphabet
    if (lhs.inputAlphabet.size() > rhs.inputAlphabet.size()) return false;
    for (const auto& name : lhs.inputAlphabet) {
        if (find(rhs.inputAlphabet.begin(), rhs.inputAlphabet.end(), name) == rhs.inputAlphabet.end())
            return false;
    }

    // check output alphabet
    if (lhs.outputAlphabet.size() > rhs.outputAlphabet.size()) return false;
    for (const auto& name : lhs.outputAlphabet) {
        if (find(rhs.outputAlphabet.begin(), rhs.outputAlphabet.end(), name) == rhs.outputAlphabet.end())
            return false;
    }
    return true;
}

void AdaptToPresentationLayer::visit(Fsm &fsmToTransform) {
    this->transformedFsm = fsmToTransform.clone();

    if (not this->arePresentationLayersAdaptable(this->transformedFsm->getPresentationLayer(), other)) {
        throw std::runtime_error("AdaptToPresentationLayer: Presentation layers are not adaptable.");
    }

    // Copy the other presentation layer to adapt to, but keep the states
    PresentationLayer adaptedPL(other.inputAlphabet, other.outputAlphabet, this->transformedFsm->getPresentationLayer().stateNames);

    // Visit all states to update transitions.
    for_each(this->transformedFsm->getStates().begin(), this->transformedFsm->getStates().end(), [this](auto& s) {s.accept(*this);});

    // Create a new FSM with the same type as the old one.
    this->transformedFsm = this->transformedFsm->modify(this->transformedFsm->getName() + nameSuffix, nullopt, adaptedPL, nullopt);
}

void AdaptToPresentationLayer::visit(Ofsm &ofsm) {
    visit(static_cast<Fsm&>(ofsm));
}

void AdaptToPresentationLayer::visit(Dfsm &dfsm) {
    visit(static_cast<Fsm&>(dfsm));
}

void AdaptToPresentationLayer::visit(State &state) {
    // The transformedFsm to this point still has the old presentation layer
    // transfer the id of this (old) state to the other presentation layer
    // Create a new transition for each old transition
    for_each(state.getTransitions().begin(), state.getTransitions().end(), [&](auto& t) {
        Index input, output, target;
        // Get the new id for the old input
        input = other.getInputId(this->transformedFsm->getPresentationLayer().inputAlphabet.at(t.getInput())).value();
        // Get the new id for the old output
        output = other.getOutputId(this->transformedFsm->getPresentationLayer().outputAlphabet.at(t.getOutput())).value();
        // Get the new id for the old target
        target = this->transformedFsm->getPresentationLayer().getStateId(this->transformedFsm->getPresentationLayer().stateNames.at(t.getTargetId())).value();
        // Create a new transition with those transferred ids, replacing the old one.
        t = Transition(state.getId(), input, output, target);
    });
}

} // namespace libfsmtest
