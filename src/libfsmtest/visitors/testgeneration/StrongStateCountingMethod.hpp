/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <unordered_map>
#include <unordered_set>
#include <optional>

#include "testsuite/Trace.hpp"
#include "testsuite/Tree.hpp"
#include "visitors/FsmVisitor.hpp"
#include "visitors/TestGenerationFrame.hpp"

namespace libfsmtest {


struct IntPairHash {
    std::size_t operator()(const std::pair<Index,Index>& k) const
    {
        return std::hash<Index>()(k.first) ^
                (std::hash<Index>()(k.second) << 1);
    }
};
 
struct IntPairEqual {
    bool operator()(const std::pair<Index,Index>& lhs, const std::pair<Index,Index>& rhs) const
    {
        return lhs.first == rhs.first && lhs.second == rhs.second;
    }
};

struct IntSetHash {
    std::size_t operator()(const std::unordered_set<Index>& set) const
    {
        size_t hash = 0;
        for (auto elem : set) {
            hash+= std::hash<Index>{}(elem);
        }
        return hash;
    }
};


/**
 * A method for testing strong reduction based on state counting.
 * This can be applied to observable FSMs only, as the
 * transformation from non-observable FSMs to observable FSMs
 * does not in general preserve the strong reductionrelation.
 */
class StrongStateCountingMethod : public TestGenerationVisitor {

protected:

    /**
     * The maximal number of additional states,
     * which the implementation FSM in minimised
     * form may have, when compared to the reference
     * model in minimised form.
     */
    const size_t numAddStates;
    
    /** 
     * If true, then during test suite generation all
     * maximal sets of pairwise r-distinguishable states
     * is computed.
     * Otherwise, a greedy algorithm is employed to
     * compute for each state a set of pairwise 
     * r-distinguishable states containing it.
     */
    const bool calculateAllMaximalRDistinguishableSets;

    /** 
     * maps each pair (qi,qj) of r-distinguishable states to an input x 
     * and the set {(qi',qj') | exists y . qi--x/y-->qi', qj--x/y-->qj'}
     * such that these (qi',qj') pairs are r-distinguishable themselves.
     */
    std::unordered_map<std::pair<Index,Index>, std::pair<std::optional<Index>,std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>>, IntPairHash, IntPairEqual> rDistGraph;
    
    /// maps (qi,qj) to an r-distinguishing tree of qi and qj
    std::unordered_map<std::pair<Index,Index>, std::shared_ptr<Tree>, IntPairHash, IntPairEqual> rDistTrees;
    
    /// maps qi to a trace d-reaching qi
    std::unordered_map<Index, Trace> dReachingSequences;

    /// maximal sets of pairwise r-distinguishable states
    std::unordered_set<std::unordered_set<Index>, IntSetHash> maximalRDistSets;


    /**
     * Compute the state reached from a given source state by a given io trace in
     * the language of the state.
     * 
     * @param fsm The Ofsm containing the source state.
     * @param sourceId The Id of the source state.
     * @param trace The io trace.
     *              Input and output trace of this io trace
     *              must be of the same length.
     *              The trace must be contained in the language
     *              of the state.
     * @return Id of the state reached from sourceId via trace.
     */
    Index getTargetOfIOTrace(Ofsm& fsm, Index sourceId, IOTrace& trace) const;


    /**
 	 * Compute a tree containing all sequences t such that t1.t and t2.t
     * are contained in parameter tree.
	 */ 
	Tree sharedExtensions(Tree& tree, const Trace& t1, const Trace& t2) const;


    /**
     * Calculates a mapping from pairs of states to inputs and target sets that 
     * represents the graph such that any contained pair of states (s1,s2) that
     * maps to (x, tgts) where x is not std::nullopt can be r-distinguished by 
     * applying the value of x and thereafter recursively continuing this process
     * with the state-pairs in tgts.
     * 
     * If x is std::nullopt, then the states are already r(0)-distinguishably 
     * (they differ in their defined inputs) and no input needs to be applied.
     */ 
    void calcRDistinguishingGraph(Ofsm& fsm);

    

    /**
     * Creates a mapping that maps each pair of r-distinguishable states to a
     * set of input sequences that r-distinguishes them.
     */ 
    void calcRDistinguishingTrees(Ofsm& fsm);

    

    /**
     * Calculates a vector of states and input sequences (q,xs) is contained
     * in the result only if q is a state of this FSM and is deterministically
     * reachable (for possibly partial FSMs) via xs.
     * Assumes that "this" is observable.
     */
    void calcDeterministicallyReachingSequences(Ofsm& fsm);

    
    /**
     * Calculates a set of sets of states of fsm that are pairwise r-distinguishable
     * such that every state of fsm is contained in at least one of the resulting sets.
     * 
     * NOTE: does not solve the maximal clique problem and hence may not return all
     *       maximal sets of states of fsm that are r-distinguishable.
     */
    void calcMaximalRDistinguishableSets(Ofsm& fsm);

    /**
     * Calculates the set of all maximal sets of states of fsm that are pairwise r-distinguishable.
     * 
     * NOTE: performs a brute force calculation that may not be feasible for large or dense FSMs
     */
    void calcAllMaximalRDistinguishableSets(Ofsm& fsm);


    /**
     * Calculate all tuples (S,rep) where S is a maximal pairwise r-distinguishable set of states of fsm,
     * and rep = m - dr + 1, where dr is the number of d-reachable states in S.
     */
    std::vector<std::pair<std::shared_ptr<std::unordered_set<Index>>,ssize_t>> getTerminationTuples(size_t m);

    /**
     * Generate the traversal set T(qi,m).
     * 
     * By construction, all IO-traces in this set are in the language of parameter qi.
     * By construction, the vector of sets in each element in the returned set is not empty.
     */
    std::vector<std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<Index>>>>> calcTraversalSet(Ofsm& fsm, Index qi, size_t m);

    /**
     * Create a set W of input sequences that r-distinguishes the given states.
     * If the inputs currently applied after both states are already r-distinguishing, then the returned tree is empty.
     */
    std::shared_ptr<Tree> augmentToRDistSet(Ofsm& fsm, Index n1, Index n2, const TreeNode* currentlyAppliedSequences);

    /**
     * Compute the initial test suite by extending for each d-reachable state s of fsm its d-reaching sequence with all inputs in Tr(s,m).
     */ 
    void initialiseTestSuiteTree(Ofsm& fsm, size_t m);

    /**
     * Updates the test suite for a given terminated pair.
     * 
     * Assumes that the currentTestSuite already contains the result of initialiseTestSuite(fsm,m).
     * Assumes that the IO-trace in nextElementOfD is in the language of parameter node.
     * Assumes that the vector of sets in nextElementOfD is not empty.
     */
    void updateTestSuite(Ofsm& fsm, Index node, const std::pair<IOTrace, std::vector<std::shared_ptr<std::unordered_set<size_t>>>>& nextElementOfD, Tree& currentTestSuite);

    /**
     * Generate an m-complete test suite for fsm.
     */ 
    void generateTestSuite(Ofsm& fsm, size_t m);

    /**
     * Return true iff the state is r-distinguishuable (w.r.t. strong reduction)
     * from otherState by tree w.
     */
    bool isRDistinguishedBy(Ofsm& fsm, Index q1, Index q2, const TreeNode* w) const;


public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation FSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     * @param calculateAllMaximalRDistinguishableSetsParam If 'true', then all maximal sets of pairwise
     *                                                     r-distinguishable states of the FSM are computed.
     *                                                     Otherwise for each state of the FSM only a single
     *                                                     such set is computed.
     */
    explicit StrongStateCountingMethod(size_t numAddStatesParam, bool calculateAllMaximalRDistinguishableSetsParam = false) :
        TestGenerationVisitor(),
        numAddStates(numAddStatesParam),
        calculateAllMaximalRDistinguishableSets(calculateAllMaximalRDistinguishableSetsParam)
    {

    }


    /**
     * Getter for the r-distinguishing trees computed for the fsm.
     */
    std::unordered_map<std::pair<Index,Index>, std::shared_ptr<Tree>, IntPairHash, IntPairEqual> getRDistinguishingTrees() const;

    /**
     * Getter for the d-reaching sequences chosen for the fsm.
     */
    std::unordered_map<Index, Trace> getDeterministicallyReachingSequences() const;

    /**
     * Getter for the rdistinguishing graphs computed for the fsm.
     */
    std::unordered_map<std::pair<Index,Index>, std::pair<std::optional<Index>,std::shared_ptr<std::unordered_set<std::pair<Index,Index>, IntPairHash, IntPairEqual>>>, IntPairHash, IntPairEqual> getRDistGraph() const;

    /**
     * Getter for the maximal pairwise r-distinguishable sets chosen for the fsm.
     */
    std::unordered_set<std::unordered_set<Index>, IntSetHash> getMaximalRDistinguishableSets();


    /**
     * Method implementing the actual State Counting method for test generation on a FSM.
     * Only called indirectly, see Fsm::accept()
     * @param fsm The FSM to generate the test suite for.
     *            If this fsm is not observable, then a runtime error is thrown,
     *            as strong reduction is defined only for observable fsms and
     *            classical approaches for transforming an fsm into an observable
     *            fsm do not in general preserve the strong reduction conformance relation.
     */
    void visit(Fsm & fsm) override;

    /**
     * Method implementing the actual State Counting method for test generation on a OFSM.
     * Only called indirectly, see Ofsm::accept()
     * @param fsm The OFSM to generate the test suite for.
     */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual State Counting method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     */
    void visit(Dfsm & dfsm) override;
    
};

} // namespace libfsmtest
