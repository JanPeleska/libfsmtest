/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>

#include "ClassicalStateCountingMethod.hpp"
#include "creators/transformers/ToPrimeTransformer.hpp"

using namespace std;

namespace libfsmtest { 


void ClassicalStateCountingMethod::visit(Fsm &fsm) {

    // classical state counting may not be applied to partial FSMs,
    // as in this case the (r-)distinguishability of states may involve
    // applying undefined inputs, which may not be desired and may not
    // be applicable in testing
    if (!fsm.isCompletelySpecified())
        throw runtime_error("Classical state counting cannot be applied to partial FSMs.");
    
    
    // First, a transformation to prime machine is needed in any case
    unique_ptr<Fsm> primeFsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
    
    // Now visit the prime machine
    primeFsm->accept(*this);    
}



void ClassicalStateCountingMethod::visit(Ofsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        size_t m = fsm.getStates().size() + numAddStates;
        generateTestSuite(fsm, m);
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime OFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
                
        // Now visit the prime OFSM
        minimisedOfsm->accept(*this);        
    }    
}

void ClassicalStateCountingMethod::visit(Dfsm &fsm) {
    
    if ( fsm.isMinimal() ) {
        
        size_t m = fsm.getStates().size() + numAddStates;
        generateTestSuite(fsm, m);
        
        // Check final version of the test case tree and output to dot format
        if ( ! testCaseTree->checkTree() ) throw runtime_error("Malformed test case tree");
        testCaseTree->toDot("TestCaseTree.dot");
        
        // Copy test case tree into test suite
        testCaseTreeToTestSuite();
        
    }
    else {
        
        // Need to transform to prime DFSM (i.e. initially connected, minimised)
        
        unique_ptr<Fsm> minimisedOfsm = transformFsm<ToPrimeTransformer>(&fsm,"_MIN");
        
                
        // Now visit the prime DFSM
        minimisedOfsm->accept(*this);        
    }    
}


} // namespace libfsmtest
