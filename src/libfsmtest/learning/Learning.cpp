#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <optional>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cassert>

#include "testsuite/TreeNode.hpp"
#include "testsuite/Tree.hpp"
#include "visitors/output/ToDotFileVisitor.hpp"

#include "Learning.hpp"

#define LEARNING_LOG_H_METHOD_PROGRESS true
#define LEARNING_LOG_DETAILED_H_METHOD_PROGRESS false
#define LEARNING_LOG_DETAILED_ITERATIONS true
#define LEARNING_LOG_HYPOTHESES false
#define LEARNING_ITERATIVE_H_METHOD true

namespace libfsmtest {

Learning::Learning(size_t numInputs, size_t numOutputs, size_t maxSutSize, size_t (*applyInputExternal) (size_t,std::optional<size_t>), void (*resetExternalDependenciesForNewTrace) (), std::optional<Trace> (*checkHypothesisExternal) (Dfsm&)) :
numInputs(numInputs),
numOutputs(numOutputs),
maxSutSize(maxSutSize),
obsTree(ObservationTree(numInputs)),
applyInputExternal(applyInputExternal),
resetExternalDependenciesForNewTrace(resetExternalDependenciesForNewTrace),
checkHypothesisExternal(checkHypothesisExternal)
{
    creationTime = std::chrono::steady_clock::now();
    lastTimePoint = creationTime;
}

std::chrono::milliseconds Learning::measureTime() {
    auto now = std::chrono::steady_clock::now();
    auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTimePoint);
    lastTimePoint = now;
    return interval;
}

std::string Learning::showMeasuredTime() {
    std::stringstream stream;
    stream << std::fixed << std::setprecision(3) << measureTime().count() / 1000.0 << "s";
    return stream.str();
}


size_t Learning::getMinimalNumberOfPairwiseDistinctStatesInSut() {
    return obsTree.getMinimalNumberOfPairwiseDistinctStates();
}

size_t Learning::applyInput(size_t input) {

    // check if it is necessary to compute the observed IO class
    auto prevOutput = obsTree.applyInputIfPreviouslyObserved(input);

    auto output = applyInputExternal(input, prevOutput);
    if (not prevOutput.has_value()) {
        // if no previous output has been observed, update the internal observation tree with the newly observed output
        addObservation(input,output);  
    }
    return output; 
}

void Learning::extendBasis(TreeNode* q) {
    basis.insert(q);
    frontier.erase(q);
    mappingCandidates.erase(q);

    // update apartness of q from states in the frontier    
    auto qID = q->getNodeId();    
    for (auto q2 : frontier) {
        if (not obsTree.areApart(qID,q2->getNodeId())) {
            mappingCandidates[q2].insert(q);
        }
    }

    std::unordered_set<size_t> missingInputs;
    for (size_t x = 0; x < numInputs; ++x) {
        auto qX = q->after(x);
        if (qX == nullptr) {
            missingInputs.insert(x);            
        } else {
            // as the basis is prefix-closed, the children of q cannot already be contained in the basis
            // thus, the separation of basis and frontier is maintained
            extendFrontier(qX);
        }
    }
    if (not missingInputs.empty()) {
        undefinedBasisInputs[q] = missingInputs;
        incompleteBasis.insert(q);
    }
}

void Learning::extendFrontier(TreeNode* q) {
    frontier.insert(q);
    access[q] = q->getPath();
    mappingCandidates[q] = std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>();
    auto qID = q->getNodeId();  
    for (auto q2 : basis) {
        if (not obsTree.areApart(qID,q2->getNodeId())) {
            mappingCandidates[q].insert(q2);
        }
    }
}

void Learning::prepareForNewTrace() {
    // reset internal state
    obsTree.beginNewObservationTrace();
}

void Learning::resetAndPrepareForNewTrace() {
    
    prepareForNewTrace();

    // reset external state
    resetExternalDependenciesForNewTrace();
}

/** Add an IO pair to the tree from the current node*/
void Learning::addObservation(Index input, Index output) {
    obsTree.addObservation(input,output);
}

std::optional<size_t> Learning::advanceObservationByPreviouslyObservedInput(size_t input) {
    return obsTree.applyInputIfPreviouslyObserved(input);
}

Trace Learning::outputQuery(TreeNode *q, Trace &trace, bool checkIfAlreadyInTree, bool trackOutputQuery) {

    ++requestedOutputQueries;

    if (checkIfAlreadyInTree) {
        Trace treeResponse;
        TreeNode* treeResponseNode = q;
        for (auto x : trace) {
            treeResponseNode = treeResponseNode->after(x);
            if (treeResponseNode == nullptr) break;
            treeResponse.append(treeResponseNode->getValue().value());
        } 
        if (treeResponse.size() == trace.size()) {

            // if the trace has previously been fully applied, return the previously observed response
            return treeResponse;            
        }
    }

    ++performedOutputQueries;

    // reset 
    resetAndPrepareForNewTrace();

    Trace observedOutputs;

    // run the access trace -- this cannot introduce new data
    // TODO: store access information for all nodes?
    for (auto x : q->getPath()) {
        observedOutputs.append(applyInput(x));
    }

    // run the added trace -- this may update data
    TreeNode* currentNode = q;
    for (auto x : trace) {

        // check whether the current node is in the base and x is a new output for it
        if (incompleteBasis.count(currentNode) == 1
            && undefinedBasisInputs[currentNode].count(x) == 1 ) {                        

            undefinedBasisInputs[currentNode].erase(x);
            if (undefinedBasisInputs[currentNode].empty()) {
                incompleteBasis.erase(currentNode);
            }
        }

        observedOutputs.append(applyInput(x));
        currentNode = currentNode->after(x);
    }

    if ( trackOutputQuery ) {
        // The Complete trace is q->getPath() + trace
        Trace completeInputTrace;

        // State Cover
        for (auto &t: q->getPath()) {
            completeInputTrace.append(t);
        }

        for (auto &t: trace) {
            completeInputTrace.append(t);
        }

        // Add to global vector
        equivalenceCheckInputStimuli.push_back(completeInputTrace);

    #if LEARNING_LOG_DETAILED_H_METHOD_PROGRESS
        std::cout << "[INFO] Complete Input Trace:";
        for (auto &t: completeInputTrace) {
            std::cout << " " << t;
        }
        std::cout << std::endl;
    #endif
    }



    // go through all frontier states whose apartness has been updated and check
    // whether any mapping canditates are no longer viable for them
    for (auto changedState : *obsTree.getNewlyApartNodes()) {

        // ignore non-frontier states
        if (frontier.count(changedState) == 0) continue;

        auto changedStateID = changedState->getNodeId();  
        std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual> removedCandidates;
        for (auto qC : mappingCandidates[changedState]) {
            if (obsTree.areApart(changedStateID,qC->getNodeId())) {
                removedCandidates.insert(qC);
            }
        }
        // remove those candidates that are newly apart 
        for (auto qC : removedCandidates) {
            mappingCandidates[changedState].erase(qC);
        }
    }

    return observedOutputs;
}

// map each frontier state to its first available mapping candidate 
Dfsm Learning::buildHypothesis() {
    
    // reset information from previous hypotheses
    state2hypothesisID.clear();
    hypothesisID2State.clear();
    size_t nextFsmStateId = 0;
    for (auto q : basis) {
        state2hypothesisID[q] = nextFsmStateId;
        hypothesisID2State[nextFsmStateId] = q;
        ++nextFsmStateId;
    }

    // create states from the basis 
    // TODO: as the basis only grows, this could be stored
    std::vector<State> states;
    for (auto q : basis) {
        auto qID = q->getNodeId();
        auto sourceID = state2hypothesisID[q];
        State qState(std::to_string(qID), sourceID);
        for (size_t x = 0; x < numInputs; ++x) {

            // x must be defined, as the basis is complete if this method is called
            auto qX = q->after(x);
            auto y = qX->getValue().value();

            auto target = qX;
            if (frontier.count(qX) == 1) {
                // if qX is in the frontier, redirect the transition to the first candidate for qX
                // this must exist, as frontier states without candidates are moved into the basis
                // before calling this method
                target = *mappingCandidates[qX].begin();
            }
            auto targetID = state2hypothesisID[target];

            Transition t(sourceID,x,y,targetID);
            qState.getTransitions().push_back(t);
        }
        states.push_back(qState);
    }

    // create a presentation layer
    std::vector<std::string> inNames;
    std::vector<std::string> outNames;
    std::vector<std::string> stateNames;
    for (auto i = 0; i < nextFsmStateId; ++i) {
        stateNames.push_back(states[i].getName());
    }
    for (auto x = 0; x < numInputs; ++x) {
        inNames.push_back(std::to_string(x));
    }
    for (auto y = 0; y < numOutputs; ++y) {
        outNames.push_back(std::to_string(y));
    }
    PresentationLayer pl(inNames, outNames, stateNames);

    auto initialState = state2hypothesisID[obsTree.getTree()->getRoot()];
    Dfsm hypothesis("h" + std::to_string(++nextHypothesisID), states, pl, initialState);
    return hypothesis;
}

TreeNode* Learning::checkConsistency(Dfsm& hypothesis) {
    std::queue<Index> hypQueue;
    std::queue<TreeNode*> obsQueue;

    auto hypTable = hypothesis.getDfsmTable();

    // first pair contains both initial states
    TreeNode* root = obsTree.getTree()->getRoot();
    obsQueue.push(root);
    hypQueue.push(hypothesis.getInitialStateId());

    while (not hypQueue.empty()) {
        auto hypID = hypQueue.front();
        hypQueue.pop();
        auto obsState = obsQueue.front();
        obsQueue.pop();

        auto hypState = hypothesisID2State[hypID];
        
        // if apart states are reached, return the state reached in the tree
        if (obsTree.areApart(hypState->getNodeId(),obsState->getNodeId())) {
            return obsState;
        }

        // otherwise go through all successor states for all defined inputs
        for (size_t x = 0; x < numInputs; ++x) {
            auto obsX = obsState->after(x);
            if (obsX == nullptr) continue; // skip inputs undefined in the tree

            // as the hypothesis is complete, it must have a successor for x
            auto hypX = hypTable[hypID][x][0].value();
            
            hypQueue.push(hypX);
            obsQueue.push(obsX);
        }
    }

    // if no inconsistency is found, return null
    return nullptr;
}

void Learning::procCounterEx(Dfsm& hypothesis, Trace& sigma) {
    
    auto r = obsTree.getTree()->getRoot()->after(sigma);
    
    // terminate if r has been pushed into the basis or frontier
    if (basis.count(r) == 1) return;
    if (frontier.count(r) == 1) return;
        
    // TODO: is q ever used?
    auto q = hypothesis.getTarget(hypothesis.getInitialStateId(), sigma).value();
    
    // get the prefix of sigma reaching the frontier
    auto rhoState = obsTree.getTree()->getRoot();
    auto rhoLen = 0;
    for (auto x : sigma) {
        ++rhoLen; // note that at least one input is necessary, as the empty trace reaches the basis only
        rhoState = rhoState->after(x);
        if (frontier.count(rhoState) == 1) break;
    }
    
    // split sigma
    size_t h = (rhoLen + sigma.size()) / 2;
    Trace sigma1;
    Trace sigma2;
    size_t i = 0;
    for (auto x : sigma) {
        if (i++ < h) {
            sigma1.append(x);
        } else {
            sigma2.append(x);
        }
    }

    auto qPrimeID = hypothesis.getTarget(hypothesis.getInitialStateId(), sigma1).value();
    auto qPrime = hypothesisID2State[qPrimeID];
    auto rPrime = obsTree.getTree()->getRoot()->after(sigma1);
    auto eta = obsTree.getApartnessWitness(hypothesisID2State[q]->getNodeId(),r->getNodeId());

    Trace t(sigma2);
    t.append(eta);
    outputQuery(qPrime, t, false, false);

    if (obsTree.areApart(qPrime->getNodeId(),rPrime->getNodeId())) {
        procCounterEx(hypothesis, sigma1);
    } else {
        Trace nxt(qPrime->getPath());
        nxt.append(sigma2); 
        procCounterEx(hypothesis, nxt);
    }
}


std::optional<Trace> Learning::getPrefixToConflict(Dfsm& fsm, Index s0, TreeNode* q0, Trace trace) {

    auto dfsmTable = fsm.getDfsmTable();
    Index s = s0;
    TreeNode* q = q0;

    Trace sigma;
    for (auto x : trace) {
        sigma.append(x);

        q = q->after(x);        
        s = dfsmTable[s][x][0].value();        

        if (obsTree.areApart(q->getNodeId(),hypothesisID2State[s]->getNodeId())) {
            return std::make_optional(sigma);
        }
    }
    return std::nullopt;
}

std::optional<Trace> Learning::getPrefixToConflict(Dfsm& fsm, Trace trace) {
    return getPrefixToConflict(fsm, fsm.getInitialStateId(), obsTree.getTree()->getRoot(), trace);
}


std::optional<Trace> Learning::distinguishFromSet(Dfsm& fsm, std::pair<TreeNode*,Index> p, std::vector<std::pair<TreeNode*,Index>>& toDist, size_t curDepth, size_t skipDepth, size_t maxDepth) {
    
    #if LEARNING_LOG_DETAILED_H_METHOD_PROGRESS
    auto now = std::chrono::steady_clock::now();
    auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(now - creationTime);
    std::cout << "----" << std::fixed << std::setprecision(3) << interval.count() / 1000.0 << "s : ";

    for (size_t i = 0; i < curDepth; ++i) std::cout << "    ";
    std::cout << "distinguishFromSet( ";
    for (auto x : p.first->getPath()) std::cout << x << ".";
    std::cout << " ,     " << curDepth << " | " << skipDepth << " | " << maxDepth << std::endl;
    #endif 

    TreeNode* n0 = p.first;
    Index s0 = p.second;
    std::optional<Index>*** dfsmTable = fsm.getDfsmTable();

    // distinguish s from all elements of toDist
    // Note: this is safe to do even if skipDepth = 0, as the states in the basis, which 
    //       form the initial elements in toDist and which contain the initial p, are 
    //       pairwise separated already
    if (curDepth > skipDepth) {
        for (auto& distPair : toDist) {
            
            TreeNode* n1 = distPair.first;
            Index s1 = distPair.second;

            // n0 and n1 converge, they need not be separated
            if (s0 == s1) continue; 

            // otherwise, separate them by the precomputed distinguishing trace
            TreeNode* q0 = hypothesisID2State[s0];
            TreeNode* q1 = hypothesisID2State[s1];
            Trace bestTrace = obsTree.getApartnessWitness(q0->getNodeId(),q1->getNodeId());

            // apply the trace to both traces in the observation tree
            Trace outSut0 = outputQuery(n0, bestTrace, true, true);
            Trace outSut1 = outputQuery(n1, bestTrace, true, true);

            // compare the observed responses agains the hypothesis
            Trace t0(n0->getPath());
            t0.append(bestTrace);
            Trace t1(n1->getPath());
            t1.append(bestTrace);

            auto res = getPrefixToConflict(fsm,t0);
            if (res.has_value()) {
                return res;
            }
            res = getPrefixToConflict(fsm,t1);
            if (res.has_value()) {
                return res;
            }
        }
    }

    // continue only if further inputs are to be applied
    if (curDepth == maxDepth) return std::nullopt;

    // perform a recursive step for each input, adding p to toDist
    toDist.push_back(p);
    for (Index x = 0; x < fsm.getInputAlphabetSize(); ++x) {
        TreeNode* nx = n0->after(x);
        if (nx == nullptr) {
            // if the input is yet undefined, apply it before proceeding
            Trace t;
            t.append(x);
            outputQuery(n0, t, false, true);
            nx = n0->after(x);

            // check whether this new input already introduces an inconsistency
            // note: this step is essential if the hypothesis is a singleton, as 
            //       in this case it never requires separation and hence cannot
            //       observe inconsistencies that way 
            if (obsTree.areApart(n0->getNodeId(),hypothesisID2State[s0]->getNodeId())) {
                // if apartness is introduced, it is reached by the access sequence of n0
                return std::make_optional(n0->getPath());
            }
        }

        Index sx = dfsmTable[s0][x][0].value();
        std::pair<TreeNode*,Index> px = std::make_pair(nx,sx);
        auto res = distinguishFromSet(fsm, px, toDist, curDepth+1, skipDepth, maxDepth);
        if (res.has_value()) return res;
    }
    // remove p
    toDist.pop_back();

    return std::nullopt;
}

std::vector<Trace> Learning::getAccessSequencesForBasis() const {
    std::vector<Trace> result;
    for(auto const &basisNode : this->basis) {
        result.push_back(basisNode->getPath());
    }
    return result;
}

std::optional<Trace> Learning::equivQuery(Dfsm& fsm) {
    // employ an online version of the H-Method

    size_t numberOfStates = fsm.size();
    if(maxSutSize < numberOfStates) {
        throw std::runtime_error("Found more states then expected during learning.");
    }
    size_t numberOfAdditionalStates = maxSutSize - numberOfStates;

    // as state cover, we can use the access sequences of the states in the basis

    // we sort these by length of their access sequence to explore the vicinity of the initial state first
    std::vector<TreeNode*> sortedBasis;
    for (auto q : basis) {
        sortedBasis.push_back(q);
    }
    std::sort(sortedBasis.begin(), sortedBasis.end(), [this](TreeNode* q0, TreeNode* q1)
        {
            return this->access[q0].size() < this->access[q1].size();
        });

    std::vector<std::pair<TreeNode*,Index>> v;
    for (auto q : sortedBasis) {
        v.push_back(std::make_pair(q,state2hypothesisID[q]));
    }

    // compared to the offline version, we do not first insert all extensions of length (m-n+1),
    // as we try to avoid handling all of these for large values of (m-n)


    #if LEARNING_LOG_H_METHOD_PROGRESS
    size_t progressCounter = 0;
    #endif 

    #if LEARNING_ITERATIVE_H_METHOD

    // we iteratively consider extensions of length k+1, k+2, ... m-n+1 
    for (size_t k = 0; k <= numberOfAdditionalStates; ++k) {

        #if LEARNING_LOG_H_METHOD_PROGRESS
        progressCounter = 0;
        #endif 

        // and each state in the state cover
        for (auto &p: v) {
            std::vector<std::pair<TreeNode*, Index>> x(v);
            // TODO: avoid repeated computation of extensions
            auto res = distinguishFromSet(fsm, p, x, 0, k, k+1);

            #if LEARNING_LOG_H_METHOD_PROGRESS
            std::cout << ":::::::: H-Method progress:  current extension length = " << k+1 
                      << "; max extension length = " << numberOfAdditionalStates + 1
                      << "; current state = " << ++progressCounter << " / " << basis.size() << "\t +" << showMeasuredTime() << std::endl;
            #endif 

            if (res.has_value()) return res;
        }
    }


        
    #else 
    // if m == n, then we directly consider all extensions of length up to 1
    if (numberOfStates == maxSutSize) {
        for (auto &p: v) {
            std::vector<std::pair<TreeNode*, Index>> x(v);
            auto res = distinguishFromSet(fsm, p, x, 0, 0, 1);

            #if LEARNING_LOG_H_METHOD_PROGRESS
            std::cout << ":::::::: H-Method progress (m==n): " << ++progressCounter << " / " << basis.size() << "\t +" << showMeasuredTime() << std::endl;
            #endif 

            if (res.has_value()) return res;
        }
        return std::nullopt;
    }

    // otherwise, we split application of all extensions of length up to m-n+1
    // into two phases.
    // Phase 1) as a heuristic, we first cover all extensions of length 1 or 2,
    //          in the hope of finding early inconsistencies

    size_t skipDepth = 2;
    

    for (auto &p: v) {
        std::vector<std::pair<TreeNode*, Index>> x(v);

        // we can skip ensuring the separation of V x V (i.e. depth 0), as the states in the basis 
        // (and hence the hypothesis) are apart in the observation tree already
        auto res = distinguishFromSet(fsm, p, x, 0, 0, skipDepth);

        #if LEARNING_LOG_H_METHOD_PROGRESS
        std::cout << ":::::::: H-Method progress (m>n, phase 1): " << ++progressCounter << " / " << basis.size() << "\t +" << showMeasuredTime() << std::endl;
        #endif 
        if (res.has_value()) return res;
    }

    // Phase 2) we cover all extensions of length at least 3 and at most m-n+1

    size_t maxDepth = numberOfAdditionalStates+1; // m-n+1

    #if LEARNING_LOG_H_METHOD_PROGRESS
    progressCounter = 0;
    #endif 

    for (auto &p: v) {
        std::vector<std::pair<TreeNode*, Index>> x(v);
        auto res = distinguishFromSet(fsm, p, x, 0, skipDepth, maxDepth);

        #if LEARNING_LOG_H_METHOD_PROGRESS
        std::cout << ":::::::: H-Method progress (m>n, phase 2): " << ++progressCounter << " / " << basis.size() << "\t +" << showMeasuredTime() << std::endl;
        #endif 

        if (res.has_value()) return res;
    }
    #endif

    return std::nullopt;
}

Dfsm Learning::performLSharp() {
    // initialise the base with the root node
    TreeNode* root = obsTree.getTree()->getRoot();
    access[root] = root->getPath();
    extendBasis(root);

    #if LEARNING_LOG_DETAILED_ITERATIONS
    size_t it = 0;
    #endif

    bool performedSimpleRule = false;
    while (true) {
        performedSimpleRule = false;

        #if LEARNING_LOG_DETAILED_ITERATIONS
        std::cout << std::endl;
        std::cout << std::endl;
        auto now = std::chrono::steady_clock::now();
        auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(now - creationTime);
        std::cout << std::fixed << std::setprecision(3) << interval.count() / 1000.0 << "s ";
        std::cout << "ITERATION " << ++it << " ";
        for (size_t i = 0; i < 10; ++i) std::cout << "#";
        std::cout << " +" << showMeasuredTime() << " ";
        for (size_t i = 0; i < 10; ++i) std::cout << "#";
        std::cout << " BASIS: " << basis.size() << " ";
        size_t remainingMappingCandidates = 0;
        for (auto q : frontier) {
            remainingMappingCandidates += mappingCandidates[q].size();
        }
        for (size_t i = 0; i < 10; ++i) std::cout << "#";
        std::cout << " MC: " << remainingMappingCandidates << " ";
        for (size_t i = 0; i < 10; ++i) std::cout << "#";
        std::cout << " QUERIES: " << requestedOutputQueries << " : " << performedOutputQueries << " ";
        for (size_t i = 0; i < 10; ++i) std::cout << "#";
        std::cout << std::endl;
        #endif 

        // try rule 1
        for (auto q : frontier) {
            if (mappingCandidates[q].empty()) {
                extendBasis(q);

                #if LEARNING_LOG_DETAILED_ITERATIONS
                std::cout << "Rule 1: extend basis with state " << q->getNodeId() << std::endl;
                #endif 

                performedSimpleRule = true;
                // break here, as extending the basis may affect the frontier as well as mappingCandidates
                break; 
            }
        }

        // try rule 2
        std::queue<TreeNode*> r2nodes;
        std::queue<size_t> r2inputs;
        for (auto q : incompleteBasis) {
            // apply all missing inputs in one iteration
            for (auto x : undefinedBasisInputs[q]) {
                r2nodes.push(q);
                r2inputs.push(x);                
            }
            
        }
        while (not r2nodes.empty()) {
            auto q = r2nodes.front();
            auto x = r2inputs.front();
            r2nodes.pop();
            r2inputs.pop();

            Trace tx;
            tx.append(x);
            // TODO: apply x multiple times? -> may require changes in outputQuery, as the additional applications may already affect apartness?
            outputQuery(q, tx, false, false);
            extendFrontier(q->after(x));

            #if LEARNING_LOG_DETAILED_ITERATIONS
            std::cout << "Rule 2: apply missing input " << x << " for state " << q->getNodeId() << std::endl;
            #endif 

            performedSimpleRule = true;
        }
        
        // try rule 3
        for (auto q : frontier) {
            if (mappingCandidates[q].size() < 2) continue; // q has at most one candidate

            auto q2 = *mappingCandidates[q].begin();
            mappingCandidates[q].erase(q2);
            auto q3 = *mappingCandidates[q].begin();
            mappingCandidates[q].erase(q3);

            // q2, q3 are guaranteed to be apart, as they are distinct in the basis
            Trace t = obsTree.getApartnessWitness(q2->getNodeId(),q3->getNodeId());

            outputQuery(q, t, false, false);

            #if LEARNING_LOG_DETAILED_ITERATIONS
            std::cout << "Rule 3: distinguish frontier state " << q->getNodeId() 
                        << " from " << q2->getNodeId() 
                        << " and " << q3->getNodeId() 
                        << " via ";
            for (auto x : t) std::cout << x;
            std::cout << std::endl;
            #endif

            // at least one of q2, q3 must now be apart from q
            auto qID = q->getNodeId();
            if (not obsTree.areApart(qID,q2->getNodeId())) {
                mappingCandidates[q].insert(q2);
            } 
            if (not obsTree.areApart(qID,q3->getNodeId())) {
                mappingCandidates[q].insert(q3);
            } 
                
            #if LEARNING_LOG_DETAILED_ITERATIONS
            if ((not obsTree.areApart(q->getNodeId(),q2->getNodeId())) && (not obsTree.areApart(q->getNodeId(),q3->getNodeId()))) {
                std::cout << "ERROR: failed to separate" << std::endl;
                std::cout << std::endl;
                std::cout << "Tree: " << std::endl << obsTree << std::endl;
                std::cout << std::endl;
                std::cout << std::endl;
                std::cout << "Basis: ";
                for (auto qb : basis) std:: cout << qb->getNodeId() << " ";
                std::cout << std::endl;
                std::cout << "Frontier: ";
                for (auto qf : frontier) std:: cout << qf->getNodeId() << " ";
                std::cout << std::endl;
                std::cout << std::endl;
                std::exit(0);
            }
            #endif 


            performedSimpleRule = true;
            break; // break here, as 'frontier' has likely been modified
        }

        // try rule 4
        if (performedSimpleRule) continue; // delay R4 as long as possible

        Dfsm hyp = buildHypothesis();
        hypotheses.push(hyp);

        #if LEARNING_LOG_DETAILED_ITERATIONS
        std::cout << "Rule 4: build hypothesis " << hyp.getName() << std::endl;
        #endif 
        
        #if 0
        ToDotFileVisitor dotFsm("hyps/" + hyp.getName() + ".dot");
        hyp.accept(dotFsm);
        dotFsm.writeToFile();
        #endif

        #if LEARNING_LOG_HYPOTHESES
        std::cout << std::endl;
        std::cout << std::endl;
        std::cout << "raw DFSM with initial state << " << hyp.getInitialStateId() << std::endl;
        std::cout << std::endl;
        auto hypTable = hyp.getDfsmTable();
        for (Index s = 0; s < hyp.size(); ++s) {
            for (Index x = 0; x < hyp.getInputAlphabetSize(); ++x) {
                
                auto t = hypTable[s][x][0].value();   
                auto y = hypTable[s][x][1].value();

                std::cout << s << " " << x << " " << y << " " << t << std::endl; 
            }
        }
        std::cout << std::endl;
        std::cout << std::endl;
        #endif 


        TreeNode* errorNode = checkConsistency(hyp);
        Trace errorTrace;
        if (errorNode == nullptr) {

            // perform external checking of the hypothesis
            std::optional<Trace> checkResult = checkHypothesisExternal(hyp);            
            
            if (checkResult.has_value()) {
                // if a distinguishing trace of SUT and hyp is reported, use it to obtain a conflict trace

                auto conflictRes = getPrefixToConflict(hyp,checkResult.value());
                assert(conflictRes.has_value());
                errorTrace = conflictRes.value();

            } else {
                // otherwise perform an equivalence query

                #if LEARNING_LOG_DETAILED_ITERATIONS
                std::cout << "no inconsistency found, apply H-Method" << std::endl;
                #endif 

                auto res = equivQuery(hyp);

                if (not res.has_value()) {

                    #if LEARNING_LOG_DETAILED_ITERATIONS
                    std::cout << "overall number of performed output queries: " << performedOutputQueries << std::endl;

                    auto tNow = std::chrono::steady_clock::now();
                    auto tInterval = std::chrono::duration_cast<std::chrono::milliseconds>(tNow - creationTime);
                    std::cout << "overall time: " << std::fixed << std::setprecision(3) << tInterval.count() / 1000.0 << "s" << std::endl;
                    #endif 

                    return hyp; // if the equivQuery passes, the hypothesis conforms to the SUT
                }
                errorTrace = res.value();
            }
        } else {
            errorTrace = errorNode->getPath();
        }

        #if LEARNING_LOG_DETAILED_ITERATIONS
        std::cout << "inconsistency found: ";
        for (auto x : errorTrace) std::cout << x << ".";
        std::cout << std::endl;
        #endif

        procCounterEx(hyp, errorTrace);
    }
}

}
