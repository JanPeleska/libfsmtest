#include <queue>
#include <utility>
#include <optional>
#include <unordered_map>
#include <unordered_set>
#include <chrono>
#include <stack>

#include "ObservationTree.hpp"
#include "fsm/Dfsm.hpp"

namespace libfsmtest {

/** Observation tree for learning deterministic implementations */
class Learning {
private:

    // number n of inputs 0,1,...,n-1 in the system
    size_t numInputs;

    // number m of Outputs 0,1,...,m-1 in the system
    size_t numOutputs;

    // upper bound on the number of states of the SUT
    size_t maxSutSize;

    // number of calls to perform output queries
    size_t requestedOutputQueries = 0;

    // number of output queries actually invoking the SUT
    size_t performedOutputQueries = 0;

    // tree for storing all observed traces
    ObservationTree obsTree;

    // time of construction
    std::chrono::steady_clock::time_point creationTime;

    // last measured time point
    std::chrono::steady_clock::time_point lastTimePoint;

    // get time since last time point and set that time point to now
    std::chrono::milliseconds measureTime();

    // measure time and prepare a string representation
    std::string showMeasuredTime();

    // function to apply an input to the SUT and observe an output
    size_t (*applyInputExternal) (size_t, std::optional<size_t>); 

    // function called prior to to equivalence checks
    // if this returns a trace x, the following assumptions hold:
    //   - the SUT and the hypothesis produce different outputs for x
    //   - an output query has been performed for x
    std::optional<Trace> (*checkHypothesisExternal) (Dfsm&); 

    // function to be called before applying an input sequence;
    // should reset the SUT, monitors etc
    void (*resetExternalDependenciesForNewTrace) ();

    // prefix-closed set of pairwise distinguishable nodes
    std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual> basis;
    // maps to each state in the basis its access sequence
    std::unordered_map<TreeNode*,Trace, TreeNodeIdHash, TreeNodeIdEqual> access;
    // children of the basis not already in the basis
    std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual> frontier;
    // maps to each state in the basis all inputs not yet defined in it
    std::unordered_map<TreeNode*,std::unordered_set<size_t>, TreeNodeIdHash, TreeNodeIdEqual> undefinedBasisInputs;
    // subset of the basis containing all incomplete elements
    std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual> incompleteBasis;
    // maps to each state in the frontier the states in the basis it is not apart from
    std::unordered_map<TreeNode*,std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>, TreeNodeIdHash, TreeNodeIdEqual> mappingCandidates;

    // maps to each state in the basis its corresponding state ID in the hypothesis
    // Note: this is necessary, as fsm-internal IDs are continuous, whereas the IDs in the basis need not be
    std::unordered_map<TreeNode*,Index, TreeNodeIdHash, TreeNodeIdEqual> state2hypothesisID;
    // reverse mapping
    std::unordered_map<Index,TreeNode*> hypothesisID2State;
    size_t nextHypothesisID = 0;

    // A Stack of all hypotheses models
    std::stack<Dfsm> hypotheses;

    /** check whether a given nonempty trace separates a state in the hypothesis from a state in the observation tree;
     *  if it does, return a nonempty prefix of it that reaches a conflict (i.e. reaches apart states)
     */
    std::optional<Trace> getPrefixToConflict(Dfsm& fsm, Index s, TreeNode* q, Trace trace);
    
    /** check whether a given nonempty trace separates the hypothesis from the observation tree;
     *  if it does, return a nonempty prefix of it that reaches a conflict (i.e. reaches apart states)
     */
    std::optional<Trace> getPrefixToConflict(Dfsm& fsm, Trace trace);
    
    /**
     * Extend a trace with all extensions up to a certain length and separate pairs of traces
     * as required by the H-Condition.
     *
     * @param fsm The hypothesis, completely specified and minimal.
     * @param p A pair (node,q) such that the path in the test suite to the node is the trace
     *          to extend, reaching state q in the reference model.
     * @param toDist A set of traces to separate p and its extension from, as required by the
     *               H-Condition.
     *               In the initial call of distinguishFromSet, this is the state cover.
     * @param curDepth A counter for the current depth (i.e. extension length)
     * @param skipDepth All extensions up to this length are not separated
     * @param maxDepth Maximum length of extensions to consider
     * @return A separating trace of the SUT and the hypothesis, if any is found in applying the
     *         separating traces w.r.t. the hypothesis.
     *         Else nothing. 
     */
    std::optional<Trace> distinguishFromSet(Dfsm& fsm, std::pair<TreeNode*,Index> p, std::vector<std::pair<TreeNode*,Index>>& toDist, size_t curDepth, size_t skipDepth, size_t maxDepth);
    
    // reset internal and external state in preparation for applying an input sequence
    void resetAndPrepareForNewTrace();

    std::vector<Trace> equivalenceCheckInputStimuli;

public:

    // create a new learning object
    Learning(size_t numInputs, size_t numOutputs, size_t maxSutSize, size_t (*applyInput) (size_t, std::optional<size_t>), void (*resetExternalDependenciesForNewTrace) (), std::optional<Trace> (*checkHypothesisExternal) (Dfsm&));

    // obtain the internal tree
    Tree* getTree() { return obsTree.getTree(); }

    // apply a single input using the SUT and observe an output
    size_t applyInput(size_t input);

    // reset internal state in preparation for applying an input sequence
    void prepareForNewTrace();

    /** extend the basis for an isolated frontier state q */
    void extendBasis(TreeNode* q);

    /** extend the frontier by a new successor q of some basis state */
    void extendFrontier(TreeNode* q);

    /** Add an IO pair to the current node of the internal observation tree */
    void addObservation(Index input, Index output);

    /** If the given input has previously beend applied after the current observation trace,
     *  return the output observed then and advance the current node of the internal observation
     *  tree for the given input.
     *  Otherwise return nullopt and leave the internal state unchanged.
     * 
     *  This does not call the SUT.
     */
    std::optional<size_t> advanceObservationByPreviouslyObservedInput(size_t input);

    /** 
     * Perform an output query.
     * 
     * @param q A state in the observation tree.
     * @param trace A trace to apply after the access seqence of q.
     * @return The outputs observed in response to the access sequence of q followed by trace.
     */
    Trace outputQuery(TreeNode *q, Trace &trace, bool checkIfAlreadyInTree, bool trackOutputQuery);

    /** create a DFSM whose states are the basis and which redirects frontier states into the basis */
    Dfsm buildHypothesis();

    /**
     * Run the hypothesis against the observation tree
     * 
     * @return If inconsistent: a pointer to a node in the tree where the path to that node 
     *                          reaches an apart state in the hypothesis.
     *         Else: nullptr
     */ 
    TreeNode* checkConsistency(Dfsm& hypothesis);

    /** process a counterexample trace */
    void procCounterEx(Dfsm& hypothesis, Trace& counterex);

    /**
     * Check whether the SUT is equivalent to the hypothesis.
     * Returns a distinguishing sequence of the SUT and the hypothesis, if any exists.
     */ 
    std::optional<Trace> equivQuery(Dfsm& hypothesis);

    /** run L# and return the learned DFSM */
    Dfsm performLSharp();

    /** Compute a lower bound on the number of distinct states in a deterministic
     *  FSM of whose language the observation tree is a subset
    */
    size_t getMinimalNumberOfPairwiseDistinctStatesInSut();

    std::vector<Trace> getAccessSequencesForBasis() const;

    /**
     * @brief
     *  Returns the stack of all learnt state machine hypotheses.
     */
    std::stack<Dfsm> getHypothesis() { return hypotheses; }

    std::vector<Trace> getEquivalenceCheckInputStimuli() { return equivalenceCheckInputStimuli; }
};


}
    