/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once
#include "Transformer.hpp"
#include "fsm/Dfsm.hpp"


namespace libfsmtest {


/**
 * Minimise initially connected, observable machines
 */
class ToMinimisedTransformer : public Transformer {

private:
    
    static bool minimiseConnected(Ofsm& fsm, States& statesMin, Strings& stateNamesMin);
    static bool minimiseConnected(Dfsm& fsm, States& statesMin, Strings& stateNamesMin);
    
public:
    
    ToMinimisedTransformer(Fsm* fsmToTransform, const std::string& nameSuffixParam = "_MIN") :
    Transformer(fsmToTransform, nameSuffixParam) { }

    /** Throw an execption, since only observable machines are admissible for minimisation */
    void visit(Fsm& fsm) override;
    
    /** Minimise observable, initially connected machine */
    void visit(Ofsm& fsm) override;
    
    /** Minimise deterministic, initially connected machine */
    void visit(Dfsm& fsm) override;
    
};

}


