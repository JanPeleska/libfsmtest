/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <utility>
#include <set>
#include "testsuite/Tree.hpp"
#include "visitors/testgeneration/TestGenerationVisitor.hpp"

namespace libfsmtest {

/**
 * Class implementing the H-Method for test generation.
 */
class HMethod2 : public TestGenerationVisitor {

protected:

    /**
     * The maximal number of additional states,
     * which the implementation DFSM in minimised
     * form may have, when compared to the reference
     * model in minimised form.
     */
    const size_t numAddStates;
    
    /** OFSM tables calculated from the minimised reference model */
    std::map< size_t, std::set<Index> > classMap;
    std::vector< std::map<Index,size_t> > state2classMaps;
    

    /**
     * Compute for each pair of distinct states a distinguishing trace of minimal length.
     * @param fsm The reference model.
     *            Assumed to be a completely specified DFSM.
     * @param dist A table that after returning stores for each pair q1, q2 of distinct states
     *             a minimal distinguishing trace in dist[q1][q2].
     *             The returned table is symmetric, i.e. dist[q1][q2] = dist[q2][q1].
     */
    void getDistinguishingTraces(Dfsm& fsm, Trace** dist);

    /**
     * Compute for each pair of distinct states a distinguishing trace of minimal length.
     * @param fsm The reference model.
     *            Assumed to be a completely specified OFSM.
     * @param dist A table that after returning stores for each pair q1, q2 of distinct states
     *             a minimal distinguishing trace in dist[q1][q2].
     *             The returned table is symmetric, i.e. dist[q1][q2] = dist[q2][q1].
     */
    void getDistinguishingTraces(Ofsm& fsm, Trace** dist);


    /**
     * Choose a distinguishing trace to separate a given pair of traces, trying to add
     * as few new branches and input symbols to the test suite.
     *
     * @param fsm The reference model.
     *            Assumed to be completely specified.
     * @param n0 The node in the test suite whose path is the first trace to separate.
     * @param s0 The state reached in the reference model by the first trace.
     * @param n1 The node in the test suite whose path is the second trace to separate.
     * @param s1 The state reached in the reference model by the second trace.
     * @param bestTrace The currently chosen distinguishing trace.
     *                  Will be replaced during execution if a better choice is found.
     * @param bestAdditionalInsertions The number of additional input symbols incurred if
     *                                 the current choice is added to the test suite at both nodes.
     * @param bestAdditionalBranches The number of additional branches incurred if the current choice
     *                               is added to the test suite at both nodes.
     * @param dist A symmetric table containing a distinguishing trace for each pair
     *             of distinct states of the reference model.
     */
    void findCheapestDistTrace(Dfsm& fsm, TreeNode* n0, Index s0, TreeNode* n1, Index s1, Trace& bestTrace, size_t& bestAdditionalInsertions, size_t& bestAdditionalBranches, Trace** dist);


    /**
     * Choose a distinguishing trace to separate a given pair of traces, trying to add
     * as few new branches and input symbols to the test suite.
     *
     * @param fsm The reference model.
     * @param n0 The node in the test suite whose path is the first trace to separate.
     * @param s0 The state reached in the reference model by the first trace.
     * @param n1 The node in the test suite whose path is the second trace to separate.
     * @param s1 The state reached in the reference model by the second trace.
     * @param bestTrace The currently chosen distinguishing trace.
     *                  Will be replaced during execution if a better choice is found.
     * @param bestAdditionalInsertions The number of additional input symbols incurred if
     *                                 the current choice is added to the test suite at both nodes.
     * @param bestAdditionalBranches The number of additional branches incurred if the current choice
     *                               is added to the test suite at both nodes.
     * @param dist A symmetric table containing a distinguishing trace for each pair
     *             of distinct states of the reference model.
     */
    void findCheapestDistTracePN(Ofsm& fsm, TreeNode* n0, Index s0, TreeNode* n1, Index s1, Trace& bestTrace, size_t& bestAdditionalInsertions, size_t& bestAdditionalBranches, Trace** dist);

    /**
     * Add a distinguishing trace to separate a given pair of traces, trying to add
     * as few new branches and input symbols to the test suite.
     *
     * @param fsm The reference model.
     *            Assumed to be completely specified.
     * @param p0 A pair (n0,q0) such that the path in the test suite to node n0 is the
     *           first trace to separate, reaching state q0 in the reference model.
     * @param p1 A pair (n0,q0) such that the path in the test suite to node n1 is the
     *           second trace to separate, reaching state q1 in the reference model.
     * @param dist A symmetric table containing a distinguishing trace for each pair
     *             of distinct states of the reference model.
     */
    void addCheapestDistTrace(Dfsm& fsm, std::pair<TreeNode*,Index> p0, std::pair<TreeNode*,Index> p1, Trace** dist);

    /**
     * Add a distinguishing trace to separate a given pair of traces, trying to add
     * as few new branches and input symbols to the test suite.
     *
     * @param fsm The reference model.
     * @param p0 A pair (n0,q0) such that the path in the test suite to node n0 is the
     *           first trace to separate, reaching state q0 in the reference model.
     * @param p1 A pair (n0,q0) such that the path in the test suite to node n1 is the
     *           second trace to separate, reaching state q1 in the reference model.
     * @param dist A symmetric table containing a distinguishing trace for each pair
     *             of distinct states of the reference model.
     */
    void addCheapestDistTracePN(Ofsm& fsm, std::pair<TreeNode*,Index> p0, std::pair<TreeNode*,Index> p1, Trace** dist);

    /**
     * Extend a trace with all extensions up to a certain length and separate pairs of traces
     * as required by the H-Condition.
     *
     * @param fsm The reference model.
     *            Assumed to be completely specified.
     * @param p A pair (node,q) such that the path in the test suite to the node is the trace
     *          to extend, reaching state q in the reference model.
     * @param toDist A set of traces to separate p and its extension from, as required by the
     *               H-Condition.
     *               In the initial call of distinguishFromSet, this is the state cover.
     * @param depth Length of extension to consider.
     *              Assumed to be non-negative.
     *              If 0, then p is still separated from toDist as required.
     * @param dist A symmetric table containing a distinguishing trace for each pair
     *             of distinct states of the reference model.
     */
    void distinguishFromSet(Dfsm& fsm, std::pair<TreeNode*,Index> p, std::set<std::pair<TreeNode*,Index>>& toDist, size_t depth, Trace** dist);

    /**
     * Extend a trace with all extensions up to a certain length and separate pairs of traces
     * as required by the H-Condition.
     *
     * @param fsm The reference model.
     * @param p A pair (node,q) such that the path in the test suite to the node is the trace
     *          to extend, reaching state q in the reference model.
     * @param toDist A set of traces to separate p and its extension from, as required by the
     *               H-Condition.
     *               In the initial call of distinguishFromSet, this is the state cover.
     * @param depth Length of extension to consider.
     *              Assumed to be non-negative.
     *              If 0, then p is still separated from toDist as required.
     * @param dist A symmetric table containing a distinguishing trace for each pair
     *             of distinct states of the reference model.
     */
    void distinguishFromSetPN(Ofsm& fsm, std::pair<TreeNode*,Index> p, std::set<std::pair<TreeNode*,Index>>& toDist, size_t depth, Trace** dist);

    
public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation DFSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     */
    HMethod2(const size_t numAddStatesParam) :
    TestGenerationVisitor(),
    numAddStates(numAddStatesParam)
    {

    }

    /**
     * Method implementing the actual H-Method for test generation on a FSM.
     * Only called indirectly, see Fsm::accept()
     * @param fsm The FSM to generate the test suite for.
     */
    void visit(Fsm & fsm) override;

    /** Generation for observable FSMs */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual H-Method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     */
    void visit(Dfsm & dfsm) override;


    /**
     * Checks whether a test suite satisfies the H-Condition.
     * @param fsm The reference model.
     * @param cover A state cover containing for each state a pair (w,q) where
     *              w is the trace in the state cover reaching q.
     * @param suite The test suite to check.
     * @return True iff the test suite satisfies the H-Condition.
     */
    static bool satisfiesHCondition(Ofsm& fsm, std::vector<std::pair<Trace,Index>>& cover, Tree& suite, size_t additionalStates);
    
};

} // namespace libfsmtest
