/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <fstream>
#include <sstream>
#include <algorithm>
#include <filesystem>
#include <utility>

#include "creators/FsmCreator.hpp"
#include "creators/transformers/Transformer.hpp"
#include "FsmFromCsvCreator.hpp"
#include "FsmFromRawCreator.hpp"

#include "FsmFromFileCreator.hpp"

using namespace std;

namespace libfsmtest {

FsmFromFileCreator::FsmFromFileCreator(const string &fileNameParam, string  fsmNameParam, unique_ptr<Transformer> transformerToApply) : fileName(fileNameParam), prefix(fileNameParam), fsmName(std::move(fsmNameParam)), transformer(move(transformerToApply)) {}

unique_ptr<Fsm> FsmFromFileCreator::createFsm() {
    // Try to recognise the format of the file name passed to the constructor.
    FileFormat_t recognisedFormat = recogniseFormat();

    unique_ptr<Fsm> fsm;
    if (recognisedFormat == NOT_SUPPORTED) {
        throw std::runtime_error("This file format is not supported.");
    }
    if (recognisedFormat == CSV) {
        fsm = libfsmtest::createFsm<FsmFromCsvCreator>(fileName, fsmName);
    }
    if (recognisedFormat == RAW) {
        // Check whether there are additional files to specify a presentation layer
        if(checkForPresentationLayer(prefix)) {
            // Files for the presentation layer found, so use them
            fsm = libfsmtest::createFsm<FsmFromRawCreator>(fileName, prefix + ".state", prefix + ".in", prefix + ".out", fsmName);
        } else {
            // Use only fsm specification file, create presentation layer with only indices.
            fsm = libfsmtest::createFsm<FsmFromRawCreator>(fileName, fsmName);
        }
    }

    if (transformer) {
        // Apply the transformer passed to the constructor
        transformer->setFsm(fsm.get());
        // and return the transformed Fsm instead
        return transformer->createFsm();
    }
    return fsm;
}

FileFormat_t FsmFromFileCreator::recogniseFormat() {
    FileFormat_t format;

    if (filesystem::exists(fileName + ".fsm")) {
        fileName.append(".fsm");
        format = RAW;
    } else if (filesystem::exists(fileName + ".csv")){
        fileName.append(".csv");
        format = CSV;
    } else if (filesystem::exists(fileName)) {
        // There is a file without an extension for the given filename, so try to identify its format.
        format = recogniseByExtension();
        if (format == NOT_RECOGNISED) format = recogniseByContent();
        if (format == NOT_RECOGNISED) format = NOT_SUPPORTED;
    } else {
        format = NOT_SUPPORTED;
    }

    return format;
}

FileFormat_t FsmFromFileCreator::recogniseByExtension() {
    // get the position of the last '.' which would be directly before an extension
    auto const lastDot = fileName.find_last_of('.');
    if (lastDot != string::npos) {
        // '.' found, extract substring after that (might be an extension)
        string extension = fileName.substr(lastDot + 1);

        if (extension == "fsm") {
            prefix = fileName.substr(0, lastDot);
            return RAW;
        } else if (extension == "csv") {
            prefix = fileName.substr(0, lastDot);
            return CSV;
        }
    }
    // no supported extension found.
    return NOT_RECOGNISED;
}

FileFormat_t FsmFromFileCreator::recogniseByContent() {
    ifstream file(fileName);
    if (!file.is_open()) {
        throw std::runtime_error("Failed to open the specified file.");
    }
    string line;
    getline(file, line);

    if (recogniseCSV(line)) return CSV;
    if (recogniseRAW(line)) return RAW;

    return NOT_RECOGNISED;
}

bool FsmFromFileCreator::recogniseCSV(const std::string& firstLine) {
    stringstream line(firstLine);
    string cell;

    getline(line, cell, ';');
    cell.erase(remove_if(cell.begin(), cell.end(), ::isspace), cell.end());
    if (!cell.empty()) {
        // The first cell of a CSV fsm file should be empty.
        return false;
    }

    while(getline(line, cell, ';')) {
        cell.erase(remove_if(cell.begin(), cell.end(), ::isspace), cell.end());
        if (cell.empty()) {
            // all following cells should not be empty.
            return false;
        }
    }

    // The first line matches the CSV FSM file format.
    return true;
}

bool FsmFromFileCreator::recogniseRAW(const std::string& firstLine) {
    stringstream line(firstLine);
    Index value;
    for (unsigned short i = 1; i <= 4; ++i) {
        line >> value;
        if (line.fail()) {
            // Could not read one an expected value.
            return false;
        }
    }

    // The first line matches the raw FSM file format.
    return true;
}

bool FsmFromFileCreator::checkForPresentationLayer(const std::string& filename) {
    return filesystem::exists(filename + ".state") and filesystem::exists(filename + ".in") and filesystem::exists(filename + ".out");
}

} // namespace libfsmtest

