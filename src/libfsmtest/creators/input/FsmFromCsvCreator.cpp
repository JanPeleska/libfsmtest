/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <cassert>
#include <vector>
#include <set>
#include <fstream>
#include <memory>
#include <algorithm>

#include "utils/CsvUtils.hpp"
#include "fsm/Dfsm.hpp"
#include "creators/transformers/ToAutoCompletedTransformer.hpp"

#include "FsmFromCsvCreator.hpp"

using namespace std;

namespace libfsmtest {

unique_ptr<Fsm> FsmFromCsvCreator::createFsm() {
    readFile();

    if (std::any_of(states.begin(), states.end(), [](auto const& s){return !s.isDeterministic();})) {
        if (std::any_of(states.begin(), states.end(), [](auto const& s){return !s.isObservable();})) {
            return make_unique<Fsm>(fsmName, move(states), move(presentationLayer), 0);
        }
        // Machine is observable, but not deterministic
        return make_unique<Ofsm>(fsmName, move(states), move(presentationLayer), 0);
    }

    // The FSM is deterministic, so we return a Dfsm instance
    return make_unique<Dfsm>(fsmName, move(states), move(presentationLayer), 0);
}

void FsmFromCsvCreator::readFile() {
    ifstream inputFile(fileName);

    auto removeWhitespace = [](string& cell) {
        cell.erase(remove_if(cell.begin(), cell.end(), ::isspace), cell.end());
    };
    Csv csv = readCsv(inputFile, ';', nullptr, removeWhitespace);

    vector<string> outputAlphabet;

    vector<string> stateNames;
    // Copy state names from the first column of the CSV file, skipping the topmost (empty) cell.
    stateNames.assign(csv.at(0).begin() + 1, csv.at(0).end());

    vector<string> inputAlphabet;
    // Create input alphabet from first row
    for (auto it = begin(csv) + 1; it != end(csv); ++it) {
        inputAlphabet.emplace_back(it->at(0));
    }

    states.reserve(stateNames.size());
    Index id = 0;
    for_each(stateNames.begin(), stateNames.end(), [&](const auto& name) {states.emplace_back(name, id); id++;});


    for (auto it = begin(csv) + 1; it != end(csv); ++it) {
        for (Index index = 0; index < stateNames.size(); ++index) {
            Transitions& t = states.at(index).getTransitions();
            // Cell defining transition from State index for current input
            istringstream cell(it->at(index + 1));

            // Since CSV format allows for FSMs, there might be multiple transitions specified for one input, separated by comma
            string singleTransition;
            while (getline(cell, singleTransition, ',')) {
                istringstream sTstream(singleTransition);
                string postState;
                string output;
                getline(sTstream, postState, '/');
                getline(sTstream, output);

                auto difference1 = distance(csv.begin() + 1, it);
                assert(difference1 >= 0);
                Index inputIndex = static_cast<Index>(difference1);

                if (postState.empty()) {
                    // autocompletion is handled after creation.
                    continue;
                }

                Index outputIndex;
                if (auto outputIt = find(outputAlphabet.begin(), outputAlphabet.end(), output);
                        outputIt == outputAlphabet.end()) {
                    outputIndex = outputAlphabet.size();
                    outputAlphabet.emplace_back(output);
                } else {
                    auto difference2 = distance(outputAlphabet.begin(), outputIt);
                    assert(difference2 >= 0);
                    outputIndex = static_cast<Index>(difference2);
                }

                auto stateIt = find(stateNames.begin(), stateNames.end(), postState);
                if (stateIt == stateNames.end()) {
                    throw std::runtime_error("Poststate is not defined");
                }
                auto difference3 = distance(stateNames.begin(), stateIt);
                assert(difference3 >= 0);
                Index postStateIndex = static_cast<Index>(difference3);

                t.emplace_back(index, inputIndex, outputIndex, postStateIndex);
            }
        }
    }

    presentationLayer = PresentationLayer(inputAlphabet, outputAlphabet, stateNames);
}

} // namespace libfsmtest
