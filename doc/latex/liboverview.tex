\chapter{Library Overview}\label{chap:over}
% ====================================================================================

\section{Top-level Directory}

The top-level directory {\tt libfsmtest/} of the \fsml\ class library is structured as follows.
\begin{enumerate}
\item Directory {\tt doc/} contains this manual, named {\tt libfsmtest.pdf}.
\item Directory {\tt resources/} contains sample FSMs, encoded in different file formats.
\item Directory {\tt src/} contains the source code.

\end{enumerate}


% ====================================================================================
\section{Source Directory src/}

The source directory {\tt src/} contains the complete \fsml\ source code, classes and main programs. It is structured as follows. 
\begin{enumerate}
\item Directory {\tt libfsmtest/} contains all classes, both cpp-files and header files.

\item Directory {\tt usage-demo/} contains main program file {\tt usage-demo.cpp}. This file contains many 
small procedures, each showing how to use a specific feature of the \fsml\ classes. If the examples shown in this documentation do not suffice, or if you wish to create your own code by copying from existing examples, this is the place to go. The file names used in this documentation refer to files that actually exist in the 
{\tt libfsmtest/resources} directory. In program {\tt usage-demo}, these files are referenced with their absolute path
\begin{quote}
\tt   RESOURCEPATH + $<$filename$>$
\end{quote}
The {\tt \#define}-value of {\tt RESOURCEPATH} is determined while building \fsml\ using {\tt cmake}.



\item Directory {\tt generator/} contains the generator main program {\tt generator.cpp}. This program
uses the \fsml\ class library to generate test suites. The reference FSMs to be used and the test generation methods to be applied are provided as command line arguments.

\item Directory {\tt checker/} contains the checker main program {\tt checker.cpp}. It takes an implementation FSM file name and a test suite file name as command line arguments and runs the test suite against this FSM.

\item Directory {\tt harness/} contains the main program of the test harness, called {\tt  harness.cpp}.
The harness needs a wrapper to refine input data to the system under test (SUT) and abstract SUT outputs back to events of the FSM output alphabet. Such a wrapper can be implemented by inserting code into the source frame {\tt sut\_wrapper.cpp}, also contained in this directory. In sub-directory {\tt example/} an example is shown, explaining  how to configure the wrapper and run the harness with a test suite against a \cpp\ application library. 

\end{enumerate}


% ====================================================================================
\section{Source Directory src/libfsmtest}

The directory containing the \fsml\ class library has the following sub-structure.
\begin{enumerate}
\item Directory {\tt creators} contains factory methods for creating FSMs in various ways.
Subdirectory {\tt creators/input} contains factory methods for creating FSMs from files in various formats.
Moreover, creator classes in subdirectory {\tt creators/randoms} contains  generators of random FSMs.
Also, creator classes in subdirectory {\tt creators/mutators} contain generators of random mutations of FSMs.
Finally, subdirectory {\tt creators/transformers} contains FSM transformations.\footnote{For example, a transformation of an arbitrary nondeterministic FSM into an observable FSM.}

\item Directory {\tt fsm/} contains the main classes for FSMs and their basic evaluation and simulation methods.
The root class is {\tt Fsm}, sub-classed by {\tt Ofsm} for observable (nondeterministic) FSMs, sub-classed by
{\tt Dfsm} (deterministic FSMs).

\item Directory {\tt visitors/} contains classes and algorithms operating on FSMs.
The virtual visit-methods are pre-defined in header file {\tt FsmVisitor.hpp} contained in this directory.
The visitors implementing the different test generation methods provided by the \fsml are located in subdirectory {\tt visitors/testgeneration}.
The concrete method visitors for test generation algorithms, however, will usually subclass from {\tt TestGenerationVisitor.hpp}, because this class extends the {\tt FsmVisitor} by operations used by most concrete test case generation visitors.
The latter are named after the method they implement, such as {\tt HMethod.hpp}, {\tt HMethod.cpp}. To study an example before adding you own test generation visitors, see the W-Method visitor {\tt WVisitor.hpp, .cpp}.

A second type of visitors is intended for writing FSM instances to files in different format. These visitors will subclass from {\tt ToFileVisitor.hpp} and are located in subdirectory {\tt visitors/output}.

\item Directory {\tt testsuite/} contains the main class {\tt TestSuite.hpp, .cpp} for creating test suites, together with auxiliary classes for representing traces in linear or tree structure.

\end{enumerate}

\subsection{Directory- and Library-wide Header Files}
For ease of use, each directory in {\tt src/libfsmtest} contains header files with corresponding names to all subdirectories.
In those files, all header files from those subdirectories are included.
The following files are available:
\begin{itemize}
	\item {\tt creators.hpp}: Including all header files in subdirectory {\tt creators} by including the directory-wide header files of all subdirectories.
	\item {\tt fsm.hpp}: Including all header files in subdirectory {\tt fsm}.
	\item {\tt testsuite.hpp}: Including all header files in subdirectory {\tt testsuite}.
	\item {\tt visitors.hpp}: Including all header files in subdirectory {\tt visitors} by including the directory-wide header files of all subdirectories.
	\item {\tt libfsmtest.hpp}: The library-wide header file, including all of the above files.
\end{itemize}

Directories {\tt creators} and {\tt visitors} are in turn containing header files for their subdirectories.
For example, including header file {\tt src/libfsmtest/creators/input.hpp} will include all header files inside the directory {\tt src/libfsmtest/creators/input/}.

Additionally, all those directory-wide header files are included in the library-wide header file {\tt src/libfsmtest/libfsmtest.hpp}.
Using this header file will then include all header files provided by the \fsml.