/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <utility>

#include "creators/FsmCreator.hpp"

namespace libfsmtest {

/**
 * Factory for reading an FSM specification from a CSV file.
 */
class FsmFromCsvCreator : public libfsmtest::FsmCreator {
private:
    /** The name or path of the CSV file to be used */
    const std::string fileName;

    /** The name the FSM will be associated with */
    const std::string fsmName;

    /** The states of the read FSM */
    States states;

    /** The PresentationLayer used by the read FSM */
    PresentationLayer presentationLayer;

    /**
     * Reads and parses the CSV file content of #fileName.
     * Fills #states and #presentationLayer.
     */
    void readFile();

public:
    /**
     * Constructor.
     * Won't start reading or parsing the input.
     *
     * @param fileNameParam The path of the CSV file to create a DFSM from.
     * @param fsmNameParam The name the resulting FSM will be associated with
     */
    explicit FsmFromCsvCreator(std::string fileNameParam, std::string fsmNameParam = "") : fileName(std::move(fileNameParam)), fsmName(std::move(fsmNameParam)) {}

    /**
     * Creates a DFSM from the given input CSV file.
     * @see readFile()
     * @return A unique pointer to the constructed DFSM.
     */
    std::unique_ptr<Fsm> createFsm() override;

};

} // namespace libfsmtest
