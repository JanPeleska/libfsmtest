/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <cassert>
#include <utility>

#include "creators/FsmCreator.hpp"
#include "FsmTraits.hpp"
#include "Generator.hpp"
#include "fsm/Dfsm.hpp"
#include "fsm/Fsm.hpp"
#include "fsm/Ofsm.hpp"
#include "creators/transformers/ToMinimisedTransformer.hpp"

#include "visitors/output/ToDotFileVisitor.hpp"

namespace libfsmtest {

template<typename Machine, FsmTrait... trait>
class RandomFsmCreator : public FsmCreator {
    static_assert(std::is_base_of<Fsm, Machine>::value, "Random generation is only supported for Fsm, Ofsm and Dfsm.");
    static_assert(sizeof...(trait) <= 3, "At most three traits are allowed");

private:
    /** The number of states the FSM will have. */
    const size_t numberOfStates;

    /** The number of inputs the alphabet will have. */
    const size_t numberOfInputs;

    /** The number of outputs the alphabet will have. */
    const size_t numberOfOutputs;

    /** The name the FSM will have */
    const std::string fsmName;

    /**
     * Generates a new Presentation Layer containing
     * #numberOfStates many state names,
     * #numberOfInputs many inputs and
     * #numberOfOutputs many outputs.
     * Uses indices as names.
     * @return A PresentationLayer for the generated FSM.
     */
    [[nodiscard]] PresentationLayer createPresentationLayer() const {
        Strings stateNames, input, output;

        input.reserve(numberOfInputs);
        for (Index i = 0; i < numberOfInputs; ++i) {
            input.emplace_back(std::to_string(i));
        }

        output.reserve(numberOfOutputs);
        for (Index i = 0; i < numberOfOutputs; ++i) {
            output.emplace_back(std::to_string(i));
        }

        stateNames.reserve(numberOfStates);
        for (Index i = 0; i < numberOfStates; ++i) {
            stateNames.emplace_back(std::to_string(i));
        }

        return {move(input), move(output), move(stateNames) };
    }

public:
    /**
     * Constructor. Won't start creating a FSM.
     * @param numberOfStatesParam The number of states the generated FSMs will have.
     * @param numberOfInputsParam The number of inputs the generated FSMs will have.
     * @param numberOfOutputsParam The number of outputs the generated FSMs will have.
     * @param fsmNameParam The name the generated FSMs will have.
     */
    RandomFsmCreator(const size_t numberOfStatesParam,
                     const size_t numberOfInputsParam,
                     const size_t numberOfOutputsParam,
                     std::string fsmNameParam = "") :
            numberOfStates(numberOfStatesParam),
            numberOfInputs(numberOfInputsParam),
            numberOfOutputs(numberOfOutputsParam),
            fsmName(std::move(fsmNameParam)) {
        if (numberOfStatesParam == 0)
            throw std::runtime_error("Cannot create an FSM with no states");

        if (numberOfInputsParam == 0)
            throw std::runtime_error("Cannot create an FSM with no inputs");

        if (numberOfOutputsParam == 0)
            throw std::runtime_error("Cannot create an FSM with no outputs");
    }

    /**
     * Starts generating a random FSM of type Machine meeting the given traits.
     * Might be called multiple times on the same instance to generate random machines
     * with the same specification.
     * @return A FSM of type Machine meeting the given traits.
     */
    std::unique_ptr<Fsm> createFsm() override {
        FsmTraits traits{trait...};
        std::unique_ptr<Fsm> fsm  = Generator<Machine>::generate(createPresentationLayer(), fsmName, traits);

        assert(checkFsmTraits(*fsm, traits));
        return fsm;
    };
};


} // namespace libfsmtest
