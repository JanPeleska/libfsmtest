
/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <random>
#include "RandomTransitionReroute.hpp"

namespace libfsmtest {

bool RandomTransitionReroute::applyMutation() {
    size_t numberOfStates = transformedFsm->size();

    std::random_device rd;
    std::default_random_engine gen(rd());
    std::uniform_int_distribution<size_t> distributeStates(0, numberOfStates - 1);

    // pick a random state
    Index state = distributeStates(gen);
    Index start = state;
    do {
        Transitions& transitions = transformedFsm->getStates().at(state).getTransitions();
        if (!transitions.empty()) {
            std::uniform_int_distribution<size_t> distributeTransitions(0, transitions.size() - 1);
            size_t transition = distributeTransitions(gen);
            Index target = distributeStates(gen);
            Index input = transitions.at(transition).getInput();
            Index output = transitions.at(transition).getOutput();

            if (target == transitions.at(transition).getTargetId()) {
                // ensure the target will actually change
                target = (target + 1) % transformedFsm->size();
            }

            // Replace the old transition with a new one with randomly generated target, but same in- and output.
            transitions.at(transition) = Transition(state, input, output, target);
            return true;
        } else {
            state = (state + 1) % numberOfStates;
        }
        //This loop is executed until a state with outgoing transitions is found or all states are found to not have outgoing transitions
    } while (transformedFsm->getStates().at(state).getTransitions().empty() and start != state);

    // No state had transitions, so none got removed.
    return false;
}

} // namespace libfsmtest

