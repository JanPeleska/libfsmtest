/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <map>
#include <set>
#include "HMethod.hpp"

namespace libfsmtest {

/**
 * Class implementing the Safety-complete H-Method for test generation.
 */
class SHMethod : public HMethod {

protected:
    
    bool isFirstCall;

    /**
     * A second FSM abstracting
     * the reference  model
     */
    std::unique_ptr<Fsm> abstractionFsm;
    
    /** Class maps of the original (potentially unminimised) reference FSM */
    std::map< size_t, std::set<Index> > classMapFsm;
    std::map<Index,size_t> state2classFsm;
    
    /**
     * Class maps of te original (potentially unminimised) abstraction FSM.
     * It is required that the state numbers in the original abstraction FSM
     * correspond to the state numbers in the original reference FSM
     */
    std::map< size_t, std::set<Index> > classMapAbsFsm;
    std::map<Index,std::set<size_t>> state2classAbsFsm;
    
    /** Mapping from classes of the reference model to classes of the abstraction
     *  model. Note that this map is generally NOT 1-1 (injective), but only
     *  surjective.
     */
    std::map<size_t,std::set<size_t>> refClass2AbsClass;


    /**
     * This method needs to be called on the first call of the Ofsm-/Dfsm-visit method,
     * because then both reference FSM and abstraction FSM still have their original
     * (potentially unminimised) format, and their states and state numbers are
     * still in one-one-correspondence.
     */
    void calculateClassMaps(Ofsm &fsm);
        
public:

    /**
     * Constructor.
     * Won't start generating until accepted on a FSM.
     * @param numAddStatesParam The maximal number of additional states,
     *                          which the implementation DFSM in minimised
     *                          form may have, when compared to the reference
     *                          model in minimised form.
     */
    SHMethod(const size_t numAddStatesParam, std::unique_ptr<Fsm> abstractionFsmParam) :
    HMethod(numAddStatesParam),
    isFirstCall(true),
    abstractionFsm(std::move(abstractionFsmParam))
    {

    }


    /** Generation for observable FSMs */
    void visit(Ofsm & fsm) override;

    /**
     * Method implementing the actual Safety-complete H-Method for test generation on a DFSM.
     * Only called indirectly, see Dfsm::accept()
     * @param dfsm The DFSM to generate the test suite for.
     */
    void visit(Dfsm & dfsm) override;
    
};

} // namespace libfsmtest
