#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <optional>
#include <algorithm>
#include <functional>

#include "testsuite/TreeNode.hpp"
#include "testsuite/Tree.hpp"

#include "ObservationTree.hpp"

namespace libfsmtest {

ObservationTree::ObservationTree(size_t numberInputIDs) :
tree(std::make_unique<Tree>(std::make_unique<TreeNode>(nullptr,0))),
currentNode(nullptr),
numInputIDs(numberInputIDs),
distMap(std::unordered_map<size_t,std::unordered_map<size_t,Index,IndexHash>,IndexHash>())
{

    for (size_t i = 0; i < numInputIDs; ++i) {
        nodesWithDefinedInput.push_back(std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>());
    }
    nodesInTree.push_back(tree->getRoot());

    beginNewObservationTrace();
}

std::ostream& operator<<(std::ostream& os, const ObservationTree& ot)
{
    std::stack<std::string> prefixes;
    std::stack<TreeNode*> nodes;

    nodes.push(ot.tree->getRoot());
    prefixes.push(std::to_string(ot.tree->getRoot()->getNodeId()) + "-");

    while (not nodes.empty()) {
        auto node = nodes.top();
        auto prefix = prefixes.top();
        nodes.pop();
        prefixes.pop();

        if (node->isLeaf()) {
            os << prefix << std::endl;
        }

        for (auto x : node->getLabelsOfOutgoingEdges()) {
            auto nxt = node->after(x);

            nodes.push(nxt);
            prefixes.push(prefix + "(" + std::to_string(x) + "/" + std::to_string(nxt->getValue().value()) +")-" + std::to_string(nxt->getNodeId()) + "-");
        }
    }

    #if OBSERVATION_TREE_PRINT_DISTINGUISHABILITY_MATRIX
    auto ns = ot.tree->getExistingNodes();
    std::cout << "distMap:" << std::endl;
    for ( Index s0 = 0; s0 < ns.size(); ++s0 ) {

        if (ot.distMap.count(ns[s0]->getNodeId()) == 0) continue;

        for ( Index s1 = s0+1; s1 < ns.size(); ++s1 ) {
            if (ot.distMap.at(ns[s0]->getNodeId()).count(ns[s1]->getNodeId()) == 0) continue;

            std::cout << "(" << ns[s0]->getNodeId() << "-" << ns[s1]->getNodeId() << ") : ";
            for (auto x : ot.distMap.at(ns[s0]->getNodeId()).at(ns[s1]->getNodeId())) std::cout << x << ".";
            std::cout << " \t ";
        }
        std::cout << std::endl;
    }
    #endif

    return os;
}

Tree* ObservationTree::getTree() {
    return tree.get();   
}

void ObservationTree::updateDistMapForEdge(TreeNode* node, Index input, Index output) {
    
    if (node->after(input) != nullptr
        && node->after(input)->getValue().value() != output) {
        
        std::cout << "ERROR: nondeterministic SUT" << std::endl;
        std::cout << "observed different output " << output 
                  << " instead of previously observed " << node->after(input)->getValue().value() 
                  << " for input " << input
                  << " after having applied input sequence ";
        for (auto x : node->after(input)->getPath()) std::cout << x << ".";
        std::cout << std::endl;
        std::exit(0);
    } 

    // if an edge already exists for the input, then skip
    // Note: as we assume the SUT to be deterministic, the target of that 
    //       edge must exhibit the given output
    if (node->after(input) != nullptr) return; 

    // mark the node as exhibiting the new input 
    nodesWithDefinedInput[input].insert(node);

    std::vector<TreeNode*> newlyDistinguishedNodes;
    auto nodeId = node->getNodeId();

    // check if the new edge makes the node 1-distinguishable from some node it is not 1-distinguishable from yet
    for (auto otherNode : nodesWithDefinedInput[input]) {
        
        if (otherNode == node) continue; 

        auto otherNodeX = otherNode->after(input);
        auto otherNodeId = otherNode->getNodeId();

        // update the dist. map if the input distinguishes the nodes and they are not already 1-distinguished
        // by another input
        if ( (otherNodeX->getValue().value() != output)  //// TODO: .value() ...
            && ( (not areApart(nodeId,otherNodeId))
                || getDistinguishingTrace(nodeId,otherNodeId).size() > 1 )) {
            addDistinguishingTraceFirstInput(nodeId,otherNodeId,input);          
            newlyDistinguishedNodes.push_back(otherNode);
            
            newlyApartNodes.insert(node);
            newlyApartNodes.insert(otherNode);
        }
    }

    // iteratively check if the k-th parent of the node is newly (k+1)-distinguishable from another node 
    // due to the added observation
    size_t k = 1;
    size_t inputToCurrentNode = 0;
    TreeNode* localCurrentNode = node;
    TreeNode* parent;
    size_t currentNodeId = localCurrentNode->getNodeId();
    size_t parentId;
    while ((not newlyDistinguishedNodes.empty()) && localCurrentNode->getParent() != nullptr) {
        
        parent = localCurrentNode->getParent();
        parentId = parent->getNodeId();
        // find the input leading to the currentNode from its parent
        for (auto x : parent->getLabelsOfOutgoingEdges()) {
            if (parent->after(x) == localCurrentNode) {
                inputToCurrentNode = x;
                break;
            }
        }            

        std::vector<TreeNode*> distinguishedParents; 
        for (auto otherNode : newlyDistinguishedNodes) {
            auto otherParent = otherNode->getParent();
            
            // only consider the parents of newly dist. nodes that are reachable by the same input as the currentNode
            if (otherParent == nullptr || otherParent->after(inputToCurrentNode) != otherNode) continue;

            auto otherParentId = otherParent->getNodeId();

            if ( (not areApart(parentId,otherParentId))
                 || getDistinguishingTrace(parentId,otherParentId).size() > k+1 ) {
                addDistinguishingTraceFirstInput(parentId,otherParentId,inputToCurrentNode);   
                distinguishedParents.push_back(otherParent);   

                newlyApartNodes.insert(parent);
                newlyApartNodes.insert(otherParent);   
            }
        }

        localCurrentNode = parent;
        currentNodeId = parentId;
        newlyDistinguishedNodes = distinguishedParents;
        ++k;
    }
}

Index ObservationTree::getDistinguishingTraceFirstInput(size_t n1, size_t n2) {
    if (n1 < n2) {
        return distMap[n1][n2];
    } else {
        return distMap[n2][n1];
    }
}

Trace ObservationTree::getDistinguishingTrace(size_t n1, size_t n2) {
    Trace t;
    auto q1 = nodesInTree[n1];
    auto q2 = nodesInTree[n2];
    do {
        auto x = getDistinguishingTraceFirstInput(q1->getNodeId(),q2->getNodeId());
        t.append(x);
        q1 = q1->after(x);
        q2 = q2->after(x);
    } while (q1 != nullptr && q2 != nullptr && q1->getValue().value() == q2->getValue().value());
    return t;
}

Trace ObservationTree::getApartnessWitness(size_t n1, size_t n2) {
    return getDistinguishingTrace(n1,n2);
}

void ObservationTree::addDistinguishingTraceFirstInput(size_t n1, size_t n2, Index x) { 
    if (n1 < n2) {
        if (distMap.count(n1) == 0) {
            // create a new map entry if n1 is not apart of any node yet
            distMap[n1] = std::unordered_map<size_t,Index,IndexHash>();
        }
        distMap[n1][n2] = x;
    } else {
        if (distMap.count(n2) == 0) {
            // create a new map entry if n2 is not apart of any node yet
            distMap[n2] = std::unordered_map<size_t,Index,IndexHash>();
        }
        distMap[n2][n1] = x;
    }
}

bool ObservationTree::areApart(size_t n1, size_t n2) {
    if (n1 < n2) {
        auto m1 = distMap.find(n1);
        return m1 != distMap.end() && m1->second.count(n2) != 0;
    } else {
        auto m2 = distMap.find(n2);
        return m2 != distMap.end() && m2->second.count(n1) != 0;
    }
}

void ObservationTree::beginNewObservationTrace() {

    newlyApartNodes = std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>();

    currentNode = tree->getRoot();
}

void ObservationTree::addObservation(Index input, Index output) {

    updateDistMapForEdge(currentNode,input,output);
    currentNode = currentNode->add(input,std::make_optional(output));
    nodesInTree.push_back(currentNode);
}

std::optional<size_t> ObservationTree::applyInputIfPreviouslyObserved(size_t input) {
    auto child = currentNode->after(input);
    
    // if no child exists, perform no modifications
    if (child == nullptr) return std::nullopt;

    currentNode = child;
    return currentNode->getValue();
}

void ObservationTree::addObservation(IOTrace& trace) {

    beginNewObservationTrace();
    while (not trace.input.isEmpty()) {
        auto x = trace.input.front();
        auto y = trace.output.front();
        trace.input.pop_front();
        trace.output.pop_front();
        addObservation(x,y);
    }
}

size_t ObservationTree::getMinimalNumberOfPairwiseDistinctStates() {
    // greedy maximal-clique search

    auto nodes = tree->getExistingNodes();

    std::vector<size_t> numberOfStatesDistinguishableFrom;
    for (size_t i = 0; i < nodes.size(); ++i) {
        numberOfStatesDistinguishableFrom.push_back(0);
    }
    for (auto const& [q0,qMap] : distMap) {
        numberOfStatesDistinguishableFrom[q0] += qMap.size();

        for (auto const& [q1,t] : qMap) {
            numberOfStatesDistinguishableFrom[q1] += 1;
        }
    }

    // sort nodes by the number of states they are distinguishable from
    std::sort(nodes.begin(), nodes.end(), [numberOfStatesDistinguishableFrom](TreeNode* q0, TreeNode* q1)
        {
            return numberOfStatesDistinguishableFrom[q0->getNodeId()] > numberOfStatesDistinguishableFrom[q1->getNodeId()];
        });

    
    std::vector<TreeNode*> clique;
    for (auto const& q0 : nodes) {
        auto id0 = q0->getNodeId();
        bool isPairwiseDist = true;
        for (auto q1 : clique) {
            if (not areApart(id0,q1->getNodeId())) {
                isPairwiseDist = false;
                break;
            }
        }
        if (isPairwiseDist) {
            clique.push_back(q0);
        }
    }
    return clique.size();


    /* greedy clique search starting from every node instead of the most distinguishable node */

    // auto nodes = tree->getExistingNodes();

    // size_t maxFoundCliqueSize = 0;
    // std::vector<TreeNode*> maxFoundClique;
         
    // for ( Index s0 = 0; s0 < nodes.size(); ++s0 ) {
    //     auto n0 = nodes[s0];
        
    //     std::vector<TreeNode*> n0clique;
    //     n0clique.push_back(n0);

    //     for ( Index s1 = s0+1; s1 < nodes.size(); ++s1 ) {
    //         auto n1 = nodes[s1];
    //         auto id1 = n1->getNodeId();

    //         bool isPairwiseDist = true;
    //         for (auto nC : n0clique) {
    //             if (not areApart(id1,nC->getNodeId())) {
    //                 isPairwiseDist = false;
    //                 break;
    //             }
    //         }
    //         if (isPairwiseDist) {
    //             n0clique.push_back(n1);
    //         }
    //     }
    //     for ( Index s1 = 0; s1 < s0; ++s1 ) {
    //         auto n1 = nodes[s1];
    //         auto id1 = n1->getNodeId();

    //         bool isPairwiseDist = true;
    //         for (auto nC : n0clique) {
    //             if (not areApart(id1,nC->getNodeId())) {
    //                 isPairwiseDist = false;
    //                 break;
    //             }
    //         }
    //         if (isPairwiseDist) {
    //             n0clique.push_back(n1);
    //         }
    //     }

    //     if (n0clique.size() > maxFoundCliqueSize) {
    //         maxFoundCliqueSize = n0clique.size();
    //         maxFoundClique = n0clique;
    //     }
    // }

    // return maxFoundCliqueSize;
}

std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>* ObservationTree::getNewlyApartNodes() {
    return &newlyApartNodes;
}

}

