/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <random>

#include "RandomTransitionRemoval.hpp"

using namespace std;

namespace libfsmtest {


void RandomTransitionRemoval::visit(Fsm &fsmToTransform) {
    this->transformedFsm = fsmToTransform.clone();
    for (size_t i = 0; i < mutationsToApply; ++i) {
        bool success = applyMutation();
        if (!success) {
            // Removing a transition failed, so don't try to remove more.
            break;
        }
    }
    this->transformedFsm = this->transformedFsm->clone();
}

bool RandomTransitionRemoval::applyMutation() {
    size_t numberOfStates = transformedFsm->size();

    random_device rd;
    default_random_engine gen(rd());
    uniform_int_distribution<size_t> distributeStates(0, numberOfStates - 1);

    // pick a random state
    Index state = distributeStates(gen);
    Index start = state;
    do {
        if (!transformedFsm->getStates().at(state).getTransitions().empty()) {
            uniform_int_distribution<size_t> distributeTransitions(0, transformedFsm->getStates().at(state).getTransitions().size() - 1);
            size_t transition = distributeTransitions(gen);
            transformedFsm->getStates().at(state).removeTransition(transition);
            return true;
        } else {
            state = (state + 1) % numberOfStates;
        }
    } while (transformedFsm->getStates().at(state).getTransitions().empty() and start != state);

    // No state had transitions, so none got removed.
    return false;
}


} // namespace libfsmtest
