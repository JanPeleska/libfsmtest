/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <set>
#include <utility>
#include <algorithm>
#include <stdexcept>

#include "State.hpp"

using namespace std;
namespace libfsmtest {

State::State(std::string  aStateName, Index theId) :
 id(theId), name(std::move(aStateName)) { }

State::State(const State& state) {
    id = state.id;
    name = state.name;
    transitions = vector(state.transitions);
}


Transitions& State::getTransitions() {
    return transitions;
}

Transitions const& State::getTransitions() const {
    return transitions;
}

bool State::isDeterministic() const {
    
    set<Index> inputSet;
    
    // Check if more than one outgoing transition
    //  is labelled with the same input value
    for (auto& tr : transitions) {
        // Recall that inputSet.insert(tr.getInput()).second
        // is false iff tr.getInput() is already in the set
        if (!inputSet.insert(tr.getInput()).second) return false;
    }
    return true;
}


bool State::isCompletelySpecified(size_t inputAlphabetSize) const {
    set<Index> inputSet;
    
    for_each(transitions.cbegin(),
             transitions.cend(),
             [&](auto& tr) { inputSet.insert(tr.getInput()); } );
    
    return (inputSet.size() == inputAlphabetSize);
}



bool State::isObservable() const {
    
    // We check that the input/output labels of outpoing transitions are unique
    set< pair<Index,Index> > labelSet;
    
    for (auto& tr : transitions) {
        // Return false if this I/O label has already been attached
        // to a previous transition
        if (!labelSet.insert(make_pair(tr.getInput(),tr.getOutput())).second)
            return false;
    }
    
    return true;
}


bool State::hasTransition(Index x, Index y) const {
    return std::any_of(transitions.begin(), transitions.end(), [x,y](const auto& t) {
        return x == t.getInput() && y == t.getOutput();
    });
}

bool State::hasTransition(size_t x) const {
    return std::any_of(transitions.begin(), transitions.end(), [x](const auto& t) {
        return x == t.getInput();
    });
}

void State::addTransition(Index inputIndex, Index outputIndex, Index targetId) {
    transitions.emplace_back(id,inputIndex,outputIndex,targetId);
}

std::unordered_set<Index> State::getDefinedInputs() const
{
    std::unordered_set<Index> result;
    for (auto& t : transitions)
    {
        result.insert(t.getInput());
    }
        
    return result;
}

std::unordered_set<Index> State::getResponsesToInput(const Index x) const {
    std::unordered_set<Index> result;
    for (auto& t : transitions)
    {
        if (t.getInput() == x)
            result.insert(t.getOutput());
    }
        
    return result;
}

void State::removeTransition(size_t offset) {
    if (offset >= transitions.size()) throw runtime_error("The transition to remove does not exist.");

    auto it = transitions.begin();
    advance(it, offset);

    transitions.erase(it);
}


}
