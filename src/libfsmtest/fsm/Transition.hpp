/*
 * Copyright 2021 Jan Peleska, Moritz Bergenthal, Robert Sachtleben, Niklas Krafczyk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <vector>
#include <cstdio>
#include "visitors/FsmVisitor.hpp"


namespace libfsmtest {

class State;

class Transition {
    
private:
    
    Index sourceId;
    Index inputIndex;
    Index outputIndex;
    Index targetId;
    
public:
    
    Transition(const Index sourceId,
               const Index inputIndex,
               const Index outputIndex,
               const Index targetId);

    Index getSourceId() const { return sourceId; }
    Index getTargetId() const { return targetId; }
    
    Index getInput() const { return inputIndex; }
    Index getOutput() const { return outputIndex; }
    
    /** Accept FsmVisitors */
    void accept(FsmVisitor& v) { v.visit(*this); }

    friend bool operator==(Transition const & t1, Transition const & t2)
    {
        return t1.inputIndex == t2.inputIndex
                && t1.outputIndex == t2.outputIndex 
                && t1.sourceId == t2.sourceId
                && t1.targetId == t2.targetId;
    }

    friend bool operator<(Transition const & t1, Transition const & t2)
    {
        if (t1.inputIndex < t2.inputIndex) return true;
        if (t1.inputIndex == t2.inputIndex) {
            if (t1.outputIndex < t2.outputIndex) return true;
            if (t1.outputIndex == t2.outputIndex) {
                if (t1.sourceId < t2.sourceId) return true;
                if (t1.sourceId == t2.sourceId) {
                    return (t1.targetId < t2.targetId);
                }
            }
        }

        return false;
    }
    
};


using Transitions = std::vector<Transition>;

} // namespace libfsmtest
