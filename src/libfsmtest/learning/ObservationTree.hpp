#pragma once

#include <memory>
#include <vector>
#include <optional>
#include <iostream>
#include <string>
#include <stack>
#include <unordered_map>
#include <unordered_set>

#include "testsuite/Tree.hpp"
#include "testsuite/Trace.hpp"
#include "fsm/definitions.hpp"
#include "testsuite/definitions.hpp"


#define OBSERVATION_TREE_PRINT_DISTINGUISHABILITY_MATRIX false

namespace libfsmtest {

struct TreeNodeIdEqual {
    bool operator()(const TreeNode* a, const TreeNode* b) const { return a->getNodeId() == b->getNodeId(); }
};

struct TreeNodeIdHash {
    size_t operator() (const TreeNode* q) const { return q->getNodeId(); }
    // size_t operator() (const TreeNode* q) const { return std::hash<size_t>{}(q->getNodeId()) * 0xd989bcacc137dcd5ull >> 32u; }
};

struct IndexHash {
    size_t operator() (const size_t x) const { return std::hash<size_t>{}(x) * 0xd989bcacc137dcd5ull >> 32u; }
};


/** Observation tree for learning deterministic implementations */
class ObservationTree {
private:
    /** Tree storing previously applied input traces and observed outputs, where 
     *  the outputs are stored as node values.
     */
    std::unique_ptr<Tree> tree;

    /** Nodes whose apartness has been updated by the last observation */
    std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual> newlyApartNodes;

    /** Node reached by the currently observed trace */
    TreeNode* currentNode;

    /** number of input ids 0..numInputIDs-1 */
    const size_t numInputIDs;

    /** maps each node id to the corresponding node */
    std::vector<TreeNode*> nodesInTree;

    /** maps each input to all nodes that have an outgoing edge for this input */
    std::vector<std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>> nodesWithDefinedInput;

    /** map containing a first input of a separating trace for each pair of states that
     *  are apart in the observation tree
     */
    std::unordered_map<size_t,std::unordered_map<size_t,Index,IndexHash>,IndexHash> distMap;
    
    /** update the dist. map if a new edge (x/y) is added to a node
     *  Note: this can affect only those key-pairs of the dist. map of
     *        which at least one element is the node or one of its parents
     */
    void updateDistMapForEdge(TreeNode* node, Index input, Index output);

    /** get a distinguishing trace from the dist. map */
    Index getDistinguishingTraceFirstInput(size_t n1, size_t n2);
    Trace getDistinguishingTrace(size_t n1, size_t n2);

    /** add a distinguishing trace to the dist. map */
    void addDistinguishingTraceFirstInput(size_t n1, size_t n2, Index x);
    
public:

    /** Constructor for an empty observation tree */
    ObservationTree(size_t numInputIDs);

    /** Retrieve the internal tree */
    Tree* getTree();

    /** Reset the current node to the root */
    void beginNewObservationTrace();
    
    /** Add an IO pair to the tree from the current node */
    void addObservation(Index input, Index output);

    /** Add an IO trace to the tree from the root */
    void addObservation(IOTrace& trace);

    /** Compute a lower bound on the number of distinct states in a deterministic
     *  FSM of whose language the observation tree is a subset
    */
    size_t getMinimalNumberOfPairwiseDistinctStates();

    /** Check whether q1 and q2 are apart */
    bool areApart(size_t n1, size_t n2);

    /** Obtain a separating trace */
    Trace getApartnessWitness(size_t n1, size_t n2);

    /** Obtain the nodes whose apartness has been modified by the last applied trace */
    std::unordered_set<TreeNode*, TreeNodeIdHash, TreeNodeIdEqual>* getNewlyApartNodes();

    /** If the current node has a child for the given input, update the current node to that child and return its value.
     *  Otherwise leave current node unmodified and return nullopt.
     */
    std::optional<size_t> applyInputIfPreviouslyObserved(size_t input);
    
    friend std::ostream& operator<<(std::ostream& os, const ObservationTree& ot);
};

}
    